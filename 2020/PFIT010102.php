<!DOCTYPE html>
<html>

<?php
  include("inc/head.php");

  $project_code   = isset($_POST['project_code'])?$_POST['project_code']:"";
  $project_name   = isset($_POST['project_name'])?$_POST['project_name']:"";
  $m   = isset($_POST['m'])?$_POST['m']:"";
  $y   = isset($_POST['y'])?$_POST['y']:"";

  $urlback = "PFIT0101.php?m=$m&y=$y";

  if($project_code == ""){
    exit("<script>window.location='PFIT0101.php';</script>");
  }
?>
<style>
  th {
    text-align: center !important;
  }

  .isDisabled {
    color: #ccc;
    cursor: not-allowed;
    opacity: 0.5;
    text-decoration: none;
  }
</style>

<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        จัดเก็บผลการทดสอบ
        <small>PFIT010102</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="PFIT0101.php"><i class="fa fa-clipboard"></i>จัดเก็บผลการทดสอบ</a></li>
        <li class="active">จัดเก็บผลการทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">

          <div class="box boxBlack">
            <div class="box-header with-border">
              <h3 class="box-title">ค้นหาข้อมูลผู้ทดสอบ</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <form class="form-horizontal" id="formSearch" data-smk-icon="glyphicon-remove-sign"  enctype="multipart/form-data">
                <div class="row" align="center">
                  <table>
                  <tr>
                    <td style="padding:5px;" align="right">ชื่อผู้ทดสอบ : </td>
                    <td style="padding:5px;" align="left">
                      <input type="text" id="personName" class="form-control" placeholder="ชื่อ" >
                    </td>
                    <td style="padding:5px;width:150px;" align="right">นามสกุลผู้ทดสอบ :</td>
                    <td style="padding:5px;" align="left">
                      <input type="text" id="personLname" class="form-control" placeholder="นามสกุล" >
                    </td>
                  </tr>
                  <tr>
                    <td style="padding:5px;" align="right">เลขประจำตัว : </td>
                    <td style="padding:5px;" align="left">
                      <input type="text" id="personId" class="form-control" placeholder="เลขประจำตัว" >
                    </td>
                    <td style="padding:5px;width:150px;" align="right"></td>
                    <td style="padding:5px;" align="left"></td>
                  </tr>
                  <tr>
                    <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                        <button type="submit" id="btnSave" class="btn btn-success btn-flat" style="width:100px">ค้นหา</button>
                        <button type="reset" class="btn btn-flat" style="width:100px">ล้างค่า</button>
                    </td>
                  </tr>
                </table>
             </div>
                  <input type="hidden" id="projectCode" value="<?= $project_code ?>">
              </form>
            </div>
          </div>

          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">แสดงรายการ</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <div style="padding:5px" align="center">
                    <span class="fa fa-circle" style="color:red;font-size:20px;"></span>&nbsp;ยังไม่ได้บันทึกข้อมูลการทดสอบ
                    <span class="fa fa-circle" style="color:#FFD100;font-size:20px;"></span>&nbsp;บันทึกข้อมูลการทดสอบบางส่วน
                    <span class="fa fa-circle" style="color:green;font-size:20px;"></span>&nbsp;บันทึกข้อมูลการทดสอบครบ
                  </div>
                  <table id="table" class="table table-bordered table-striped"
                         data-toggle="table"
                         data-toolbar="#toolbar"
                         data-url="ajax/PFIT010102/show.php?code=<?= $project_code?>&&id="
                         data-pagination="true"
                         data-page-size = 20
                         data-page-list= "[20, 50, 100, ALL]"
                         data-search="false"
                         data-flat="true"
                         data-show-refresh="false"
                         >
                      <thead>
                        <tr>
                            <th data-field="operate" data-formatter="operateList" data-align="center"></th>
                            <th data-sortable="true" data-field="person_number" data-align="center">เลขประจำตัว</th>
                            <th data-sortable="true" data-field="person_name" data-align="left">ชื่อ</th>
                            <th data-sortable="true" data-field="person_lname" data-align="left">สกุล</th>
                            <th data-sortable="true" data-formatter="getGender" data-align="center">เพศ</th>
                            <th data-field"operate" data-formatter="getDateToTH" data-align="center">วันเดือนปีเกิด</th>
                            <th data-sortable="true" data-field="wiegth" data-align="right">น้ำหนัก</th>
                            <th data-sortable="true" data-field="height" data-align="right">ส่วนสูง</th>
                            <th data-field="operate" data-formatter="getStatusColor" data-align="center">สถานะ</th>
                            <th data-field="operate" data-formatter="operateprint" data-align="center"></th>
                        </tr>
                      </thead>
                  </table>
                </div>
                <div class="box-footer with-border">
                  <div class="box-tools pull-right">
                    <button class="btn btn-success" onclick="gotoPage('<?= $urlback ?>')" style="width:80px;">ย้อนกลับ</button>
                  </div>
                </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">ข้อมูลผลการทดสอบ</h4>
            </div>
            <form id="formDataSave" data-smk-icon="glyphicon-remove-sign" novalidate>
            <!-- <form data-smk-icon="glyphicon-remove-sign" method="post" action="ajax/PFIT010102/manage.php"> -->
              <div class="modal-body" id="formShow">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                <button type="submit" class="btn btn-primary" style="width:100px;">บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- <textarea name="content_data" id="content_data" ></textarea> -->

  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>
 <script src="ckeditor/ckeditor.js"></script>
 <script src="js/PFIT010102.js"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
