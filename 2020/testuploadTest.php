<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include("inc/header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <form action="ajax/PFIT0201/manageUpload.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="box boxBlack">
                    <div class="box-header with-border">
                      <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group col-md-12">
                              <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                              <div class="col-sm-8">
                                <input type="file" name="filepath" accept=".xlsx,.xls,.txt" required>
                              </div>
                            </div>
                            <div class="form-group col-md-12">
                              <label class="col-sm-4 control-label">เพิ่มเติม</label>
                              <div class="col-sm-8">
                                <textarea name="remark" class="form-control" rows="3"></textarea>
                              </div>
                            </div>
                            <div class="text-center col-md-12">
                              <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                            </div>
                        </div>
                      </div>
                    </div>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
          <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
        </div>
      </form>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>

</body>
</html>
