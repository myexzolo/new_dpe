<?php
  include 'mpdf/mpdf.php';
  ob_start();
?>
<!DOCTYPE html>
<html>
  <body>

    <h2 style="text-align:center"><strong>การดันพื้น<br />
Push &ndash; Ups </strong></h2>

<h3><strong>อุปกรณ์</strong></h3>

<p>พื้นที่ราบเรียบ ไม่ขรุขระ</p>

<h3><strong>วิธีปฏิบัติ</strong> สำหรับชาย</h3>

<ol>
	<li>1. จับคู่กันสองคน ให้คู่ทำหน้าที่เป็นผู้นับจำนวนครั้งที่ทำได้</li>
	<li>2. ผู้ทดสอบนอนคว่ำราบกับพื้น มือทั้งสองวางดันพื้นห่างกันประมาณช่วงไหล่ ให้ปลายนิ้วหันไปข้างหน้า แขนเหยียดตรง ลำตัวตั้งแต่ส่วนเอวลงมาให้ยกขึ้นพ้นพื้น และให้เท้าวางยันไว้ที่พื้น</li>
	<li>3. เมื่อพร้อมแล้วให้ผู้ทดสอบยุบแขนลงไม่ให้ลำตัวสัมผัสพื้น แล้วดันตัวขึ้นสู่ท่าเริ่มต้น ผู้เป็นคู่นับ &ldquo;หนึ่ง &ldquo;</li>
	<li>4. ปฏิบัติติดต่อกันไปเรื่อยๆ สม่ำเสมอในจังหวะขึ้น &ndash; ลง ภายในเวลา 2 วินาที หากใช้เวลามากกว่านี้ ให้ยุติการทดสอบ</li>
</ol>

<h3><strong>วิธีปฏิบัติ</strong> สำหรับหญิง</h3>

<ol>
	<li>1. จับคู่กันสองคน ให้คู่ทำหน้าที่เป็นผู้นับจำนวนครั้งที่ทำได้</li>
	<li>2. ผู้ทดสอบนอนคว่ำราบกับพื้น มือทั้งสองดันพื้น ให้ปลายนิ้วหันไปข้างหน้าและวางแยกห่างกันประมาณช่วงไหล่ แขนเหยียดตรงอนุญาตให้ลำตัวตั้งแต่ส่วนเอวลงมาสัมผัสพื้นได้</li>
	<li>3. เมื่อพร้อมแล้ว ให้ผู้ทดสอบยุบแขนลง ไม่ให้ลำตัวส่วนบนสัมผัสพื้น แล้วดันตัวขึ้น สู่ท่าเริ่มต้น ผู้เป็นคู่นับ &ldquo;หนึ่ง &ldquo;</li>
	<li>4. ปฏิบัติติดต่อกันไปเรื่อยๆ สม่ำเสมอในจังหวะขึ้น &ndash; ลง ภายในเวลา 2 วินาที หากใช้ เวลามากกว่านี้ ให้ยุติการทดสอบ</li>
</ol>

<p><strong>การบันทึก</strong> นับจำนวนครั้งของการดันพื้น ที่ถูกต้อง</p>


  </body>
</html>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    $mpdf=new mPDF('utf-8');
    $mpdf->autoScriptToLang = false;
    $mpdf->margin_header = 2;
    // $mpdf->SetHeader('รายงานโดย codingthailand | รายงานลูกค้าทั้งหมด | ออกรายงานเมื่อ: '.date('d/m/Y H:i:s'));
    $mpdf->margin_footer = 9;
    // $mpdf->SetFooter('หน้าที่ {PAGENO}');
    // Define a Landscape page size/format by name
    //$mpdf=new mPDF('utf-8', 'A4-L');
    // Define a page size/format by array - page will be 190mm wide x 236mm height
    //$mpdf=new mPDF('utf-8', array(190,236));
    $stylesheet = file_get_contents('bootstrap/css/printpdf.css');
    //$mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    //$mpdf->Output();
    $mpdf->Output(time(),'I');


    exit;
?>
