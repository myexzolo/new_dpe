<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }
  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 195mm;
        height: auto;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
<body>
  <?php
      $projectCode = $_POST['code'];
      $str = "WHERE pfit_t_project_test.project_code = '$projectCode' ";
      $str .= isset($_POST['id'])?"AND pfit_t_person.person_number = {$_POST['id']}":"";

      $sqlp = "SELECT pfit_t_person.person_number,pfit_t_person.person_name,pfit_t_person.person_lname,
               pfit_t_person.person_gender,".getQueryDate('date_of_birth').",pfit_t_person.wiegth,pfit_t_person.height
               FROM pfit_t_project_test
               INNER JOIN pfit_t_person ON pfit_t_project_test.project_code = pfit_t_person.project_code
               $str
               GROUP BY pfit_t_person.person_number,pfit_t_person.person_name,pfit_t_person.person_lname,
               pfit_t_person.person_gender,pfit_t_person.date_of_birth,pfit_t_person.wiegth,pfit_t_person.height";

      //echo $sqlp;
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      foreach ($rowp['data'] as $key => $value) {
        $person_numberp = $value['person_number'];
        $person_name    = $value['person_name'];
        $person_lname   = $value['person_lname'];
        $person_gender  = $value['person_gender']=='M'?"ชาย":"หญิง";
        $date_of_birth  = $value['date_of_birth'];
        $wiegth         = $value['wiegth'];
        $height         = $value['height'];

        // $num = 0;
        $sql_date = "SELECT * FROM pfit_t_result WHERE person_number = '$person_numberp'";
        $query_date = DbQuery($sql_date,null);
        $row_date  = json_decode($query_date, true);
        $num = $row_date['dataCount'];
        $date_create = '...../...../..........';
        if($num > 0){
          $date_create = convDatetoThai($row_date['data'][0]['date_create']);
        }
  ?>
  <table style="width: 100%;" border="0" >
    <tr>
      <td colspan="5" style="font-size:20pt;font-weight:500;padding-top:20px;" align="center"><b>ผลการทดสอบสมรรถภาพทางกาย</b></td>
    </tr>
    <tr>
      <td align="left" style="width:250px"><img src="images/dep_logo.png" style="height:70px;width:75px;"></td>
      <td colspan="3"></td>
      <td align="right" style="width:250px;font-size:14pt">สถานที่ : สำนักวิทยาศาสตร์การกีฬา<br> วันที่ : <?php echo $date_create; ?></td>
    </tr>


    <tr style="padding-top:20px;">
      <td colspan="5" style="padding-top:20px;" class="info">
        <b>ชื่อ :</b> <?php echo $person_name,' ',$person_lname; ?> &emsp;
        <b>เพศ :</b> <?php echo $person_gender; ?> &emsp;
        <b>อายุ :</b> <?php echo yearBirth($date_of_birth); ?> ปี &emsp;
      </td>
    </tr>
    <tr>
      <td colspan="5" style="width:250px"class="info">
        <b>น้ำหนัก :</b> <?php echo $wiegth; ?> กิโลกรัม &emsp;
        <b>ส่วนสูง :</b> <?php echo $height; ?> เซนติเมตร &emsp;
        <b>ดัชนีมวลกาย :</b> <?php echo bmi($wiegth,$height)[0]; ?> &emsp;
        <b>อยู่ในเกณฑ์ :</b> <?php echo bmi($wiegth,$height)[1]; ?>
      </td>
    </tr>

  </table>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
    <tbody>
  		<tr>
  			<td class="thStyle">รายการทดสอบ</td>
  			<td class="thStyle">คะแนน</td>
  			<td class="thStyle">เกณฑ์มาตรฐาน</td>
  			<td class="thStyle">ผลการทดสอบ</td>
  			<td width="20%" class="thStyle">คำแนะนำ</td>
  		</tr>

        <?php

          $sqlp = "SELECT * FROM pfit_t_result
                   INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
                   LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
                   LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code
                   WHERE pfit_t_result.project_code = '$projectCode' AND pfit_t_result.person_number = '$person_numberp' ORDER BY pfit_t_result.test_code DESC";
          $queryp = DbQuery($sqlp,null);
          $rowp  = json_decode($queryp, true);
          $num = $rowp['dataCount'];

          if($num>0){

            foreach ($rowp['data'] as $key => $value) {
              $test_unit = $value['test_unit'];
              $test_code = $value['test_code'];
              $result = $value['result']!=''?$value['result']:"-";
              $result_cal = $value['result_cal']!=''?$value['result_cal']:"-";
              $min = $value['min']!=''?$value['min']:"";
              $max = $value['max']!=''?$value['max']:"";
              $test_suggest = $value['test_suggest']!=''?$value['test_suggest']:"-";
              $category_criteria_detail_name = $value['category_criteria_detail_name']!=''?$value['category_criteria_detail_name']:"-";
              if($value['test_calculator'] == 5){
                $result = str_replace("|", " , ", $result);;
              }
          ?>
          <tr>
      			<td class="content"><?php echo $value['test_name']; ?><br>(<?php echo $value['test_unit']; ?>)</td>
      			<td class="content" align="center"><?php echo $result_cal; ?></td>
            <?php
            $minmax = '-';
            if($value['test_calculator'] == 5){
              $sqlc = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code'";
              $queryc = DbQuery($sqlc,null);
              $jsonc  = json_decode($queryc, true);
              $rowc   = $jsonc['data'];
              $num = $jsonc['dataCount'];
              if($num > 0){
                $minmax = $rowc[2]['min'].' - '.$rowc[2]['max'];
              }

            }
            ?>
      			<td class="content" align="center"><?php echo $minmax; ?></td>
      			<td class="content" align="center"><?php echo strip_tags($category_criteria_detail_name); ?></td>
      			<td class="content" style="text-align: justify;">
      			<?php echo strip_tags($test_suggest); ?>
      			</td>
      		</tr>
          <?php }
          }else{
            $sqlp = "SELECT * FROM pfit_t_result
                     INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
                     LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
                     LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code WHERE pfit_t_result.project_code = '$projectCode'
                     GROUP BY pfit_t_test.test_code";
            $queryp = DbQuery($sqlp,null);
            $rowp  = json_decode($queryp, true);

            if($rowp['dataCount'] > 0){
              foreach ($rowp['data'] as $key => $value) {
              $test_unit = $value['test_unit'];
              $result_cal = "";
              $min = "";
              $max = "";
              $total = $min.''.$max;
              $test_suggest = "";
              $category_criteria_detail_name = "";

              ?>
              <tr>
                <td class="content"><?php echo $value['test_name']; ?><br>(<?php echo $value['test_unit']; ?>)</td>
                <td class="content" align="center"><?php echo $result_cal; ?></td>
                <td class="content" align="center"><?php echo $total; ?></td>
                <td class="content" align="center"><?php echo strip_tags($category_criteria_detail_name); ?></td>
                <td class="content" style="text-align: justify;"><?= strip_tags($test_suggest); ?></td>
              </tr>
      <?php }}} ?>

  	</tbody>
  </table>
  <div align="center" style="font-size:14pt;padding-top:10px;">
    * หมายเหตุ ค่าเกณฑ์สมรรถภาพทางกายสำหรับแต่ละช่วงวัยดูได้ที่ http://www.dpe.go.th/th/subarticle/1/29
  </div>
  <div align="center" style="font-size:14pt;font-weight: bold;">
    กรมพลศึกษา กระทรวงการท่องเที่ยวและกีฬา โทร. 0 2214 2577
  </div>
  <div class="break"></div>

  <div class="container">
    <h2 style="text-align:center">
      <img src="images/dep_logo.png" style="height:70px;width:75px;">
      <strong>สมรรถภาพทางกายเชิงสุขภาพ</strong>
    </h2>
    <table align="center" border="1" bordercolor="#ccc" cellpadding="5" cellspacing="0" style="border-collapse:collapse; width:100%">
      <tbody>
        <tr>
          <td class="content" style="text-align:center"><strong>รายการตรวจวัดสุขภาพ</strong></td>
          <td class="content" style="text-align:center"><strong>ผลการตรวจวัด</strong></td>
          <td class="content" style="text-align:center"><strong>ค่ามาตราฐาน</strong></td>
          <td class="content" style="text-align:center"><strong>ปกติ</strong></td>
          <td class="content" style="text-align:center"><strong>ดี</strong></td>
          <td class="content" style="text-align:center"><strong>สูง</strong></td>
          <td class="content" style="text-align:center"><strong>ต่ำ</strong></td>
        </tr>
        <tr>
          <td class="content"><strong>FAT (%)</strong> ไขมันสะสมใต้ผิวหนัง</td>
          <td class="content">&nbsp;</td>
          <td class="content" style="text-align:center">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><strong>TBW (%)</strong> ระดับน้ำในร่างกาย</td>
          <td class="content">&nbsp;</td>
          <td class="content">
          <p style="text-align:center"><strong>ชาย</strong>/ค่าปกติ 50-65%<br />
          <strong>หญิง</strong>/ค่าปกติ 45-60%</p>
          </td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><strong>Bone Mass (Kg)</strong> น้ำหนักของกระดูก</td>
          <td class="content">&nbsp;</td>
          <td class="content" style="text-align:center">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><strong>Visceral Fat</strong> ระดับไขมันในช่องท้อง</td>
          <td class="content">&nbsp;</td>
          <td class="content" style="text-align:center"><strong>ชาย</strong> ไม่เกินระดับ 9<br />
          <strong>หญิง</strong> ไม่เกินระดับ 5</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><strong>BMR</strong> การเผาผลาญพนังงานของร่างกาย</td>
          <td class="content">&nbsp;</td>
          <td class="content" style="text-align:center"><strong>ค่าแต่ละบุคคล</strong> (kcal)</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><strong>Metabolic Age</strong> เปรียบเทียบอายุการเผาผลาญ</td>
          <td class="content">&nbsp;</td>
          <td class="content" style="text-align:center"><strong>ควรต่ำกว่าอายุจริง</strong> (ปี)</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <p><strong>Fat Mass</strong> คือ ปริมาณไขมันที่สะสมในร่างกายมีหน่วยวัดเป็นกิโลกรัม และวิเคราะห์เป็นค่าร้อยละ</p>
    <p><strong>TBW (Total Body Water)</strong> คือ เปอร์เซ็นต์ของน้ำในร่างกาย เป็นสัดส่วนของน้ำในร่างกายเมื่อเทียบกับน้ำหนักตัว</p>
    <p><strong>Bone Mass</strong> คือ นำหนักของกระดูกทั้งหมดในร่างกาย มีหน่วยวัดเป็นกิโลกรัม</p>
    <p><strong>Visceral Fat Rating</strong> คือ ระดับไขมันที่เกาะเลือดหรือสะสมคามอวัยวะภายในช่องท้อง</p>
    <p><strong>BMR (Basal Metabolic Rate)</strong> คือ อัตราการเผาผลาญพลังงานพื้นฐานเป็นค่าการใช้พลังงานของร่างกาย ของแต่ละคน</p>
    <ol>
      <li>สำหรับผู้ชาย BMR = 66 + (13.7 x น้ำหนักตัวเป็น กก. ) + ( 5 x ส่วนสูงเป็น ซม. ) - ( 6.8 x อายุ )</li>
      <li>สำหรับผู้หญิง&nbsp;BMR = 665&nbsp;+ (9.6 x น้ำหนักตัวเป็น กก. ) + ( 1.8&nbsp;x ส่วนสูงเป็น ซม. ) - ( 4.7&nbsp;x อายุ )</li>
    </ol>
    <p><strong>Metabolie Age</strong> คือ อายุเปรียบเทียบกับอัตราเผาผลาญพลังงาน เป็นค่าที่แสดงอัตราเผาผลาญพลังงานในปัจจุบันและมวลกล้ามเนื้อที่วัดได้เทียบกับอายุ</p>
    <p><strong>Muscle Mass</strong> หมายถึง น้ำหนักของกล้ามเนื้อในร่างกาย หน่วยวัดเป็นกิโลกรัม</p>
    <p><strong>BMI (Body Mass Index)</strong> หมายถึงดัชนีมวลกาย ซึ่งคำนวณจาก น้ำหนักตัว(กิโลกรัม) / ส่วนสูง<sup>2</sup>&nbsp;(เมตร)</p>
    <p><strong>Ideal Body Weight</strong> หมายถึง น้ำหนักตัวที่เหมาะสม เป็นค่าที่ได้จากการคำนวณระดับองค์ประกอบของร่างกาย</p>
    <p>&nbsp;</p>
    <p><em><strong>กลุ่มพัฒนาสมรรถภาพทางกาย&nbsp; สำนักวิทยาศาสตร์การกีฬา&nbsp; กรมพลศึกษา&nbsp; กระทรวงการท่องเที่ยวและกีฬา</strong></em></p>

  </div>

  <div class="break"></div>
  <?php } ?>
</body>
</html>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      // window.print();
      // window.close();
    }, 100);
  });
</script>
