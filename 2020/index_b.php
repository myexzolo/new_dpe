<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<style>
#sortable, #sortableCat{ list-style-type: none; margin-top: 10px; padding: 0; width: 100%; }
#sortable li, #sortableCat li { margin-bottom: 10px; padding: 10px; font-size: 1.2em; }
html>body #sortable li ,html>body #sortableCat li  { line-height: 1.2em; }
.ui-state-highlight { height:62px; line-height: 1.2em; }

.visibility-non{
  visibility: hidden;
}
</style>

<body class="hold-transition skin-blue sidebar-mini" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;">
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard sss</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">Class Schedules</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <div class="pull-left">
                    <button class="btn btn-primary" onclick="searchData()">เพิ่ม</button>
                  </div>
                  <table id="table"
                         data-toggle="table"
                         data-toolbar="#toolbar"
                         data-url="json/data_json.json"
                         data-pagination="true"
                         data-page-list="[10, 25, 50, 100, ALL]"
                         data-search="true"
                         data-flat="true"
                         data-show-refresh="true"
                         >
                      <thead>
                          <tr>
                              <th data-sortable="true" data-field="no" data-align="center">ลำดับ</th>
                              <th data-sortable="true" data-field="code" data-align="center">รหัส</th>
                              <th data-sortable="true" data-field="detail" data-align="center">รายการแปลผลการทดสอบ</th>
                              <th data-sortable="true" data-field="detail2" data-align="center">รายละเอียด</th>
                              <th data-sortable="true" data-field="status" data-align="center">สถานะ</th>
                              <th data-field="operate" data-formatter="operateOpen" data-align="center">ดู</th>
                              <th data-field="operate" data-formatter="operateEdit" data-align="center">แก้ไข</th>
                              <th data-field="operate" data-formatter="operateDelete" data-align="center">ลบ</th>
                          </tr>
                      </thead>
                  </table>

                </div>

          </div>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <form>
              <div class="modal-body" id="formShow">

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>


  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>

 <script type="text/javascript">
   function operateOpen(value, row, index) {
       return [
           '<a onclick="searchData('+row.no+',200)" ',
           '<span class="glyphicon glyphicon-search"></span>',
           '</a>'
       ].join('');
   }
   function operateEdit(value, row, index) {
       return [
           '<a onclick="searchData('+row.no+')"',
           '<span class="glyphicon glyphicon-list-alt"></span>',
           '</a>'
       ].join('');
   }
   function operateDelete(value, row, index) {
       return [
           '<a onclick="formDel('+row.no+')" ',
           '<span class="glyphicon glyphicon-trash"></span>',
           '</a>'
       ].join('');
   }

   function searchData(no = "",status =""){
     $('#myModal2').modal({backdrop:'static'});
     formShows(no,status);
     x = 0;
     y = 0;
   }
   function formShows(code,status){
     $.get("ajax/criteria/form.php?code="+code+"&status="+status)
     .done(function( data ) {
         $("#formShow").html(data);
     });

   }
   function formDel(code){
     $.smkConfirm({
       text:'ต้องการลบ?',
       accept:'ตกลง',
       cancel:'ยกเลิก'
     },function(res){
       // Code here
       if (res) {
         $.smkAlert({text: 'ลบข้อมูลเรียบร้อย!!'+code, type:'success'});
       }
     });
   }


   var rowData = $('.rowData').length;
   var max_fields      = 4-rowData; //maximum input boxes allowed

   var x = 0;
   var y = 0;
   function add(){
     var wrapper         = $("#input_fields_wrap"); //Fields wrapper
     var add_button      = $(".add_field_button"); //Add button ID
    //  var x = 1; //initlal text box count
     if(x < max_fields){ //max input box allowed
             x++; //text box increment
             y++;
             var id = 'row'+y;
             $(wrapper).append('<div id="'+id+'" class="rowData">'+
             '<div class="col-md-1">'+
                '<div class="form-group">'+
                  '<label>ลบ</label>'+
                  '<button class="btn btn-danger" type="button" onclick="remove(\''+id+'\')"><span class="glyphicon glyphicon-trash"></span></button>'+
                '</div>'+
              '</div>'+
             '<div class="col-md-2">'+
                '<div class="form-group">'+
                  '<label>รหัส</label>'+
                  '<input type="text" class="form-control" placeholder="รหัส">'+
                '</div>'+
              '</div>'+
              '<div class="col-md-3">'+
               '<div class="form-group">'+
                 '<label>รายการผลการทดสอบ</label>'+
                 '<input type="text" class="form-control" placeholder="รายการผลการทดสอบ">'+
               '</div>'+
             '</div>'+
             '<div class="col-md-3">'+
               '<div class="form-group">'+
                 '<label>คะแนนการทดสอบ</label>'+
                 '<input type="text" class="form-control" placeholder="คะแนนการทดสอบ">'+
               '</div>'+
             '</div>'+
             '<div class="col-md-3">'+
               '<div class="form-group">'+
                 '<label>รายละเอียด</label>'+
                 '<select class="form-control">'+
                   '<option selected>ใช้งาน</option>'+
                   '<option>ไม่ใช้งาน</option>'+
                 '</select>'+
               '</div>'+
             '</div>'+
             '</div>'); //add input box
         }else{
           alert('max_fields 5');
         }
   }


   function remove(rowid){

     if(x<1){
       alert("Not Remove");
     }else{
       $('#'+rowid).remove();
       x--;
     }
   }


 </script>


</div>
<!-- ./wrapper -->

</body>
</html>
