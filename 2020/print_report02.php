<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
//$projectCode = '61100004_1539515202';

$projectCode = $_POST['project_code'];

?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/Chart.min.js"></script>

</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }

  table{
    text-align: center;
  }

  tbody tr td{
    font-size:12pt;
    padding: 5px;
  }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;
  }

  page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    padding: 0mm 10mm 10mm 10mm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"][layout="landscape"] {
  width: 29.7cm;
}

  thead{
    font-weight: bolder;
  }

  .text-left{
    text-align: left;
  }
  .text-center{
    text-align: center;
  }

  .color-e0e0e0{
    background-color: #e0e0e0;
  }

  @media print {
      @page {
        size: Landscape;
        margin: 0;
        background-color: #ffffff;
      }
      html, body {
        size: Landscape;
      }
      .break{
        page-break-after: always;
      }
      .color-e0e0e0{
        background-color: #e0e0e0;
      }
  }
</style>
<html>
<body>
  <page size="A4" layout="landscape">
  <script>
    function chartJsShow(id,name,str,str_m,str_f){
      var res = str.split("|");
      var res_m = str_m.split("|");
      var res_f = str_f.split("|");
      let myChart = document.getElementById(id).getContext('2d');
      Chart.defaults.global.defaultFontFamily = 'THSarabun';
      Chart.defaults.global.defaultFontSize = 12;
      Chart.defaults.global.defaultFontColor = '#777';

      let massPopChart = new Chart(myChart, {
        type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        defaultFontSize: '8',
        data:{
          labels: res ,
          datasets:[{
            label:'ชาย',
            data:res_m,
            fill: false,
            backgroundColor:'rgba(255, 99, 132, 0.6)',
            borderWidth:2,
            borderColor:'rgba(255, 99, 132, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000',
            lineTension: 0
          },
          {
            label:'หญิง',
            data:res_f,
            fill: false,
            backgroundColor:'rgba(54, 162, 235, 0.6)',
            borderWidth:2,
            borderColor:'rgba(54, 162, 235, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000',
            lineTension: 0
          }]
        },
        options:{
          responsive: true,
          title:{
            display:true,
            fontStyle:'bold',
            text:name,
            fontSize:16
          },
          legend:{
            display:true,
            position:'bottom',
            labels:{
              fontColor:'#000'
            }
          },
          tooltips:{
            enabled:true
          },
          scales: {
            yAxes: [{
                ticks: {
                    // max: 150,
                    min: 0,
                    stepSize: 10
                }
            }]
          }
        }
      });
    }
  </script>

  <?php
    $break = false;
    $total_teat_c = array();
    $total_teat_p = array();

    $sql = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode'";
    $query = DbQuery($sql,null);
    $row  = json_decode($query, true);

    $num   = $row['dataCount'];
    if($num>0){

    foreach ($row['data'] as $value) {
      $project_codep = $value['project_code'];
      $project_name_title  = $value['project_name'];
      $sqlp = "SELECT DISTINCT(stage) FROM pfit_t_person WHERE project_code = '$project_codep' ORDER BY stage  ASC";
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      $stage = '';
      // print_r($rowp);
      $nump   = $rowp['dataCount'];
      // if($nump>0){

      foreach ($rowp['data'] as $keyp => $valuep) {

        $stage = isset($valuep['stage'])?$valuep['stage']:"";
        $stage_id = isset($valuep['stage'])?$valuep['stage']:"";

        if($stage == ""){
          continue;
        }

        $sql_g = "SELECT DISTINCT(grade) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' ORDER BY grade  ASC";
        $query_g = DbQuery($sql_g,null);
        $row_g  = json_decode($query_g, true);
          //echo $stage_id;
          foreach ($row_g['data'] as $key_g => $value_g) {

            $grade = $value_g['grade'];

            $sql_r = "SELECT DISTINCT(room) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' ORDER BY room  ASC";
            $query_r = DbQuery($sql_r,null);
            $row_r  = json_decode($query_r, true);

            foreach ($row_r['data'] as $key_r => $value_r) {

              $room   = $value_r['room'];
              if($stage == 1){
                $stageTxt = 'นักเรียนชั้นมัธยมศึกษาปีที่';
              }elseif($valuep['stage'] == 2){
                $stageTxt = 'นักเรียนชั้นประถมศึกษาปีที่';
              }
              $grade = $value_g['grade'];

              $sqlpp = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                           WHERE ptpt.test_code = ptt.test_code
                           AND ptpt.project_code = '$project_codep' ORDER BY ptpt.test_seq ASC";
              $querypp = DbQuery($sqlpp,null);
              $rowpp  = json_decode($querypp, true);
              $numpp   = $rowpp['dataCount'];

              foreach ($rowpp['data'] as $keypp => $valuepp) {
                $category_criteria_code = $valuepp['category_criteria_code'];
                $test_code = $valuepp['test_code'];
                $sqlppp = "SELECT * FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
                $queryppp = DbQuery($sqlppp,null);
                $rowppp  = json_decode($queryppp, true);
                $numppp   = $rowppp['dataCount'];

                foreach ($rowppp['data'] as $keyppp => $valueppp) {
                  $category_criteria_detail_code = $valueppp['category_criteria_detail_code'];

                  //echo $test_code.",".$category_criteria_detail_code."<br>";
                  if(!isset($total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                    $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                  }

                  if(!isset($total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                    $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                  }
                }

              }
              if($break){
                echo "<div class='break'></div>";
              }else{
                $break = true;
              }
  ?>
  <div align="center"><h2>ผลการทดสอบสมรรถภาพทางกาย</h2></div>
  <div align="center"><h2 style="margin-top: -5px;"><?=$project_name_title.' '?><?php echo $stageTxt.' '.$grade.'/'.$room; ?></h2></div>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
    <thead>
      <tr align="center">
  			<td colspan="<?= ($numpp + 9) ?>" style="height:32px;border-right:solid #fff 1px;border-top:solid #fff 1px;border-left:solid #fff 1px;"></td>
  		</tr>
  		<tr align="center">
  			<td rowspan="2">เลขที่</td>
  			<td rowspan="2">เลขประจำตัว</td>
  			<td rowspan="2">เพศ</td>
  			<td rowspan="2">ชื่อ</td>
  			<td rowspan="2">นามสกุล</td>
  			<td>น้ำหนัก</td>
  			<td>ส่วนสูง</td>
        <?php
            foreach ($rowpp['data'] as $valuepp) {
        ?>
  			<td><?php echo $valuepp['test_name']; ?></td>
        <?php } ?>

  			<td>คะแนน</td>
  		</tr>
  		<tr align="center">

  			<td class="color-e0e0e0">(กก.)</td>
  			<td class="color-e0e0e0">(ซม.)</td>
        <?php
            $total_max_values = 0;
            foreach ($rowpp['data'] as $valuepp) {
              $category_criteria_code = $valuepp['category_criteria_code'];

              $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
              $query_value = DbQuery($sql_value,null);
              $row_value  = json_decode($query_value, true);
              $num_value  = $row_value['dataCount'];
              $data_value = $row_value['data'];

              $total_max_values += $data_value[0]['max_value'];
        ?>
  			<td class="color-e0e0e0">(<?php echo $valuepp['test_unit']; ?>)</td>
        <?php } ?>

  			<td class="color-e0e0e0">(เต็ม <?=$total_max_values; ?>)</td>
  		</tr>
    </thead>
    <tbody>
      <?php

      $sqlperson = "SELECT * FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' AND room = '$room' order by date_create";
      //echo $sqlperson;
      $queryperson = DbQuery($sqlperson,null);
      $rowperson  = json_decode($queryperson, true);
      $i = 1;
        foreach ($rowperson['data'] as $value) {
        $person_number = $value['person_number'];
        $person_name = $value['person_name'];
        $person_lname = $value['person_lname'];
        $wiegth = $value['wiegth'];
        $height = $value['height'];
        $person_gender = $value['person_gender']=='M'?'ชาย':'หญิง';
        $bmi = bmi($wiegth,$height)[0];
      ?>
      <tr align="center">
  			<td ><?php echo $i; ?></td>
  			<td ><?php echo $person_number; ?></td>
  			<td ><?php echo $person_gender; ?></td>
  			<td class="text-left" ><?php echo $person_name; ?></td>
  			<td class="text-left" ><?php echo $person_lname; ?></td>

  			<td ><?php echo $wiegth; ?></td>
  			<td ><?php echo $height; ?></td>
        <?php
            $total_max_value = 0;
            $chkres = "F";
            $results = array();
            $resultTxt = "";
            $n = 0;
            foreach ($rowpp['data'] as $valuepp) {
              $test_code = $valuepp['test_code'];

              $sqlres = "SELECT * FROM pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd, pfit_t_person ptp
                         WHERE ptc.test_criteria_code = pts.test_criteria_code
                         AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
                         AND pts.project_code = '$project_codep' AND ptp.person_number = pts.person_number
                         AND pts.test_code = '$test_code' AND pts.person_number = '$person_number'";

              //echo $sqlres."<br>";
              $queryres = DbQuery($sqlres,null);
              $rowres  = json_decode($queryres, true);

              $numres   = $rowres['dataCount'];
              $result = '-';
              $resultCat = "-";
              if($numres>0){
                if(isset($rowres['data'][0]['result'])){
                  $total_max_value += $rowres['data'][0]['category_criteria_detail_value'];
                  //$result = $rowres['data'][0]['result'];
                  $result = $rowres['data'][0]['result_cal'];

                  $resultCat = $rowres['data'][0]['category_criteria_detail_name'];

                  $category_criteria_detail_code = $rowres['data'][0]['category_criteria_detail_code'];
                  if($rowres['data'][0]['gender'] == 'M'){
                    $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                  }else{
                    $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                  }

                }else{
                  $chkres = "N";
                }

                $results[$n] = $resultCat;
                $n++;
              }
        ?>
  			<td><?php echo $result; ?></td>
        <?php
        }
        if($chkres == "N"){
          $total_txt_res = "ข้อมูลไม่ครบ";
        }else{
          $total_txt_res = calTestFit($total_max_values,$total_max_value,60);
        }

        ?>

  			<td><?=$total_max_value ?></td>
  		</tr>

      <tr align="center">
  			<td style="border-bottom:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
  			<td class="text-left" style="border:solid #333 1px;"></td>
  			<td class="text-left" style="border:solid #333 1px;"></td>

  			<td style="border:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
        <?php
          for($x = 0; $x < count($results); $x++) {

        ?>
  			<td style="border:solid #333 1px;"><?php echo $results[$x]; ?></td>
        <?php } ?>

  			<td style="border:solid #333 1px;"><?=$total_txt_res; ?></td>
  		</tr>

      <?php $i++; }  ?>
  	</tbody>
    <tfoot>
      <tr align="center" style="border:solid #fff 1px;">
        <td colspan="<?= ($numpp + 9) ?>" style="height:5mm;border:solid #fff 1px;"></td>
      </tr>
    </tfoot>
  </table>

  <?php } ?>
  <div class="break"></div>
  <div class="text-center">
    <h1>แผนภูมิแสดงระดับสมรรถภาพทางกาย<?=$project_name_title ?></h1>
    <h1 style="margin-top: -5px;"><?php echo $stageTxt.' '.$grade; ?></h1>
  </div>
  <?php
    $ids = 0;
  ?>
    <div class="row" align="center">

    <?php
    $m_score = array();
    // print_r($rowpp['data']);
    foreach ($rowpp['data'] as $keypp => $valuepp) {
      $ids = $keypp;
      $test_name = $valuepp['test_name'];
      $test_code = $valuepp['test_code'];

      $category_criteria_code = $valuepp['category_criteria_code'];

      $sql_c = "SELECT DISTINCT(pttc.category_criteria_detail_code),ptccd.category_criteria_detail_name,ptccd.category_criteria_code FROM pfit_t_cat_criteria_detail ptccd , pfit_t_test_criteria pttc
                WHERE ptccd.category_criteria_detail_code = pttc.category_criteria_detail_code
                AND category_criteria_code = '$category_criteria_code'";
      $query_c = DbQuery($sql_c,null);
      $row_c  = json_decode($query_c, true);
      $category_criteria_detail_name = array();
      $m_score = array();
      $f_score = array();
      $score = array();
        foreach ($row_c['data'] as $key_c => $value_c) {
          $category_criteria_detail_code = isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
          if($category_criteria_detail_code!= ""){
            $m_score[] = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
            $f_score[] = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
            // print_r($f_score);
            $category_criteria_detail_name[] =  $value_c['category_criteria_detail_name'];
          }else{
            $m_score[] = "";
            $f_score[] = "";
            // print_r($f_score);
            $category_criteria_detail_name[] =  "";
          }

        }
        $str =  implode("|",$category_criteria_detail_name);
        $str_m =  implode("|",$m_score);
        $str_f =  implode("|",$f_score);

  ?>
    <?php
      $dd  =  $stage_id.'_'.$grade.'_'.$ids;
    ?>

      <div class="col-xs-6">
        <div style="margin:0px;padding-top:40px">
          <canvas id="<?php echo $stage_id.'_'.$grade.'_'.$ids; ?>"></canvas>
          <script>
            chartJsShow('<?php echo $dd; ?>','<?php echo $test_name ?>','<?php echo $str; ?>','<?php echo $str_m; ?>','<?php echo $str_f; ?>');
          </script>
        </div>
      <br><br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
      		<tr align="center">
            <td><?=$test_name?></td>
            <td>ชาย/จำนวน</td>
            <td>หญิง/จำนวน</td>
            <td>รวม/คน</td>
          </tr>
        <tbody>
          <?php
            foreach ($row_c['data'] as $key_c => $value_c) {
              $category_criteria_detail_name =  isset($value_c['category_criteria_detail_name'])?$value_c['category_criteria_detail_name']:"";
              $category_criteria_detail_code =  isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
              $tm = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
              $tf = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
              $tt = (intval($tm) + intval($tf));
          ?>
          <tr align="center">
            <td><?=$category_criteria_detail_name?></td>
            <td><?= $tm ?></td>
            <td><?= $tf ?></td>
            <td><?= $tt ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <br><br>
      <div class="col-xs-6">
        <p class="text-center">&nbsp;</p>
      </div>
      <div class="col-xs-6">
        <p class="text-center">&nbsp;</p>
      </div>


     </div>
   <!-- </div> -->
  <?php }
  ?>
</div>
<div class="break"></div>
  <?php
        }
      }
    }
  }
  ?>
</page>
</body>
</html>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 2000);
  });
</script>
