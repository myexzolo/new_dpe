<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');


include 'mpdf/mpdf.php';
ob_start();
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>

<html>

<body>

<?php


  $person_number = $_POST['person_number'];
  $projectCode = $_POST['project_code'];

  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcode/temp'.DIRECTORY_SEPARATOR;
  //html PNG location prefix
  $PNG_WEB_DIR = 'qrcode/temp/';
  include "qrcode/qrlib.php";

  if (!file_exists($PNG_TEMP_DIR)){
    mkdir($PNG_TEMP_DIR);
  }

  $filename = $PNG_TEMP_DIR.$person_number.'.png';
  $CorrectionLevel = 'L';

  $textQr = "$person_number";
  $matrixPointSize = 4;
  QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


  $sql = "SELECT * FROM pfit_t_project where project_code = '$projectCode'";
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);

  $wiegthP   = chkNull($row['data'][0]['wiegth']);
  $heightP   = chkNull($row['data'][0]['height']);
?>
<h3 class="text-center"><b><?php echo $row['data'][0]['project_name']; ?></b></h3>
<br>
<table style="width:100%">
		<tr>
			<td colspan="5" style="text-align:center"></td>
		</tr>
		<tr>
			<td colspan="1" rowspan="3" style="position: relative;">
        <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" />
        <!-- <img class="img-responsive" src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=โรงเรียน" /></td> -->
			<td colspan="4" rowspan="1" style="text-align:right"><strong>วันที่ทำการทดสอบ : ____/____/________</strong></td>
		</tr>
    <?php

      $sql = "SELECT project_code,person_number,person_gender,person_name,person_lname,
               ".getQueryDate('date_of_birth').",wiegth,height,person_type,education,stage,grade
              FROM pfit_t_person where project_code = '$projectCode' and person_number = '$person_number'";

      $query = DbQuery($sql,null);
      $row  = json_decode($query, true);

      $date_of_birth = $row['data'][0]['date_of_birth'];
      //echo ">>>>".$date_of_birth;
      $age  =  yearBirth($date_of_birth);
    ?>
		<tr valign="middle">
			<td><strong><h4>เลขประจำตัว : <?php echo $row['data'][0]['person_number']; ?></h4></strong></td>
			<td colspan="2" rowspan="1"><strong><h4>ชื่อ : <?php echo $row['data'][0]['person_name']; ?></h4></strong></td>
			<td><strong><h4>สกุล : <?php echo $row['data'][0]['person_lname']; ?></h4></strong></td>
		</tr>
		<tr>
			<td><strong><h4>วันเดือนปี เกิด : <?php echo convDatetoThai($date_of_birth) ?></h4></strong></td>
			<td><strong><h4>เพศ : <?php echo $row['data'][0]['person_gender']=='M'?"ชาย":"หญิง"; ?></h4></strong></td>
			<td><strong></td>
			<td><strong></td>
		</tr>
</table>
</p>
    <?php
      $sqlp = "SELECT * FROM pfit_t_project_test
              INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
              WHERE pfit_t_project_test.project_code = '$projectCode' and pfit_t_test.test_age_min <= $age and pfit_t_test.test_age_max >= $age
              ORDER BY pfit_t_project_test.test_seq ASC";
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);

      $numRow = 0;
      ?>
      <table class="table table-bordered" cellpadding="10">
          <tr>
            <td colspan="4"><h4><strong>ฐานการทดสอบ</strong></h4></td>
          </tr>
          <tr>
            <td class="text-center"><h4><strong>ลำดับ</strong></h4></td>
            <td class="text-center"><h4><strong>รายการ</strong></h4></td>
            <td class="text-center"><h4><strong>หน่วย</strong></h4></td>
            <td class="text-center"><h4><strong>ผลการวัด</strong></h4></td>
          </tr>
      <?php
      foreach ($rowp['data'] as $key => $value) {
        // echo $value['test_code'];
      if( $value['test_code'] == "BMI"){
        $numRow++;
      ?>
      <tr>
        <td width="10%" class="text-center"><h4><?= $numRow; ?></h4></td>
        <td width="35%" class="text-left"><h4><?= "น้ำหนัก" ?></h4></td>
        <td width="20%" class="text-center"><h4>กิโลกรัม</h4></td>
        <td width="35%" class="text-center"><h4><?= $wiegthP; ?></h4></td>
      </tr>
      <tr>
        <td width="10%" class="text-center"><h4><?= $numRow+1; ?></h4></td>
        <td width="35%" class="text-left"><h4><?= "ส่วนสูง" ?></h4></td>
        <td width="20%" class="text-center"><h4>เซนติเมตร</h4></td>
        <td width="35%" class="text-center"><h4><?= $heightP; ?></h4></td>
      </tr>
      <?php
      $numRow++;
    }else{
      $numRow++;
    ?>

    <tr>
      <td width="10%" class="text-center"><h4><?php echo $numRow; ?></h4></td>
      <td width="35%" class="text-left"><h4><?php echo $value['test_name']; ?></h4></td>
      <td width="20%" class="text-center"><h4><?php echo $value['test_unit']; ?></h4></td>
      <td width="35%" class="text-center"><h4></h4></td>
    </tr>

  <?php }} ?>

</table>

</body>
<?php
    $html = ob_get_contents();
    ob_end_clean();
    $mpdf->autoPageBreak = true;
    $mpdf=new mPDF('utf-8');
    $mpdf->SetAutoFont();
		$md = strcode2utf("ระบบการจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย");
		$mpdf->SetTitle($md);


    $mpdf->autoScriptToLang = false;
    $mpdf->margin_header = 2;
    // $mpdf->SetHeader('รายงานโดย codingthailand | รายงานลูกค้าทั้งหมด | ออกรายงานเมื่อ: '.date('d/m/Y H:i:s'));
    $mpdf->margin_footer = 9;
    // $mpdf->SetFooter('หน้าที่ {PAGENO}');
    // Define a Landscape page size/format by name
    //$mpdf=new mPDF('utf-8', 'A4-L');
    // Define a page size/format by array - page will be 190mm wide x 236mm height
    //$mpdf=new mPDF('utf-8', array(190,236));
    $stylesheet = file_get_contents('bootstrap/css/printpdf.css');
    //$mpdf->SetDisplayMode('fullpage');


    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    //$mpdf->Output();
    $mpdf->Output(time(),'I');


    exit;
?>
</html>
