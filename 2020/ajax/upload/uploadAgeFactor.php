<?php
require_once '../../Classes/PHPExcel.php';

include '../../Classes/PHPExcel/IOFactory.php';
include('../../conf/connect.php');
include('../../conf/utils.php');
session_start();

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;


$success = 0;
$fail    = 0;
$total   = 0;


if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);

    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $headerName = $headingsArray['A'];

    $headingsArray = array("A" => "age",
                           "B" => "value");

    $r = -1;

    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
        $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
        if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
            ++$r;
            foreach($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }
    }
    //
    $i = 0;
    foreach ($namedDataArray as $result) {
        $total++;
        $age               = trim($result["age"]);
        $value             = trim($result["value"]);
        try {
            $sql = "SELECT * FROM pfit_t_agefactor WHERE age = '$age' ";
            $json = DbQuery($sql,null);
            $obj = json_decode($json, true);
            $dataCount = $obj['dataCount'];
            $strSQL = "";
            if($dataCount > 0){
                $sql = "UPDATE pfit_t_agefactor SET value = '$value' WHERE age = '$age'";
            }else{
                $sql = "INSERT INTO pfit_t_agefactor (age,value) VALUES('$age','$value')";
            }
            echo $sql;
            DbQuery($sql,null);
            $success++;
        }catch (Exception $e) {
            $fail++;
        }
    }
    echo "total :".$total." ,success :".$success." ,fail :".$fail;
}

?>
