<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$sql = "SELECT * FROM t_page WHERE is_active = 'Y' ORDER BY module_id";

$queryPage = DbQuery($sql,null);

$rowPage = json_decode($queryPage, true);
$numPage  = $rowPage['dataCount'];
$dataPage = $rowPage['data'];


$role_id = "";
$role_name = "";
$role_desc = "";
$role_code = "";
$is_active = "";
$option    = "";

$checked1 = 'checked';
$checked2 = '';
$checked3 = '';
$checked4 = '';


if(!empty($_POST['value'])){
  $sql = "SELECT * FROM t_role WHERE role_id = ?";

  $ParamData = array($_POST['value']);
  $query = DbQuery($sql,$ParamData);

  $row = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  $role_id    = $data[0]['role_id'];
  $role_name  = $data[0]['role_name'];
  $role_desc  = $data[0]['role_desc'];
  $role_code  = $data[0]['role_code'];
  $role_access  = isset($data[0]['role_access'])?$data[0]['role_access']:"";
  $is_active  = $data[0]['is_active'];
  $page_list  = $data[0]['page_list'];

  $mapPage = explode(",", $page_list);



  if($role_access == 'E'){
    $checked1 = '';
    $checked2 = 'checked';
  }else if($role_access == 'ED'){
    $checked1 = '';
    $checked3 = 'checked';
  }else if($role_access == 'ALL'){
    $checked1 = '';
    $checked4 = 'checked';
  }

  for ($i=0; $i < $numPage ; $i++) {

    $page_id   = $dataPage[$i]['page_id'];
    $page_name = $dataPage[$i]['page_name'];

    $checked = ' ';
    if(array_search($page_id,$mapPage) != ""){
      $checked =  "checked";
    }
    $option .= '<div class="form-group col-md-6">
    <label for="c_'.$page_id.'" style="font-weight:200"><input class="minimal" name="page_list[]" id="c_'.$page_id.'" type="checkbox" value="'.$page_id.'" '.$checked.'>
    '.'  '.$page_name.'</label></div>';
  }
}else{
  for ($i=0; $i < $numPage ; $i++) {
    $page_id   = $dataPage[$i]['page_id'];
    $page_name = $dataPage[$i]['page_name'];

    $option .= '<div class="form-group col-md-6"><input class="minimal"  name="page_list[]" type="checkbox" value="'.$page_id.'" >'.'  '.$page_name.'</div>';
  }
}
?>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label>Role Code</label>
      <input value="<?= $role_code ?>" name="role_code" type="text" maxlength="6" class="form-control" placeholder="Code" required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>Role Name</label>
      <input value="<?= $role_name ?>" name="role_name" type="text" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>Role Description</label>
      <input value="<?= $role_desc ?>" name="role_desc" type="text" class="form-control" placeholder="Name">
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>Role Access</label>
      <div class="box-body">
      <div class="form-group col-md-6">
        <input class="minimal" name="role_access" id="r1" type="radio" value="V" <?=$checked1 ?>> <label for="r1" style="font-weight:200"> View</label>
      </div>
      <div class="form-group col-md-6">
        <input class="minimal" name="role_access" id="r2" type="radio" value="E" <?=$checked2 ?>> <label for="r2" style="font-weight:200"> Edit</label>
      </div>
      <div class="form-group col-md-6">
        <input class="minimal" name="role_access" id="r3" type="radio" value="ED" <?=$checked3 ?>> <label for="r3" style="font-weight:200"> Edit, Delete</label>
      </div>
      <div class="form-group col-md-6">
        <input class="minimal" name="role_access" id="r4" type="radio" value="ALL" <?=$checked4 ?>> <label for="r4" style="font-weight:200"> ALL</label>
      </div>
    </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>Select Page</label>
        <div class="box-body">
          <?=$option ?>
        </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" required>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
</div>
<input type="hidden" name="role_id" value="<?= $role_id ?>">
