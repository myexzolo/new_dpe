<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


?>
<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required>
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="tableUpload" class="table table-bordered table-striped"
                   data-toggle="table"
                   data-toolbar="#toolbar"
                   data-url="ajax/PFIT0202/showHisUpload.php"
                   data-pagination="true"
                   data-page-list="[10, 25, 50, 100, ALL]"
                   data-search="true"
                   data-flat="true"
                   data-show-refresh="true"
                   >
                <thead>
                    <tr>
                        <th data-sortable="true" data-formatter="runNo" data-align="center">ลำดับ</th>
                        <th data-sortable="true" data-field="file_name" data-align="center" class="align-left">ชื่อไฟล์</th>
                        <th data-sortable="true" data-formatter="getdateUploadTH" data-align="center">วันที่นำเข้า</th>
                        <th data-field="operate" data-formatter="getStatusUpload" data-align="center">สถานะ</th>
                        <th data-sortable="true" data-field="total_record" data-align="center">Reccord ทั้งหมด</th>
                        <th data-sortable="true" data-field="success_record" data-align="center">Reccord สำเร็จ</th>
                        <th data-sortable="true" data-field="fail_record" data-align="center">Reccord ล้มเหลว</th>
                    </tr>
                </thead>
            </table>
          </div>
    </div>
  </div>
</div>
