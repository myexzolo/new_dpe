<style>
table.dataTable thead tr {
  border: 1px;
}
</style>
<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$test_code = $_POST['test_code'];
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
		<tr>
			<th rowspan="2" style="vertical-align:middle;">อายุ</th>
			<th rowspan="2" style="vertical-align:middle;" >เพศ</th>
      <?php
        $sqlCat = "SELECT * FROM pfit_t_test t, pfit_t_cat_criteria_detail c
        where t.test_code = '$test_code' and t.category_criteria_code = c.category_criteria_code order by category_criteria_detail_code";

        //echo $sqlCat;
        $query = DbQuery($sqlCat,null);
        $json = json_decode($query, true);
        $numCat  = $json['dataCount'];
        $rowCat  = $json['data'];

        $criteriaDelCodeArr = array();
        for($i=0; $i < $numCat; $i++)
        {

          $categoryCriteriaCode          = $rowCat[$i]['category_criteria_code'];
          $category_criteria_detail_code = $rowCat[$i]['category_criteria_detail_code'];
          $category_criteria_detail_name = $rowCat[$i]['category_criteria_detail_name'];

          $category_criteria_detail_name = explode("/", $category_criteria_detail_name);
          $category_criteria_detail_name = $category_criteria_detail_name[0];

          array_push($criteriaDelCodeArr,$category_criteria_detail_code);
      ?>
			   <th colspan="2" align="center"><?=$category_criteria_detail_name?></th>
      <?php
        }
      ?>
			<th rowspan="2" style="vertical-align:middle;">คัดลอก</th>
      <th rowspan="2" style="vertical-align:middle;">ดู</th>
      <th rowspan="2" style="vertical-align:middle;">แก้ไข</th>
      <th rowspan="2" style="vertical-align:middle;">ลบ</th>
		</tr>
		<tr>
      <?php
      $numArr = count($criteriaDelCodeArr);
      for($i=0; $i < $numArr; $i++)
      {
      ?>
      <th align="center" style="border-top:0px">min</th>
			<th align="center" style="border-right:1px solid #eef;border-top:0px">max</th>
      <?php
      }
      ?>
		</tr>
    <tbody>
      <?php
      $sqlCriteria = "SELECT age,gender,min,max,category_criteria_detail_code FROM pfit_t_test_criteria
                      WHERE test_code = '$test_code'
                      GROUP by  age,gender,min,max,category_criteria_detail_code
                      ORDER by  gender DESC,age";
      //echo $sqlCriteria;
      $query = DbQuery($sqlCriteria,null);
      $json = json_decode($query, true);
      $numCri  = $json['dataCount'];
      $rowCri  = $json['data'];

      $ageTmp = "";
      $genderTmp = "";
      $f = "";
      $minArr = array();
      $maxArr = array();
      for($i=0; $i < $numCri; $i++)
      {
        $age      = $rowCri[$i]['age'];
        $gender   = $rowCri[$i]['gender'];
        $min      = $rowCri[$i]['min'];
        $max      = $rowCri[$i]['max'];
        $category_criteria_detail_code = $rowCri[$i]['category_criteria_detail_code'];

        $min  = sprintf('%01s',$min);
        $max  = sprintf('%01s',$max);

        if($f == ""){
          $f  = 'N';
          $ageTmp = $age;
          $genderTmp = $gender;


          for($z=0; $z < $numArr; $z++)
          {
            //echo $category_criteria_detail_code.",".$criteriaDelCodeArr[$z].",".$z." >>N $ageTmp >> $min >> $max<br>";
            if($criteriaDelCodeArr[$z] == $category_criteria_detail_code){
              $minArr[$z] = $min;
              $maxArr[$z] = $max;
              break;
            }
          }
          continue;
        }else{
          if($ageTmp == $age && $genderTmp == $gender){
            for($e=0; $e < $numArr; $e++)
            {

              if($criteriaDelCodeArr[$e] == $category_criteria_detail_code){
                //echo $category_criteria_detail_code.",".$criteriaDelCodeArr[$e].",".$e." >>F $ageTmp >> $min >> $max<br>";
                $minArr[$e] = $min;
                $maxArr[$e] = $max;
                break;
              }
            }
            if(($numCri - $i) > 1){
              continue;
            }else{
?>
<tr>
  <td align="center"><?= $ageTmp ?></td>
  <td align="center"><?= $genderTmp== 'M'? 'ชาย':'หญิง' ?></td>
<?php
  for($j=0; $j < $numArr; $j++)
  {
  ?>
    <td align="center"><?= $minArr[$j]?></td>
    <td align="center"><?= $maxArr[$j] ?></td>
  <?php
  }

  ?>
  <td align="center">
    <a onclick="searchData('<?= $test_code ?>','COPY','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
    <span class="fa fa-copy"></span>
    </a>
  </td>
  <td align="center">
    <a onclick="searchData('<?= $test_code ?>','VIEW','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
    <span class="fa fa-search"></span>
    </a>
  </td>
  <td align="center">
    <a onclick="searchData('<?= $test_code ?>','EDIT','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
    <span class="fa fa-edit"></span>
    </a>
  </td>
  <td align="center">
    <a onclick="formDelV2('<?= $test_code ?>','<?= $ageTmp ?>','<?= $genderTmp ?>')">
    <span class="glyphicon glyphicon-trash"></span>
    </a>
  </td>
</tr>
<?php
            }
          }else{
        ?>
        <tr>
    			<td align="center"><?= $ageTmp ?></td>
    			<td align="center"><?= $genderTmp== 'M'? 'ชาย':'หญิง' ?></td>
        <?php
          for($j=0; $j < $numArr; $j++)
          {
          ?>
            <td align="center"><?= isset($minArr[$j])?$minArr[$j]:"" ?></td>
      			<td align="center"><?= isset($maxArr[$j])?$maxArr[$j]:"" ?></td>
          <?php
          }

          ?>
    			<td align="center">
            <a onclick="searchData('<?= $test_code ?>','COPY','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
            <span class="fa fa-copy"></span>
            </a>
          </td>
    			<td align="center">
            <a onclick="searchData('<?= $test_code ?>','VIEW','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
            <span class="fa fa-search"></span>
            </a>
          </td>
    			<td align="center">
            <a onclick="searchData('<?= $test_code ?>','EDIT','<?= $ageTmp ?>','<?= $genderTmp ?>','<?= $categoryCriteriaCode ?>')">
            <span class="fa fa-edit"></span>
            </a>
          </td>
    			<td align="center">
            <a onclick="formDelV2('<?= $test_code ?>','<?= $ageTmp ?>','<?= $genderTmp ?>')">
            <span class="glyphicon glyphicon-trash"></span>
            </a>
          </td>
    		</tr>
        <?php
              //echo ">>T $age >> $min >> $max<br>";
              for($e=0; $e < $numArr; $e++)
              {
                if($criteriaDelCodeArr[$e] == $category_criteria_detail_code){
                  //echo $category_criteria_detail_code.",".$criteriaDelCodeArr[$e].",".$e." >>F $ageTmp >> $min >> $max<br>";
                  $minArr[$e] = $min;
                  $maxArr[$e] = $max;
                  break;
                }else{
                  $minArr[$e] = "";
                  $maxArr[$e] = "";
                }
              }
            }
          }
          $ageTmp = $age;
          $genderTmp = $gender;
          }
      ?>
    </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
   'paging'      : false,
   'lengthChange': false,
   'searching'   : false,
   'ordering'    : false,
   'info'        : false,
   'autoWidth'   : true
 })
})
</script>
