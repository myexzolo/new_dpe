<?php
require_once '../../Classes/PHPExcel.php';

include '../../Classes/PHPExcel/IOFactory.php';
include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();



$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;

$type_test = 1;

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try
   {
       $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
       $objReader = PHPExcel_IOFactory::createReader($inputFileType);
       $objReader->setReadDataOnly(true);
       $objPHPExcel = $objReader->load($inputFileName);

       $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
       $highestRow = $objWorksheet->getHighestRow();
       $highestColumn = $objWorksheet->getHighestColumn();

       $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
       $headingsArray = $headingsArray[1];

       $test_code = $headingsArray['A'];

       if(trim($test_code) != "")
       {
          $headingsArray = array("A" => "age_start",
                              "B" => "age_end",
                              "C" => "gender",
                              "D" => "min",
                              "E" => "max",
                              "F" => "category_criteria_detail_code",
                              "G" => "test_suggest",
                              "H" => "test_suggest_en");
          $r = -1;

          $namedDataArray = array();
          for ($row = 3; $row <= $highestRow; ++$row) {
              $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
              if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                  ++$r;
                  foreach($headingsArray as $columnKey => $columnHeading) {
                      $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                  }
              }
          }
          $i = 0;
          foreach ($namedDataArray as $result) {
             try
             {
                   $i++;
                   $total++;
                   $age_start       = trim($result["age_start"]);
                   $age_end         = trim($result["age_end"]);
                   $gender          = trim($result["gender"]);
                   $min             = trim($result["min"]);
                   $max             = trim($result["max"]);
                   $category_criteria_detail_code  = trim($result["category_criteria_detail_code"]);
                   $test_suggest    = $result["test_suggest"];
                   $test_suggest_en = $result["test_suggest_en"];

                   if($age_end == "60 ขึ้นไป"){
                     $age_end = 90;
                   }

                   $category =  explode(":", $category_criteria_detail_code);
                   $category_criteria_detail_code = $category[0];
                   for($age = $age_start; $age <= $age_end; $age++)
                   {
                     $sql = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' and age = '$age' and gender = '$gender' and category_criteria_detail_code = '$category_criteria_detail_code' ";
                     $json = DbQuery($sql,null);
                     //print_r (">>>>>".$json."\n");
                     $obj = json_decode($json, true);
                     $dataCount = $obj['dataCount'];
                     $strSQL = "";
                     if($dataCount > 0){
                       $strSQL = "UPDATE pfit_t_test_criteria SET
                                   min    = '$min',
                                   max    = '$max',
                                   category_criteria_detail_code  = '$category_criteria_detail_code',
                                   test_suggest     = '$test_suggest',
                                   test_suggest_en  = '$test_suggest_en'
                                   WHERE test_code  = '$test_code' and age = '$age' and gender = '$gender' and category_criteria_detail_code = '$category_criteria_detail_code'";
                     }else{
                       $test_criteria_code = "";
                       $sql    = "SELECT test_criteria_code FROM pfit_t_test_criteria where test_criteria_code like '$test_code%' order by test_criteria_code desc";
                       $query  = DbQuery($sql,null);

                       $json = json_decode($query, true);
                       $num  = $json['dataCount'];
                       $row  = $json['data'];

                       if($num > 0){
                         $test_criteria_code = $row[0]['test_criteria_code'];
                       }

                       if(!empty($test_criteria_code)){
                         $lastNum = substr($test_criteria_code,strlen($test_code));
                         $lastNum = $lastNum + 1;
                         $test_criteria_code = $test_code.sprintf("%03d", $lastNum);
                       }else{
                         $test_criteria_code = $test_code.sprintf("%03d", 1);
                       }


                       $strSQL = "INSERT INTO pfit_t_test_criteria
                            (test_criteria_code,test_code,age,gender,min,max,category_criteria_detail_code,type_test,test_suggest,status,test_suggest_en)
                            VALUES('$test_criteria_code','$test_code','$age','$gender','$min','$max','$category_criteria_detail_code','$type_test','$test_suggest','Y','$test_suggest_en')";
                     }
                     //echo $strSQL."<br>";
                     $js      = DbQuery($strSQL,null);
                     $row     = json_decode($js, true);
                     $status  = $row['status'];

                     $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
                     if($errorInfo > 0){
                       $fail++;
                       continue;
                     }else{
                       $success++;
                     }
                   }
             }catch (Exception $ex) {
               $fail++;
             }
          }
      }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }

   if($_SESSION['TYPE_CONN'] == '1'){
      $str = "INSERT INTO pfit_t_history_upload
              (HISTORY_UPLOAD_ID,file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code)
              VALUES
              (T_HISTORY_UPLOAD.NEXTVAL,'$inputFileName','S','$total','$success','$fail','','pfit_t_test_criteria','PFIT0202')";
   }else{
     $str = "INSERT INTO pfit_t_history_upload
             (file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code)
             VALUES
             ('$inputFileName','S','$total','$success','$fail','','pfit_t_test_criteria','PFIT0202')";
   }
   DbQuery($str,null);

   $query    = DbQuery($sql,null);
   $json     = json_decode($query, true);
   $status   = $json['status'];

   if($status == "success"){
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกสมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
     //echo  "บันทึกสมบูรณ์ :".$total.", success :".$success." ,fail :".$fail;
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกไม่สมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
    //echo  "บันทึกไม่สมบูรณ์";
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
  //echo "ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง";
}

?>
