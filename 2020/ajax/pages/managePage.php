<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');


$page_id    = $_POST['page_id'];
$page_code  = $_POST['page_code'];
$page_name  = $_POST['page_name'];
$page_seq   = $_POST['page_seq'];
$is_active  = $_POST['is_active'];
$page_path  = $_POST['page_path'];
$module_id  = $_POST['module_id'];
$user_id_update = $_SESSION['user_id'];


if(!empty($page_id)){
  $sql = "UPDATE t_page SET
          page_code       = '$page_code',
          page_name       = '$page_name',
          page_seq        = '$page_seq',
          is_active       = '$is_active',
          page_path       = '$page_path',
          module_id       = '$module_id',
          user_id_update  = '$user_id_update'
          WHERE page_id   = $page_id";

  $query  = DbQuery($sql,null);
  $row    = json_decode($query, true);
  $status = $row['status'];

  if($status == "success"){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
   if($_SESSION['TYPE_CONN'] == '1'){
    $sql = "INSERT INTO t_page
           (page_id,page_code,page_name,page_seq,is_active,page_path,module_id,user_id_update,update_date)
           VALUES(T_PAGE_SEQ.NEXTVAL,'$page_code','$page_name','$page_seq','$is_active','$page_path','$module_id','$user_id_update',CURRENT_TIMESTAMP)";
   }else{
     $sql = "INSERT INTO t_page
            (page_code,page_name,page_seq,is_active,page_path,module_id,user_id_update)
            VALUES('$page_code','$page_name','$page_seq','$is_active','$page_path','$module_id','$user_id_update')";
   }

   $query      = DbQuery($sql,null);
   $json       = json_decode($query, true);
   $status     = $json['status'];

  if($status == "success"){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}
?>
