<?php
session_start();
include('../conf/connect.php');
include('../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = date("Y/m/t", strtotime($dateStart));

  $dateStart = queryDate($dateStart);
  $dateEnd   = queryDate($dateEnd);

   //echo ">>".$_SESSION['ROLEACCESS'];
   //echo ">>".$_SESSION['ROLECODE'];
  // echo ">>".$_SESSION['USER_NAME'];

  if(!strstr($_SESSION['ROLECODE'], 'ADM')){
      $con = " and user_login = '".$_SESSION['USER_NAME']."'";
  }

  if($_SESSION['TYPE_CONN'] == '2'){
      $sql = "SELECT * FROM pfit_t_project where status <> 'C' and start_date between $dateStart and  $dateEnd $con ORDER BY start_date";
  }else{
      $sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date
      FROM pfit_t_project
      where status <> 'C' and start_date between $dateStart and  $dateEnd $con ORDER BY start_date";
  }



  //echo $sql;
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  for ($i=0; $i < $num ; $i++) {
    $project_code = $data[$i]['project_code'];
    $project_name = $data[$i]['project_name'];
?>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-gray btnList" style="padding:10px;" onclick="postURL('PFIT0101.php?code=<?= $project_code ?>')">
        <div class="info-box-number" style="height:80px;"><?= $project_name ?></div>
        <div style="font-size:14px;height:30px;">
          <?= DateThai($data[$i]['start_date']) ?> - <?= DateThai($data[$i]['end_date']) ?>
        </div>
    </div>
  </div>
<?php
}
if($num == 0){
?>
  <div class="col-xs-12">
    <div class="box box-solid" style="padding:15px;height:50px;">
      <p align="center">ไม่พบกิจกรรม</p>
    </div>
  </div>
<?php
}
?>
