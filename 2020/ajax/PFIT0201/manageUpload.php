<?php
require_once '../../Classes/PHPExcel.php';

include '../../Classes/PHPExcel/IOFactory.php';
include('../../conf/connect.php');
include('../../conf/utils.php');
session_start();


$remark         = $_POST['remark'];

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);

    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $headerName = $headingsArray['A'];

    $headingsArray = array("A" => "age",
                           "B" => "gender",
                           "C" => "test_code",
                           "D" => "min",
                           "E" => "max",
                           "F" => "category_criteria_detail_code",
                           "G" => "type_test",
                           "H" => "test_suggest",
                           "I" => "test_suggest_en");

    $r = -1;

    $namedDataArray = array();
    for ($row = 1; $row <= $highestRow; ++$row) {
        $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
        if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
            ++$r;
            foreach($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }
    }
    //
    $i = 0;
    foreach ($namedDataArray as $result) {
        $age                = trim($result["age"]);
        $gender             = trim($result["gender"]);
        $test_code          = trim($result["test_code"]);
        $min                = trim($result["min"]);
        $max                = trim($result["max"]);
        $category_criteria_detail_code  = trim($result["category_criteria_detail_code"]);
        $type_test          = trim($result["type_test"]);
        $test_suggest       = trim($result["test_suggest"]);
        $test_suggest_en    = stripslashes(trim($result["test_suggest_en"]));



        $sql = "SELECT * FROM pfit_t_test_criteria
        WHERE test_code = '$test_code' and category_criteria_detail_code = '$category_criteria_detail_code' and age = '$age' and gender = '$gender'";
        $json = DbQuery($sql,null);
        $obj = json_decode($json, true);
        $dataCount = $obj['dataCount'];
        $strSQL = "";
        if($dataCount > 0){
            $test_criteria_code = $obj['data'][0]['test_criteria_code'];
            $sql = "UPDATE pfit_t_test_criteria SET
                    age           = '$age',
                    gender        = '$gender',
                    min           = '$min',
                    max           = '$max',
                    category_criteria_detail_code = '$category_criteria_detail_code',
                    type_test     = '$type_test',
                    test_suggest  = '$test_suggest',
                    test_suggest_en  = '$test_suggest_en',
                    status        = 'Y'
                    WHERE test_criteria_code   = '$test_criteria_code'";
        }else{
            $test_criteria_code = "";
            $sql    = "SELECT test_criteria_code FROM pfit_t_test_criteria order by test_criteria_code desc";
            $query  = DbQuery($sql,null);

            $json = json_decode($query, true);
            $num  = $json['dataCount'];
            $row  = $json['data'];

            if($num > 0){
              $test_criteria_code = $row[0]['test_criteria_code'];
            }

            if(!empty($test_criteria_code)){
              $lastNum = substr($test_criteria_code,strlen($test_code));
              $lastNum = $lastNum + 1;
              $test_criteria_code = $test_code.sprintf("%03d", $lastNum);
            }else{
              $test_criteria_code = $test_code.sprintf("%03d", 1);
            }

            $sql = "INSERT INTO pfit_t_test_criteria
                   (test_criteria_code,test_code,age,gender,min,max,category_criteria_detail_code,type_test,test_suggest,status,test_suggest_en)
                   VALUES('$test_criteria_code','$test_code','$age','$gender','$min','$max','$category_criteria_detail_code','$type_test','$test_suggest','Y','$test_suggest_en')";
      }
      DbQuery($sql,null);

    }
}


?>
