<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code        = $_GET['code'];
$projectCode = $_GET['projectCode'];
$projectType = $_GET['projectType'];



$statusType = $_GET['status'];
$disabled = '';
$readonly = '';
$visibility = '';

$project_code   = "";
$person_number  = "";
$person_gender  = "";
$person_name    = "";
$person_lname   = "";
$date_of_birth  = date('Y/m/d');
$wiegth         = "";
$height         = "";
$person_type    = "";
$status         = "";
$education      = "";
$stage          = "";
$grade          = "";
$room           = "";
$points_1       = "";
$points_2       = "";
$points_3       = "";

//
if(!empty($code)){
  $type = "EDIT";

  $sql = "SELECT project_code,person_number,person_gender,person_name,person_lname,points_1,points_2,points_3,
           ".getQueryDate('date_of_birth').",wiegth,height,person_type,education,stage,grade,room
          FROM pfit_t_person where project_code = '$projectCode' and person_number = '$code'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];


  if($num > 0){
    $person_number  = $row[0]['person_number'];
    $person_gender  = $row[0]['person_gender'];
    $person_name    = $row[0]['person_name'];
    $person_lname   = $row[0]['person_lname'];
    $date_of_birth  = $row[0]['date_of_birth'];
    $wiegth         = $row[0]['wiegth'];
    $height         = $row[0]['height'];
    $person_type    = $row[0]['person_type'];
    $education      = $row[0]['education'];
    $stage          = $row[0]['stage'];
    $grade          = $row[0]['grade'];
    $room           = $row[0]['room'];
    $points_1       = $row[0]['points_1'];
    $points_2       = $row[0]['points_2'];
    $points_3       = $row[0]['points_3'];

    //echo "points_1 :".$points_1.","."points_2 :".$points_2.","."points_3 :".$points_3;

  }

  $readonly = 'readonly';
  if($statusType == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

  }
}

if($person_type == "" && $projectType != "4"){
  $person_type = "1";
}else if($person_type == ""){
  $person_type = "2";
}

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$ip = $_SESSION['USER_NAME'];

?>

<div class="row">
  <div class="col-md-12">
      <label>USER : <?= $ip ?></label>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-success" onclick="readSmartCard()" style="width:160px;">อ่านบัตรสมาร์ทการ์ด</button>
      </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>เลขประจำตัว/เบอร์โทรศัพท์</label>
      <input type="hidden" value="<?= $type ?>" name="type" required>
      <input type="hidden" value="<?= $ip ?>"  id="ipClient" name="type" required>

      <input type="hidden" value="<?= $projectCode ?>" name="project_code" required>
      <input type="text" <?=$readonly?> id="person_number" name="person_number" value="<?= $person_number ?>" class="form-control" placeholder="เลขประจำตัว" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ชื่อ</label>
      <input type="text" name="person_name" id="person_name" value="<?= $person_name; ?>" class="form-control" placeholder="ชื่อ" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>สกุล</label>
      <input type="text" name="person_lname" id="person_lname" value="<?= $person_lname; ?>" class="form-control" placeholder="สกุล" <?=$disabled ?> required >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เพศ</label>
      <div class="form-control" style="border-style: hidden;">
        <input <?= $disabled?> type="radio" name="person_gender" id="gender_m" class="minimal" value="M" <?= $person_gender == 'M'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
        <input <?= $disabled?> type="radio" name="person_gender" id="gender_f" class="minimal" value="F" <?= $person_gender == 'F'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
      </div>
    </div>
  </div>
  <div class="col-md-4" style="display:none;">
    <div class="form-group">
      <label>ประเภทผู้ทดสอบ</label>
      <select name="person_type" id="person_type" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required onchange="setDataStudent()">
        <option value="" selected></option>
        <option value="2" <?= ($person_type == '2' ? 'selected="selected"':'') ?>>ประชาชนทั่วไป</option>
        <option value="1" <?= ($person_type == '1' ? 'selected="selected"':'') ?>>นักเรียน/นักศึกษา</option>
        <option value="3" <?= ($person_type == '3' ? 'selected="selected"':'') ?>>นักกีฬา</option>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>วันเดือนปี เกิด</label>
      <!--input id="inputdatepicker" name="date_of_birth" value="<?= $date_of_birth; ?>" class="form-control datepicker" data-date-format="mm/dd/yyyy" <?=$disabled ?> required>
      <input  type="date" id="date_of_birth" name="date_of_birth" value="<?= $date_of_birth; ?>" class="form-control datepicker" data-date-format="mm/dd/yyyy" <?=$disabled ?> required-->
      <input class="form-control datepicker" value="<?= DateThai($date_of_birth) ?>" id="date_of_birth" name="date_of_birth" type="text" data-provide="datepicker" data-date-language="th-th" required>

    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>น้ำหนัก</label>
      <input type="text" name="wiegth" value="<?= $wiegth; ?>"  class="form-control" data-smk-type="decimal" placeholder="น้ำหนัก"  <?=$disabled ?> >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ส่วนสูง</label>
      <input type="text" name="height" value="<?= $height; ?>" class="form-control" data-smk-type="decimal" placeholder="ส่วนสูง"  <?=$disabled ?> >
    </div>
  </div>
  <div id="typeStudent" style="<?= $person_type == '1' ? '':'display:none;'?>">
    <div class="col-md-5">
      <div class="form-group">
        <label>ชื่อสถานศึกษา</label>
        <input type="text" name="education" value="<?= $education ?>" class="form-control" placeholder="ชื่อสถานศึกษา"  <?=$disabled ?> >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ระดับชั้น</label>
        <select name="stage" class="form-control select2" style="width: 100%;" <?=$disabled; ?>>
          <option value=""></option>
          <option value="2" <?= ($stage == '2' ? 'selected="selected"':'') ?>>ประถมศึกษา</option>
          <option value="1" <?= ($stage == '1' ? 'selected="selected"':'') ?>>มัธยมศึกษา</option>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ชั้นปี</label>
        <input type="text" name="grade" value="<?=$grade ?>" class="form-control" placeholder=""  <?=$disabled ?> >
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ห้องเรียน</label>
        <input type="text" name="room" value="<?=$room ?>" class="form-control" placeholder=""  <?=$disabled ?> >
      </div>
    </div>
  </div>
  <?php //echo $person_type != '1' ? '':'display:none;'

    if($person_type != '1'){

      $q1 = "ข้อที่ 1 ท่านออกกำลังกายกี่วันต่อสัปดาห์";
      $q2 = "ข้อที่ 2 ท่านออกกำลังกายแบบไหน";
      $q3 = "ข้อที่ 3 ท่านออกกำลังกายแต่ละครั้งนานเท่าใด";

      $a[0] = "ไม่เคยเลย";
      $a[1] = "1-2 วันต่อสัปดาห์";
      $a[2] = "3-4 วันต่อสัปดาห์";
      $a[3] = "5-7 วันต่อสัปดาห์";

      $b[0] = "เดิน, วิ่ง, เต้นแอโรบิค, ปั่นจักรยาน";
      $b[1] = "โยคะ";
      $b[2] = "การออกกำลังโดยใช้แรงต้าน (ยางยืด, บาร์เบล, ดัมบ์เบลล์)";
      $b[3] = "อื่นๆ";

      $c[0] = "น้อยกว่า 10 นาที";
      $c[1] = "11-20 นาที";
      $c[2] = "21-30 นาที";
      $c[3] = "31 นาทีขึ้นไป";

    }else{
      $q1 = "ข้อที่ 1 มีกิจกรรมทางกายและการออกกำลังกาย กี่วัน/สัปดาห์ (นอกจากชั่วโมงพลศึกษา)";
      $q2 = "ข้อที่ 2 รูปแบบกิจกรรมทางกาย (นอกจากชั่วโมงพลศึกษา)";
      $q3 = "ข้อที่ 3 ระยะเวลาในการทำกิจกรรมต่อวัน";

      $a[0] = "ไม่เคยเลย";
      $a[1] = "1-2 วันต่อสัปดาห์";
      $a[2] = "3-4 วันต่อสัปดาห์";
      $a[3] = "5-7 วันต่อสัปดาห์";

      $b[0] = "กีฬาชนิดต่างๆ เช่น ฟุตบอล, เทควันโด, ปั่นจักรยาน เป็นต้น";
      $b[1] = "กิจกรรมนันทนาการ เช่น เต้นรำ, เต้นแอโรบิก, ลีดเดอร์";
      $b[2] = "การละเล่นพื้นบ้าน เช่น หมากเก็บ, กระโดดยาง, ดี่จับ";
      $b[3] = "งานบ้าน เช่น ล้างจาน, ถูบ้าน, รดน้ำต้นไม้";

      $c[0] = "น้อยกว่า 15 นาที";
      $c[1] = "15-30 นาที";
      $c[2] = "31-60 นาที";
      $c[3] = "61 นาทีขึ้นไป";
    }

  ?>
<div style="">
  <div class="col-md-12">
    <div class="form-group">
      <label><?= $q1 ?></label>
      <div class="row">
        <?php
        for ($i=0; $i < count($a); $i++) { ?>
        <div class="col-md-6">
            <input type="radio" id="points_1_<?= $i ?>" name="points_1" value="<?= $i ?>" <?= ($points_1 == $i && $points_1 != "" ? 'checked':'') ?>>
            <label for="points_1_<?= $i ?>" style="font-weight:400;"><?= $a[$i]; ?></label>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label><?= $q2 ?></label>
      <div class="row">
        <?php

        for ($i=0; $i < count($b); $i++) { ?>
        <div class="col-md-6">
            <input type="radio" id="points_2_<?= $i ?>" name="points_2" value="<?= $i ?>" <?= ($points_2 == $i && $points_2 != "" ? 'checked':'') ?>>
            <label for="points_2_<?= $i ?>" style="font-weight:400;"><?= $b[$i]; ?></label>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label><?= $q3 ?></label>
      <div class="row">
        <?php
        for ($i=0; $i < count($c); $i++) { ?>
        <div class="col-md-6">
            <input type="radio" id="points_3_<?= $i ?>" name="points_3" value="<?= $i ?>" <?= ($points_3 == $i && $points_3  != "" ? 'checked':'') ?>>
            <label for="points_3_<?= $i ?>" style="font-weight:400;"><?= $c[$i]; ?></label>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$(function () {
  $('.datepicker').datepicker();
  $(".datepicker").change(function(){
    $(this).datepicker('hide');
  });
})
</script>
