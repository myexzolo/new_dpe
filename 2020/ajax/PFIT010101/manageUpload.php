<?php
require_once '../../Classes/PHPExcel.php';

include '../../Classes/PHPExcel/IOFactory.php';
include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();


$person_type    = $_POST['person_type'];
$remark         = $_POST['remark'];
$project_code   = $_POST['projectCode'];
$project_name   = isset($_POST['projectName'])?$_POST['projectName']:"";
//$end_date       = isset($_POST['end_date'])?$_POST['end_date']:date("Y-m-d");


$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try {

     $sql   = "SELECT TO_CHAR( end_date, 'YYYY-MM-DD') as end_date FROM pfit_t_project WHERE project_code = '$project_code'";
     $query = DbQuery($sql,null);
     $json  = json_decode($query, true);
     $row   = $json['data'];

     $end_date = $row[0]['end_date'];


     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
     $objReader->setReadDataOnly(true);
     $objPHPExcel = $objReader->load($inputFileName);

     $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
     $highestRow = $objWorksheet->getHighestRow();
     $highestColumn = $objWorksheet->getHighestColumn();

     $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
     $headingsArray = $headingsArray[1];

     $headerName = $headingsArray['A'];

     if(trim($headerName) == "ระบุชื่อสถานศึกษา"){
       $headerName = $project_name;
     }

     // $headingsArray = array("A" => "NO",
     //                        "B" => "person_number",
     //                        "C" => "person_gender",
     //                        "D" => "person_name",
     //                        "E" => "person_lname",
     //                        "F" => "date_of_birth",
     //                        "G" => "wiegth",
     //                        "H" => "height",
     //                        "I" => "stage",
     //                        "J" => "grade",
     //                        "K" => "room",
     //                        "L" => "points_1");


     $headingsArray = array("A" => "NO",
                            "B" => "person_number",
                            "C" => "person_gender",
                            "D" => "person_name",
                            "E" => "person_lname",
                            "F" => "date_of_birth",
                            "G" => "wiegth",
                            "H" => "height",
                            "I" => "stage",
                            "J" => "grade",
                            "K" => "room",
                            "L" => "points_1"
                            "M" => "TST16",
                            "N" => "TST14",
                            "O" => "TST02",
                            "P" => "TST17"
                          );



     $r = -1;

     $namedDataArray = array();
     for ($row = 3; $row <= $highestRow; ++$row) {
         $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
         if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
             ++$r;
             foreach($headingsArray as $columnKey => $columnHeading) {
                 $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
             }
         }
     }
     $i = 0;
       foreach ($namedDataArray as $result) {
          try
          {
            $i++;
            $total++;
            $person_number  = trim($result["person_number"]);
            $person_gender  = trim($result["person_gender"]);
            $person_name    = $result["person_name"];
            $person_lname   = $result["person_lname"];
            $date_of_birth  = queryDate(formatDateThtoEn($result["date_of_birth"]));
            $wiegth         = trim($result["wiegth"]);
            $height         = trim($result["height"]);
            $stage          = trim($result["stage"]);
            $grade          = trim($result["grade"]);
            $room           = trim($result["room"]);
            $points_1       = trim($result["points_1"]);

            $TST16          = trim($result["TST16"]);
            $TST14          = trim($result["TST14"]);
            $TST02          = trim($result["TST02"]);
            $TST17          = trim($result["TST17"]);


            switch ($person_gender) {
                case "ชาย":
                    $person_gender = "M";
                    break;
                case "ด.ช.":
                    $person_gender = "M";
                    break;
                case "เด็กชาย":
                    $person_gender = "M";
                    break;
                case "นาย":
                    $person_gender = "M";
                    break;
                case "หญิง":
                    $person_gender = "F";
                    break;
                case "ด.ญ.":
                    $person_gender = "F";
                    break;
                case "เด็กหญิง":
                    $person_gender = "F";
                    break;
                case "นาง":
                    $person_gender = "F";
                    break;
                case "น.ส.":
                    $person_gender = "F";
                    break;
                case "นางสาว":
                    $person_gender = "F";
                    break;
                case "F":
                    $person_gender = "F";
                    break;
                case "M":
                    $person_gender = "M";
                    break;
                case "f":
                    $person_gender = "F";
                    break;
                case "m":
                    $person_gender = "M";
                    break;
                default:
                    $person_gender = "";
            }

            if(strpos($stage,"ประถม") !== "false"){
              $stage = '2';
            }else if(strpos($stage,"มัธยม") !== "false"){
              $stage = '1';
            }else{
              $stage = '0';
            }

            $sql = "SELECT * FROM pfit_t_person WHERE person_number = '$person_number' and project_code = '$project_code'";
            $json = DbQuery($sql,null);
            //print_r (">>>>>".$json."\n");
            $obj = json_decode($json, true);
            $dataCount = $obj['dataCount'];
            $strSQL = "";
            if($dataCount > 0){
              $strSQL = "UPDATE pfit_t_person SET
                          person_gender = '$person_gender',
                          person_name   = '$person_name',
                          person_lname  = '$person_lname',
                          date_of_birth = $date_of_birth,
                          wiegth        = '$wiegth',
                          height        = '$height',
                          stage         = '$stage',
                          grade         = '$grade',
                          education     = '$headerName',
                          person_type   = '$person_type',
                          room          = '$room',
                          points_1      = '$points_1'
                          WHERE project_code = '$project_code' and person_number = '$person_number'";
            }else{
              $strSQL = "INSERT INTO pfit_t_person
                        (project_code,person_number,person_gender,person_name,person_lname,date_of_birth,wiegth,height,person_type,stage,grade,education,status_test,room,points_1)
                        VALUES
                        ('$project_code','$person_number','$person_gender','$person_name','$person_lname',$date_of_birth,'$wiegth','$height','$person_type','$stage','$grade','$headerName',0,'$room','$points_1')";
            }
            //echo $strSQL."<br>";
            $js      = DbQuery($strSQL,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];

            $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
            if($errorInfo > 0){
              $fail++;
              continue;
            }else{
              $success++;
            }


            // $date_2 = date("Y-m-d");
            // $yearBirth = dateDifference($date_of_birth , $date_2 , '%y');
            $yearBirth = yearBirth2($dateOfBirth,$end_date);

            if($wiegth !="" && $height != "")
            {
              $value  = 'BMI';
              $result = $wiegth."|".$height;
              $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              $row    = $json['data'];

              $res = bmi($wiegth,$height);


              $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                            AND age = '$yearBirth' AND gender = '$person_gender'
                            AND min <= '$res' ORDER BY min DESC";
              $querys     = DbQuery($sqls,null);
              $json  = json_decode($querys, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              //echo ">>".$sqls."<br>";
              $test_criteria_code = '';
              if($num > 0)
              {
                $test_criteria_code = $row[0]['test_criteria_code'];
              }

              $datenow = queryDate(date("Y/m/d"));
              $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                       and person_number = '$person_number' and test_code = '$value' ";
              $queryr = DbQuery($sqlr,null);
              $jsonr  = json_decode($queryr, true);
              $numr   = $jsonr['dataCount'];
              //echo $sqlr."  ".$numr."<br>";
              if($numr > 0){
                $sqlr = "UPDATE pfit_t_result SET
                        result = '$result',
                        test_code = '$value',
                        test_criteria_code = '$test_criteria_code',
                        result_cal = '$res'
                        WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
              }else{
                $sqlr = "INSERT INTO pfit_t_result
                        (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                        VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,$res)";
              }

              $js      = DbQuery($sqlr,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = $row['errorInfo'][0];
              if($errorInfo > 0){
                $fail++;
                continue;
              }
            }

            //----ลุก-นั่ง 60 วินาที TST16
            if($TST16 != "")
            {
              $value  = 'TST16';
              $result = $TST16;
              $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              $row    = $json['data'];

              //echo $sql."<br>";
              $test_calculator = $row[0]['test_calculator'];

              if($test_calculator == 1){
                $res = $result;
              }else if($test_calculator == 2){
                $res = ValuePerWiegth($result,$wiegth);
              }else if($test_calculator == 3){
                $res = ValuePerHeight($result,$height);
              }else if($test_calculator == 4){
                $res = bmi($wiegth,$height);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 6){
                $res = BloodPressureValue($result);
                //echo "   >>xxxx".$res;
              }

              $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                            AND age = '$yearBirth' AND gender = '$person_gender'
                            AND min <= '$res' ORDER BY min DESC";
              $querys     = DbQuery($sqls,null);
              $json  = json_decode($querys, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              //echo ">>".$sqls."<br>";
              $test_criteria_code = '';
              if($num > 0)
              {
                $test_criteria_code = $row[0]['test_criteria_code'];
              }

              $datenow = queryDate(date("Y/m/d"));
              $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                       and person_number = '$person_number' and test_code = '$value' ";
              $queryr = DbQuery($sqlr,null);
              $jsonr  = json_decode($queryr, true);
              $numr   = $jsonr['dataCount'];
              //echo $sqlr."  ".$numr."<br>";
              if($numr > 0){
                        //$result_id = $_POST['result_id'];

                $sqlr = "UPDATE pfit_t_result SET
                        result = '$result',
                        test_code = '$value',
                        test_criteria_code = '$test_criteria_code',
                        result_cal = '$res'
                        WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
              }else{
                $sqlr = "INSERT INTO pfit_t_result
                        (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                        VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,$res)";
              }

              $js      = DbQuery($sqlr,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = $row['errorInfo'][0];
              if($errorInfo > 0){
                $fail++;
                continue;
              }
            }


            //----ดันพื้นประยุกต์ 30 วินาที TST14
            if($TST14 != "")
            {
              $value  = 'TST14';
              $result = $TST14;
              $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              $row    = $json['data'];

              //echo $sql."<br>";
              $test_calculator = $row[0]['test_calculator'];

              if($test_calculator == 1){
                $res = $result;
              }else if($test_calculator == 2){
                $res = ValuePerWiegth($result,$wiegth);
              }else if($test_calculator == 3){
                $res = ValuePerHeight($result,$height);
              }else if($test_calculator == 4){
                $res = bmi($wiegth,$height);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 6){
                $res = BloodPressureValue($result);
                //echo "   >>xxxx".$res;
              }

              $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                            AND age = '$yearBirth' AND gender = '$person_gender'
                            AND min <= '$res' ORDER BY min DESC";
              $querys     = DbQuery($sqls,null);
              $json  = json_decode($querys, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              //echo ">>".$sqls."<br>";
              $test_criteria_code = '';
              if($num > 0)
              {
                $test_criteria_code = $row[0]['test_criteria_code'];
              }

              $datenow = queryDate(date("Y/m/d"));
              $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                       and person_number = '$person_number' and test_code = '$value' ";
              $queryr = DbQuery($sqlr,null);
              $jsonr  = json_decode($queryr, true);
              $numr   = $jsonr['dataCount'];
              //echo $sqlr."  ".$numr."<br>";
              if($numr > 0){
                        //$result_id = $_POST['result_id'];

                $sqlr = "UPDATE pfit_t_result SET
                        result = '$result',
                        test_code = '$value',
                        test_criteria_code = '$test_criteria_code',
                        result_cal = '$res'
                        WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
              }else{
                $sqlr = "INSERT INTO pfit_t_result
                        (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                        VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,$res)";
              }

              $js      = DbQuery($sqlr,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = $row['errorInfo'][0];
              if($errorInfo > 0){
                $fail++;
                continue;
              }
            }

            //----นั่งงอตัวไปข้างหน้า TST02
            if($TST02 != "")
            {
              $value  = 'TST02';
              $result = $TST02;
              $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              $row    = $json['data'];

              //echo $sql."<br>";
              $test_calculator = $row[0]['test_calculator'];

              if($test_calculator == 1){
                $res = $result;
              }else if($test_calculator == 2){
                $res = ValuePerWiegth($result,$wiegth);
              }else if($test_calculator == 3){
                $res = ValuePerHeight($result,$height);
              }else if($test_calculator == 4){
                $res = bmi($wiegth,$height);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 6){
                $res = BloodPressureValue($result);
                //echo "   >>xxxx".$res;
              }

              $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                            AND age = '$yearBirth' AND gender = '$person_gender'
                            AND min <= '$res' ORDER BY min DESC";
              $querys     = DbQuery($sqls,null);
              $json  = json_decode($querys, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              //echo ">>".$sqls."<br>";
              $test_criteria_code = '';
              if($num > 0)
              {
                $test_criteria_code = $row[0]['test_criteria_code'];
              }

              $datenow = queryDate(date("Y/m/d"));
              $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                       and person_number = '$person_number' and test_code = '$value' ";
              $queryr = DbQuery($sqlr,null);
              $jsonr  = json_decode($queryr, true);
              $numr   = $jsonr['dataCount'];
              //echo $sqlr."  ".$numr."<br>";
              if($numr > 0){
                        //$result_id = $_POST['result_id'];

                $sqlr = "UPDATE pfit_t_result SET
                        result = '$result',
                        test_code = '$value',
                        test_criteria_code = '$test_criteria_code',
                        result_cal = '$res'
                        WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
              }else{
                $sqlr = "INSERT INTO pfit_t_result
                        (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                        VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,$res)";
              }

              $js      = DbQuery($sqlr,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = $row['errorInfo'][0];
              if($errorInfo > 0){
                $fail++;
                continue;
              }
            }

            //----ยืนยกเข่าขึ้นลง 3 นาที TST17
            if($TST17 != "")
            {
              $value  = 'TST17';
              $result = $TST17;
              $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
              $query  = DbQuery($sql,null);
              $json   = json_decode($query, true);
              $row    = $json['data'];

              //echo $sql."<br>";
              $test_calculator = $row[0]['test_calculator'];

              if($test_calculator == 1){
                $res = $result;
              }else if($test_calculator == 2){
                $res = ValuePerWiegth($result,$wiegth);
              }else if($test_calculator == 3){
                $res = ValuePerHeight($result,$height);
              }else if($test_calculator == 4){
                $res = bmi($wiegth,$height);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 5){
                $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
              }else if($test_calculator == 6){
                $res = BloodPressureValue($result);
                //echo "   >>xxxx".$res;
              }

              $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                            AND age = '$yearBirth' AND gender = '$person_gender'
                            AND min <= '$res' ORDER BY min DESC";
              $querys     = DbQuery($sqls,null);
              $json  = json_decode($querys, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              //echo ">>".$sqls."<br>";
              $test_criteria_code = '';
              if($num > 0)
              {
                $test_criteria_code = $row[0]['test_criteria_code'];
              }

              $datenow = queryDate(date("Y/m/d"));
              $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                       and person_number = '$person_number' and test_code = '$value' ";
              $queryr = DbQuery($sqlr,null);
              $jsonr  = json_decode($queryr, true);
              $numr   = $jsonr['dataCount'];
              //echo $sqlr."  ".$numr."<br>";
              if($numr > 0){
                        //$result_id = $_POST['result_id'];

                $sqlr = "UPDATE pfit_t_result SET
                        result = '$result',
                        test_code = '$value',
                        test_criteria_code = '$test_criteria_code',
                        result_cal = '$res'
                        WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
              }else{
                $sqlr = "INSERT INTO pfit_t_result
                        (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                        VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,$res)";
              }

              $js      = DbQuery($sqlr,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = $row['errorInfo'][0];
              if($errorInfo > 0){
                $fail++;
                continue;
              }
            }

          }catch (Exception $ex) {
            $fail++;
          }
        }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }

   if($total == $success){
     $status_upload = "S";
   }else{
     $status_upload = "F";
   }

   if($_SESSION['TYPE_CONN'] == '1'){
      $str = "INSERT INTO pfit_t_history_upload
              (HISTORY_UPLOAD_ID,file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code)
              VALUES
              (T_HISTORY_UPLOAD.NEXTVAL,'$inputFileName','$status_upload','$total','$success','$fail','','pfit_t_person','$project_code')";
   }else{
     $str = "INSERT INTO pfit_t_history_upload
             (file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code)
             VALUES
             ('$inputFileName','$status_upload','$total','$success','$fail','','pfit_t_person','$project_code')";
   }

   $query    = DbQuery($str,null);
   $json     = json_decode($query, true);
   $status   = $json['status'];

   if($status == "success"){
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกสมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'บันทึกไม่สมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
}


?>
