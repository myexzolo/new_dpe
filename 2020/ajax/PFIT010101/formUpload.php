<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code        = $_GET['code'];
$projectCode = $_GET['projectCode'];
$projectType = $_GET['projectType'];
$projectName = $_GET['projectName'];

if($projectType != "4"){
  $person_type = "1";
}else{
  $person_type = "2";
}


$statusType = $_GET['status'];
$disabled = '';
$readonly = '';
$visibility = '';

?>

<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required>
                      <input type="hidden" name="projectCode" value="<?= $projectCode ?>" required>
                      <input type="hidden" name="projectName" value="<?= $projectName ?>" required>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">ประเภทผู้ทดสอบ</label>
                    <div class="col-sm-8">
                      <select name="person_type" id="person_type" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required onchange="setDataStudent()">
                        <option value="" selected></option>
                        <option value="1" <?= ($person_type == '1' ? 'selected="selected"':'') ?>>นักเรียน (7 - 18 ปี)</option>
                        <option value="2_0" <?= ($person_type == '2' ? 'selected="selected"':'') ?>>ประชาชนทั่วไป (7 - 18 ปี)</option>
                        <option value="2_1">ประชาชนทั่วไป (19 - 59 ปี)</option>
                        <option value="2_2">ประชาชนทั่วไป (60 ปีขึ้นไป)</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">เพิ่มเติม</label>
                    <div class="col-sm-8">
                      <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="tableUpload" class="table table-bordered table-striped"
                   data-toggle="table"
                   data-toolbar="#toolbar"
                   data-url="ajax/PFIT010101/showHisUpload.php?code=<?= $projectCode?>"
                   data-pagination="true"
                   data-page-list="[10, 25, 50, 100, ALL]"
                   data-search="true"
                   data-flat="true"
                   data-show-refresh="true"
                   >
                <thead>
                    <tr>
                        <th data-sortable="true" data-formatter="runNo" data-align="center">ลำดับ</th>
                        <th data-sortable="true" data-field="file_name" data-align="center" class="align-left">ชื่อไฟล์</th>
                        <th data-sortable="true" data-formatter="getdateUploadTH"  data-align="center">วันที่นำเข้า</th>
                        <th data-field="operate" data-formatter="getStatusUpload" data-align="center">สถานะ</th>
                        <th data-sortable="true" data-field="total_record" data-align="center"> ทั้งหมด</th>
                        <th data-sortable="true" data-field="success_record" data-align="center">สำเร็จ</th>
                        <th data-sortable="true" data-field="fail_record" data-align="center">ล้มเหลว</th>
                        <th data-sortable="true" data-field="path_file" data-align="center">เลขที่ล้มเหลว</th>
                    </tr>
                </thead>
            </table>
          </div>
    </div>
  </div>
</div>
