<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$module_id = "";
$module_code = "";
$module_name = "";
$module_order = "";
$is_active = "";
$module_image = "";
$module_type = "";
$module_icon = "";

if(!empty($_POST['value'])){
  $sql = "SELECT * FROM t_module WHERE module_id = ?";

  $ParamData = array($_POST['value']);
  $query = DbQuery($sql,$ParamData);

  $row  = json_decode($query, true);
  $data = $row['data'];

  $module_id      = $data[0]['module_id'];
  $module_code    = $data[0]['module_code'];
  $module_name    = $data[0]['module_name'];
  $module_order   = $data[0]['module_order'];
  $is_active      = $data[0]['is_active'];
  $module_image   = $data[0]['module_image'];
  $module_type    = $data[0]['module_type'];
  $module_icon    = $data[0]['module_icon'];
}
?>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label>Module Code</label>
      <input value="<?= $module_code ?>" name="module_code" type="text" maxlength="6" class="form-control" placeholder="Code" required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>Module Name</label>
      <input value="<?= $module_name ?>" name="module_name" type="text" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Module Sequence</label>
      <input value="<?= $module_order ?>" name="module_order" type="number" maxlength="3" class="form-control" placeholder="Sequence" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Type</label>
      <select name="module_type" class="form-control select2" style="width: 100%;" required>
        <option value="1" <?= ($module_type == '1' ? 'selected="selected"':'') ?> >Black Office</option>
        <option value="2" <?= ($module_type == '2' ? 'selected="selected"':'') ?> >Front Office</option>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" required>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>Module Icon</label>
      <input value="<?= $module_icon ?>" name="module_icon" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>Module Image</label>
      <input value="" name="module_image" type="file" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>
        <img src="<?= $module_image ?>" onerror="this.onerror='';this.src='images/list.png'" style="height: 60px;">
      </label>
    </div>
  </div>
</div>
<input type="hidden" name="module_id" value="<?= $module_id ?>">
