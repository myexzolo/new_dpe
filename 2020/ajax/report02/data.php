<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = date("Y/m/t", strtotime($dateStart));

  $dateStart = queryDate($dateStart);
  $dateEnd   = queryDate($dateEnd);

  if(!strstr($_SESSION['ROLECODE'], 'ADM')){
      $con = " and user_login = '".$_SESSION['USER_NAME']."'";
  }

  if($_SESSION['TYPE_CONN'] == '2'){
      $sql = "SELECT * FROM pfit_t_project where status <> 'C' and project_type in (1,2) and start_date between $dateStart and  $dateEnd $con ORDER BY start_date";
  }else{
      $sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date
      FROM pfit_t_project
      where status <> 'C' and project_type in (1,2) and start_date between $dateStart and  $dateEnd $con ORDER BY start_date";
  }



  //echo $sql;
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  for ($i=0; $i < $num ; $i++) {
    $date = date('Y-m',strtotime($data[$i]['end_date']));
    $project_code = $data[$i]['project_code'];
    $pram = "?date=$date&project_code=$project_code";
    $url = 'print_report02.php'.$pram;
?>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box btnList" style="padding:10px;" onclick="postURL_blank('<?=$url; ?>')" >
        <span class="info-box-number"><?= $data[$i]['project_name'] ?></span>
        <span style="font-size:12px">
          <?= formatDateTh($data[$i]['start_date']) ?> - <?= formatDateTh($data[$i]['end_date']) ?>
        </span>
    </div>
  </div>
<?php
}
?>
