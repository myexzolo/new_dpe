<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>User Login</td>
      <td>Name</td>
      <td>สถานะ</td>
      <td style="width:60px">Export</td>
      <td style="width:60px">Edit</td>
      <td style="width:60px">Del</td>
      <td style="width:100px">Re Password</td>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM t_user where is_active <> 'C' ORDER BY user_id  DESC";

  $query = DbQuery($sql,null);
  $row = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  for ($i=0; $i < $num ; $i++) {
?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td><?= $data[$i]['user_login']; ?></td>
      <td><?= $data[$i]['user_name']; ?></td>
      <td><?= ($data[$i]['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td>
        <button type="button" class="btn btn-primary btn-sm" onclick="exportUser(<?= $data[$i]['user_id'] ?>)">Export</button>
      </td>
      <td>
        <button type="button" class="btn btn-warning btn-sm" onclick="showFormEdit(<?= $data[$i]['user_id'] ?>,'edit')">Edit</button>
      <td>
        <button type="button" class="btn btn-danger btn-sm" onclick="removeUser(<?= $data[$i]['user_id'] ?>)">Del</button>
      </td>
      <td>
        <button type="button" class="btn btn-sm" onclick="showFormEdit(<?= $data[$i]['user_id'] ?>,'resetPw')">Reset</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
