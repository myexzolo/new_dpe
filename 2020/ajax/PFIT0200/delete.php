<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$code = $_POST['code'];
$ParamData = array($code);

$sql    = "SELECT * FROM pfit_t_test where category_criteria_code = ?";
$query  = DbQuery($sql,$ParamData);
$row    = json_decode($query, true);
$num    = $row['dataCount'];

if($num == 0){
  $sql  = "DELETE FROM pfit_t_category_criteria WHERE category_criteria_code = ?";
  $sql2 = "DELETE FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = ?";


  //echo DbQuery($sql,$ParamData);
  //echo DbQuery($sql2,$ParamData);
  $query  = DbQuery($sql,$ParamData);
  $row    = json_decode($query, true);
  $status = $row['status'];

  $query2  = DbQuery($sql2,$ParamData);
  $row2    = json_decode($query2, true);
  $status2 = $row2['status'];

  if($status == "success" && $status2 == "success")
  {
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail : ไม่สามารถลบได้ เนื่องจากแปลผลการทดสอบมีการใช้งานในระบบ')));
}



?>
