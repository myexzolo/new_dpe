<?php
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }
  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 195mm;
        height: auto;
        margin-left: 10px;
      }
  }
</style>
<html>
<body>
  <table style="width: 100%;" border="0" >
    <tr>
      <td colspan="5" style="font-size:20pt;font-weight:500;padding-top:20px;" align="center"><b>ผลการทดสอบสมรรถภาพทางกาย</b></td>
    </tr>
    <tr>
      <td align="left" style="width:250px"><img src="images/dep_logo.png" style="height:70px;width:75px;"></td>
      <td colspan="3"></td>
      <td align="right" style="width:250px;font-size:14pt">สถานที่ : สำนักวิทยาศาสตร์การกีฬา<br> วันที่ : 22/08/2561</td>
    </tr>
    <tr style="padding-top:20px;">
      <td colspan="5" style="padding-top:20px;" class="info">
        <b>ชื่อ :</b> สุทธิพันธ์  ป้องสุวรรณ &emsp;
        <b>เพศ :</b> ชาย &emsp;
        <b>อายุ :</b> 34 ปี &emsp;
      </td>
    </tr>
    <tr>
      <td colspan="5" style="width:250px"class="info">
        <b>น้ำหนัก :</b> .............. กิโลกรัม &emsp;
        <b>ส่วนสูง :</b> ................. เซนติเมตร &emsp;
        <b>ดัชนีมวลกาย :</b> ................. &emsp;
        <b>อยู่ในเกณฑ์ :</b> ..................
      </td>
    </tr>
    <tr>
      <td colspan="5" style="width:250px"class="info">
        <b>ความดันโลหิต :</b> .................... &nbsp;
        <b>อยู่ในเกณฑ์ :</b> .......................... &emsp;
        <b>ชีพจร :</b> ................ ครั้ง/นาที &nbsp;
        <b>อยู่ในเกณฑ์ :</b> ..........................
      </td>
    </tr>

  </table>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
    <tbody>
  		<tr>
  			<td class="thStyle">รายการทดสอบ</td>
  			<td class="thStyle">วิธีการทดสอบ</td>
  			<td class="thStyle">คะแนน</td>
  			<td class="thStyle">เกณฑ์มาตรฐาน</td>
  			<td class="thStyle">ผลการทดสอบ</td>
  			<td class="thStyle">คำแนะนำ</td>
  		</tr>
  		<tr>
  			<td class="content">ความแข็งแรงของกล้ามเนื้อแขน</td>
  			<td class="content" align="center">แรงบีบมือ<br>(ก.ก./น.น.ตัว)</td>
  			<td class="content"></td>
  			<td class="content"></td>
  			<td class="content" align="center">ดีมาก</td>
  			<td class="content" style="text-align: justify;">
  			ท่านสามารถออกกำลังกายในรูปแบบเดิมเป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
  			</td>
  		</tr>
      <tr>
  			<td class="content">ความอ่อนตัว</td>
  			<td class="content" align="center">ความอ่อนตัว<br>(เซนติเมตร)</td>
  			<td class="content"></td>
  			<td class="content"></td>
  			<td class="content" align="center">ดีมาก</td>
  			<td class="content" style="text-align: justify;">
  			ท่านสามารถออกกำลังกาย และยึดเหยียดกล้ามเนื้ออยู่เป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
  			</td>
  		</tr>
      <tr>
  			<td class="content">ความจุของปอด</td>
  			<td class="content" align="center">ความจุปอด<br>(ม.ล./น.น.ตัว)</td>
  			<td class="content"></td>
  			<td class="content"></td>
  			<td class="content" align="center">ดีมาก</td>
  			<td class="content" style="text-align: justify;">
  			ท่านสามารถออกกำลังกายในรูปแบบเดิมเป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
  			</td>
  		</tr>
      <tr>
  			<td class="content">พลังของกล้ามเนื้อขา</td>
  			<td class="content" align="center">ยืนกระโดดสูง<br>(เซนติเมตร)</td>
  			<td class="content"></td>
  			<td class="content"></td>
  			<td class="content" align="center">ดีมาก</td>
  			<td class="content" style="text-align: justify;">
  			ท่านสามารถออกกำลังกายในรูปแบบเดิม ที่มีการใช้กล้ามเนื้อ ส่วนขาเป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
  			</td>
  		</tr>
      <tr>
        <td class="content">การทรงตัว</td>
        <td class="content" align="center">การทรงตัว<br>(วินาที)</td>
        <td class="content"></td>
        <td class="content"></td>
        <td class="content" align="center">ดีมาก</td>
        <td class="content" style="text-align: justify;">
        ท่านสามารถออกกำลังกายที่ช่วยฝึกการทรงตัว ที่ทำอยู่เป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
        </td>
      </tr>
      <tr>
        <td class="content">ความทนทานของระบบหัวใจไหลเวียนเลือด</td>
        <td class="content" align="center">การใช้ออกซิเจน<br>(ม.ล./ก.ก./นาที)</td>
        <td class="content"></td>
        <td class="content"></td>
        <td class="content" align="center">ดีมาก</td>
        <td class="content" style="text-align: justify;">
        ท่านสามารถออกกำลังกายในรูปแบบเดิมเป็นประจำเพื่อรักษาสมรรถภาพทางร่างกายที่ดีเอาไว้
        </td>
      </tr>
  	</tbody>
  </table>
  <div align="center" style="font-size:14pt;padding-top:10px;">
    * หมายเหตุ ค่าเกณฑ์สมรรถภาพทางกายสำหรับแต่ละช่วงวัยดูได้ที่ http://www.dpe.go.th/th/subarticle/1/29
  </div>
  <div align="center" style="font-size:14pt;font-weight: bold;">
    กรมพลศึกษา กระทรวงการท่องเที่ยวและกีฬา โทร. 0 2214 2577
  </div>
</body>
</html>
<?php
  include("../../inc/footer.php");
?>

<script type="text/javascript">

</script>
