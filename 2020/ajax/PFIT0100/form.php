<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code = $_GET['code'];
$statusType = $_GET['status'];
$disabled = '';
$visibility = '';

$time = $_SESSION['TYPE_CONN']==2?"_".time():"";

$project_code     = "";
$project_name     = "";
$start_date       = date('Y/m/d');
$end_date         = date('Y/m/d');
$location         = "";
$project_detail   = "";
$status           = "";
$test_code        = "";
$project_type     = "";

$category_criteria_code = "";

$option = '';
$type = "";

$testCodeArr = array();
$testSeq     = array();

$optionCat= "<option></option>";

$sqlCat = "SELECT * FROM pfit_t_category_criteria where status = 'Y'";

$queryCat = DbQuery($sqlCat,null);
$jsonCat  = json_decode($queryCat, true);
$numCat   = $jsonCat['dataCount'];
$rowCat   = $jsonCat['data'];

if(!empty($code)){
  $type = "EDIT";
  if($_SESSION['TYPE_CONN'] == '2'){
      $sql = "SELECT * FROM pfit_t_project where project_code  = '$code'";
  }else{
      $sql = "SELECT project_code, project_name, location, project_detail,status,project_type,
      TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date
      FROM pfit_t_project
      where project_code  = '$code'";
  }

  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  if($num > 0){
    $project_code     = $row[0]['project_code'];
    $project_name     = $row[0]['project_name'];
    $start_date       = $row[0]['start_date'];
    $end_date         = $row[0]['end_date'];
    $location         = $row[0]['location'];
    $project_detail   = $row[0]['project_detail'];
    $status           = $row[0]['status'];
    $project_type     = $row[0]['project_type'];

    $sqlPT = "SELECT * FROM pfit_t_project_test where project_code = '$code'";
    $queryPT  = DbQuery($sqlPT,null);
    $jsonPT   = json_decode($queryPT, true);

    $numPT    = $jsonPT['dataCount'];
    $rowPT    = $jsonPT['data'];


    //print_r($rowPT);
    foreach ($rowPT as $key => $value) {
      if(isset($value['test_code'])){
        $test_code  = $value['test_code'];
        $test_seq   = $value['test_seq'];
        array_push($testCodeArr,$test_code);
        $testSeq[$test_code] = $test_seq;
      }
    }


    //$testCodeArr = explode("," , $test_code);
    // if($testCodeArr){
    //     for ($i=0; $i < $testCodeArr ; $i++) {
    //
    //     }
    // }
  }

  if($numCat > 0){
    for ($i=0; $i < $numCat ; $i++) {
      $selected = "";
      $category_criteria_name = $rowCat[$i]['category_criteria_name'];
      $categoryCriteriaCode = $rowCat[$i]['category_criteria_code'];
      if($category_criteria_code == $categoryCriteriaCode){
        $selected = 'selected="selected"';
      }
      $optionCat .= "<option value='$categoryCriteriaCode' $selected >$category_criteria_name</option>";
    }
  }
  if($statusType == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

    //echo $statusType;
  }
}else{
  $type = "ADD";

  $sql = "SELECT project_code FROM pfit_t_project where project_code NOT LIKE 'M%' order by project_code desc";

  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  if($num > 0){
    $project_code = $row[0]['project_code'];
  }
  $date = date("Y");
  $dates = date("m");

  if($date < 2500){
    $date += 543;
  }

  if(!empty($project_code)){
    $project_code = explode("_", $project_code);
    $lastNum = substr($project_code[0],4);
    $lastNum = $lastNum + 1;

    $project_code = substr($date, 2, 4).$dates.sprintf("%04d", $lastNum).$time;
  }else{
    $project_code = substr($date, 2, 4).$dates.sprintf("%04d", 1).$time;
  }

  if($numCat > 0){
    for ($x=0; $x < $numCat; $x++) {
      $selected = "";
      $category_criteria_name = $rowCat[$x]['category_criteria_name'];
      $category_criteria_code = $rowCat[$x]['category_criteria_code'];
      $optionCat .= "<option value='$category_criteria_code' $selected >$category_criteria_name</option>";
    }
  }
}
$con = "";
// echo $_SESSION['ROLECODE'];
$posSCH     = strrpos($_SESSION['ROLECODE'], "SCH");//โรงเรียน 4
$posJPL     = strrpos($_SESSION['ROLECODE'], "JPL");//จพล 3
$posPFIT    = strrpos($_SESSION['ROLECODE'], "PFIT");//กรมพละ 2
$posADM     = strrpos($_SESSION['ROLECODE'], "ADM");//Admin
 //echo "posSCH :".$posSCH;
 //echo "<br>posJPL :".$posJPL;
 //echo "<br>posPFIT :".$posPFIT;
 //echo "<br>posADM :".$posADM;
$testDisplay = "";
if($posPFIT == false && $posADM == false)
{
  if($posSCH !== false && $posSCH >= 0)
  {
    $testDisplay .= " OR test_display LIKE '%4%' ";
  }

  if($posJPL !== false && $posJPL >= 0)
  {
    $testDisplay .= " OR test_display LIKE '%3%' ";
  }

  if ($testDisplay != "") {
      $con = " where test_display LIKE '%1%' $testDisplay ";
  }
}


$sql_test = "SELECT * FROM pfit_t_test $con order by test_opjective";
$query_test = DbQuery($sql_test,null);
$jsonTest   = json_decode($query_test, true);
$num        = $jsonTest['dataCount'];
$row_test   = $jsonTest['data'];
//echo $sql_test;
//print_r($testSeq);
if($num > 0){
  foreach ($row_test as $key => $value) {
    $check = '';
    $test_code = $value['test_code'];
    $test_seq = "";
    if (in_array($test_code,$testCodeArr)){
         $check     = 'checked';
         $test_seq  = $testSeq[$test_code];
    }else{
         $check = '';
    }

      $option .= "<tr class='text-center'>
                    <td><input $disabled type='checkbox' id='$test_code' name='test_code[]' value='$test_code' $check></td>
                    <td align='left'><label for='$test_code' style='font-weight:200;'>".$value['test_name']."</label></td>
                    <td align='left'>".getTestOpjective($value['test_opjective'])."</td>
                    <td><input $disabled style='width:50px;text-align: right;' type='number' name='test_seq[$test_code]' value='$test_seq'></td>
                  </tr>";

  }
}

?>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อกิจกรรม</label>
      <input type="hidden" value="<?= $type ?>" name="type" required >
      <input type="text" name="project_name" value="<?= $project_name ?>" class="form-control" placeholder="ชื่อกิจกรรม" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>รหัส</label>
      <input type="text" name="project_code" value="<?= $project_code; ?>" class="form-control" placeholder="รหัสกิจกรรม" <?=$disabled ?> readonly >
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>สถานที่</label>
      <input type="text" name="location" value="<?= $location; ?>" class="form-control" placeholder="สถานที่"  <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>วันที่เริ่ม</label>
      <!-- <input type="date" name="start_date" value="<?= $start_date; ?>" class="form-control" placeholder="วันที่เริ่ม"  <?=$disabled ?> required> -->
      <input class="form-control datepicker" value="<?= DateThai($start_date) ?>" name="start_date" type="text" placeholder="วันที่เริ่ม" data-provide="datepicker" data-date-language="th-th" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>วันที่สิ้นสุด</label>
      <!-- <input type="date" name="end_date" value="<?= $end_date; ?>" class="form-control" placeholder="วันที่สิ้นสุด"  <?=$disabled ?> required> -->
      <input class="form-control datepicker" value="<?= DateThai($end_date) ?>" name="end_date" type="text" placeholder="วันที่สิ้นสุด" data-provide="datepicker" data-date-language="th-th" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>รายละเอียด</label>
      <input type="text" class="form-control" value="<?= $project_detail; ?>" name="project_detail" placeholder="รายละเอียด" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ประเภทองค์กร</label>
      <select name="project_type" class="form-control" <?=$disabled ?> required>
        <option value="" ></option>
        <option value="1" <?= ($project_type == '1' ? 'selected="selected"':'') ?>>โรงเรียน</option>
        <option value="2" <?= ($project_type == '2' ? 'selected="selected"':'') ?>>มหาวิทยาลัย</option>
        <option value="3" <?= ($project_type == '3' ? 'selected="selected"':'') ?>>สถานศึกษาอื่น</option>
        <option value="4" <?= ($project_type == '4' ? 'selected="selected"':'') ?>>ประชาชนทั่วไป</option>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>

    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr class="text-center">
            <td></td>
            <td>รายการทดสอบ</td>
            <td>องค์ประกอบที่ต้องการวัด</td>
            <td>ลำดับรายการทดสอบ</td>
          </tr>
        </thead>
        <tbody>
          <?php echo $option; ?>
        </tbody>
      </table>
    </div>
</div>
<script>
$(function () {
  $('.datepicker').datepicker();
})
</script>
