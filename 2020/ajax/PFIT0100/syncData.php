<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

$_SESSION['TYPE_CONN'] = '1';


$arrTable = "";
$success = 0;
$fail    = 0;
$total   = 0;
$sqlErr  = "";

$input = file_get_contents('php://input');
$data = json_decode($input,true);

$json_data = $data;

if($json_data != ""){
  $arr  = explode('\\',$json_data);

  if(is_array($arr)){
    $data = base64_decode($arr[0]);
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      foreach ($arrTable['data'] as $key => $value) {
        try {
              $project_code   = $value['project_code'];
              $project_name   = $value['project_name'];
              $start_date     = queryDateOracle($value['start_date']);
              $end_date       = queryDateOracle($value['end_date']);
              $location       = $value['location'];
              $project_detail = $value['project_detail'];
              $status         = $value['status'];
              $user_login     = $value['user_login'];
              $project_type   = $value['project_type'];

              $str   = "SELECT * FROM pfit_t_project WHERE project_code = '$project_code' and status <> 'C'";
              $query = DbQuery($str,null);
              $json  = json_decode($query, true);
              $num   = $json['dataCount'];

              if($num > 0){
                  $str = "UPDATE pfit_t_project SET
                          project_name   = '$project_name',
                          start_date     = $start_date,
                          end_date       = $end_date,
                          location       = '$location',
                          project_detail = '$project_detail',
                          status         = '$status',
                          user_login     = '$user_login',
                          project_type   = '$project_type'
                        WHERE project_code = '$project_code'";
                  //echo $str;
              }else{
                  $str = "INSERT INTO pfit_t_project (project_code, project_name, start_date, end_date, location, project_detail, status, user_login,project_type)
                          VALUES (
                          '$project_code',
                          '$project_name',
                           $start_date,
                           $end_date,
                          '$location',
                          '$project_detail',
                          '$status',
                          '$user_login',
                          '$project_type')";
                  //echo $str;
              }

              $js      = DbQuery($str,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
              if($errorInfo > 0){
                $fail++;
              }else{
                $success++;
              }
        }catch (Exception $e) {
            $fail++;
        }
        $total++;
      }
    }

    $data = base64_decode($arr[1]);
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      foreach ($arrTable['data'] as $key => $value) {
        try {
              $project_code   =  $value['project_code'];
              $person_number  =  $value['person_number'];
              $person_gender  =  $value['person_gender'];
              $person_name    =  $value['person_name'];
              $person_lname   =  $value['person_lname'];
              $date_of_birth  =  queryDate($value['date_of_birth']);
              $wiegth         =  $value['wiegth'];
              $height         =  $value['height'];
              $person_type    =  $value['person_type'];
              $education      =  $value['education'];
              $stage          =  $value['stage'];
              $grade          =  $value['grade'];
              $room           =  $value['room'];
              $status_test    =  $value['status_test'];
              $points_1       =  $value['points_1'];
              $points_2       =  $value['points_2'];
              $points_3       =  $value['points_3'];

              $str = "SELECT * FROM pfit_t_person WHERE project_code = '$project_code' AND person_number = '$person_number'";
              $query = DbQuery($str,null);
              $json  = json_decode($query, true);
              $num   = $json['dataCount'];

              if($num > 0){
                $str = "UPDATE pfit_t_person SET
                        person_number  =  '$person_number',
                        person_gender  =  '$person_gender',
                        person_name    =  '$person_name',
                        person_lname   =  '$person_lname',
                        date_of_birth  =   $date_of_birth,
                        wiegth         =  '$wiegth',
                        height         =  '$height',
                        person_type    =  '$person_type',
                        education      =  '$education',
                        stage          =  '$stage',
                        grade          =  '$grade',
                        room           =  '$room',
                        status_test    =  '$status_test',
                        points_1       =  '$points_1',
                        points_2       =  '$points_2',
                        points_3       =  '$points_3'
                        WHERE project_code = '$project_code' AND person_number = '$person_number'";
                  //echo $str."<br>";
              }else{
                $str = "INSERT INTO pfit_t_person (project_code, person_number, person_gender,
                                    person_name, person_lname, date_of_birth, wiegth, height, person_type,
                                    education, stage, grade, room, status_test,points_1,points_2,points_3)
                       VALUES (
                         '$project_code',
                         '$person_number',
                         '$person_gender',
                         '$person_name',
                         '$person_lname',
                          $date_of_birth,
                         '$wiegth',
                         '$height',
                         '$person_type',
                         '$education',
                         '$stage',
                         '$grade',
                         '$room',
                         '$status_test',
                         '$points_1',
                         '$points_2',
                         '$points_3'
                       )";
                  //echo $str."<br>";
              }
              $js      = DbQuery($str,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
              if($errorInfo > 0){
                $fail++;
              }else{
                $success++;
              }
          }catch (Exception $e) {
            $fail++;
          }
          $total++;
       }
    }

    $data = base64_decode($arr[2]);
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      foreach ($arrTable['data'] as $key => $value) {
        try {
              $project_code = $value['project_code'];
              $test_code    = $value['test_code'];
              $test_seq     = $value['test_seq'];

              $str = "SELECT * FROM pfit_t_project_test WHERE project_code = '$project_code' and test_code = '$test_code'";
              $query = DbQuery($str,null);
              $json  = json_decode($query, true);
              $num   = $json['dataCount'];
              if($num > 0){
                $str = "UPDATE pfit_t_project_test SET
                               test_seq    = '$test_seq'
                       WHERE project_code  = '$project_code' and test_code   = '$test_code'";
                  //echo $str."<br>";
              }else{
                $str = "INSERT INTO pfit_t_project_test (project_code,test_code, test_seq)VALUES('$project_code','$test_code','$test_seq')";
                //echo $str."<br>";
              }
              $js      = DbQuery($str,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
              if($errorInfo > 0){
                $fail++;
              }else{
                $success++;
              }
        }catch (Exception $e) {
          $fail++;
        }
        $total++;
      }
    }

    $data = base64_decode($arr[3]);
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      foreach ($arrTable['data'] as $key => $value) {
        try {
                $project_code                   = $value['project_code'];
                $test_code                      = $value['test_code'];
                $person_number                  = $value['person_number'];
                $result                         = $value['result'];
                $test_criteria_code	            = $value['test_criteria_code'];
                $date_create                    = queryDateOracle($value['date_create']);
                $result_cal                     = $value['result_cal'];

                $str = "SELECT * FROM pfit_t_result WHERE project_code = '$project_code' and person_number = '$person_number' and test_code = '$test_code'";
                $query = DbQuery($str,null);
                $json  = json_decode($query, true);
                $num   = $json['dataCount'];

                if($num > 0){
                  $str = "UPDATE pfit_t_result SET
                           result                   = '$result',
                           test_criteria_code       = '$test_criteria_code',
                           result_cal               = '$result_cal'
                         WHERE project_code = '$project_code' and person_number = '$person_number' and test_code = '$test_code'";
                    //echo $str."<br>";
                }else{
                  $str = "INSERT INTO pfit_t_result (project_code,test_code,person_number,result,test_criteria_code,date_create,result_cal)
                         VALUES (
                           '$project_code',
                           '$test_code',
                           '$person_number',
                           '$result',
                           '$test_criteria_code',
                            $date_create,
                           '$result_cal')";
                  //echo $str."<br>";
                }

                $js      = DbQuery($str,null);
                $row     = json_decode($js, true);
                $status  = $row['status'];

                $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
                if($errorInfo > 0){
                  $fail++;
                  $sqlErr = $str;
                }else{
                  $success++;
                }
         }catch (Exception $e) {
           $fail++;
         }
         $total++;
      }
    }

    $data = base64_decode($arr[4]);
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      foreach ($arrTable['data'] as $key => $value) {
        try {
              $user_login         = $value['user_login'];
              $user_password	    = $value['user_password'];
              $user_name          = $value['user_name'];
              $is_active          = $value['is_active'];
              $role_list          = $value['role_list'];
              $user_img	          = $value['user_img'];
              $user_id_update	    = $value['user_id_update'];
              $note1	            = $value['note1'];
              $note2	            = $value['note2'];


              $str = "SELECT * FROM t_user WHERE user_login = '$user_login'";
              $query = DbQuery($str,null);
              $json  = json_decode($query, true);
              $num   = $json['dataCount'];

              if($num > 0){
                $sql = "UPDATE t_user SET
                          user_password	   = '$user_password',
                          user_name        = '$user_name',
                          is_active        = '$is_active',
                          role_list        = '$role_list',
                          user_img	       = '$user_img',
                          user_id_update	 = '$user_id_update',
                          note1	           = '$note1',
                          note2	           = '$note2'
                       WHERE user_login  = '$user_login'";
                  //echo $str."<br>";
              }else{
                if($_SESSION['TYPE_CONN'] == '1'){
                  $sql = "INSERT INTO t_user
                         (user_id,user_login,user_name,user_password,is_active,
                           role_list,user_img,user_id_update,note1,note2)
                         VALUES(T_USER_SEQ.NEXTVAL,'$user_login','$user_name','$user_password','$is_active',
                           '$role_list','$user_img','$user_id_update','$note1','$note2')";
                }else{
                  $sql = "INSERT INTO t_user
                         (user_login,user_name,user_password,is_active,
                           role_list,user_img,user_id_update,note1,note2)
                         VALUES('$user_login','$user_name','$user_password','$is_active',
                           '$role_list','$user_img','$user_id_update','$note1','$note2')";
                }
              }
              $js      = DbQuery($str,null);
              $row     = json_decode($js, true);
              $status  = $row['status'];

              $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
              if($errorInfo > 0){
                $fail++;
              }else{
                $success++;
              }
        }catch (Exception $e) {
          $fail++;
        }
        $total++;
      }
    }
  }
}
header('Content-Type: application/json');
exit(json_encode(array('status' => 'success', 'message' => 'success', 'success'=>$success, 'fail'=>$fail, 'total'=>$total , 'sql'=>$sqlErr)));

?>
