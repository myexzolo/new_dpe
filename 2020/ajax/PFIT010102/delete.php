<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$project_code = $_POST['project_code'];
$code         = $_POST['code'];
$ParamData = array($project_code,$code);

$sql  = "DELETE FROM pfit_t_person WHERE project_code = ? and person_number = ?";

$query  = DbQuery($sql,$ParamData);
$row    = json_decode($query, true);
$status = $row['status'];

if($status == "success")
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}
?>
