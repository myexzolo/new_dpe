<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = date("Y/m/t", strtotime($dateStart));

  $y    = date("Y", strtotime($dateStart));
  $m    = date("m", strtotime($dateStart));

  $dateStart = queryDate($dateStart);
  $dateEnd   = queryDate($dateEnd);

  if(!strstr($_SESSION['ROLECODE'], 'ADM')){
      $con = " and user_login = '".$_SESSION['USER_NAME']."'";
  }

  if($_SESSION['TYPE_CONN'] == '2'){
      $sql = "SELECT * FROM pfit_t_project where status <> 'C' and start_date between $dateStart and  $dateEnd $con ORDER BY start_date";
  }else{
      $sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date,project_type
      FROM pfit_t_project
      where status <> 'C' and start_date between $dateStart and  $dateEnd $con ORDER BY start_date DESC";
  }



  //echo $sql;
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  for ($i=0; $i < $num ; $i++) {
    $date = date('Y-m',strtotime($data[$i]['end_date']));
    $project_code = $data[$i]['project_code'];
    $project_name = $data[$i]['project_name'];
    $project_type = $data[$i]['project_type'];
    $pram = "?project_code=".$project_code."&project_name=".urlencode($project_name)."&project_type=".$project_type."&m=".$m."&y=".$y;

?>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-gray" style="padding:10px;" >
        <div class="info-box-number" style="height:80px;"><?= $project_name ?></div>
        <div style="font-size:14px;height:30px;">
          <?= DateThai($data[$i]['start_date']) ?> - <?= DateThai($data[$i]['end_date']) ?>
        </div>
        <div class="row">
          <div class="col-md-4">
            <button type="button" style="width: 100%;text-align: center;" class="btn btn-social bg-olive btn-flat btnList" onclick="postURL('<?= "PFIT010101.php".$pram ?>')">
              <i class="fa fa-user-plus"></i> ผู้ทดสอบ
            </button>
          </div>
          <div class="col-md-4">
            <button type="button" style="width: 100%;text-align: center;" class="btn btn-social bg-gray-active btn-flat" onclick="postURL('<?= "PFIT010102.php".$pram ?>')">
              <i class="fa fa fa-floppy-o"></i> จัดเก็บผล</button>
          </div>
          <div class="col-md-4">
            <button type="button" style="width: 100%;text-align: center;" class="btn btn-social bg-light-blue btn-flat" onclick="postURL('<?= "PFIT010103.php".$pram ?>')">
              <i class="fa fa-file-text-o"></i> รายงาน</button>
          </div>
        </div>
    </div>
  </div>
<?php
}
?>
