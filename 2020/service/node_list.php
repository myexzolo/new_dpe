<?php
session_start();
include('../conf/connect.php');
include('../inc/utils.php');
include('function.php');

FIX_PHP_CORSS_ORIGIN();
//
$data = file_get_contents("php://input");
$dataJsonDecode = json_decode($data);
$project_code = @$dataJsonDecode->project_code;
$person_number = @$dataJsonDecode->person_number;
$test_type = @$dataJsonDecode->test_type;
$test_code = @$dataJsonDecode->test_code;
$type_con = @$dataJsonDecode->type_con;
$_SESSION['TYPE_CONN'] = $type_con;

// $project_code = '61100004_1539515202';
// $person_number = '12606';
// $test_type = '0';
// $test_code = 'TST05';
//
// $_SESSION['TYPE_CONN'] = 2;

if($test_type == 1){
  $add_dataall = array();
  $sql = "SELECT * FROM pfit_t_project_test
          INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
          WHERE pfit_t_project_test.project_code = '$project_code'
          AND pfit_t_test.test_type = '$test_type'
          ORDER BY pfit_t_project_test.test_seq";
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  foreach ($data as $key => $value) {

    $add_dataall[$key]['test_code'] = $value['test_code'];
    $add_dataall[$key]['test_name'] = $value['test_name'];
    $add_dataall[$key]['test_unit'] = $value['test_unit'];

    $sqlsub = "SELECT * FROM pfit_t_result
               WHERE person_number = '$person_number'
               AND test_code = '{$value['test_code']}'
               AND project_code = '$project_code'";
    $querysub = DbQuery($sqlsub,null);
    $rowsub  = json_decode($querysub, true);
    $numsub  = $rowsub['dataCount'];
    $datasub = $rowsub['data'];
    $add_dataall[$key]['result'] = '';
    if($numsub != 0){
      $add_dataall[$key]['result']    = $datasub[0]['result'];
    }
  }

  echo(json_encode(array('status' => '200','message' => 'Success', 'sql' => $sql, 'data' => array('listAll' => $add_dataall ))));


}else{

  $add_dataall = array();
  $sql = "SELECT * FROM pfit_t_project_test
          INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
          WHERE pfit_t_project_test.project_code = '$project_code'
          AND pfit_t_test.test_type = '$test_type'
          AND pfit_t_test.test_code != '$test_code'
          ORDER BY pfit_t_project_test.test_seq";
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  foreach ($data as $key => $value) {


    $add_dataall[$key]['test_code'] = $value['test_code'];
    $add_dataall[$key]['test_name'] = $value['test_name'];
    $add_dataall[$key]['test_unit'] = $value['test_unit'];

    $sqlsub = "SELECT * FROM pfit_t_result
               WHERE person_number = '$person_number'
               AND test_code = '{$value['test_code']}'
               AND project_code = '$project_code'";
    $querysub = DbQuery($sqlsub,null);
    $rowsub  = json_decode($querysub, true);
    $numsub  = $rowsub['dataCount'];
    $datasub = $rowsub['data'];

    $add_data[$key]['result'] = '0';

    if($value['test_code'] == 'VO2'){
      $add_dataall[$key]['result'] = '0|0';
    }

    if($value['test_code'] == 'BMI'){
      $add_dataall[$key]['result'] = '0|0';
    }

    if($value['test_code'] == 'ABP'){
      $add_dataall[$key]['result'] = '0|0';
    }

    if($numsub != 0){
      $add_dataall[$key]['result']    = $datasub[0]['result'];
    }

  }

  $sqlOne = "SELECT * FROM pfit_t_project_test
             INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
             WHERE pfit_t_project_test.project_code = '$project_code'
             AND pfit_t_test.test_type = '$test_type'
             AND pfit_t_test.test_code = '$test_code'
             ORDER BY pfit_t_project_test.test_seq";
  $queryOne = DbQuery($sqlOne,null);
  $rowOne  = json_decode($queryOne, true);
  $numOne  = $rowOne['dataCount'];
  $dataOne = $rowOne['data'];

  $add_data = array();
    $add_data[0]['result']    = '';
    $add_data[0]['test_code'] = $dataOne[0]['test_code'];
    $add_data[0]['test_name'] = $dataOne[0]['test_name'];
    $add_data[0]['test_unit'] = $dataOne[0]['test_unit'];

    $sqlOnes = "SELECT * FROM pfit_t_result
                WHERE person_number = '$person_number'
                AND test_code = '$test_code'
                AND project_code = '$project_code'";
    $queryOnes = DbQuery($sqlOnes,null);
    $rowOnes  = json_decode($queryOnes, true);
    $numOnes  = $rowOnes['dataCount'];
    $dataOnes = $rowOnes['data'];

    if($test_code == 'VO2'){
      $add_data[0]['result'] = '0|0';
    }

    if($value['test_code'] == 'BMI'){
      $add_dataall[$key]['result'] = '0|0';
    }

    if($value['test_code'] == 'ABP'){
      $add_dataall[$key]['result'] = '0|0';
    }

    if($numOnes != 0){
      $add_data[0]['result'] = $dataOnes[0]['result'];
    }


  echo(json_encode(array('status' => '200','message' => 'Success', 'data' => array('listAll' => $add_dataall , 'listOne' => $add_data , 'sql' => $sqlOnes ))));


}



?>
