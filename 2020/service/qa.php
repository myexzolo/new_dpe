<?php

session_start();
include('../conf/connect.php');
include('../inc/utils.php');
include('function.php');

FIX_PHP_CORSS_ORIGIN();


// $_SESSION['TYPE_CONN'] = 2;
$_SESSION['TYPE_CONN'] = $_GET['type_con'];

$projectCode = $_GET['project_code'];
$person_number = $_GET['person_number'];
// $projectCode = '61100004_1539515202';
// $person_number = '12603';

$sql = "SELECT * FROM pfit_t_person
        WHERE project_code = '$projectCode' AND person_number = '$person_number'";
$query = DbQuery($sql,null);
$row  = json_decode($query, true);
$data = $row['data'];

$points_1 = $data[0]['points_1'];
$points_2 = $data[0]['points_2'];
$points_3 = $data[0]['points_3'];

$person_type = $data[0]['person_type'];
//$person_type = 0;

if($person_type != '1'){
  //project == 4
  $a[0] = array('anss' =>"ไม่เคยเลย", 'no' => '1');
  $a[1] = array('anss' =>"1-2 วันต่อสัปดาห์", 'no' => '2');
  $a[2] = array('anss' =>"3-4 วันต่อสัปดาห์", 'no' => '3');
  $a[3] = array('anss' =>"5-7 วันต่อสัปดาห์", 'no' => '4');


  $b[0] = array('anss' =>"เดิน, วิ่ง, เต้นแอโรบิค, ปั่นจักรยาน", 'no' => '1');
  $b[1] = array('anss' =>"โยคะ", 'no' => '2');
  $b[2] = array('anss' =>"การออกกำลังโดยใช้แรงต้าน (ยางยืด, บาร์เบล, ดัมบ์เบลล์)", 'no' => '3');
  $b[3] = array('anss' =>"อื่นๆ", 'no' => '4');


  $c[0] = array('anss' =>"น้อยกว่า 10 นาที", 'no' => '1');
  $c[1] = array('anss' =>"11-20 นาที", 'no' => '2');
  $c[2] = array('anss' =>"21-30 นาที", 'no' => '3');
  $c[3] = array('anss' =>"31 นาทีขึ้นไป", 'no' => '4');

  $q1  = array('qa'=> $points_1 , 'Question' => "ข้อที่ 1 ท่านออกกำลังกายกี่วันต่อสัปดาห์", 'no' => 'q1', 'ans' => $a);
  $q2  = array('qa'=> $points_2 , 'Question' => "ข้อที่ 2 ท่านออกกำลังกายแบบไหน", 'no' => 'q2', 'ans' => $b);
  $q3  = array('qa'=> $points_3 , 'Question' => "ข้อที่ 3 ท่านออกกำลังกายแต่ละครั้งนานเท่าใด", 'no' => 'q3','ans' => $c);

}else{

  $a[0] = array('anss' =>"ไม่เคยเลย", 'no' => '1');
  $a[1] = array('anss' =>"1-2 วันต่อสัปดาห์", 'no' => '2');
  $a[2] = array('anss' =>"3-4 วันต่อสัปดาห์", 'no' => '3');
  $a[3] = array('anss' =>"5-7 วันต่อสัปดาห์", 'no' => '4');


  $b[0] = array('anss' =>"กีฬาชนิดต่างๆ เช่น ฟุตบอล, เทควันโด, ปั่นจักรยาน เป็นต้น", 'no' => '1');
  $b[1] = array('anss' =>"กิจกรรมนันทนาการ เช่น เต้นรำ, เต้นแอโรบิก, ลีดเดอร์", 'no' => '2');
  $b[2] = array('anss' =>"การละเล่นพื้นบ้าน เช่น หมากเก็บ, กระโดดยาง, ดี่จับ", 'no' => '3');
  $b[3] = array('anss' =>"งานบ้าน เช่น ล้างจาน, ถูบ้าน, รดน้ำต้นไม", 'no' => '4');


  $c[0] = array('anss' =>"น้อยกว่า 15 นาที", 'no' => '1');
  $c[1] = array('anss' =>"15-30 นาที", 'no' => '2');
  $c[2] = array('anss' =>"31-60 นาที", 'no' => '3');
  $c[3] = array('anss' =>"61 นาทีขึ้นไป", 'no' => '4');


  $q1  = array('qa'=> $points_1 , 'Question' => "ข้อที่ 1 มีกิจกรรมทางกายและการออกกำลังกาย กี่วัน/สัปดาห์ (นอกจากชั่วโมงพลศึกษา)", 'no' => 'q1', 'ans' => $a);
  $q2  = array('qa'=> $points_2 , 'Question' => "ข้อที่ 2 รูปแบบกิจกรรมทางกาย (นอกจากชั่วโมงพลศึกษา)", 'no' => 'q2', 'ans' => $b);
  $q3  = array('qa'=> $points_3 , 'Question' => "ข้อที่ 3 ระยะเวลาในการทำกิจกรรมต่อวัน", 'no' => 'q3','ans' => $c);
}

$data = array($q1,$q2,$q3);
echo(json_encode(array('status' => '200','message' => 'Success', 'data' => $data)));

?>
