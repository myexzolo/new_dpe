<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
//$projectCode = '61100004_1539515202';

$projectCode = $_POST['project_code'];

?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/Chart.min.js"></script>

</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }

  table{
    text-align: center;
  }

  tbody tr td{
    font-size:12pt;
    padding: 5px;
  }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;
  }

  page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    padding: 0mm 10mm 10mm 10mm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"][layout="landscape"] {
  width: 29.7cm;
}

  thead{
    font-weight: bolder;
  }

  .text-left{
    text-align: left;
  }
  .text-center{
    text-align: center;
  }

  .color-e0e0e0{
    background-color: #e0e0e0;
  }

  @media print {
      @page {
        size: Landscape;
        margin: 0;
        background-color: #ffffff;
      }
      html, body {
        size: Landscape;
      }
      .break{
        page-break-after: always;
      }
      .color-e0e0e0{
        background-color: #e0e0e0;
      }
  }
</style>
<html>
<body>
  <page size="A4" layout="landscape">
  <script>
    function chartJsShow(id,name,str,str_m,str_f){
      var res = str.split("|");
      var res_m = str_m.split("|");
      var res_f = str_f.split("|");
      let myChart = document.getElementById(id).getContext('2d');
      Chart.defaults.global.defaultFontFamily = 'THSarabun';
      Chart.defaults.global.defaultFontSize = 12;
      Chart.defaults.global.defaultFontColor = '#777';

      let massPopChart = new Chart(myChart, {
        type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        defaultFontSize: '8',
        data:{
          labels: res ,
          datasets:[{
            label:'ชาย',
            data:res_m,
            fill: false,
            backgroundColor:'rgba(255, 99, 132, 0.6)',
            borderWidth:2,
            borderColor:'rgba(255, 99, 132, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000',
            lineTension: 0
          },
          {
            label:'หญิง',
            data:res_f,
            fill: false,
            backgroundColor:'rgba(54, 162, 235, 0.6)',
            borderWidth:2,
            borderColor:'rgba(54, 162, 235, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000',
            lineTension: 0
          }]
        },
        options:{
          responsive: true,
          title:{
            display:true,
            fontStyle:'bold',
            text:name,
            fontSize:16
          },
          legend:{
            display:true,
            position:'bottom',
            labels:{
              fontColor:'#000'
            }
          },
          tooltips:{
            enabled:true
          },
          scales: {
            yAxes: [{
                ticks: {
                    // max: 150,
                    min: 0,
                    stepSize: 10
                }
            }]
          }
        }
      });
    }
  </script>

  <?php
    $break = false;
    $total_teat_c = array();
    $total_teat_p = array();

    $sql = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode'";
    $query = DbQuery($sql,null);
    $row  = json_decode($query, true);

    $num   = $row['dataCount'];
    if($num>0){

    foreach ($row['data'] as $value) {
      $project_name_title  = $value['project_name'];
      $sqlp = " SELECT p.person_name,p.person_lname,p.person_gender,p.wiegth,p.height, r.*,c.age,c.gender,c.category_criteria_detail_code, t.test_name,cd.category_criteria_detail_value,cd.category_criteria_detail_name
                FROM pfit_t_result r, pfit_t_test_criteria c, pfit_t_person p, pfit_t_test t,pfit_t_project_test pt, pfit_t_cat_criteria_detail cd
                WHERE r.project_code = '$projectCode' and p.project_code = '$projectCode'  and pt.project_code = '$projectCode'
                and r.test_code = pt.test_code
                and r.person_number = p.person_number
                and r.test_code = t.test_code
                and r.test_criteria_code = c.test_criteria_code
                and c.category_criteria_detail_code = cd.category_criteria_detail_code
                order by c.age,r.person_number,pt.test_seq";

      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      $nump   = $rowp['dataCount'];
      $data   = array();
      if($nump>0){
        $i = 1;
        foreach ($rowp['data'] as $keyp => $valuep) {

          $age    = $valuep['age'];

          if($age < 14){
              $data[13][] = $valuep;
          }else if($age >= 14 && $age <= 24){
              $data[14][] = $valuep;
          }else if($age >= 25 && $age <= 59){
              $data[25][] = $valuep;
          }else{
              $data[60][] = $valuep;
          }
        }
      }

      if($data != null){
        $numData = count($data);
      }else{
        $numData = 0;
      }


      for($xx = 0 ; $xx < $numData ; $xx++){
          $str = "";
          $inx = 0;
          $con = "";
          if(isset($data[13]))
          {
              $str = "ช่วงอายุ ต่ำกว่า 14 ปี";
              $inx = 13;
              $con  = " and ptt.test_age_min <= 13 and ptt.test_age_max >= 13 ";
          }
          else if(isset($data[14]))
          {
            $str = "ช่วงอายุ 14 - 24 ปี";
            $inx = 14;
            $con  = " and ptt.test_age_min <= 14 and ptt.test_age_max >= 14 ";
          }
          else if(isset($data[25]))
          {
            $str = "ช่วงอายุ 25 - 59 ปี";
            $inx = 25;
            $con  = " and ptt.test_age_min <= 25 and ptt.test_age_max >= 25 ";
          }
          else if(isset($data[60]))
          {
            $str = "ช่วงอายุ 60 ปี ขึ้นไป";
            $inx = 60;
            $con  = " and ptt.test_age_min <= 61 and ptt.test_age_max >= 61 ";
          }

          if($inx > 0){
            $resArr = $data[$inx];
            $data[$inx] = null;

            $sqlpp = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                         WHERE ptpt.test_code = ptt.test_code
                         AND ptpt.project_code = '$projectCode' $con ORDER BY ptpt.test_seq ASC";
            $querypp = DbQuery($sqlpp,null);
            $rowpp  = json_decode($querypp, true);
?>
<div class="row" align="center">
<div class="text-center col-xs-12" style="margin-top:40px">
    <div align="center"><h2>ผลการทดสอบสมรรถภาพทางกาย</h2></div>
    <div align="center"><h2 style="margin-top: -5px;"><?=$project_name_title.' '.$str ?></h2></div>
</div>
</div>
    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
    <thead>
      <tr align="center">
  			<td colspan="<?= ($numpp + 9) ?>" style="height:32px;border-right:solid #fff 1px;border-top:solid #fff 1px;border-left:solid #fff 1px;"></td>
  		</tr>
  		<tr align="center">
  			<td rowspan="2">เลขที่</td>
  			<td rowspan="2">เลขประจำตัว</td>
  			<td rowspan="2">เพศ</td>
  			<td rowspan="2">ชื่อ</td>
  			<td rowspan="2">นามสกุล</td>
  			<td>น้ำหนัก</td>
  			<td>ส่วนสูง</td>
        <?php
            foreach ($rowpp['data'] as $valuepp) {
        ?>
  			   <td><?= $valuepp['test_name']; ?></td>
        <?php } ?>

  			<td>คะแนน</td>
  		</tr>
  		<tr align="center">

  			<td class="color-e0e0e0">(กก.)</td>
  			<td class="color-e0e0e0">(ซม.)</td>
        <?php
            $total_max_values = 0;
            foreach ($rowpp['data'] as $valuepp) {
              $category_criteria_code = $valuepp['category_criteria_code'];

              $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
              $query_value = DbQuery($sql_value,null);
              $row_value  = json_decode($query_value, true);
              $num_value  = $row_value['dataCount'];
              $data_value = $row_value['data'];

              $total_max_values += $data_value[0]['max_value'];
        ?>
  			<td class="color-e0e0e0">(<?= $valuepp['test_unit']; ?>)</td>
        <?php }

        ?>
  			<td class="color-e0e0e0">(เต็ม <?=$total_max_values; ?>)</td>
  		</tr>
    </thead>
    <tbody>
<?php
      $numPerson  = count($resArr);
      $resPerson  = array();
      for($e = 0; $e < $numPerson; $e ++){
        $person_number  = $resArr[$e]['person_number'];
        $person_gender  = $resArr[$e]['person_gender'];
        $person_name    = $resArr[$e]['person_name'];
        $person_lname   = $resArr[$e]['person_lname'];
        $wiegth         = $resArr[$e]['wiegth'];
        $height         = $resArr[$e]['height'];
        $result_cal     = $resArr[$e]['result_cal'];
        $test_code      = $resArr[$e]['test_code'];
        $category_criteria_detail_code  = $resArr[$e]['category_criteria_detail_code'];
        $category_criteria_detail_value = $resArr[$e]['category_criteria_detail_value'];
        $category_criteria_detail_name  = $resArr[$e]['category_criteria_detail_name'];


        if(!isset($resPerson[$person_number])){
          $resPerson[$person_number]['person_number'] = $person_number;
          $resPerson[$person_number]['person_gender'] = $person_gender;
          $resPerson[$person_number]['person_name']   = $person_name;
          $resPerson[$person_number]['person_lname']  = $person_lname;
          $resPerson[$person_number]['wiegth']        = $wiegth;
          $resPerson[$person_number]['height']        = $height;
        }

        if(!isset($ft_score[$inx][$test_code][$category_criteria_detail_code])){
          $ft_score[$inx][$test_code][$category_criteria_detail_code] = 0;
          $mt_score[$inx][$test_code][$category_criteria_detail_code] = 0;

        }

        if($person_gender == "M"){
          $mt_score[$inx][$test_code][$category_criteria_detail_code]++;

        }else{
          $ft_score[$inx][$test_code][$category_criteria_detail_code]++;
        }
        //echo "M".$inx.",".$test_code.",".$category_criteria_detail_code.",".$mt_score[$inx][$test_code][$category_criteria_detail_code]."<br>";
        //echo "F".$inx.",".$test_code.",".$category_criteria_detail_code.",".$ft_score[$inx][$test_code][$category_criteria_detail_code]."<br>";

        $resPerson[$person_number][$test_code]['result_cal']  = $result_cal;
        $resPerson[$person_number][$test_code]['category_criteria_detail_value'] = $category_criteria_detail_value;
        $resPerson[$person_number][$test_code]['category_criteria_detail_name']  = $category_criteria_detail_name;

      }
      $no = 0;
      foreach($resPerson as $key => $x_value) {
        $person_number  = $key;
        $person_gender  = $x_value['person_gender'];
        $person_name    = $x_value['person_name'];
        $person_lname   = $x_value['person_lname'];
        $wiegth         = $x_value['wiegth'];
        $height         = $x_value['height'];

        $person_gender = $person_gender=='M'?'ชาย':'หญิง';
        $no++;
?>
      <tr align="center">
  			<td ><?= $no; ?></td>
  			<td ><?= $person_number; ?></td>
  			<td ><?= $person_gender; ?></td>
  			<td class="text-left" ><?= $person_name; ?></td>
  			<td class="text-left" ><?= $person_lname; ?></td>
  			<td ><?= $wiegth; ?></td>
  			<td ><?= $height; ?></td>
<?php
      $total_max_value = 0;
      $results = array();
      $cc = 0;

      $chkres = "F";
      foreach ($rowpp['data'] as $valuepp) {
        $test_code = $valuepp['test_code'];
        $result = '-';
        $resultCat = "-";

        if(isset($x_value[$test_code]['category_criteria_detail_value']))
        {
          $result           = $x_value[$test_code]['result_cal'];
          $total_max_value += $x_value[$test_code]['category_criteria_detail_value'];
          $resultCat        = $x_value[$test_code]['category_criteria_detail_name'];
        }
        else
        {
          $chkres = "N";
        }
        $results[$cc] = $resultCat;
        $cc++;
?>
  			<td><?= $result ?></td>
<?php
}
    if($chkres == "N"){
      $total_txt_res = "ข้อมูลไม่ครบ";
    }else{
      $total_txt_res = calTestFit($total_max_values,$total_max_value,60);
    }
     ?>
  <td style="border:solid #333 1px;"><?= $total_max_value; ?></td>
  </tr>
  <tr align="center">
  			<td style="border-bottom:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
  			<td class="text-left" style="border:solid #333 1px;"></td>
  			<td class="text-left" style="border:solid #333 1px;"></td>

  			<td style="border:solid #333 1px;"></td>
  			<td style="border:solid #333 1px;"></td>
        <?php
          for($x = 0; $x < count($results); $x++) {

        ?>
  			<td style="border:solid #333 1px;"><?= $results[$x]; ?></td>
        <?php } ?>

  			<td style="border:solid #333 1px;"><?= $total_txt_res; ?></td>
  		</tr>
<?php
}
?>
    </tbody>
</table>
<?php
}
?>
          <div class="break"></div>
          <div class="text-center" style="margin-top:30px;">
            <h1>แผนภูมิแสดงระดับสมรรถภาพทางกาย<?=$project_name_title ?></h1>
            <h1 style="margin-top: -5px;"><?= $str; ?></h1>
          </div>
          <div class="row" style="width:100%;">
          <?php
            $ids = 0;
          ?>
          <?php
          $m_score = array();
          foreach ($rowpp['data'] as $valuepp) {
            $test_name = $valuepp['test_name'];
            $test_code = $valuepp['test_code'];

            $category_criteria_code = $valuepp['category_criteria_code'];

            $sql_c = "SELECT DISTINCT(pttc.category_criteria_detail_code),ptccd.category_criteria_detail_name,ptccd.category_criteria_code
                FROM pfit_t_cat_criteria_detail ptccd , pfit_t_test_criteria pttc
                WHERE ptccd.category_criteria_detail_code = pttc.category_criteria_detail_code
                AND category_criteria_code = '$category_criteria_code' order by pttc.category_criteria_detail_code";

            $query_c = DbQuery($sql_c,null);
            $row_c  = json_decode($query_c, true);
            $category_criteria_detail_name = array();

            $m_score = array();
            $f_score = array();
            $score = array();
            foreach ($row_c['data'] as $key_c => $value_c) {
              $category_criteria_detail_code = isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
              if($category_criteria_detail_code!= ""){
                //echo $test_code.",".$category_criteria_detail_code.",".$inx."<br>";
                $m_score[]= isset($mt_score[$inx][$test_code][$category_criteria_detail_code])?$mt_score[$inx][$test_code][$category_criteria_detail_code]: 0;
                $f_score[]= isset($ft_score[$inx][$test_code][$category_criteria_detail_code])?$ft_score[$inx][$test_code][$category_criteria_detail_code]: 0;
                // print_r($f_score);
                $category_criteria_detail_name[] =  $value_c['category_criteria_detail_name'];
              }else{
                $m_score[] = "";
                $f_score[] = "";
                // print_r($f_score);
                $category_criteria_detail_name[] =  "";
              }

            }

            $str    =  implode("|",$category_criteria_detail_name);
            $str_m  =  implode("|",$m_score);
            $str_f  =  implode("|",$f_score);

            $dd  =  $test_code.'_'.$inx;
          ?>
          <div class="col-xs-6" style="height:550px;margin-top:30px">
            <canvas id="<?php echo $dd; ?>"></canvas>
            <script>
              chartJsShow('<?php echo $dd; ?>','<?php echo $test_name ?>','<?php echo $str; ?>','<?php echo $str_m; ?>','<?php echo $str_f; ?>');
            </script>
            <br><br>
            <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
          		<tr align="center">
                <td><?=$test_name?></td>
                <td>ชาย/จำนวน</td>
                <td>หญิง/จำนวน</td>
                <td>รวม/คน</td>
              </tr>
            <tbody>
              <?php
                foreach ($row_c['data'] as $key_c => $value_c) {
                  $category_criteria_detail_name =  isset($value_c['category_criteria_detail_name'])?$value_c['category_criteria_detail_name']:"";
                  $category_criteria_detail_code =  isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
                  $tm = isset($mt_score[$inx][$test_code][$category_criteria_detail_code])?$mt_score[$inx][$test_code][$category_criteria_detail_code]: 0;
                  $tf = isset($ft_score[$inx][$test_code][$category_criteria_detail_code])?$ft_score[$inx][$test_code][$category_criteria_detail_code]: 0;

                  $tt = (intval($tm) + intval($tf));
              ?>
              <tr align="center">
                <td><?=$category_criteria_detail_name?></td>
                <td><?= $tm ?></td>
                <td><?= $tf ?></td>
                <td><?= $tt ?></td>
              </tr>
              <?php } ?>
            </tbody>
            </table>
          </div>

<?php
      }
?>
</div>
<div class="break"></div>
<?php
    }
    }
  }
  ?>
</page>
</body>
</html>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 2000);
  });
</script>
