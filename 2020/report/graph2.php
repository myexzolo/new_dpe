<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <title>My Chart.js Chart</title>

</head>

<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:12pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:12pt;
    text-align: center;
  }

  .bolder{
    font-weight: bolder;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:12pt;
    font-weight: bold;
    padding: 5px;
  }
  .text-bold{
    font-weight: bold;
  }
  .mar{
    margin: 20px 0px;
  }
   .row{
     margin-right: 0px;
     margin-left: 0px;
   }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:12pt;

  }
  @page {
    size: A4-L;
    margin: 0;
  }
  @media print {
      html, body {
        /*width: 210mm;
        height: 297mm;*/
        margin-left: 0px;
      }
      .break{
        page-break-after: always;
      }
  }

/*@media print{
  @page {size: landscape}
  .break{
    page-break-after: always;
  }
}*/

</style>
<body>
  <!-- <div class="container"> -->
    <!-- <div class="row"> -->
      <div class="col-xs-12">
        <h1 class="text-center text-bold">
          แผนภูมิแสดงระดับสมรรถภาพทางกายนักเรียนโรงเรียนเซนต์หลุยศึกษา <br> ประจำปีการศึกษา 2561
        </h1>
        <h2 class="text-center text-bold">ชั้นประถมศึกษาปีที่ 1</h2>
      </div>
      <?php for ($i=1; $i <=10 ; $i++) { ?>
      <div class="col-xs-6">
        <canvas id="myChart<?php echo $i; ?>"></canvas>

        <div class="col-xs-10 col-xs-offset-1">
          <table class="table table-bordered">
            <thead>
              <tr class="text-center">
                <td>ระดับดัชนีมวลกาย</td>
                <td>จำนวน</td>
                <td>จำนวน</td>
                <td>คน</td>
              </tr>
            </thead>
            <tbody>
              <?php
                $arr = array('สมส่วน', 'ลำตัวหนา', 'อ้วน', 'อ้วนมาก', 'ผอมบาง');
                for ($j=1; $j <=5 ; $j++) {
              ?>
              <tr class="text-center">
                <td><?php echo $arr[$j-1]; ?></td>
                <td>40</td>
                <td>30</td>
                <td>คน</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <p class="text-center">
            นักเรียนชายมีค่าดัชนีมวลกาย เฉลี่ยน 15.5 <br />
            ค่ามาตราฐานนักเรียนชาย 22.9
          </p>
        </div>

      </div>
      <?php } ?>
    <!-- </div> -->
  <!-- </div> -->


  <script src="../js/jquery.min.js"></script>
  <script>

    function chartJsShow(id){
      let myChart = document.getElementById(id).getContext('2d');
      Chart.defaults.global.defaultFontFamily = 'THSarabun';
      Chart.defaults.global.defaultFontSize = 12;
      Chart.defaults.global.defaultFontColor = '#777';

      let massPopChart = new Chart(myChart, {
        type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        defaultFontSize: '8',
        data:{
          labels:['สมส่วน', 'ลำตัวหนา', 'อ้วน', 'อ้วนมาก', 'ผอมบาง'],
          datasets:[{
            label:'ชาย',
            data:[60,20,40,30,130],
            fill: false,
            backgroundColor:'rgba(255, 99, 132, 0.6)',
            borderWidth:2,
            borderColor:'rgba(255, 99, 132, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000'
          },
          {
            label:'หญิง',
            data:[20,40,60,50,100],
            fill: false,
            backgroundColor:'rgba(54, 162, 235, 0.6)',
            borderWidth:2,
            borderColor:'rgba(54, 162, 235, 0.6)',
            hoverBorderWidth:1,
            hoverBorderColor:'#000'
          }]
        },
        options:{
          responsive: true,
          title:{
            display:true,
            fontStyle:'bold',
            text:'ดัชนีมวลกาย คำนวนค่าจากน้ำหนักและส่วนสูง',
            fontSize:16
          },
          legend:{
            display:true,
            position:'bottom',
            labels:{
              fontColor:'#000'
            }
          },
          tooltips:{
            enabled:true
          },
          scales: {
            yAxes: [{
                ticks: {
                    max: 150,
                    min: 0,
                    stepSize: 50
                }
            }]
          }
        }
      });
    }



    for (var i = 1; i <= 10; i++) {
        var myCharts = 'myChart'+i;
      chartJsShow(myCharts);
    }




  </script>
</body>
</html>
