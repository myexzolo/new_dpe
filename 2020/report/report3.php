<?php
session_start();
include('../conf/connect.php');
include('../inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:12pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
    /*text-align: center;*/
  }

  .bolder{
    font-weight: bolder;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:12pt;
    font-weight: bold;
    padding: 5px;
  }

  .mar{
    margin: 20px 0px;
  }
   .row{
     margin-right: 0px;
     margin-left: 0px;
   }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16pt;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 297mm;
        height: 210mm;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
  <body>

    <div class="container">
      <p style="text-align:center">รายงานการทดสอบสมรรถภาพทางกาย เดือน กันยายน 2561</p>

<table border="1" bordercolor="#ccc" cellpadding="5" cellspacing="0" style="border-collapse:collapse; width:100%">
	<tbody>
		<tr>
			<td class="content" colspan="1" rowspan="2" style="text-align:center">ลำดับ</td>
			<td class="content" colspan="1" rowspan="2" style="text-align:center">สถานที่</td>
			<td class="content" colspan="1" rowspan="2" style="text-align:center">วัน/เดือน/ปี</td>
			<td class="content" colspan="1" rowspan="2" style="text-align:center">จำนวนรวม</td>
			<td class="content" colspan="4" rowspan="1" style="text-align:center">ชาย (คน)</td>
			<td class="content" colspan="4" rowspan="1" style="text-align:center">หญิง (คน)</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">ต่ำกว่า 14 ปี</td>
			<td class="content" style="text-align:center">14-24 ปี</td>
			<td class="content" style="text-align:center">25-59 ปี</td>
			<td class="content" style="text-align:center">60 ปีขึ้นไป</td>
			<td class="content" style="text-align:center">ต่ำกว่า 14 ปี</td>
			<td class="content" style="text-align:center">14-24 ปี</td>
			<td class="content" style="text-align:center">25-59 ปี</td>
			<td class="content" style="text-align:center">60 ปีขึ้นไป</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">1</td>
			<td class="content">โรงเรียนบ้านสวน จ.ชลบุรี</td>
			<td class="content" style="text-align:center">4 ก.ย. 61</td>
			<td class="content" style="text-align:center">170</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">65</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">105</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">2</td>
			<td class="content">โรงเรียนฤทธิยวรรณาลัย 2</td>
			<td class="content" style="text-align:center">6 ก.ย. 61</td>
			<td class="content" style="text-align:center">48</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">27</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">21</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">3</td>
			<td class="content">โรงเรียนเซนต์หลุยส์ศึกษา</td>
			<td class="content" style="text-align:center">7 ก.ย. 61</td>
			<td class="content" style="text-align:center">497</td>
			<td class="content" style="text-align:center">163</td>
			<td class="content" style="text-align:center">80</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">162</td>
			<td class="content" style="text-align:center">92</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">4</td>
			<td class="content">สถาบันพัฒนาข้าราชการฝ่ายตุลาการ<br />
			(ศาลยุคิธรรม)</td>
			<td class="content" style="text-align:center">11 ก.ย. 61</td>
			<td class="content" style="text-align:center">124</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">76</td>
			<td class="content" style="text-align:center">12</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">33</td>
			<td class="content" style="text-align:center">3</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">5</td>
			<td class="content">โรงเรียนสีตบุตรบำรุง</td>
			<td class="content" style="text-align:center">12 ก.ย. 61</td>
			<td class="content" style="text-align:center">384</td>
			<td class="content" style="text-align:center">158</td>
			<td class="content" style="text-align:center">100</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">101</td>
			<td class="content" style="text-align:center">25</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">6</td>
			<td class="content">โรงเรียนทุ่งแฝกพิยาคม</td>
			<td class="content" style="text-align:center">13 ก.ย. 61</td>
			<td class="content" style="text-align:center">297</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">148</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">149</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content">&nbsp;</td>
			<td class="content" style="text-align:center">&nbsp;</td>
			<td class="content" style="text-align:center"><strong>1520</strong></td>
			<td class="content" style="text-align:center"><strong>321</strong></td>
			<td class="content" style="text-align:center"><strong>420</strong></td>
			<td class="content" style="text-align:center"><strong>76</strong></td>
			<td class="content" style="text-align:center"><strong>12</strong></td>
			<td class="content" style="text-align:center"><strong>263</strong></td>
			<td class="content" style="text-align:center"><strong>392</strong></td>
			<td class="content" style="text-align:center"><strong>33</strong></td>
			<td class="content" style="text-align:center"><strong>3</strong></td>
		</tr>
	</tbody>
</table>

    </div>


  </body>
</html>
<?php
include("../inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      // window.print();
      // window.close();
    }, 100);
  });
</script>
