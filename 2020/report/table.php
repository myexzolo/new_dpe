<?php
session_start();
include('../conf/connect.php');
include('../inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:12pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:12pt;
    text-align: center;
  }

  .bolder{
    font-weight: bolder;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:12pt;
    font-weight: bold;
    padding: 5px;
  }

  .mar{
    margin: 20px 0px;
  }
   .row{
     margin-right: 0px;
     margin-left: 0px;
   }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:12pt;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 297mm;
        height: 210mm;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
<body>

  <div class="row">
    <?php for ($j=1; $j <=6 ; $j++) { ?>
      <div class="col-xs-6 mar">
        <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="content bolder">ป.<?php echo $j; ?> (ชาย)</td>
      			<td class="content bolder">BMI</td>
            <td class="content bolder">JUMP</td>
            <td class="content bolder">HAND</td>
      			<td class="content bolder">SIT</td>
      			<td class="content bolder">FLEX</td>
      			<td class="content bolder">RUN</td>
      			<td class="content bolder"></td>
      		</tr>
          <?php
            $arr1 = array('สมส่วน','ลำตัวหนา','อ้วน','อ้วนมาก','ผอมบาง');
            $arr2 = array('ต่ำมาก','ต่ำ','พอใช้','ดี','ดีมาก');
            for ($i=1; $i <=5 ; $i++) {
          ?>
            <tr>
        			<td class="content"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">3</td>
        			<td class="content">2</td>
        			<td class="content">3</td>
        			<td class="content">14</td>
        			<td class="content"><?php echo $arr2[$i-1]; ?></td>
        		</tr>
          <?php } ?>
        </body>
      </table>
      </div>
      <div class="col-xs-6 mar">
        <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="content bolder">ป.<?php echo $j; ?> (หญิง)</td>
      			<td class="content bolder">BMI</td>
            <td class="content bolder">JUMP</td>
            <td class="content bolder">HAND</td>
      			<td class="content bolder">SIT</td>
      			<td class="content bolder">FLEX</td>
      			<td class="content bolder">RUN</td>
      			<td class="content bolder"></td>
      		</tr>
          <?php
            $arr1 = array('สมส่วน','ลำตัวหนา','อ้วน','อ้วนมาก','ผอมบาง');
            $arr2 = array('ต่ำมาก','ต่ำ','พอใช้','ดี','ดีมาก');
            for ($i=1; $i <=5 ; $i++) {
          ?>
            <tr>
        			<td class="content"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">3</td>
        			<td class="content">2</td>
        			<td class="content">3</td>
        			<td class="content">14</td>
        			<td class="content"><?php echo $arr2[$i-1]; ?></td>
        		</tr>
          <?php } ?>
        </body>
      </table>
      </div>
    <?php } ?>
    <?php for ($j=1; $j <=6 ; $j++) { ?>
      <div class="col-xs-6 mar">
        <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="content bolder">ม.<?php echo $j; ?> (ชาย)</td>
      			<td class="content bolder">BMI</td>
            <td class="content bolder">JUMP</td>
            <td class="content bolder">HAND</td>
      			<td class="content bolder">SIT</td>
      			<td class="content bolder">FLEX</td>
      			<td class="content bolder">RUN</td>
      			<td class="content bolder"></td>
      		</tr>
          <?php
            $arr1 = array('สมส่วน','ลำตัวหนา','อ้วน','อ้วนมาก','ผอมบาง');
            $arr2 = array('ต่ำมาก','ต่ำ','พอใช้','ดี','ดีมาก');
            for ($i=1; $i <=5 ; $i++) {
          ?>
            <tr>
        			<td class="content"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">3</td>
        			<td class="content">2</td>
        			<td class="content">3</td>
        			<td class="content">14</td>
        			<td class="content"><?php echo $arr2[$i-1]; ?></td>
        		</tr>
          <?php } ?>
        </body>
      </table>
      </div>
      <div class="col-xs-6 mar">
        <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="content bolder">ม.<?php echo $j; ?> (หญิง)</td>
      			<td class="content bolder">BMI</td>
            <td class="content bolder">JUMP</td>
            <td class="content bolder">HAND</td>
      			<td class="content bolder">SIT</td>
      			<td class="content bolder">FLEX</td>
      			<td class="content bolder">RUN</td>
      			<td class="content bolder"></td>
      		</tr>
          <?php
            $arr1 = array('สมส่วน','ลำตัวหนา','อ้วน','อ้วนมาก','ผอมบาง');
            $arr2 = array('ต่ำมาก','ต่ำ','พอใช้','ดี','ดีมาก');
            for ($i=1; $i <=5 ; $i++) {
          ?>
            <tr>
        			<td class="content"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">3</td>
        			<td class="content">2</td>
        			<td class="content">3</td>
        			<td class="content">14</td>
        			<td class="content"><?php echo $arr2[$i-1]; ?></td>
        		</tr>
          <?php } ?>
        </body>
      </table>
      </div>
    <?php } ?>
  </div>

  <div class="row">
    <?php for ($j=1; $j <=6 ; $j++) { ?>
      <div class="col-xs-4 mar">
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="thStyle bolder">ป.<?php echo $j; ?></td>
      			<td class="thStyle bolder">ชาย</td>
            <td class="thStyle bolder">หญิง</td>
            <td class="thStyle bolder">HAND</td>
      		</tr>
          <tr>
      			<td class="thStyle bolder">( ร้อยละ )</td>
      			<td class="thStyle bolder">จำนวน</td>
            <td class="thStyle bolder">จำนวน</td>
            <td class="thStyle bolder">คน</td>
      		</tr>
          <?php
            $arr1 = array('80 ขึ้นไป','70','60','50','40 ลงมา');
            for ($i=1; $i <=3 ; $i++) {
          ?>
            <tr>
        			<td class="thStyle bolder"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">คน</td>
        		</tr>
          <?php } ?>
        </body>
      </table>
    </div>
    <?php } ?>
  </div>
  <div class="row mar">
    <div class="col-xs-4 col-xs-offset-4">
    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
      <tbody>
        <tr>
          <td class="thStyle bolder">รวม (ประถม)</td>
          <td class="thStyle bolder">ชาย</td>
          <td class="thStyle bolder">หญิง</td>
          <td class="thStyle bolder">HAND</td>
        </tr>
        <tr>
          <td class="thStyle bolder">( ร้อยละ )</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">คน</td>
        </tr>
        <?php
          $arr1 = array('80 ขึ้นไป','70','60','50','40 ลงมา');
          for ($i=1; $i <=3 ; $i++) {
        ?>
          <tr>
            <td class="thStyle bolder"><?php echo $arr1[$i-1]; ?></td>
            <td class="content">3</td>
            <td class="content">13</td>
            <td class="content">คน</td>
          </tr>
        <?php } ?>
      </body>
    </table>

  </div>
  </div>

  <div class="row">
    <?php for ($j=1; $j <=6 ; $j++) { ?>
      <div class="col-xs-4 mar">
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tbody>
          <tr>
      			<td class="thStyle bolder">ม.<?php echo $j; ?></td>
      			<td class="thStyle bolder">ชาย</td>
            <td class="thStyle bolder">หญิง</td>
            <td class="thStyle bolder">HAND</td>
      		</tr>
          <tr>
      			<td class="thStyle bolder">( ร้อยละ )</td>
      			<td class="thStyle bolder">จำนวน</td>
            <td class="thStyle bolder">จำนวน</td>
            <td class="thStyle bolder">คน</td>
      		</tr>
          <?php
            $arr1 = array('80 ขึ้นไป','70','60','50','40 ลงมา');
            for ($i=1; $i <=3 ; $i++) {
          ?>
            <tr>
        			<td class="thStyle bolder"><?php echo $arr1[$i-1]; ?></td>
        			<td class="content">3</td>
              <td class="content">13</td>
              <td class="content">คน</td>
        		</tr>
          <?php } ?>
        </body>
      </table>
    </div>
    <?php } ?>
  </div>

  <div class="row mar">
    <div class="col-xs-4 col-xs-offset-2">
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
      <tbody>
        <tr>
          <td class="thStyle bolder">รวม (มัธยม)</td>
          <td class="thStyle bolder">ชาย</td>
          <td class="thStyle bolder">หญิง</td>
          <td class="thStyle bolder">HAND</td>
        </tr>
        <tr>
          <td class="thStyle bolder">( ร้อยละ )</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">คน</td>
        </tr>
        <?php
          $arr1 = array('80 ขึ้นไป','70','60','50','40 ลงมา');
          for ($i=1; $i <=3 ; $i++) {
        ?>
          <tr>
            <td class="thStyle bolder"><?php echo $arr1[$i-1]; ?></td>
            <td class="content">3</td>
            <td class="content">13</td>
            <td class="content">คน</td>
          </tr>
        <?php } ?>
      </body>
    </table>
    </div>
    <div class="col-xs-4">
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
      <tbody>
        <tr>
          <td class="thStyle bolder">รวม(ทุกระดับชั้น)</td>
          <td class="thStyle bolder">ชาย</td>
          <td class="thStyle bolder">หญิง</td>
          <td class="thStyle bolder">HAND</td>
        </tr>
        <tr>
          <td class="thStyle bolder">( ร้อยละ )</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">จำนวน</td>
          <td class="thStyle bolder">คน</td>
        </tr>
        <?php
          $arr1 = array('80 ขึ้นไป','70','60','50','40 ลงมา');
          for ($i=1; $i <=3 ; $i++) {
        ?>
          <tr>
            <td class="thStyle bolder"><?php echo $arr1[$i-1]; ?></td>
            <td class="content">3</td>
            <td class="content">13</td>
            <td class="content">คน</td>
          </tr>
        <?php } ?>
      </body>
    </table>
    </div>
  </div>

</html>
<?php
include("../inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 100);
  });
</script>
