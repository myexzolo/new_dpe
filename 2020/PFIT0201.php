<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<style>
  th {
    text-align: center !important;
  }
</style>

<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ฐานการทดสอบ
        <small>PFIT0201</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-list-ul"></i> ข้อมูลหลัก</a></li>
        <li class="active">ฐานการทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">รายการฐานการทดสอบ</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-primary" onclick="searchData('','','')" style="width:80px;">เพิ่ม</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="table" class="table table-bordered table-striped"
                         data-toggle="table"
                         data-toolbar="#toolbar"
                         data-url="ajax/PFIT0201/show.php"
                         data-pagination="true"
                         data-page-size = 20
                         data-page-list= "[20, 50, 100, ALL]"
                         data-search="true"
                         data-flat="true"
                         data-show-refresh="true"
                         >
                      <thead>
                          <tr>
                              <th data-sortable="true" data-formatter="runNo" data-align="center">ลำดับ</th>
                              <th data-sortable="true" data-field="test_code" data-align="center">รหัสฐาน</th>
                              <th data-sortable="true" data-field="test_name" data-align="left">ชื่อฐานการทดสอบ</th>
                              <th data-sortable="true" data-formatter="getObjectStr" data-align="left">องค์ประกอบที่ต้องการวัด</th>
                              <th data-sortable="true" data-formatter="getAgeRange" data-align="center">ช่วงอายุ</th>
                              <th data-sortable="true" data-formatter="getStatus" data-align="center">สถานะ</th>
                              <th data-field="operate" data-formatter="operatePDF" data-align="center">QR</th>
                              <th data-field="operate" data-formatter="operateOpen" data-align="center">ดู</th>
                              <th data-field="operate" data-formatter="operateEdit" data-align="center">แก้ไข</th>
                              <th data-field="operate" data-formatter="operateDelete" data-align="center">ลบ</th>
                          </tr>
                      </thead>
                  </table>
                </div>
          </div>
        </div>
      </div>
  <!-- /.row -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">ฐานการทดสอบ</h4>
        </div>
        <form id="formData" data-smk-icon="glyphicon-remove-sign" novalidate >
        <!-- <form id="formData" data-smk-icon="glyphicon-remove-sign" novalidate method="post" action="ajax/PFIT0201/manage.php"> -->
          <div class="modal-body" id="formShow">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
            <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  </section>
    <!-- /.content -->

  <!-- Modal -->

  <!-- <textarea name="content_data" id="content_data" ></textarea> -->
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>
 <script src="ckeditor/ckeditor.js"></script>
 <script src="js/PFIT0201.js"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
