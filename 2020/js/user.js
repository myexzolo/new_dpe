function randomPass(){
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!&#@";
  var pass = "";
  var maxs = 10;
  for(var x=0; x<=maxs; x++){
    var i = Math.floor(Math.random()*chars.length);
    pass += chars.charAt(i);
  }
  return pass;
}

function showForm(value){
  $.post("ajax/user/formUser.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}

function exportUser(id){
  var url = "exportUser.php?id="+id;
  gotoPage(url);
}

function showFormEdit(value,typeEdit){
  $.post("ajax/user/formUser.php",{value:value,typeEdit:typeEdit})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}

function showPage(){
  $.post("ajax/user/showUser.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function generatePass(){
  $('.pass').val(randomPass());
}

function removeUser(value){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/user/delUser.php",{value:value})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPage();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$("#panel2").smkPanel({
  hide: 'full,remove'
});
showPage();



$('#formAddUser').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddUser').smkValidate()) {
    var flag = true;
    if(!$.smkEqualPass('#user_password', '#user_password_comfirm')) {
      // Code here
      //$.smkAddError('#user_password', "xxxxx"));
      flag = false;
    }
    if(flag)
    {
      $.ajax({
          url: 'ajax/user/manageUser.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formAddUser').smkClear();
          showPage();
          $.smkAlert({text: data.message,type: data.status});
          $('#myModal').modal('toggle');
        }, 1000);
      });
    }

  }
});
