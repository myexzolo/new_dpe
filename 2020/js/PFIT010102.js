function operateUser(value, row, index) {
    return [
        '<a href="PFIT010101.php">',
        '<span class="fa fa-user-plus"></span>',
        '</a>'
    ].join('');
}
function operateSave(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'VIEW\',\''+row.project_name+'\')""',
        '<span class="fa fa-floppy-o"></span>',
        '</a>'
    ].join('');
}

function operateList(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\''+row.person_number+'\')">',
        '<span class="fa fa-list"></span>',
        '</a>'
    ].join('');
}

function operateprint(value, row, index) {
    if(row.status_test == 2){
      var url = "report_result.php?code="+row.project_code+"&id="+row.person_number;
      return [
          '<a onclick=\"postURLV2(\''+url+'\')\">',
          '<span class="glyphicon glyphicon-print"></span>',
          '</a>'
      ].join('');
    }else{
      return [
          '<a class="isDisabled">',
          '<span class="glyphicon glyphicon-print"></span>',
          '</a>'
      ].join('');
    }
}

function getStatusColor(value, row, index) {
    var color = "";
    if(row.status_test == 0 || row.status_test == ""){
      color = 'red';
    }else if(row.status_test == 1){
      color = '#FFD100';
    }else if(row.status_test == 2){
      color = 'green';
    }
    return [
        '<span class="fa fa-circle" style="color:'+color+';font-size:20px;"></span>'
    ].join('');
}


function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}

function getDateToTH(value, row, index){
  var date =  row.date_of_birth.split("-");
  var dob  = date[2] + '/' + date[1] + '/' +  (parseInt(date[0]) + 543);
  return [dob].join('');
}

function operateExport(value, row, index) {
    return [
        '<a target="_blank" href="qrcode.php" >',
        '<span class="glyphicon glyphicon-save"></span>&nbsp;',
        '</a>'
    ].join('');
}

function disableResult(id,obj,classN){
  $('#'+id).val("");
  $('#'+id).smkClear();

  if(classN == 1){
    if(obj.checked){
       $('.'+id).val('');
       $('.'+id).prop('required', false);
       $('.'+id).prop('readonly', true);
    }else{
       $('.'+id).prop('required', true);
       $('.'+id).prop('readonly', false);
    }
  }else{
    if(obj.checked){
       $('#'+id).val('');
       $('#'+id).prop('required', false);
       $('#'+id).prop('readonly', true);

    }else{
       $('#'+id).prop('required', true);
       $('#'+id).prop('readonly', false);
    }
  }


}

function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function getGender(value, row, index){
  var status = row.person_gender;
  var str = "ชาย";
  if(status == 'F'){
    str = "หญิง";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function openModal(projectCode,projectName){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,'ADD','');
}

function search(){
  var code = $('#projectCode').val();
  var id = $('#personId').val();
  var name = $('#personName').val();
  var lname = $('#personLname').val();
  var urlSearch = "ajax/PFIT010102/show.php?code="+code+"&&id="+id+"&&name="+name+"&&lname="+lname;
  //alert(urlSearch);
  $('#table').bootstrapTable('refresh', {
      url: urlSearch
  });
}

function searchData(projectCode,personNumber ){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,personNumber);
}

function formShows(projectCode,personNumber){
  $.get("ajax/PFIT010102/form.php?projectCode="+projectCode+"&personNumber="+personNumber)
  .done(function( data ) {
      $("#formShow").html(data);
  });

}

function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT010101/delete.php",{code:code})
        .done(function( data ) {
          $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

function checkMaxmin(id,min,max){
  var ids = $('#'+id).val();
  var re  = $('#re_'+id).val();
  if(re == ''){
    if(parseInt(min) != 0 && parseInt(max) != 0){
      if(parseInt(ids) < parseInt(min) || parseInt(ids) > parseInt(max) && parseInt(min) != 'null' ){
          $('#re_'+id).val(1);
          $.smkConfirm({
            text:'ดำเนินการต่อ ค่าอยู่ระหว่าง '+min+' ถึง '+max,
            accept:'ใช่',
            cancel:'ยกเลิก'
          },function(res){
            if (!res) {
              $('#'+id).val("");
              $('#re_'+id).val('');
            }
          });
      }
    }
  }
}

$('#formDataSave').on('submit', function(event) {
  event.preventDefault();
  if ($('#formDataSave').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT010102/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formDataSave').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

$('#formSearch').on('submit', function(event) {
  event.preventDefault();
  if ($('#formSearch').smkValidate()) {
    search();
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
