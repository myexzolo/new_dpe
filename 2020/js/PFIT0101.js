function operateUser(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.PROJECT_CODE+'\',\'VIEW\',\''+row.project_name+'\')" >',
        '<span class="fa fa-user-plus"></span>',
        '</a>'
    ].join('');
}

function operateGoto(value, row, index) {
  return [
      '<a onclick="postURL(\'PFIT010101.php?project_code='+row.project_code+'&project_name='+row.project_name+'&project_type='+row.project_type+'\')" >',
      '<span class="fa fa-user-plus"></span>',
      '</a>'
  ].join('');
}



function operateSave(value, row, index) {
  return [
      '<a onclick="postURL(\'PFIT010102.php?project_code='+row.project_code+'&project_name='+row.project_name+'&project_type='+row.project_type+'\')" >',
      '<span class="fa fa-floppy-o"></span>',
      '</a>'
  ].join('');
}


function operateExport(value, row, index) {
  return [
      '<a onclick="postURL(\'PFIT010103.php?project_code='+row.project_code+'&project_name='+row.project_name+'&project_type='+row.project_type+'\')" >',
      '<span class="fa fa-file-text-o"></span>',
      '</a>'
  ].join('');
}


function postURL(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function getDateProject(value, row, index){
   var startDate  = dateDMY(row.start_date);
   var endDate    = dateDMY(row.end_date);
   return [ startDate + " - " + endDate].join('');
}


function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}
function runNo(value, row, index) {
  return [index+1].join('');
}
// function operateDelete(value, row, index) {
//     return [
//         '<a onclick="formDel(\''+row.PROJECT_CODE+'\',\''+row.PROJECT_NAME+'\')" ',
//         '<span class="glyphicon glyphicon-trash"></span>',
//         '</a>'
//     ].join('');
// }
function searchData(no,status,name){
  $('#myModal').modal({backdrop:'static'});
  formShows(no,status,name);
}
function formShows(code,status,name){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT0101/form.php?code="+code+"&status="+status+"&nameCategory="+name)
  .done(function( data ) {
      $("#formShow").html(data);
      // CKEDITOR.replace( 'content_data',{
      //   height: '200px'
      // } );
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT0101/delete.php",{code:code})
        .done(function( data ) {
          $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();

  if ($('#formData').smkValidate()) {
    // var id = $('#content_data').val(CKEDITOR.instances['content_data'].getData());
    console.log(id);
    $.ajax({
        url: 'ajax/PFIT0101/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
