function setDataStudent(){

  if($("#person_type").val() == 1){
    $('#typeStudent').css('display', 'block');
  }else{
    $('#typeStudent').css('display', 'none');
  }
}

function readSmartCard(){
  var ipClient = $('#ipClient').val();

  $.get("ajax/PFIT010101/readSmartCard.php?ipClient="+ipClient)
    .done(function( data ) {
      //alert(data.person_number);
      if(data.person_number != "" && data.person_number != null){
        var dob = dateThDMY(data.date_of_birth);
        $('#person_number').val(data.person_number);
        $('#person_name').val(data.person_name);
        $('#person_lname').val(data.person_lname);
        $('#date_of_birth').val(dob);
        if(data.person_gender == "M"){
          $("#gender_m").prop("checked", true);
        }else{
          $("#gender_f").prop("checked", true);
        }
        $('#person_number').smkValidate();
        $('#person_name').smkValidate();
        $('#person_lname').smkValidate();
        $('#date_of_birth').smkValidate();
        $('#gender_m').smkValidate();
        $('#gender_f').smkValidate();
      }else{
        $.smkAlert({text: 'กรุณาเสียบบัตรประชาชน', type:'warning'});
      }
  });
}

function exportExcel(project_type)
{
    //postURL_blank('template/person_template.xlsx');
  $('#myModal3').modal({backdrop:'static'});

}

function loadExcel(url)
{
  postURL_blank(url);
  $('#myModal3').modal('toggle');
}


function operateUser(value, row, index) {
    return [
        '<a href="PFIT010101.php">',
        '<span class="fa fa-user-plus"></span>',
        '</a>'
    ].join('');
}
function operateSave(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'VIEW\',\''+row.project_name+'\')""',
        '<span class="fa fa-floppy-o"></span>',
        '</a>'
    ].join('');
}

function operateDEl(value, row, index) {
    return [
        '<a onclick="delPerson(\''+row.person_number+'\',\''+row.person_name+'\')""',
        '<span class="fa fa-trash-o"></span>',
        '</a>'
    ].join('');
}

function getdateUploadTH(value, row, index){
  var date  = row.date_upload;
  var str   =  dateThDMYHIS(date);
  return [str].join('');
}

function delPerson(id,name){
  $.smkConfirm({
    text:'ต้องการลบ '+name+' ใช่ไหม',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT010101/deletePerson.php",{code:id})
        .done(function( data ) {

          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
            $('#table').bootstrapTable('refresh');
          }, 1000);

      });
    }
  });

}


function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}


function operateEdit(value, row, index) {

    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}


function operateExport(value, row, index) {
    var pram = '?person_number='+row.person_number+'&project_code='+row.project_code;
    return [
        '<a onclick="postURL_blank(\'qrcode.php'+pram+'\')" >',
        '<span class="fa fa-qrcode"></span>&nbsp;',
        '</a>',
        '<a onclick="postURL_blank(\'print.php'+pram+'\')">',
        ' <span class="fa fa-print"> </span>',
        '</a>'
    ].join('');
}

function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}



function getDateToTH(value, row, index){
  var date =  row.date_of_birth.split("-");
  var dob  = date[2] + '/' + date[1] + '/' +  (parseInt(date[0]) + 543);
  return [dob].join('');
}


function getGender(value, row, index){
  var status = row.person_gender;
  //alert(status);
  var str = "ชาย";
  if(status == 'F'){
    str = "หญิง";
  }
  return [str].join('');
}

function getStatusUpload(value, row, index) {
  var str = "สมบูรณ์";
  if(row.status_upload == "F"){
    str = "ผิดพลาด";
  }

  return [str].join('');
}

function runNo(value, row, index) {
  var str = "";
  if(row){
    str = index+1;
  }

  return [str].join('');
}
// function operateDelete(value, row, index) {
//     return [
//         '<a onclick="formDel(\''+row.PROJECT_CODE+'\',\''+row.PROJECT_NAME+'\')" ',
//         '<span class="glyphicon glyphicon-trash"></span>',
//         '</a>'
//     ].join('');
// }

function openModal(projectCode,projectName,projectType){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,'ADD','',projectType);
}

function upload(projectCode,projectName,projectType){
  $('#myModal2').modal({backdrop:'static'});
  formShowsUpload(projectCode,'UPLOAD','',projectType,projectName);
}

function formShowsUpload(projectCode,status,code,projectType,projectName){
  var pam = "?code="+code+"&status="+status+"&projectCode="+projectCode+"&projectType="+projectType+"&projectName="+projectName;
  $.get("ajax/PFIT010101/formUpload.php"+pam)
  .done(function( data ) {
      $("#formShow2").html(data);
      $('#tableUpload').bootstrapTable();
      $('#typeStudent').css('display', 'none');
  });

}


function searchData(projectCode ,status ,code,projectType){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,status,code,projectType);
}

function formShows(projectCode,status,code,projectType){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  var pam = "?code="+code+"&status="+status+"&projectCode="+projectCode+"&projectType="+projectType;
  $.get("ajax/PFIT010101/form.php"+pam)
  .done(function( data ) {
      $("#formShow").html(data);
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT010101/delete.php",{code:code})
        .done(function( data ) {
          $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();
  $('#loading').removeClass("none");
  $('#loading').addClass('loading');
  //alert("xxxx");
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT010101/manageUpload2.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        //$('#formDataUpload').smkClear();
        $('#tableUpload').bootstrapTable('refresh');
        $('#table').bootstrapTable('refresh');
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
    }).fail(function (jqXHR, exception) {
        alert("ไม่สามารถนำเข้าได้");
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
    });
  }else{
    $('#loading').removeClass('loading');
    $('#loading').addClass('none');
  }
});




$('#formData').on('submit', function(event) {
  event.preventDefault();

  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT010101/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
