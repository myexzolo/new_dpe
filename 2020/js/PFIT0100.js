function operateOpen(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'VIEW\',\''+row.project_name+'\')" >',
        '<span class="fa fa-search"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.project_name+'\')" >',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}

function operateExport(value, row, index) {
    return [
        '<a href="exportProject.php?code='+row.project_code+'">',
        '<span class="fa fa-sign-out" style="font-size:17px;"></span>',
        '</a>'
    ].join('');
}

function operateDelete(value, row, index) {
    return [
        '<a onclick="formDel(\''+row.project_code+'\',\''+row.project_name+'\')" ',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}

function operateSync(value, row, index) {
    var con = $('#connectInternet').val();
    var disabled = '',onclicks = '';
    if(con != 'C'){
      disabled = 'disabled';
    }else{
      onclicks = "onclick=\"syncData(\'"+row.project_code+"\',\'"+row.project_name+"\')\"";
    }
    return [
      '<button '+onclicks+' '+disabled+' class="btn btn-link btn-xs">',
        '<span class="fa fa-cloud-upload" style="font-size:17px;"></span>',
      '</button>'
    ].join('');
}

function getDateProject(value, row, index){
   var startDate  = dateThDMY(row.start_date);
   var endDate    = dateThDMY(row.end_date);
   return [ startDate + " - " + endDate].join('');
}

function syncData(code,name){
  $('#loading').removeClass('none');
  var url = $('#serverName').val() + "/ajax/PFIT0100/syncData.php";
  $.post("ajax/PFIT0100/syncProject.php",{code:code})
    .done(function( data ) {
      if(data != ""){
        var datajson = JSON.stringify(data.data);
        $.ajax({
           url: url,
           type: "POST",
           data: datajson,
           processData: false,
           contentType: "application/json; charset=utf-8",
           async: true,
           crossDomain: true,
           dataType: 'json',
            success: function(result){
              //console.log(result);
              $('#loading').addClass('none');
               if(result.status == "success"){
                 $.smkAlert({text: 'Sync ข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
               }else{
                 $.smkAlert({text: result.message,type: result.status});
               }
               $('#table').bootstrapTable('refresh');
            },
            error(){
                alert("err");
                console.log('Error');
            }
        });
      }else{
          $.smkAlert({text: 'ไม่พบข้อมูล',type: data.status});
      }
  });
}



function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}


function closeModel(){
  $('#table').bootstrapTable('refresh');
  $('#myModal2').modal('toggle');
}

function upload(){
  $('#myModal2').modal({backdrop:'static'});
  $.get("ajax/PFIT0100/formUpload.php?status=UPLOAD")
  .done(function( data ) {
      $("#formShow2").html(data);
      $('#tableUpload').bootstrapTable();
  });
}

function searchData(no,status,name){
  $('#myModal').modal({backdrop:'static'});
  formShows(no,status,name);
}
function formShows(code,status,name){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT0100/form.php?code="+code+"&status="+status+"&nameCategory="+name)
  .done(function( data ) {
      $("#formShow").html(data);
      // CKEDITOR.replace( 'content_data',{
      //   height: '200px'
      // } );
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT0100/delete.php",{code:code})
        .done(function( data ) {
          if(data.status == "success"){
            $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          }else{
            $.smkAlert({text: data.message,type: data.status});
          }
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();

  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT0100/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();

  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT0100/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formDataUpload').smkClear();
        $.smkAlert({text: data.message+' success['+data.success+'] fail['+data.fail+'] total['+data.total+']',type: data.status});

        $('#tableUpload').bootstrapTable('refresh', {url: 'ajax/PFIT010101/showHisUpload.php?code='+data.project_code+'&type_file='+data.type_file});

      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});




function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
