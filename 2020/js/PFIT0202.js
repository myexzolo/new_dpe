function operateOpen(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.test_code+'\',\'VIEW\',\''+row.test_criteria_code+'\',\''+row.category_criteria_code+'\')" >',
        '<span class="fa fa-search"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.test_code+'\',\'EDIT\',\''+row.test_criteria_code+'\',\''+row.category_criteria_code+'\')" >',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}

function getObjectStr(value, row, index){
  return [getTestOpjective(row.test_opjective)].join('');
}


showForm();

function showForm(){
  var test_code =  $('#test_code').val();
  $.post("ajax/PFIT0202/show2.php",{test_code:test_code})
    .done(function( data ) {
      $('#show2').html(data);
  });
}

function operateGoto(value, row, index) {
  return [
      '<a onclick="postURL(\'PFIT020201.php?test_code='+row.test_code+'&test_name='+row.test_name+'&category_criteria_code='+row.category_criteria_code+'\')" >',
      '<span class="fa fa-file-text-o"></span>',
      '</a>'
  ].join('');
}

function operateUpload(value, row, index) {
  return [
      '<a onclick="upload(\''+row.test_code+'\',\''+row.test_name+'\')" >',
      '<span class="fa fa-download"></span>',
      '</a>'
  ].join('');
}

function getStatusUpload(value, row, index) {
  var str = "สมบูรณ์";
  if(row.status_upload == "F"){
    str = "ผิดพลาด";
  }

  return [str].join('');
}


function upload(){
  $('#myModal2').modal({backdrop:'static'});
  formShowsUpload();
}

function formShowsUpload(){
  $.get("ajax/PFIT0202/formUpload.php")
  .done(function( data ) {
      $("#formShow2").html(data);
      $('#tableUpload').bootstrapTable();
  });

}


$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();;
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT0202/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        //$('#formDataUpload').smkClear();
        $('#tableUpload').bootstrapTable('refresh');
    }).fail(function (jqXHR, exception) {
        alert("ไม่สามารถนำเข้าได้");
    });
  }
});



function postURL(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function getdateUploadTH(value, row, index){
  var date  = row.date_upload;
  var str   =  dateThDMYHIS(date);
  return [str].join('');
}

function getGenderStr(value, row, index){
  var status = row.gender;
  var str = "ชาย";
  if(status == 'F'){
    str = "หญิง";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function operateDelete(value, row, index) {
    return [
        '<a onclick="formDel(\''+row.test_criteria_code+'\')" ',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}

function operateCopy(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.test_code+'\',\'COPY\',\''+row.test_criteria_code+'\',\''+row.category_criteria_code+'\')" >',
        '<span class="fa fa-copy"></span>',
        '</a>'
    ].join('');
}

function openModal(testCode,categoryCriteriaCode){
  $('#myModal').modal({backdrop:'static'});
  formShows(testCode,'ADD','','',categoryCriteriaCode);
}


function searchData(testCode,status,age,gender,categoryCriteriaCode){
  $('#myModal').modal({backdrop:'static'});
  formShows(testCode,status,age,gender,categoryCriteriaCode);
}

function formDelV2(testCode,age,gender){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      //alert(testCode+","+age+","+gender);
      $.post("ajax/PFIT0202/delete.php",{testCode:testCode,age:age,gender:gender})
        .done(function( data ) {
          if(data.status == "success"){
            $.smkAlert({text: 'ลบข้อมูล เรียบร้อย!! ', type:'success'});
          }else{
            $.smkAlert({text: data.message,type: data.status});
          }
          showForm();
      });
    }
  });
}


function formShows(testCode,status,age,gender,categoryCriteriaCode){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }

  var url = "ajax/PFIT0202/form2.php?testCode="+testCode+"&statusData="+status+"&age="+age+"&gender="+gender+"&categoryCriteriaCode="+categoryCriteriaCode;
  //alert(url);
  //console.log("ajax/PFIT0202/form.php?testCode="+testCode+"&statusData="+status+"&testCriteriaCode="+testCriteriaCode+"&categoryCriteriaCode="+categoryCriteriaCode);
  $.get(url)
  .done(function( data ) {
      $("#formShow").html(data);
  });
}

function formDel(code){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT0202/delete.php",{code:code})
        .done(function( data ) {
          $('#table').bootstrapTable('refresh');
          $.smkAlert({text: 'ลบข้อมูล เรียบร้อย!! ', type:'success'});
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();
  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT0202/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      $('#formData').smkClear();
      $('#myModal').modal('toggle');
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $.smkAlert({text: data.message,type: data.status});
        showForm();
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });
  }
});
