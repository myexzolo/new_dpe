$( document ).ajaxStart(function() {
  $(".loadingImg").removeClass('none');
});

$( document ).ajaxStop(function() {
  $(".loadingImg").addClass('none');
});

function gotoPage(url){
  window.location.href = url;
}

function postURLV2(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.target="_blank";
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function dateThToEn(date,format,delimiter)
{
    var formatLowerCase=format.toLowerCase();
    var formatItems=formatLowerCase.split(delimiter);
    var dateItems=date.split(delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yyyy");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;
    var dateth = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    var yearth = dateth.getFullYear()
    if( yearth > 2450){
      yearth -= 543;
    }

    var dateEN = yearth + "/" + ("0" + (dateth.getMonth() +　1)).slice(-2) + "/" + ("0" + (dateth.getDate())).slice(-2);
    return dateEN;
}

function postURL(url, multipart) {
  //alert("url:" + url);
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function getTestOpjective(id){
  var str = "";
  if(id =="1" ){ str = "ความแข็งแรงและความอดทนของกล้ามเนื้อแขน";}
  else if(id =="2" ){str = "ความแข็งแรงและความอดทนของกล้ามเนื้อขา";}
  else if(id =="3" ){str = "ความแข็งแรงและความอดทนของกล้ามเนื้อหน้าท้อง";}
  else if(id =="4" ){str = "ความอ่อนตัวของกล้ามเนื้อต้นขาด้านหลัง/หลังล่าง";}
  else if(id =="5" ){str = "ความอ่อนตัวของกล้ามเนื้อไหล่/แขน";}
  else if(id =="6" ){str = "ความอ่อนตัวของกล้ามเนื้อลำตัว";}
  else if(id =="7" ){str = "ความอดทนของระบบหัวใจและไหลเวียนเลือด";}
  else if(id =="8" ){str = "ความแคล่วคล่องว่องไว";}
  else if(id =="9" ){str = "ความเร็ว";}
  else if(id =="10"){str = "ปฏิกิริยาตอบสนอง";}
  else if(id =="11"){str = "การทรงตัว";}
  else if(id =="12"){str = "กำลัง/พลังกล้ามเนื้อขา";}
  else if(id =="13"){str = "องค์ประกอบของร่างกาย";}
  else if(id =="14"){str = "น้ำหนักของกระดูก Bone Mass (Kg)";}
  else if(id =="15"){str = "ระดับไขมันช่องท้อง Visceral Fat (Ratio)";}
  else if(id =="16"){str = "การเผาพลาญพลังงานของร่างกาย BMR (Kcal)";}
  else if(id =="17"){str = "เปรียบเทียบอายุการเผาพลาญ Matabolic Age (Year)";}
  else if(id =="18"){str = "ดัชนีความหนาแน่นในร่างกาย BMI (Kg/m<sup>2</sup>)";}
  else if(id =="19"){str = "การวัดสัดส่วนรอบเอว/รอบสะโพก WHR";}

  return str;
}

function dateDMY(d){
  var today = new Date(d);
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  if(dd<10){
      dd='0'+dd;
  }
  if(mm<10){
      mm='0'+mm;
  }
  var today = dd+'/'+mm+'/'+yyyy;
  return today;
}

function dateThDMY(d){
  var date  = new Date(d);
  var dd    = ("0" + (date.getDate())).slice(-2);
  var mm    = ("0" + (date.getMonth() +　1)).slice(-2);
  var yyyy  = date.getFullYear();
  if(yyyy<2450){
      yyyy += 543;
  }

  var today = dd+'/'+mm+'/'+yyyy;
  return today;
}


function dateThDMYHIS(d){
  var date  = new Date(d);
  var dd    = ("0" + (date.getDate())).slice(-2);
  var mm    = ("0" + (date.getMonth() +　1)).slice(-2);
  var yyyy  = date.getFullYear();
  if(yyyy<2450){
      yyyy += 543;
  }

  var dateTh = dd+'/'+mm+'/'+yyyy+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
  return dateTh;
}
