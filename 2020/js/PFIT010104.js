function operateAdd(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="glyphicon glyphicon-plus-sign"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="glyphicon glyphicon-list-alt"></span>',
        '</a>'
    ].join('');
}


function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function getGender(value, row, index){
  var status = row.person_gender;
  var str = "ชาย";
  if(status == 'F'){
    str = "หญิง";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}


function openModal(projectCode,projectName){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,'ADD','');
}


function searchData(projectCode ,status ,code){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,status,code);
}

function formShows(projectCode,status,code){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT010103/form.php?code="+code+"&status="+status+"&projectCode="+projectCode)
  .done(function( data ) {
      $("#formShow").html(data);
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT010103/delete.php",{code:code})
        .done(function( data ) {
          $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();

  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT010103/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
