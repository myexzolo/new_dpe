<?php

  session_start();
  include('conf/connect.php');

  $name  = "ExpData.txt";
  $filName = 'text/'.$name;
  $objWrite = fopen($filName, "w");

  $data = "";


  $sql = "SELECT CATEGORY_CRITERIA_CODE, CATEGORY_CRITERIA_NAME,DETAIL,STATUS
          FROM PFIT_T_CATEGORY_CRITERIA";
  $query  = DbQuery($sql,null);
  $query  = iconv("UTF-8","TIS-620",$query);
  $query  = base64_encode($query);
  $data   .= $query."\\";

  $sql = "SELECT CATEGORY_CRITERIA_DETAIL_CODE, CATEGORY_CRITERIA_CODE, CATEGORY_CRITERIA_DETAIL_NAME, CATEGORY_CRITERIA_DETAIL_VALUE, STATUS
          FROM PFIT_T_CAT_CRITERIA_DETAIL";
  $query  = DbQuery($sql,null);
  $query  = iconv("UTF-8","TIS-620",$query);
  $query  = base64_encode($query);
  $data   .= $query."\\";

  $sql = "SELECT TEST_CODE, TEST_NAME, TEST_OPJECTIVE, TEST_DETAIL, CATEGORY_CRITERIA_CODE, TEST_UNIT, TEST_MIN, TEST_MAX, TEST_CALCULATOR, STATUS, TEST_TYPE
          FROM PFIT_T_TEST";
  $query  = DbQuery($sql,null);
  $query  = iconv("UTF-8","TIS-620",$query);
  $query  = base64_encode($query);
  $data   .= $query."\\";

  $sql = "SELECT TEST_CRITERIA_CODE, TEST_CODE, AGE, GENDER, MIN, MAX, CATEGORY_CRITERIA_DETAIL_CODE, TYPE_TEST, TEST_SUGGEST,TEST_SUGGEST_EN, STATUS
          FROM PFIT_T_TEST_CRITERIA";
  $query  = DbQuery($sql,null);
  $query  = iconv("UTF-8","TIS-620",$query);
  $query  = base64_encode($query);
  $data   .= $query;


  fwrite($objWrite, "$data");
  fclose($objWrite);
  header('Content-Disposition: attachment; filename="'.$name.'"');
  readfile($filName);
?>
