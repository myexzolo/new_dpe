<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php
  include("inc/sidebar.php");
  if(isset($_POST['test_code'])){
    echo $test_code         = $_POST['test_code'];
    $test_name              = $_POST['test_name'];
    $category_criteria_code = $_POST['category_criteria_code'];
  }else{
    exit("<script>window.location='PFIT0202.php';</script>");
  }

  ?>
  <style>
    th {
      text-align: center !important;
    }
  </style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        เกณฑ์การทดสอบ
        <small>PFIT020201</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-list-ul"></i> ข้อมูลหลัก</a></li>
        <li><a href="PFIT0202.php">เกณฑ์การทดสอบ</a></li>
        <li class="active">ข้อมูลเกณฑ์การทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">เกณฑ์การทดสอบ : <?= $test_name ?></h3>
                  <div class="box-tools pull-right">
                    <input type="hidden" id="test_code" value="<?= $test_code?>">
                    <button class="btn btn-primary" onclick="openModal('<?=$test_code?>','<?=$category_criteria_code?>')" style="width:80px;">เพิ่ม</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="show2"></div>
                <div class="box-footer with-border">
                  <div class="box-tools pull-right">
                    <button class="btn btn-success" onclick="gotoPage('PFIT0202.php')" style="width:80px;">ย้อนกลับ</button>
                  </div>
                </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เกณฑ์การทดสอบ</h4>
            </div>
            <form id="formData" data-smk-icon="glyphicon-remove-sign" novalidate>
              <div class="modal-body" id="formShow">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- <textarea name="content_data" id="content_data" ></textarea> -->

  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>

 <script src="ckeditor/ckeditor.js"></script>
 <script src="js/PFIT0202.js"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
