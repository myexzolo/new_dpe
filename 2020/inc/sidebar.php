<?php
include('auth_admin.php');

  $role_List = "";
  $page_List = "";

  $menu = "";

  $admin_user = $_SESSION['user_id'];
  //$admin_user = 1;

  $sql = "SELECT * FROM t_user WHERE user_id = ?";

  $ParamData = array($admin_user);
  $query = DbQuery($sql,$ParamData);


  //print_r($query);

  $row = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];


  if(isset($data[0]['role_list'])){
    $role_List = substr($data[0]['role_list'] , 1);
  }
  $sql = "SELECT * FROM t_role WHERE role_id in ($role_List) and is_active = 'Y' ";


  $queryRole = DbQuery($sql,null);
  // echo "=======================================>";
  // print_r($queryRole);
  // echo "<br>";
  $rowRole  = json_decode($queryRole, true);
  $numRole  = $rowRole['dataCount'];
  $dataRole = $rowRole['data'];

  for ($i=0; $i < $numRole ; $i++) {
    $pl       = $dataRole[$i]['page_list'];
    $page_List .=  $pl;

  }



  if(!empty($page_List)){
    // $page_List = substr($page_List , 1);

    $arrPage = array_unique(explode(",",$page_List));
    sort($arrPage);
    $arrPage = implode(",",$arrPage);

    if($arrPage[0] == ','){
      $arrPage = substr($arrPage,1);
    }
  }



  $sql = "SELECT p.module_id, m.module_order,m.module_name,m.module_icon, count(p.page_id) as num
          FROM t_page p INNER JOIN t_module m
              ON p.module_id = m.module_id
          where p.page_id in ($arrPage)
              and p.is_active = 'Y' and m.is_active = 'Y' and m.module_type = '1'
          GROUP BY p.module_id , m.module_order, m.module_name, m.module_icon
          order by m.module_order asc";

  // $queryModule  = mysqli_query($conn,$sql);
  // $numModule    = mysqli_num_rows($queryModule);

  //echo "=======================================>".$sql;

  $queryModule  = DbQuery($sql,null);

  $rowModule  = json_decode($queryModule, true);
  $numModule  = $rowModule['dataCount'];
  $dataModule = $rowModule['data'];


  for ($i=0; $i < $numModule ; $i++) {
    $numMod          = $dataModule[$i]['num'];
    $module_id       = $dataModule[$i]['module_id'];
    $module_name     = $dataModule[$i]['module_name'];
    $module_icon     = $dataModule[$i]['module_icon'];


    $sql = "SELECT * FROM t_page WHERE module_id = $module_id and page_id in ($page_List) and is_active = 'Y' order by page_seq";
    // $queryPage  = mysqli_query($conn,$sql);
    // $numPage    = mysqli_num_rows($queryPage);
    //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$sql;
    $queryPage = DbQuery($sql,null);
    $rowPage  = json_decode($queryPage, true);
    $numPage  = $rowPage['dataCount'];
    $dataPage = $rowPage['data'];

    $page_path = "";
    $active    = "";
    $pagePath = $_SERVER['SCRIPT_NAME'];

    if($numPage == 1){
      $page_id       = $dataPage[0]['page_id'];
      $page_name     = $dataPage[0]['page_name'];
      $page_path     = $dataPage[0]['page_path'];
      //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$i.":".strstr($_SERVER['SCRIPT_NAME'], $page_path).">>> ".$_SERVER['SCRIPT_NAME']."<>".$page_path;
      $pos = strrpos($pagePath, $page_path);
      if($pos !== false){
          $active =  'active';
      }
      //$nav = '?MN='.base64_encode($module_name).'&PN=&IC='.base64_encode('fa '.$module_icon);
      $menu  .= "<li class='$active'>";
      $menu  .= "<a href='$page_path.php' class='txtMenu'>";
      $menu  .= "<i class='fa $module_icon'></i>";
      $menu  .= "<span>$module_name</span></a></li>";
    }else{
      $menuOpen = "";
      for ($x=0; $x < $numPage ; $x++)
      {
        $page_path = $dataPage[$x]['page_path'];
        $pos       = strrpos($pagePath, $page_path);
        if($pos !== false)
        {
            $menuOpen = "active menu-open";
            break;
        }
      }

      $menu  .= "<li class='treeview $menuOpen' >";
      $menu  .= "<a href='#' class='txtMenu'>";
      $menu  .= "<i class='fa $module_icon'></i><span>$module_name</span>";
      $menu  .= "<span class='pull-right-container'>";
      $menu  .= "<i class='fa fa-angle-left pull-right'></i></span></a>";
      $menu  .= "<ul class='treeview-menu'>";

      for ($x=0; $x < $numPage ; $x++) {

        $page_id       = $dataPage[$x]['page_id'];
        $page_name     = $dataPage[$x]['page_name'];
        $page_path     = $dataPage[$x]['page_path'];

        $pos = strrpos($pagePath, $page_path);
        //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$x.":".$pagePath.">>> ".$page_path."<<".$pos;
        $active = "";
        if($pos !== false)
        {
            $active =  'active';
        }
        //$nav = '?MN='.base64_encode($module_name).'&PN='.base64_encode($page_name).'&IC='.base64_encode('fa '.$module_icon);
        $menu  .= "<li class='$active'><a href='$page_path.php' class='txtMenu txtMenuBg'><i class='fa fa-circle-o'></i>$page_name</a></li>";
      }
      $menu  .= "</ul></li>";
    }
  }
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img width="150" src="<?= $data[0]['user_img']; ?>" class="img-circle" onerror="this.onerror='';this.src='images/man.png'" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $data[0]['user_name']; ?></p>
          <?php
            if($_SESSION['TYPE_CONN'] == 2){
              echo "<a href='#'><i class='fa fa-circle text-red'></i>Offline</a>";
            }else{
              echo "<a href='#'><i class='fa fa-circle text-success'></i>Online</a>";
            }
          ?>

        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
          <?= $menu ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
