<?php
session_start();
include('conf/connect.php');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบการจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย</title>

    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/main.css">
    <link rel="stylesheet" href="css/main.css">
    <!-- Smoke -->
    <link rel="stylesheet" href="css/smoke.css">

    <!-- lightbox -->
    <link rel="stylesheet" href="css/lightbox.css" >

    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="Ionicons/css/ionicons.min.css">

    <!-- fullCalendar -->
    <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.print.min.css" media="print">

    <!-- daterange picker -->
    <link rel="stylesheet" href="bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="iCheck/all.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	  <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">

    <link rel="stylesheet" href="css/bootstrap-toggle.min.css">

    <link rel="stylesheet" href="css/bootstrap-table.min.css">

    <link rel="stylesheet" href="css/paper.css">

    <link rel="stylesheet" href="css/select2.min.css">


    <!-- datepicker -->
  <link rel="stylesheet" href="dist/css/datepicker.css">
  <!-- datetimepicker -->
  <link rel="stylesheet" href="dist/css/bootstrap-datetimepicker.css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <style>
  body {
      font-family: "Kanit";
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%
  }
  html{

  }

  @font-face {
      font-family: "Kanit";
      src: url("fonts/Kanit/Kanit-Light.ttf");
  }

  @font-face {
      font-family: "THSarabun";
      src: url("font/THSarabunNew/THSarabunNew.ttf");
  }

  .number {
      /* font-family: "THSarabun"; */
  }

  .fontKanit {
      font-family: "Kanit";
      src: url("fonts/Kanit/Kanit-Light.ttf");
  }

  .visibility-non{
    visibility: hidden;
  }
  </style>

  <div class="loadingImg none">
    <img src="../../images/loading.svg" alt="Loading..." style="width:70px;">
  </div>
