<style>
.fontKanit {
    font-family: "Kanit";
    src: url("fonts/Kanit/Kanit-Light.ttf");
}
</style>

<header class="main-header mainH" style="background-color:#ff4c00" >
    <!-- Logo -->
    <a href="index.php" class="logo mainHeader" style="color:#f1f1f1">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini fontKanit" ><b>P</b>FIT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg fontKanit" style="font-size:18px">ระบบจัดเก็บผลการทดสอบ</span>
      <hr>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top " style="background-color:#ff4c00" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle mainHeader" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu" style="background-color:#ff4c00">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->

          <li class="dropdown user user-menu">
            <?php
              $user_id = "";
              if(isset($_SESSION['user_id'])){
                $user_id = $_SESSION['user_id'];
              }
              $sql = "SELECT * FROM t_user WHERE user_id = ?";
              //$query = mysqli_query($conn,$sql);
              //$row = mysqli_fetch_assoc($query);

              $ParamData = array($user_id);
              $query = DbQuery($sql,$ParamData);
              //print_r (">>>>>".$query."\n");
              $row  = json_decode($query, true);
              $data = $row['data'];

            ?>
            <a href="#" class="dropdown-toggle mainH" data-toggle="dropdown">
              <img width="160" src="<?= $data[0]['user_img']; ?>" class="user-image" onerror="this.onerror='';this.src='images/man.png'" alt="User Image">
              <span class="hidden-xs"><?= $data[0]['user_name']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img width="160" src="<?= $data[0]['user_img']; ?>" class="img-circle" onerror="this.onerror='';this.src='images/man.png'" alt="User Image">
                <p>
                  Name - <?= $data[0]['user_name']; ?>
                  <small>Member ID. <?= $data[0]['user_login']; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a onclick="logout()" class="btn btn-primary btn-flat">ออกจากระบบ</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>

    </nav>
  </header>
