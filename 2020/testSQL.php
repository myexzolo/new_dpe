<!DOCTYPE html>
<html>

<?php
    include("inc/head.php");
    include("inc/utils.php");

    $sql = "";
    $typeConnect = "";
    if(isset($_POST['type_connect']) && !empty($_POST['type_connect']))
    {
      $typeConnect  = $_POST['type_connect'];
      $sql          = $_POST['sql'];

      $_SESSION['TYPE_CONN']   = $typeConnect;

      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $status     = $json['status'];
      echo "SQL : ".$sql."<br>";
      print_r($query);
    }
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include("inc/header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <form action="testSQL.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="box boxBlack">
                    <div class="box-header with-border">
                      <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group col-md-12">
                              <label class="col-sm-4 control-label">ประเภท Login</label>
                              <div class="col-sm-8">
                                <select name="type_connect" class="form-control" required>
                                  <option value=""></option>
                                  <option value="1">Oracle</option>
                                  <option value="2">Mysql</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group col-md-12">
                              <label class="col-sm-4 control-label">Query</label>
                              <div class="col-sm-8">
                                <textarea  name="sql" rows="4" cols="100"></textarea>
                              </div>
                            </div>
                            <div class="text-center col-md-12">
                              <button type="submit" class="btn btn-success" style="width:80px;">Execute</button>
                            </div>
                        </div>
                      </div>
                    </div>
              </div>
        </div>
      </form>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>

</body>
</html>
