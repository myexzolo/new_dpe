<!DOCTYPE html>
<html>
<?php include("inc/head.php"); ?>
<body class="bgDEP">

  <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login">
          <div align="center"><img src="images/dep_logo2.png"  class="logo-login"></div>
          <h4 style="color:#FFFFFF;text-align:center;font-size:20px;">ระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย</h3>
          <form id="form1" smk-icon="glyphicon-remove" novalidate autocomplete="off" action="ajax/login/checkLogin.php" method="post">
              <div class="form-group">
                <input name="user" id="user" type="text" class="form-control" placeholder="Username" smk-text="กรุณากรอก Username" required>
              </div>
              <div class="form-group">
                <input name="pass" id="pass" type="password" autocomplete="new-password" class="form-control" placeholder="Password"  smk-text="กรุณากรอก Password" required>
              </div>
              <div class="form-group checkbox" style="display:none;">
                <label style="color:#fff">
                  <!-- typeConnect = 1 online  2 = offline-->
                  <input type="checkbox" data-toggle="toggle" id="toggleOffline" value="">
                  <input type="hidden" name="typeConnect" id="typeConnect" value="2">
                  Offline IP Address
                </label>
              </div>
              <div class="form-group" id="offline" style="display:none;">
                <input name="ip" id="ip" type="text" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" class="form-control" placeholder="IP Address">
              </div>

              <button type="submit" class="btn bg-navy btn-block non-radius">LOGIN</button>
          </form>
        </div>
      </div>
    </div>

<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#user").focus();
});


  $(function() {
    $('#toggleOffline').change(function() {
      if($('#toggleOffline').prop('checked')){
        $('#offline').show();
        $("#ip").prop('required',true);
        $('#typeConnect').val(2);

      }else{
        $('#offline').hide();
        $('#ip').val("");
        $("#ip").prop('required',false);
        $('#typeConnect').val("1");
      }
    })
  })
</script>
</body>
</html>
