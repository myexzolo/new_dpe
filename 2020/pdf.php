<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
include 'mpdf/mpdf.php';


ob_start();

?>
<!DOCTYPE html>
<html>

<body>

  <?php
  $sql = "SELECT * FROM pfit_t_test WHERE test_code = '{$_REQUEST['test_code']}'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcode/temp'.DIRECTORY_SEPARATOR;

  //html PNG location prefix
  $PNG_WEB_DIR = 'qrcode/temp/';
  include "qrcode/qrlib.php";

  if (!file_exists($PNG_TEMP_DIR)){
    mkdir($PNG_TEMP_DIR);
  }

  $tetCode = $json['data'][0]['test_code'];
  $tetName = $json['data'][0]['test_name'];
  $filename = $PNG_TEMP_DIR.$tetCode.'.png';
  $CorrectionLevel = 'L';

  $str = "http://sportsci.dpe.go.th/pdf_test.php?test_code=".$tetCode;
  $textQr = "$str";
  $matrixPointSize = 5;
  QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


  ?>
  <table style="width:100%;">
    <tr>
      <td style="width:200px;" align="left"><img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" /></td>
      <td align="center" style="font-size:16pt;"><p><b><?= $tetName ?></b></p></td>
      <td style="width:200px;"></td>
    </tr>
  </table>
  <?php
  echo $test_detail = $json['data'][0]['test_detail'];
  ?>

</body>
</html>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    $mpdf=new mPDF('utf-8');
    $mpdf->autoScriptToLang = false;
    $mpdf->margin_header = 2;
    // $mpdf->SetHeader('รายงานโดย codingthailand | รายงานลูกค้าทั้งหมด | ออกรายงานเมื่อ: '.date('d/m/Y H:i:s'));
    $mpdf->margin_footer = 9;
    // $mpdf->SetFooter('หน้าที่ {PAGENO}');
    // Define a Landscape page size/format by name
    //$mpdf=new mPDF('utf-8', 'A4-L');
    // Define a page size/format by array - page will be 190mm wide x 236mm height
    //$mpdf=new mPDF('utf-8', array(190,236));
    $stylesheet = file_get_contents('bootstrap/css/printpdf.css');
    //$mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    //$mpdf->Output();
    $mpdf->Output(time(),'I');


    exit;
?>
