<?php

require '../../inc/function/connect.php';
require '../lib/mainFunc.php';

$request = $_POST;

$status = 401;
$message = 'functionName is not macth';
$data = array();
$path = 'service/';
if(empty($request['token'])){
  header('Content-Type: application/json');
  exit(response_data($status,'Token Fail',$data));
}else{

  // check token ......

  switch ($request['wsfunction']) {
    case 'getProfile':
      include $path."getProfile.php";
      break;

    case 'getResultInfo':
      include $path."getResultInfo.php";
      break;

    case 'getLineHelpdesk':
      include $path."getLineHelpdesk.php";
      break;

    default:
      header('Content-Type: application/json');
      exit(response_data($status,'Function is not macth',$data));
      break;
  }
}
header("Content-Type: application/json;");
exit(response_data($status,$message,$data));

?>
