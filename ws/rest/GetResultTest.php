<?php

    $status = 401;
    $message = 'Fail';
    $flag = true;

    $con = "";

    $person_number    = isset($request['personalID'])?$request['personalID']:"";
    $person_name      = isset($request['firstName'])?$request['firstName']:"";
    $person_lname     = isset($request['lastName'])?$request['lastName']:"";

    // $person_number  = $_POST['person_number'];
    // $person_name    = $_POST['person_name'];
    // $person_lname   = $_POST['person_lname'];


    if($person_number != "")
    {
        $con .= " and ps.person_number = '$person_number'";
    }

    if($person_name != "")
    {
      $con     .= " and ps.person_name LIKE '%$person_name%'";
    }

    if($person_lname != "")
    {
      $con     .= " and ps.person_lname LIKE '%$person_lname%'";
    }

    if($con == "")
    {
      $con = " and ps.project_code = '1234546789'";
    }

    $sql  = "SELECT ps.project_code,ps.person_number,ps.person_gender,ps.person_name,ps.person_lname,ps.status_test,p.project_name,
             ".getQueryDate('date_of_birth').",ps.wiegth,ps.height,ps.person_type
             FROM pfit_t_person ps,pfit_t_project p  WHERE ps.project_code = p.project_code and ps.status_test = 2 $con
             ORDER BY p.project_code ,ps.date_create desc";
 //echo $sql;

 $query = DbQuery($sql,null);
 $json  = json_decode($query, true);
 $dataCount  = $json['dataCount'];
 $rows       = $json['data'];

 if($dataCount > 0){
   $x = 0;
   foreach ($rows as $key => $value)
   {
       $gender          = $value['person_gender'];
       $person_type     = $value['person_type'];
       $status_test     = $value['status_test'];
       $project_code    = $value['project_code'];
       $person_number   = $value['person_number'];
       $dob             = $value['date_of_birth'];

       $age   = yearBirth($dob);


       $data[$x]['personalID']    = $person_number;
       $data[$x]['firstName']     = $value['person_name'];
       $data[$x]['lastName']      = $value['person_lname'];
       $data[$x]['dateOfBirth']   = $dob;
       $data[$x]['genderCode']    = $gender;
       $data[$x]['wiegth']        = $value['wiegth'];
       $data[$x]['height']        = $value['height'];
       $data[$x]['project_name']  = $value['project_name'];


       $sql = "SELECT r.result, r.result_cal, r.test_criteria_code,cd.category_criteria_detail_name
               FROM pfit_t_result r
               LEFT JOIN pfit_t_test_criteria tc ON r.test_criteria_code = tc.test_criteria_code
               LEFT JOIN pfit_t_cat_criteria_detail cd ON tc.category_criteria_detail_code = cd.category_criteria_detail_code
               WHERE r.person_number = '$person_number' and r.project_code = '$project_code'
               and r.test_code = 'BMI'";
       //echo $sql;
       $query   = DbQuery($sql,null);
       $json    = json_decode($query, true);
       $row     = $json['data'];
       $num     = $json['dataCount'];

       $wiegth         = "";
       $height         = "";
       $BMI            = array('','');
       $result         = isset($row[0]['result'])?$row[0]['result']:"";

       if($num > 0 && $result != "-" && $result != "")
       {
         $value  = explode("|",$result);
         $wiegth = $value[0];
         $height = $value[1];

         $BMI[0] = isset($row[0]['result_cal']) && !empty($row[0]['result_cal'])?$row[0]['result_cal']:"-";
         $BMI[1] = isset($row[0]['category_criteria_detail_name']) && !empty($row[0]['category_criteria_detail_name'])?$row[0]['category_criteria_detail_name']:"-";
       }

       $data[$x]['bmi']       = @$BMI[0];
       $data[$x]['bmiDesc']   = @$BMI[1];


       $sql = "SELECT r.result, r.result_cal,r.test_code, r.test_criteria_code,cd.category_criteria_detail_name,t.test_name,t.test_unit,tc.test_suggest,t.link_youtube2
               FROM pfit_t_result r
               LEFT JOIN pfit_t_test_criteria tc ON r.test_criteria_code = tc.test_criteria_code
               LEFT JOIN pfit_t_cat_criteria_detail cd ON tc.category_criteria_detail_code = cd.category_criteria_detail_code
               LEFT JOIN pfit_t_test t ON r.test_code = t.test_code
               WHERE r.person_number = '$person_number' and r.project_code = '$project_code'
               and r.test_code <> 'BMI' and $age >= t.test_age_min and $age <= t.test_age_max
               group by r.result, r.result_cal,r.test_code, r.test_criteria_code,cd.category_criteria_detail_name,
               t.test_name,t.test_unit,tc.test_suggest,t.link_youtube2,t.test_seq_def
               order by t.test_seq_def";


         $query   = DbQuery($sql,null);
         $json    = json_decode($query, true);
         $row     = $json['data'];
         $num     = $json['dataCount'];

         //ECHO $sql;

         for($i=0;$i<$num ;$i++)
         {
           $test_name  = $row[$i]['test_name'];
           $test_code  = $row[$i]['test_code'];
           $test_unit  = $row[$i]['test_unit'];

           $result_cal = isset($row[$i]['result_cal']) && ($row[$i]['result_cal'] != null)?$row[$i]['result_cal']:"-";
           $link_youtube2 = isset($row[$i]['link_youtube2']) && !empty($row[$i]['link_youtube2'])?$row[$i]['link_youtube2']:"";

           $category_criteria_detail_name = isset($row[$i]['category_criteria_detail_name']) && !empty($row[$i]['category_criteria_detail_name'])?$row[$i]['category_criteria_detail_name']:"-";
           $test_suggest = isset($row[$i]['test_suggest']) && !empty($row[$i]['test_suggest'])?strip_tags($row[$i]['test_suggest']):"";

           $criteriaS = "";

           if($test_code == "ABP"){
             $criteriaS = "120/80";
             if($resultTest != ""){
               $result_cal = str_replace("|", "/", $resultTest);
             }
           }else{
             $sql_m = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' AND age = '$age' AND gender = '$gender' order by category_criteria_detail_code";
             $query_m = DbQuery($sql_m,null);
             $row_m  = json_decode($query_m, true);
             $num_m = $row_m['dataCount'];
             $postion =  ceil($num_m/2)-1;

             $min = "";
             $max = "";
             if(isset($row_m['data'][$postion]['min'])){
               $min = $row_m['data'][$postion]['min']!=''?$row_m['data'][$postion]['min']:"";
             }
             if(isset($row_m['data'][$postion]['max'])){
               $max = $row_m['data'][$postion]['max']!=''?$row_m['data'][$postion]['max']:"";
             }

             if(substr($min,0,1) == "."){
               $min = "0".$min;
             }

             if(substr($max,0,1) == "."){
               $max = "0".$max;
             }
             $criteriaS = $min.' - '.$max;
           }


           if($link_youtube2 != ""){
             $link_youtube2 = "https://youtu.be/".$link_youtube2;
           }


           $data[$x]['result'][$i]['testName']    = $test_name;
           $data[$x]['result'][$i]['result']      = $result_cal;
           $data[$x]['result'][$i]['testUnit']    = $test_unit;
           $data[$x]['result'][$i]['benchMark']   = $criteriaS;
           $data[$x]['result'][$i]['resultDesc']  = $category_criteria_detail_name;
           $data[$x]['result'][$i]['suggest']     = $test_suggest;
           $data[$x]['result'][$i]['youtube']     = $link_youtube2;
         }
      }

    $status = 200;
    $message = 'success';
  }else{
    $status = 404;
    $message = 'No data available';
  }

?>
