<?php
include('../inc/function/connect.php');
include('../inc/function/mainFunc.php');

$status = 200;
$message = 'success';

$hostPath = getUrlHost();

$data = array();
$search_txt     = isset($request['search_txt'])?$request['search_txt']:"";
$project_code   = isset($request['project_code'])?$request['project_code']:"";
$project_type   = isset($request['project_type'])?$request['project_type']:"";
$num_page       = isset($request['num_page'])?$request['num_page']:"0";

$numPage = 10;
//$start   = 0;
// if($num_page > 0){
//   $start = ($numPage * $num_page) + 1;
// }

$start = ($num_page + 1);



$con = "";
$projectCodes = "";

if($search_txt != "")
{
  if(strpos($search_txt,"|") > 0)
  {
      $arr = explode("|",$search_txt);
      $search_txt   = $arr[0];
      $projectCodes = $arr[1];
      if($projectCodes != $project_code)
      {
        $status = 401;
        $message = 'ไม่พบข้อมูลผู้ทดสอบ';
      }
  }
  $con .= " and person_number like '%".$search_txt."%' or person_name like '%".$search_txt."%' or person_lname like '%".$search_txt."%' ";
}

// $sql  = "SELECT project_code,person_number,person_gender,person_name,person_lname,stage,grade,room,
//          TO_CHAR( date_of_birth, 'YYYY-MM-DD') as date_of_birth, wiegth, height, person_type
//          FROM pfit_t_person
//          WHERE project_code = '$project_code' $con
//          ORDER BY date_create DESC
//          OFFSET $start ROWS FETCH NEXT $numPage ROWS ONLY";

$sql = "SELECT project_code,person_number,person_gender,person_name,person_lname,stage,grade,room,
               TO_CHAR( date_of_birth, 'YYYY-MM-DD') as date_of_birth, wiegth, height, person_type
        FROM
        (
            SELECT a.*, rownum r__
            FROM
            (
                SELECT * FROM pfit_t_person
                WHERE project_code = '$project_code' $con
                ORDER BY date_create DESC
            ) a
            WHERE rownum < (($start * $numPage) + 1 )
        )
        WHERE project_code = '$project_code' and r__ >= ((($start-1) * $numPage) + 1)";

 $query = DbQuery($sql,null);
 $row  = json_decode($query, true);
 $num  = $row['dataCount'];
 $dataSql = $row['data'];

for ($i=0; $i < $num ; $i++) {
  $person_number  = $dataSql[$i]['person_number'];
  $person_name    = $dataSql[$i]['person_name'];
  $person_lname   = $dataSql[$i]['person_lname'];
  $person_gender  = $dataSql[$i]['person_gender'];
  $stage          = isset($dataSql[$i]['stage'])?$dataSql[$i]['stage']:"";//1=มัธยมศึกษา 2=ประถมศึกษา
  $grade          = isset($dataSql[$i]['grade'])?$dataSql[$i]['grade']:"";//ชั้นปี
  $room           = isset($dataSql[$i]['room'])?$dataSql[$i]['room']:"";//ห้องเรียน

  $strStd = "";
  if($stage != ""){
      if($stage == '1'){
        $strStd = "ม.";
      }else{
        $strStd = "ป.";
      }
      $strStd = $strStd.$grade.'/'.$room." ";
  }

  if($person_gender != ""){
    if($person_gender == "M")
    {
      $person_gender = "เพศชาย";
    }
    else if($person_gender == "F")
    {
      $person_gender = "เพศหญิง";
    }
  }

  $data[$i]['person_number'] = $person_number;
  $data[$i]['name'] = $person_name.' '.$person_lname;
  $data[$i]['detail'] = $strStd.$person_number." ".$person_gender;
  $data[$i]['dob'] = $dataSql[$i]['date_of_birth'];
  $data[$i]['gender'] = $dataSql[$i]['person_gender'];
  $data[$i]['wiegth'] = $dataSql[$i]['wiegth'];
  $data[$i]['height'] = $dataSql[$i]['height'];
  $data[$i]['url'] = $hostPath."result/".$person_number."_".$project_code.".pdf";
}
?>
