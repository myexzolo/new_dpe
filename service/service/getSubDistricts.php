<?php
include('../inc/function/connect.php');
include('../inc/function/mainFunc.php');

$zipCode      = isset($request['zipCode'])?$request['zipCode']:"";
$districtsId   = isset($request['districtsId'])?$request['districtsId']:"";

$con = "";
if($zipCode != ""){
 $con .= " and zip_code = $zipCode ";
}

if($districtsId != ""){
  $con .= " and districts_id = $districtsId ";
}

$sql = "SELECT * FROM subdistricts where districts_id > 0 $con ORDER BY name_th asc";

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$rows       = $json['data'];

$arr        = array();
$arr['data']      = $rows;
if(intval($errorInfo[0]) == 0){
  $arr['status']    = "200";
  $arr['message']   = "success";
}else{
  $arr['status']    = "400";
  $arr['message']   = "Fail";
}

header('Content-Type: application/json');
exit(json_encode($arr));

?>
