<?php
include('../inc/function/connect.php');
include('../inc/function/mainFunc.php');

$provinceId   = isset($request['provinceId'])?$request['provinceId']:"";
$geoId        = isset($request['geoId'])?$request['geoId']:"";


$con = "";
if($provinceId != ""){
$con .= " and province_id = $provinceId ";
}

if($geoId != ""){
 $con .= " and geo_id = $geoId ";
}


$sql = "SELECT province_id,TRIM(province_name) as province_name, province_name_eng, geo_id  FROM province where province_id > 0 $con ORDER BY province_name ASC";

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$rows       = $json['data'];

$arr['data']  = $rows;


if(intval($errorInfo[0]) == 0){
  $arr['status']    = "200";
  $arr['message']   = "success";
}else{
  $arr['status']    = "400";
  $arr['message']   = "Fail";
}

header('Content-Type: application/json');
exit(json_encode($arr));

?>
