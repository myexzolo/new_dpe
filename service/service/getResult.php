<?php
include('../inc/function/connect.php');
include('../inc/function/mainFunc.php');

$status = 200;
$message = 'success';

$project_code   = isset($request['project_code'])?$request['project_code']:"";
$person_number  = isset($request['person_number'])?$request['person_number']:"";
$dob            = isset($request['dob'])?$request['dob']:"";
$wiegth         = isset($request['wiegth'])?$request['wiegth']:"";
$height         = isset($request['height'])?$request['height']:"";

$age = 0;
if($dob != ""){
  $age  =  yearBirth($dob);
}

$sql = "SELECT *
        FROM pfit_t_project_test ptpt, pfit_t_test ptt
        WHERE ptpt.test_code = ptt.test_code
        AND ptpt.project_code = '$project_code' and $age >= ptt.test_age_min  and $age <= ptt.test_age_max
        ORDER BY ptpt.test_seq ,ptt.test_seq_def";


$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$count      = $json['dataCount'];


for ($i=0; $i < $count ; $i++) {

  $test_code = $rows[$i]['test_code'];
  $test_unit = $rows[$i]['test_unit'];

  $sqlr = "SELECT result FROM pfit_t_result
           WHERE test_code = '$test_code' and  project_code = '$project_code' and person_number = '$person_number'";

  $queryr      = DbQuery($sqlr,null);
  $jsonr       = json_decode($queryr, true);
  $rowr        = $jsonr['data'];
  $countr      = $jsonr['dataCount'];

  $result  = "";
  $result2 = "";
  if($countr > 0){
      $result = $rowr[0]['result'];
      if($result == "-"){
        $result = "";
      }
  }

  $valNum = '1';
  if($test_code == 'BMI')
  {
      if($result != ""){
        $arrRes = explode("|",$result);
        $result = $arrRes[0];
        $result2 = $arrRes[1];
      }else{
        $result = $wiegth;
        $result2 = $height;
      }
      $valNum = '2';
      $test_unit = 'น้ำหนัก(Kg)/ส่วนสูง(cm)';
  }
  else if($test_code == 'ABP')
  {
    if($result != ""){
      $arrRes = explode("|",$result);
      $result = $arrRes[0];
      $result2 = $arrRes[1];
    }
    $valNum = '2';
    $test_unit = 'มม.ปรอท';
  }
  else if($test_code == 'VO2')
  {
    if($result != ""){
      $arrRes = explode("|",$result);
      $result = $arrRes[0];
      $result2 = $arrRes[1];
    }
    $valNum = '2';
    $test_unit = 'HR/Load';
  }


  $data[$i]['test_code'] = $test_code;
  $data[$i]['name'] = $rows[$i]['test_name'];
  $data[$i]['unit'] = $test_unit;
  $data[$i]['option'][0]['value'] = $result;
  if($valNum == '2'){
    $data[$i]['option'][1]['value'] = $result2;
  }
}
?>
