<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$status = 200;
$message = 'success';

$data = array();
$user_id      = isset($request['user_id'])?$request['user_id']:"";
$user_login   = isset($request['user_login'])?$request['user_login']:"";
$type_user    = isset($request['type_user'])?$request['type_user']:""; //ADMIN , MEMBER
$num_page     = isset($request['num_page'])?$request['num_page']:"0";
$search_txt   = isset($request['search_txt'])?$request['search_txt']:""; //search

$numPage = 10;
// $start   = 1;
// if($num_page > 0){
//   $start = ($numPage * $num_page) + 1;
// }

$start = ($num_page + 1);

$con = "";

if($search_txt != "")
{
  $con .= " and (user_login like '%".$search_txt."%' or project_name like '%".$search_txt."%') ";
}

if($type_user != "ADMIN")
{
    $con .= " and user_login = '".$user_login."'";
}


// $sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date, user_login, project_type
//         FROM pfit_t_project
//         where status <> 'C' $con ORDER BY start_date DESC, project_name asc
//         OFFSET $start ROWS FETCH NEXT $numPage ROWS ONLY";

$sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date, user_login, project_type
        FROM
        (
            SELECT a.*, rownum r__
            FROM
            (
                SELECT * FROM pfit_t_project WHERE status <> 'C' $con
                ORDER BY start_date DESC, project_name asc
            ) a
            WHERE rownum < (($start * $numPage) + 1 )
        )
        WHERE r__ >= ((($start-1) * $numPage) + 1)";
//echo $sql;
$query = DbQuery($sql,null);
$row  = json_decode($query, true);
$num  = $row['dataCount'];
$dataSql = $row['data'];

for ($i=0; $i < $num ; $i++) {
  $project_code = $dataSql[$i]['project_code'];
  $project_name = $dataSql[$i]['project_name'];
  $project_type = $dataSql[$i]['project_type'];

  $start_date   = DateThai($dataSql[$i]['start_date']);
  $end_date     = DateThai($dataSql[$i]['end_date']);

  $strProject   = "";
  if($type_user == "ADMIN")
  {
      $strProject   = "ผู้สร้างกิจกรรม : ".$dataSql[$i]['user_login'];
  }

  $data[$i]['act_name'] = "$project_name";
  $data[$i]['act_user'] =  $strProject;//_ถ้ามีค่า ให้เพิ่มอีก  บรรทัด
  $data[$i]['act_date'] = "$start_date - $end_date";
  //$data[$i]['act_img'] = "https://onesittichok.co.th/pfit/mobile/service/img/logo.png";
  $data[$i]['project_code'] = $project_code;
  $data[$i]['project_type'] = $project_type;//1=``
}

?>
