<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

// $request['action'];
//
// $request['name'];
// $request['lname'];
// $request['department'];
// $request['email'];
// $request['mobile'];
// $request['objective'];
// $request['address'];
// $request['postcode'];
// $request['tumbon'];
// $request['aunpore'];
// $request['province'];
//
// $request['profile'];
// $request['doc'];
//
// $request['username'];
// $request['password'];

$action           = isset($request['action'])?$request['action']:"ADD";
$typeDevice       = "MOBILE";

$member_name      = isset($request['name'])?$request['name']:"";
$member_lname     = isset($request['lname'])?$request['lname']:"";
$department_name  = isset($request['department'])?$request['department']:"";
$email            = isset($request['email'])?$request['email']:"";
$tel              = isset($request['mobile'])?$request['mobile']:"";
$objective        = isset($request['objective'])?$request['objective']:"";
$address          = isset($request['address'])?$request['address']:"";
$province_id      = isset($request['province'])?$request['province']:"";
$district_id      = isset($request['aunpore'])?$request['aunpore']:"";
$subdistrict_id   = isset($request['tumbon'])?$request['tumbon']:"";
$zipcode          = isset($request['postcode'])?$request['postcode']:"";

$user_login       = isset($request['username'])?$request['username']:"";
$password         = isset($request['password'])?$request['password']:"";

$profile          = isset($request['profile'])?$request['profile']:"";
$doc              = isset($request['doc'])?$request['doc']:"";


$user_name        = $member_name." ".$member_lname;

$user_password     = @md5($password);

$user_img   = "";
$document   = "";
$updateImg  = "";
$updateDoc  = "";

$user_id_update = 1;

$pathImg      = "../image/member/";
$pathUpload   = "../upload/doc/";

$txt = "";
if($typeDevice == "WEB"){
    $regTxt = "REG WEB";
    $txt    = " ผ่านช่องทาง Website";
}else if($typeDevice == "MOBILE"){
    $regTxt = "REG MOBILE";
    $txt    = " ผ่านช่องทาง Mobile App";
}

if($profile != "")
{
  $user_img = Base64ToImage($profile,$pathImg,$user_login);
  if($action == "EDIT")
  {
    $updateImg = "img = '$user_img',";
  }
}


if($doc != "")
{
  $document = Base64ToImage($doc,$pathUpload,$user_login);
  if($action == "EDIT" && $document != "")
  {
    $updateDoc = "document = '$document',";
  }
}

if($action == 'ADD'){
  $sqlUser = "SELECT * FROM t_user WHERE user_login = '$user_login' AND is_active = 'Y'";
  $query   = DbQuery($sqlUser,null);
  $json    = json_decode($query, true);
  $count   = $json['dataCount'];

  if($count > 0)
  {
    $status = 401;
    $message = 'ผู้มีใช้รหัสผู้ใช้งานนี้แล้ว !!';
  }else{
    $sqlid      = "SELECT T_USER_SEQ.NEXTVAL AS next_id FROM DUAL";

    $query      = DbQuery($sqlid,null);
    $json       = json_decode($query, true);
    $user_id    = $json['data'][0]['next_id'];

    $sqlUser = "INSERT INTO t_user
               (user_id,user_login,user_name,user_password,is_active,role_list,user_img,user_id_update,note1,note2,pw,type_user)
               VALUES($user_id,'$user_login','$user_name','$user_password','R',
               '','$user_img','1','','','$password','MEMBER')";

    $query      = DbQuery($sqlUser,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];

    if(intval($errorInfo[0]) == 0)
    {
      $sqlMember  = "INSERT INTO pfit_t_member
                     (user_id,member_name,member_lname,department_name,email,tel,
                      objective,address,province_id,district_id,subdistrict_id,zipcode,document)
                     VALUES
                     ('$user_id','$member_name','$member_lname','$department_name','$email','$tel',
                      '$objective','$address','$province_id','$district_id','$subdistrict_id','$zipcode','$document')";

      $query      = DbQuery($sqlMember,null);
      $row        = json_decode($query, true);
      //print_r($row);
      $errorInfo  = $row['errorInfo'];

      if(intval($errorInfo[0]) == 0){

        $message   = "\r\n"."มีผู้ลงทะเบียน ".$txt."\r\n";
        $message  .= "ชื่อผู้ลงทะเบียน : ".$user_name."\r\n";
        $message  .= "หน่วยงาน : ".$department_name."\r\n";
        $message  .= "เบอร์โทรติดต่อ : ".$tel."\r\n";
        $message  .= "E-mail : ".$email."\r\n";
        $message  .= "สถานะ : รอตรวจสอบข้อมูล"."\r\n";
        $message  .= "Link : https://sportsci.dpe.go.th";


        $token = GET_LINE_TOKEN("");

        LINE_NOTIFY_MESSAGE($message,"",$token);

        $sql        = "SELECT * FROM pfit_t_member WHERE user_id = '$user_id'";
        $query      = DbQuery($sql,null);
        $row        = json_decode($query, true);

        $status = 200;
        $message = 'success';
        $data = $row['data'][0];

      }else{
        $status = 401;
        $message = 'user or pass fail';
      }
    }
    else
    {
      $status = 401;
      $message = 'Insert fail';
    }

  }
}
?>
