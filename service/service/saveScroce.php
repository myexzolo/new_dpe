<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$hostPath = getUrlHost();
$status = 200;
$message = 'success';

$data = array();


$projectCode    = isset($request['project_code'])?$request['project_code']:"";
$person_number  = isset($request['person_number'])?$request['person_number']:"";
$dob            = isset($request['dob'])?$request['dob']:"";
$person_gender  = isset($request['gender'])?$request['gender']:"";
$dataResult     = $request['result'];
$yearBirth      = yearBirth($dob);


$test_str = '';
$testSeq = 0;
$time = 0;

if($person_number != ""){
  $dateNow = queryDateTimeOracle(date("Y/m/d H:i:s"));

  $sql = "SELECT MAX(DISTINCT time) as time FROM pfit_t_result_history
          WHERE project_code = '$projectCode' and person_number = '$person_number'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  if($num > 0){
    $time  = $row[0]['time'];
    $time = $time + 1;
  }else{
    $time = 1;
  }
  $url = '';
  $wiegth = 0;
  $height = 0;
  foreach ($dataResult as $key => $value) {
    $testSeq++;
    $result    = $value['score'];
    $test_code = $value['test_code'];

    $res    = '';
    $test_criteria_code = '';

    if($result != '-' || $result != '')
    {
      $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$test_code'";
      $query  = DbQuery($sql,null);
      $json   = json_decode($query, true);
      $row    = $json['data'];

      if($test_code == "BMI")
      {
        $arrRes = explode("|",$result);
        $wiegth = $arrRes[0];
        $height = $arrRes[1];
      }

      if($test_code == "TST01" && $wiegth == 0)
      {
         $sqlperson     = "SELECT wiegth,height FROM pfit_t_person WHERE project_code = '$projectCode' and person_number = '$person_number'";
         $queryPerson   = DbQuery($sqlperson,null);
         $jsonPerson    = json_decode($queryPerson, true);
         $rowPerson     = $jsonPerson['data'];

         $wiegth = $rowPerson[0]['wiegth'];
         $height = $rowPerson[0]['height'];
      }

      $test_calculator = $row[0]['test_calculator'];

      $res = getResultCalculator($test_calculator,$result,$wiegth,$height,$yearBirth,$person_gender,$projectCode,$person_number);
      //$url .= $test_calculator.",".$result.",".$wiegth.",".$height.",".$yearBirth.",".$person_gender.",".$projectCode.",".$person_number.",".$res.'<<>>';
      $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' AND age = '$yearBirth' AND gender = '$person_gender' AND min <= '$res' ORDER BY min DESC";
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $num        = $json['dataCount'];
      $row        = $json['data'];

      if($num > 0){
        $test_criteria_code = $row[0]['test_criteria_code'];
      }
    }

    $sql       = "SELECT * FROM pfit_t_result WHERE person_number = '$person_number' AND project_code = '$projectCode' AND test_code = '$test_code'";
    $query     = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $num        = $json['dataCount'];

    if($num > 0)
    {
        $sqlr = "UPDATE pfit_t_result SET
                 result = '$result',
                 test_criteria_code = '$test_criteria_code',
                 date_create = $dateNow,
                 result_cal = '$res'
                 where person_number = '$person_number' AND project_code = '$projectCode' AND test_code = '$test_code'";
    }
    else
    {
      $sqlr = "INSERT INTO pfit_t_result
              (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
              VALUES('$projectCode','$person_number','$result','$test_code','$test_criteria_code',$dateNow,'$res')";
    }
    DbQuery($sqlr,null);

    $sqlrr = "INSERT INTO pfit_t_result_history
              (project_code,person_number,result,test_code,test_criteria_code,date_create,resultHis_cal,time,test_seq)
              VALUES('$projectCode','$person_number','$result','$test_code','$test_criteria_code',$dateNow,'$res','$time','$testSeq')";
    DbQuery($sqlrr,null);
  }

  $sql = "UPDATE pfit_t_person SET status_test = '2' WHERE person_number = '$person_number' and project_code = '$projectCode' ";
  DbQuery($sql,null);

  require 'pdf.php';

  //$url = 'https://kosummarket.onesittichok.co.th/pages/sale_silp/pdf.php';
}else{
  $status = 401;
  $message = 'ไม่พบ User ID';
}
?>
