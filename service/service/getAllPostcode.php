<?php
include('../inc/function/connect.php');
include('../inc/function/mainFunc.php');


$condition = '';
$condition .= isset($request['ZIP_CODE'])?"AND ZIP_CODE = '{$request['ZIP_CODE']}'":"";
$condition .= isset($request['pro_id'])?"AND pro.PROVINCE_ID = '{$request['pro_id']}'":"";
//$condition .= isset($request['PRO_ID'])?"AND PRO_ID = '{$request['PRO_ID']}'":"";

 $sql = "
 SELECT
    subdis.ID AS SUB_ID,
    subdis.ZIP_CODE AS ZIP_CODE,
    subdis.NAME_TH AS SUB_NAME,
    dis.ID AS DIS_ID,
    dis.NAME_TH AS DIS_NAME,
    pro.PROVINCE_ID AS PRO_ID,
    pro.PROVINCE_NAME AS PRO_NAME
FROM
    subdistricts subdis , DISTRICTS dis , PROVINCE pro
WHERE subdis.DISTRICTS_ID = dis.ID AND dis.PROVINCE_ID = pro.PROVINCE_ID $condition  order by pro.PROVINCE_NAME
 ";

 //echo $sql;
 $query      = DbQuery($sql,null);
 $json       = json_decode($query, true);
 $errorInfo  = $json['errorInfo'];
 $rows       = $json['data'];
 $data       = $rows;

 if(intval($errorInfo[0]) == 0){
   $status = 200;
   $message = 'success';
 }else{
   $status = 400;
   $message = 'Fail';
 }


?>
