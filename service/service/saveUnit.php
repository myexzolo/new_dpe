<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$user_id      = isset($request['results']['user_id'])?$request['results']['user_id']:"";
$user_login   = isset($request['results']['user_login'])?$request['results']['user_login']:"";
$name         = isset($request['results']['name'])?$request['results']['name']:"";
$plant        = isset($request['results']['plant'])?$request['results']['plant']:"";
$province     = isset($request['results']['province'])?$request['results']['province']:"";
$aunpore      = isset($request['results']['aunpore'])?$request['results']['aunpore']:"";
$date_start   = isset($request['results']['date_start'])?queryDateOracle($request['results']['date_start']):"";
$date_end     = isset($request['results']['date_end'])?queryDateOracle($request['results']['date_end']):"";
$detail       = isset($request['results']['detail'])?$request['results']['detail']:"";
// $name_dep     = isset($request['results']['name_dep'])?$request['results']['name_dep']:"";
$age          = isset($request['results']['age'])?$request['results']['age']:"";
$unit         = isset($request['results']['unit'])?$request['results']['unit']:"";
$type_person  = isset($request['results']['type_person'])?$request['results']['type_person']:"";



$status       = 'Y';

$dateNow   =  queryDateTimeOracle(date('Y/m/d H:i:s'));
// name: "แปอปอแแปอ"
// plant: "แอแอแอ"
// province: "6"
// aunpore: "91"
// date_start: "2020-05-31"
// date_end: "2020-05-31"
// detail: "หฟกหฟกห"
// name_dep: "ฟหกฟหก"
// age: ["1", "2"]
// unit: ["0", "1"]
// type_person: "1"
$project_code = "";
$date = date("Y");
$dates = date("m");

if($date < 2500){
  $date += 543;
}
$code = substr($date, 2, 4).$dates;

$sql = "SELECT project_code
        FROM pfit_t_project
        where project_code LIKE '$code%' order by project_code desc";

$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$num   = $json['dataCount'];
$row   = $json['data'];

if($num > 0){
  $project_code = $row[0]['project_code'];
}


if(!empty($project_code)){
  $project_code = explode("_", $project_code);
  $lastNum = substr($project_code[0],4);
  $lastNum = $lastNum + 1;

  $project_code = $code.sprintf("%04d", $lastNum);
}else{
  $project_code = $code.sprintf("%04d", 1);
}

$sql = "";

if(count($unit) > 0){
  sort($unit);
  foreach($unit as $key => $v ) {
    $sql = "INSERT INTO pfit_t_project_test(project_code,test_code) VALUES('$project_code','$v')";
    DbQuery($sql,null);
  }
}

$sql = "INSERT INTO pfit_t_project(
        project_code,project_name,
        start_date,end_date,
        location,project_detail,
        status,user_login,
        project_type,province_id,
        districts_id,date_create)
        VALUES(
        '$project_code','$name',
        $date_start,$date_end,
        '$plant','$detail',
        '$status','$user_login',
        '$type_person','$province',
        '$aunpore',$dateNow)";

 // $message =  $sql;

  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  //print_r($row);
  $errorInfo  = $row['errorInfo'];

  if(intval($errorInfo[0]) == 0){
    $status = 200;
    $message = 'success';
    $data[0]['project_code'] = $project_code;
    $data[0]['project_type'] = $type_person;
  }else{
    $status = 401;
    $message = 'fail';
  }
?>
