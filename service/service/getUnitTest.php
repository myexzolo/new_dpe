<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$status = 200;
$message = 'success';

$str1 = '';
$str2 = '';
$data = array();
$role_list = $request['role_list'];
$arrRole = explode(",",$role_list);
if($role_list != "" && count($arrRole) == 0)
{
  $arrRole = array($role_list);
}
if(count($arrRole) > 0)
{
  foreach ($arrRole as $value)
  {
    //role 0 = ผู้ดูแลระบบสูงสุด  1 = ผู้ดูแลระบบ  2 = เจ้าหน้าที่กรม  14 = โรงเรียน  15 = จพล.
    //test_display 1 = all 2 = กรมพลศึกษา  3 = จพล.  4 = โรงเรียน  5 = Mobile
    if($value == "0" || $value == "1"){
      $str1 = "";
      break;
    }else if($value == "2")
    {
      $value = "2";
    }else if($value == "14")
    {
      $value = "4";
    }else if($value == "15")
    {
      $value = "3";
    }

    if($str1 == ""){
       $str1 = " (test_display = '$value' or  test_display like '%,$value' or test_display like '$value,%' or test_display like '%,$value,%')";
    }else{
       $str1 .= " or (test_display = '$value' or  test_display like '%,$value' or test_display like '$value,%' or test_display like '%,$value,%') ";
    }
  }
}

foreach ($request['results'] as $value)
{
 if($str2 == ""){
   $str2 = " (age_range = '$value' or  age_range like '%,$value' or age_range like '$value,%' or age_range like '%,$value,%')";
 }
   $str2 .= " or (age_range = '$value' or  age_range like '%,$value' or age_range like '$value,%' or age_range like '%,$value,%') ";
}

$str3 = "";

if($str1 !=""){
  $str3 = " where (".$str1.")";
}

if($str3 !=""){
  $str3 .= " and (".$str2.")";
}else{
  $str3 = " where (".$str2.")";
}

if($str3 != ""){
  $sql = "select * from pfit_t_test $str3 and status = 'Y' order by test_seq_def ASC";


  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $rows       = $json['data'];
  $count      = $json['dataCount'];
  //echo $sql;
  $data = array();
  for ($i=0; $i < $count ; $i++)
  {
    $data[$i]['unit_code'] = $rows[$i]['test_code'];
    $data[$i]['unit_name'] = $rows[$i]['test_name'];
  }
}

?>
