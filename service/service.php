<?php
include('config.php');
$status = 404;
$message = 'functionName is not macth';
$data = array();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata,true);
$functionName = @$request['functionName'];
switch (@$request['functionName']) {
  case 'getlogin':
    include('service/login.php');
    break;

  case 'forgotpassword':
    include('service/forgotpassword.php');
    break;

  case 'createUserProfile':
    include('service/createUserProfile.php');
    break;

  case 'getActivityAll':
    include('service/getActivityAll.php');
    break;

  case 'getSubDistricts':
    include('service/getSubDistricts.php');
    break;

  case 'getDistricts':
    include('service/getDistricts.php');
    break;

  case 'getProvince':
    include('service/getProvince.php');
    break;

  case 'getVideo':
    include('service/getVideo.php');
    break;

  case 'getEbook':
    include('service/getEbook.php');
    break;

  case 'getAllPostcode':
    include('service/getAllPostcode.php');
    break;

  case 'getLengthAge':
    include('service/getLengthAge.php');
    break;

  case 'getUnitTest':
    include('service/getUnitTest.php');
    break;

    case 'saveUnit':
      include('service/saveUnit.php');
      break;

    case 'getStudentAll':
      include('service/getStudentAll.php');
      break;

    case 'addStudent':
      include('service/addStudent.php');
      break;

    case 'getResult':
      include('service/getResult.php');
      break;

    case 'saveScroce':
      include('service/saveScroce.php');
      break;

    case 'checkVersions':
      include('service/checkVersions.php');
      break;

    case 'getLineHelpdesk':
      include('service/getLineHelpdesk.php');
      break;

  default:

    break;
}
exit(json_encode(
  array( "status" => $status , "message" => $message ,
  "data" => $data ,"functionName" => $functionName  ) ));

?>
