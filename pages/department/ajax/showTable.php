<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>ลำดับ</th>
      <th>Code</th>
      <th>ชื่อสังกัด/แผนก</th>
      <th>สถานะ</th>
      <th style="width:50px;">Edit</th>
      <th style="width:50px;">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_department ORDER BY 	department_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?=$i + 1;?></td>
      <td><?=$rows[$i]['department_code'];?></td>
      <td align="left"><?=$rows[$i]['department_name'];?></td>
      <td><?=$rows[$i]['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$rows[$i]['department_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$rows[$i]['department_id']?>','<?=$rows[$i]['department_name']?>')"></i></a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $(function () {
      $('#tableDisplay').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  })
</script>
