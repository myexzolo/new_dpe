$(function () {
  $('.select2').select2();
  searchProject();
})


function searchProject(){
//var date = $('#s_year').val()+"/"+$('#s_month').val() + "/01";

var dateStart = "";
var dateEnd   = "";

if($('#dateStart').val() != ""){
  dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
}

if($('#dateEnd').val() != ""){
  dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");
}

var typeDate  = $('#typeDate').val();
var gender    = $('#gender').val();
var age_start = $('#age_start').val();
var age_end   = $('#age_end').val();
var province  = $('#province').val();
var district    = $('#district').val();
var user_login  = $('#user_login').val();
var person_number = $('#person_number').val();
var person_name   = $('#person_name').val();
var person_lname  = $('#person_lname').val();

$.post("ajax/showtable.php",{person_number:person_number,person_name:person_name,person_lname:person_lname,dateStart:dateStart,dateEnd:dateEnd,typeDate:typeDate,gender:gender,age_start:age_start,age_end:age_end,province:province,district:district,user_login:user_login})
  .done(function( data ) {
    //console.log(data);
    $('#show-table').html(data);
});
}

function getProvince(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#district').html(data);
        getDistrict($('#district').val(),'D');
  });
}

function getDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#subdistrict').html(data);
        //getSubDistrict($('#subdistrict').val(),'SD');
  });
}
