<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - รายงานผลการทดสอบสมรรถภาพทางกาย</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/report06.css">
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>รายงานผลการทดสอบสมรรถภาพทางกาย  <small>report06</small></h1>

          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">รายงานผลการทดสอบสมรรถภาพทางกาย</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php");
            include('../../inc/function/mainFunc.php');

            $dateStart  = DateThai(date("Y/m/01"));
            $dateEnd    = DateThai(date("Y/m/t"));

            $optionProvince     = getoptionProvince("");
            $optionDistrict     = getoptionDistrict("","");

            $user_login = $_SESSION['member'][0]['user_login'];
            $adminRole  = $_SESSION['member'][0]['adminRole'];

            //echo $user_login;
           ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ค้นหารายชื่อทดสอบ</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>เลขประจำตัว/เบอร์โทร</label>
                          <input class="form-control " value="" id="person_number" type="text" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ชื่อ</label>
                          <input class="form-control " value="" id="person_name" type="text" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>สกุล</label>
                          <input class="form-control " value="" id="person_lname" type="text" >
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ช่วงอายุตั้งแต่</label>
                          <input class="form-control datepicker" value="" id="age_start" type="text">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ถึงช่วงอายุ</label>
                          <input class="form-control datepicker" value="" id="age_end" type="text">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>เพศ</label>
                          <select class="form-control select2" style="width: 100%;" id="gender">
                            <option value="" >&nbsp;</option>
                            <option value="M" >ชาย</option>
                            <option value="F" >หญิง</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ตั้งแต่วันที่</label>
                          <input class="form-control datepicker" value="" id="dateStart" type="text" data-provide="datepicker" data-date-language="th-th" >
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ถึงวันที่</label>
                          <input class="form-control datepicker" value="" id="dateEnd" type="text" data-provide="datepicker" data-date-language="th-th" >
                        </div>
                      </div>
                      <?php if($adminRole == "Y"){ ?>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>จังหวัด</label>
                          <select name="province_id" id="province" onchange="getProvince(this.value,'P')" class="form-control select2" style="width: 100%;" required>
                            <option value="">&nbsp;</option>
                            <?=$optionProvince;?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>อำเภอ</label>
                          <select name="districts_id" id="district" onchange="getDistrict(this.value,'D')" class="form-control select2" style="width: 100%;" required>
                            <option value="">&nbsp;</option>
                            <?=$optionDistrict;?>
                          </select>

                        </div>
                      </div>
                    <?php }else{ ?>
                        <input value="<?=$user_login ?>" id="user_login" type="hidden">
                    <?php } ?>
                  </div>
                </div>
                <div class="box-footer with-border" align="center">
                    <button class="btn btn-primary" onclick="searchProject()" style="width:80px;">ค้นหา</button>
                </div>
              </div>

              <div id="show-table"></div>
              <!-- /.box -->
            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="js/report06.js"></script>

  </body>
</html>
