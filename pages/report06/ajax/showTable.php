<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$person_number  = $_POST['person_number'];
$dateStart      = $_POST['dateStart'];
$dateEnd        = $_POST['dateEnd'];
$typeDate       = $_POST['typeDate'];
$gender         = $_POST['gender'];
$age_start      = $_POST['age_start'];
$age_end        = $_POST['age_end'];
$province       = $_POST['province'];
$district       = $_POST['district'];
$person_name    = $_POST['person_name'];
$person_lname   = $_POST['person_lname'];
$user_login     = @$_POST['user_login'];

$con = "";

if($person_number != "")
{
    $con .= " and ps.person_number = '$person_number'";
}

$dateNow = date("Y/m/d");
$age_start = intval($age_start);
$age_end   = intval($age_end);

if($age_start > 0 && $age_end > 0 && $age_end >=  $age_start)
{
  $con .= " and EXTRACT(YEAR FROM TO_DATE('$dateNow','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) between $age_start and $age_end";
}

if($dateStart != "" and $dateEnd != ""){
  $con .= " and p.end_date between TO_DATE('$dateStart 00:00:00','YYYY-MM-DD HH24:MI:SS') and TO_DATE('$dateEnd 23:59:59','YYYY-MM-DD HH24:MI:SS')";
}


$user_login = $_SESSION['member'][0]['user_login'];
$adminRole  = $_SESSION['member'][0]['adminRole'];


if ($gender != "")
{
  $con     .= " and ps.person_gender ='$gender'";
}


if ($province != "")
{
  $con     .= " and p.province_id ='$province'";
}

if ($district != "")
{
  $con     .= " and p.districts_id ='$district'";
}

if($user_login != "" and $adminRole != "Y")
{
  $con     .= " and p.user_login = '$user_login'";
}

if($person_name != "")
{
  $con     .= " and ps.person_name LIKE '%$person_name%'";
}

if($person_lname != "")
{
  $con     .= " and ps.person_lname LIKE '%$person_lname%'";
}

if($con == "")
{
  $con = " and ps.project_code = '1234546789'";
}


?>
<style>
.justify-content-center {
    -webkit-box-pack: center!important;
    -ms-flex-pack: center!important;
    justify-content: center!important;
}
</style>
<div class="box box-primary">
    <div class="box-body">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px"></th>
      <th style="width:180px;">เลขประจำตัว</th>
      <th>ชื่อ</th>
      <th>นามสกุล</th>
      <th style="width:140px;">วันเดือนปีเกิด</th>
      <th style="width:80px;">เพศ</th>
      <th style="width:80px;">น้ำหนัก</th>
      <th style="width:80px;">ส่วนสูง</th>
      <th>ชื่อกิจกรรม</th>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
        <th style="width:70px;">พิมพ์</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
       $sql  = "SELECT ps.project_code,ps.person_number,ps.person_gender,ps.person_name,ps.person_lname,ps.status_test,p.project_name,
                ".getQueryDate('date_of_birth').",ps.wiegth,ps.height,ps.person_type
                FROM pfit_t_person ps,pfit_t_project p  WHERE ps.project_code = p.project_code and ps.status_test = 2 $con
                ORDER BY p.project_code ,ps.date_create desc";
    //echo $sql;

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0){
      $x = 1;
    foreach ($rows as $key => $value)
    {
        $gender = $value['person_gender']=="M"?"ชาย":"หญิง";
        $person_type = $value['person_type'];
        $status_test = $value['status_test'];
        $project_code   = $value['project_code'];
        $person_number  = $value['person_number'];

        $color    = "";
        $disable  = "disabled";
        $oncick   = "";
        $statusTest  = "N";

        if($status_test == 0 || $status_test == ""){
          $color = 'red';
        }else if($status_test == 1){
          $color = '#FFD100';
          $statusTest  = "Y";
        }else if($status_test == 2){
          $color = 'green';
          $statusTest  = "Y";
          $url    = "../result/index.php?project_code=$project_code&person_number=$person_number";
          $oncick = "postURL_blank('$url')";
          $class= "";
        }
    ?>
    <tr class="text-center">
      <td><?= $x++ ?></td>
      <td align="left"><?=$value['person_number'];?></td>
      <td align="left"><?=$value['person_name'];?></td>
      <td align="left"><?=$value['person_lname'];?></td>
      <td ><?= DateThai($value['date_of_birth']); ?></td>
      <td><?=$gender ;?></td>
      <td><?=$value['wiegth'];?></td>
      <td><?=$value['height'];?></td>
      <td align="left"><?=$value['project_name'];?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
      <td>
        <a class="btn_point <?=$disable ?>"><i class="fa fa-print" onclick="<?=$oncick ?>"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
</div>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
