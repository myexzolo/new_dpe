<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$type               = $_POST['type'];
$test_code          = $_POST['test_code'];
$age                = $_POST['age'];
$gender             = $_POST['gender'];
$type_test          = $_POST['type_test'];
$status             = $_POST['status'];

$categoryCrDetCodeArr   = $_POST['categoryCrDetCode'];
$minArr                 = $_POST['min'];
$maxArr                 = $_POST['max'];
$testSuggestArr         = $_POST['test_suggest'];
$testSuggestEnArr       = $_POST['test_suggest_en'];

$test_criteria_code     = "";

  $numCat = count($categoryCrDetCodeArr);
  for($i=0; $i<$numCat; $i++){
    $min = $minArr[$i];
    $max = $maxArr[$i];
    $test_suggest     = $testSuggestArr[$i];
    $test_suggest_en  = $testSuggestEnArr[$i];
    $category_criteria_detail_code = $categoryCrDetCodeArr[$i];

    $sql = "SELECT * FROM pfit_t_test_criteria
    WHERE test_code = '$test_code' and category_criteria_detail_code = '$category_criteria_detail_code' and age = '$age' and gender = '$gender'";
    $json = DbQuery($sql,null);
    $obj = json_decode($json, true);
    $dataCount = $obj['dataCount'];
    $strSQL = "";
    //echo "<br>".$sql;
    if($dataCount > 0){
        $test_criteria_code = $obj['data'][0]['test_criteria_code'];
        //echo ">>>".$test_criteria_code;
        $sql = "UPDATE pfit_t_test_criteria SET
                age           = '$age',
                gender        = '$gender',
                min           = '$min',
                max           = '$max',
                type_test     = '$type_test',
                test_suggest  = '$test_suggest',
                test_suggest_en  = '$test_suggest_en',
                status        = '$status'
                WHERE test_criteria_code = '$test_criteria_code' ";

    }else{
      if($test_criteria_code == ""){
        $sql    = "SELECT test_criteria_code FROM pfit_t_test_criteria where test_criteria_code like '$test_code%' order by test_criteria_code desc";
        $query  = DbQuery($sql,null);

        $json = json_decode($query, true);
        $num  = $json['dataCount'];
        $row  = $json['data'];

        if($num > 0){
          $test_criteria_code = $row[0]['test_criteria_code'];
        }
      }

      if(!empty($test_criteria_code)){
        $lastNum = substr($test_criteria_code,strlen($test_code));
        $lastNum = $lastNum + 1;
        $test_criteria_code = $test_code.sprintf("%03d", $lastNum);
      }else{
        $test_criteria_code = $test_code.sprintf("%03d", 1);
      }
      $sql = "INSERT INTO pfit_t_test_criteria
           (test_criteria_code,test_code,age,gender,min,max,category_criteria_detail_code,type_test,test_suggest,status,test_suggest_en)
           VALUES('$test_criteria_code','$test_code','$age','$gender','$min','$max','$category_criteria_detail_code','$type_test','$test_suggest','$status','$test_suggest_en')";
    }
    //echo "<br>====>".$sql;

    $query  = DbQuery($sql,null);
    $row    = json_decode($query, true);
    $statusErr = $row['status'];

    if($statusErr != "success"){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail :'.$sql)));
    }
  }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
?>
