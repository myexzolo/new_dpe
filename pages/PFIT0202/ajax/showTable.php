<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>ลำดับ</th>
      <th>รหัสฐาน</th>
      <th>ชื่อฐานการทดสอบ</th>
      <th>องค์ประกอบที่ต้องการวัด</th>
      <th style="width:70px;">ข้อมูล</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql = "SELECT * FROM pfit_t_test order by test_opjective,test_code";

      $query     = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {

        // $url = "qr/index.php?test_code=".$value['test_code'];
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['test_code'];?></td>
      <td style="text-align:left;"><?=$value['test_name'];?></td>
      <td style="text-align:left;"><?=getTestOpjective($value['test_opjective']);?></td>
      <td><a class="btn_point text-green"><i class="fa fa-file-text-o" onclick="showData('<?=$value['test_code']?>','<?=$value['test_name']?>','<?=$value['category_criteria_code']?>')"></i></a></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
