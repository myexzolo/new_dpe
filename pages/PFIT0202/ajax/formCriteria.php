<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$testCode             = $_POST['testCode'];
$categoryCriteriaCode = $_POST['categoryCriteriaCode'];
$age                  = $_POST['age'];
$gender               = $_POST['gender'];
$statusData           = $_POST['statusData'];
$disabled = '';
$visibility = '';
$type_test  = "";
$status = "";



$sqlCat = "SELECT * FROM pfit_t_cat_criteria_detail where category_criteria_code = '$categoryCriteriaCode' order by category_criteria_detail_code";
$queryCat = DbQuery($sqlCat,null);
$jsonCat = json_decode($queryCat, true);
$numCat  = $jsonCat['dataCount'];
$rowCat  = $jsonCat['data'];

// echo $sqlCat;

if($numCat > 0){
  for ($i=0; $i < $numCat ; $i++) {
    $minArr[$i] = "";
    $maxArr[$i] = "";
    $testSuggestArr[$i] = "";
    $testSuggestEnArr[$i] = "";
  }
}

if(!empty($age)){
  $sql = "SELECT * FROM pfit_t_test_criteria where test_code  = '$testCode' and age = '$age' and gender = '$gender'";
  //echo $sql;
  $query = DbQuery($sql,null);

  $json = json_decode($query, true);
  $num  = $json['dataCount'];
  $row  = $json['data'];

  $type_test          = $row[0]['type_test'];
  $status             = $row[0]['status'];
  for ($i=0; $i < $num ; $i++) {

    $category_criteria_detail_code = $row[$i]['category_criteria_detail_code'];
    $min                = $row[$i]['min'];
    $max                = $row[$i]['max'];
    $test_suggest       = $row[$i]['test_suggest'];
    $test_suggest_en    = $row[$i]['test_suggest_en'];

    for($z=0; $z < $numCat; $z++)
    {
      if($rowCat[$z]['category_criteria_detail_code'] == $category_criteria_detail_code){
        $minArr[$z] = $min;
        $maxArr[$z] = $max;
        $testSuggestArr[$z] = $test_suggest;
        $testSuggestEnArr[$z] = $test_suggest_en;
        break;
      }
    }

  }
  if($statusData == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';
    //echo $statusType;
  }
}

?>

<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>อายุ</label>
      <input type="hidden" value="<?= $statusData ?>" name="type" required>
      <input type="hidden" name="test_code" value="<?= $testCode; ?>" required>
      <input type="number" name="age" value="<?= $age ?>" class="form-control" placeholder="อายุ" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เพศ</label>
      <div class="form-control" style="border-style: hidden;">
        <input <?= $disabled?> type="radio" name="gender" id="gender_m" class="minimal" value="M" <?= $gender == 'M'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
        <input <?= $disabled?> type="radio" name="gender" id="gender_f" class="minimal" value="F" <?= $gender == 'F'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ประเภทผู้ทดสอบ</label>
      <select name="type_test" class="form-control" <?=$disabled ?> required>
        <option></option>
        <option value="1" <?= ($type_test == '1' ? 'selected="selected"':'') ?>>บุคคลทั่วไป</option>
        <option value="2" <?= ($type_test == '2' ? 'selected="selected"':'') ?>>นักกีฬา</option>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>รายการแปลผลการทดสอบ</label>
      <table class="table table-bordered table-striped" id="tableDisplay">
        <thead>
      		<tr>
            <th>ผลการทดสอบ</th>
            <th>Min</th>
            <th>Max</th>
            <th>คำแนะนำภาษาไทย</th>
            <th>คำแนะนำภาษาอังกฤษ</th>
      		</tr>
          <tbody>
            <?php
            if($numCat > 0){
              for ($i=0; $i < $numCat ; $i++) {
                $selected = "";
                $categoryCriteriaDetailName = $rowCat[$i]['category_criteria_detail_name'];
                $categoryCriteriaDetailCode = $rowCat[$i]['category_criteria_detail_code'];
            ?>
            <tr>
              <td>
                <?= $categoryCriteriaDetailName ?>
                <input type="hidden" name="categoryCrDetCode[]" value="<?= $categoryCriteriaDetailCode ?>">
              </td>
              <td style="width:100px">
                <input type="text" name="min[]" value="<?= $minArr[$i] ?>" class="form-control" placeholder="ค่าต่ำสุดที่ใช้วัดเกณฑ์" <?=$disabled ?> required>
              </td>
              <td style="width:100px">
                <input type="text" name="max[]" value="<?= $maxArr[$i] ?>" class="form-control" placeholder="ค่าสูงสุดที่ใช้วัดเกณฑ์" <?=$disabled ?> required>
              </td>
              <td><textarea class="form-control" rows="5" name="test_suggest[]" <?=$disabled ?>><?= $testSuggestArr[$i]; ?></textarea></td>
              <td><textarea class="form-control" rows="5" name="test_suggest_en[]" <?=$disabled ?>><?= $testSuggestEnArr[$i]; ?></textarea></td>
        		</tr>
            <?php
                }
              }
            ?>
          <tbody>
      </table>
    </div>
  </div>
</div>
