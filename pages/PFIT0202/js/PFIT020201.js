$(function () {
    showTableCriteria();
})


function showTableCriteria(){
  var test_code = $("#test_code").val();
  $.post( "ajax/showTableCriteria.php",{test_code:test_code})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function openForm(status,testCode,categoryCriteriaCode){
  $('#myModal').modal({backdrop:'static'});
  formShows(testCode,status,'','',categoryCriteriaCode);
}

function searchData(testCode,status,age,gender,categoryCriteriaCode){
  $('#myModal').modal({backdrop:'static'});
  formShows(testCode,status,age,gender,categoryCriteriaCode);
}

function formShows(testCode,status,age,gender,categoryCriteriaCode){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }

  $.post("ajax/formCriteria.php",{testCode:testCode,statusData:status,age:age,gender:gender,categoryCriteriaCode:categoryCriteriaCode})
  .done(function( data ) {
      $("#formShow").html(data);
  });
}

function formDelV2(testCode,age,gender){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      //alert(testCode+","+age+","+gender);
      $.post("ajax/delete.php",{testCode:testCode,age:age,gender:gender})
        .done(function( data ) {
          if(data.status == "success"){
            $.smkAlert({text: 'ลบข้อมูล เรียบร้อย!! ', type:'success'});
          }else{
            $.smkAlert({text: data.message,type: data.status});
          }
          showTableCriteria();
      });
    }
  });
}


$('#formData').on('submit', function(event) {
  event.preventDefault();
  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        showTableCriteria();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
