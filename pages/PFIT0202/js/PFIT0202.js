
$(function () {
    showTable();
})

function chkRequired(obj,id)
{
  $('#'+id).val(obj.value);
  console.log("id :"+ id);
}

function showData(test_code,test_name,category_criteria_code)
{
  var url = "PFIT020201.php?test_code="+test_code+"&test_name="+test_name+"&category_criteria_code="+category_criteria_code;
  postURL(url);
}



function upload(){
  $('#myModal2').modal({backdrop:'static'});
  $.get("ajax/formUpload.php")
  .done(function( data ) {
      $("#formShow2").html(data);
  });
}

function showTableHistory(){
  $.post("ajax/showHisUpload.php",{type_file:'pfit_t_test_criteria'})
  .done(function( data ) {
      $("#show-history").html(data);
  });
}


function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 //console.log(form);
 form.submit();
}

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",code=""){
  $.post("ajax/form.php",{value:value,code:code})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
      CKEDITOR.replace( 'content_data',{
        height: '200px'
      } );
  });
}

function runNo(value, row, index) {
  return [index+1].join('');
}


function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}


$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();;
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        //$('#formDataUpload').smkClear();
        showTableHistory();
    }).fail(function (jqXHR, exception) {
        alert("ไม่สามารถนำเข้าได้");
    });
  }
});
