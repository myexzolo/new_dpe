<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - เกณฑ์การทดสอบ</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];

      if(isset($_POST['test_code'])){
        $test_code              = $_POST['test_code'];
        $test_name              = $_POST['test_name'];
        $category_criteria_code = $_POST['category_criteria_code'];
      }else{
        exit("<script>window.location='../PFIT0202/';</script>");
      }
      //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$_SESSION['RE_URI'];
    ?>
    <link rel="stylesheet" href="css/PFIT0202.css">
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>เกณฑ์การทดสอบ  <small>PFIT0202</small></h1>

          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="../PFIT0202/">เกณฑ์การทดสอบ</a></li>
            <li class="active">ข้อมูลเกณฑ์การทดสอบ</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php"); ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">

                  <?php if($_SESSION['ROLE_USER']['is_import'])
                  {
                  ?>
                    <h3 class="box-title">
                        <?= $test_name ?>
                    </h3>
                    <div class="box-tools pull-right" id="addCriteria">
                      <a class="btn btn-block btn-social btn-success btnAdd" onclick="openForm('ADD','<?=$test_code?>','<?=$category_criteria_code?>')" style="text-align:center;">
                        <i class="fa fa-plus"></i> เพิ่ม
                      </a>
                    </div>
                  <?php
                  }
                  ?>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <input type="hidden" value="<?= $test_code ?>" id="test_code">
                  <div id="showTable"></div>
                </div>
                <div class="box-footer with-border">
                  <div class="box-tools pull-right">
                    <button class="btn btn-primary" onclick="gotoPage('../PFIT0202/')" style="width:80px;">ย้อนกลับ</button>
                  </div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">เกณฑ์การทดสอบ</h4>
                    </div>
                    <form id="formData" novalidate>
                    <!-- <form novalidate action="ajax/AED.php" method="post"> -->
                      <div class="modal-body" id="formShow">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                        <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="../../ckeditor/ckeditor.js"></script>
    <script src="js/PFIT020201.js"></script>
  </body>
</html>
