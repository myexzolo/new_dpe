showTable();

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",id=""){
  $.post("ajax/form.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล User : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AEDUsers.php",{action:'DEL',user_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


function resetPass(id){
  $.post("ajax/formResetPass.php",{id:id})
    .done(function( data ) {
      $('#myModalReset').modal({backdrop:'static'});
      $('#show-form-rw').html(data);
  });
}

function checkUserCode(){
  var text = $('#user_login').val();
  $.post("ajax/checkUserCode.php",{UserCode:text})
  .done(function( data ) {
      //console.log(data);
      if(data.status)
      {
        $.smkAlert({
          text: 'User Login ซ้ำ !!',
          type: 'danger',
          position:'top-center'
        });
        $('#user_login').val('');
        $('#user_login').focus();
      }
    });
  }

function getProvince(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#district').html(data);
        getDistrict($('#district').val(),'D');
  });
}

function getDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#subdistrict').html(data);
        getSubDistrict($('#subdistrict').val(),'SD');
  });
}

function getSubDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#zipcode').val(data.zip_code);
  });
}

$('#formAddUsers').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formAddUsers').smkValidate()) {

    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AEDUsers.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#fff',
                barColor: '#242d6d',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formAddUsers').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AEDUsers.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddUsers').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  }
});


$('#formResetPassword').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formResetPassword').smkValidate()) {
      if( $.smkEqualPass('#pass1', '#pass2') ){
          $.ajax({
              url: 'ajax/AEDUsers.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            console.log(data);
            $.smkProgressBar({
              element:'body',
              status:'start',
              bgColor: '#fff',
              barColor: '#242d6d',
              content: 'Loading...'
            });
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              $('#formResetPassword').smkClear();
              showTable();
              showSlidebar();
              $.smkAlert({text: data.message,type: data.status});
              $('#myModalReset').modal('toggle');
            }, 1000);
          });
      }
  }
});
