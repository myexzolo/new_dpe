<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id = $_POST['id'];


$sql   = "SELECT *
          FROM t_user u
          LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
          where u.user_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $user_login     = $row[0]['user_login'];
  $user_name      = $row[0]['user_name'];
  $role_list      = $row[0]['role_list'];
  $user_password  = $row[0]['user_password'];
  $note1          = $row[0]['note1'];
  $note2          = $row[0]['note2'];
  $is_active      = $row[0]['is_active'];
  $member_name      = isset($row[0]['member_name'])?$row[0]['member_name']:"";
  $member_lname     = isset($row[0]['member_lname'])?$row[0]['member_lname']:"";
  $department_name  = isset($row[0]['department_name'])?$row[0]['department_name']:"";
  $email            = isset($row[0]['email'])?$row[0]['email']:"";
  $pw               = isset($row[0]['pw'])?$row[0]['pw']:"";

  $user_img       = isset($row[0]['user_img'])?$row[0]['user_img']:"";

  $readonly       = "";

  $sqlp   = "SELECT count(user_login) as count
             FROM pfit_t_project
             where user_login = '$user_login'
             group by user_login";

 //echo $sqlp;
 $queryp      = DbQuery($sqlp,null);
 $jsonp       = json_decode($queryp, true);
 $rowp        = $jsonp['data'];

 $count       = isset($rowp[0]['count'])?$rowp[0]['count']:0;

 if($count > 0)
 {
   $readonly       = "readonly";
 }


 // print_r($_SESSION['member']);

?>
<input type="hidden" id="action" name="action" value="RESET">
<input type="hidden" name="user_id" value="<?=@$id?>">
<input type="hidden" name="member_name" value="<?=$member_name?>">
<input type="hidden" name="member_lname" value="<?=$member_lname?>">
<input type="hidden" name="department_name" value="<?=$department_name?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>User Login</label>
        <input value="<?=@$user_login?>" data-smk-msg="&nbsp;" name="user_login" type="text" class="form-control" placeholder="User Login" required <?= $readonly ?>>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?=@$user_name?>" data-smk-msg="&nbsp;" name="user_name" type="text" class="form-control" placeholder="Name" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>E-Mail</label>
        <input value="<?=$email?>" data-smk-msg="&nbsp;" name="email" type="text" class="form-control" placeholder="email" required readonly>
      </div>
    </div>
    <?php if(@$_SESSION['member'][0]['user_login'] == "admin"){?>
    <div class="col-md-3">
      <div class="form-group">
        <label>Password</label>
        <input value="<?=$pw ?>" data-smk-msg="&nbsp;" type="text" class="form-control" placeholder="Password" required>
      </div>
    </div>
  <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <label>Reset Password</label>
        <input value="" data-smk-msg="&nbsp;" name="user_password" id="pass1" type="password" autocomplete="new-password"  class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" data-smk-msg="&nbsp;" name="cfm_user_password" id="pass2" type="password" autocomplete="new-password"  class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
