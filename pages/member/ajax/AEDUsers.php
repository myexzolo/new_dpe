<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


if(!isset($_REQUEST)){
  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata,true);
}else{
  $request = $_REQUEST;
}

$action           = isset($request['action'])?$request['action']:"";
$typeDevice       = isset($request['typeDevice'])?$request['typeDevice']:"";

$user_id          = isset($_POST['user_id'])?$_POST['user_id']:"";
$member_name      = isset($request['member_name'])?$request['member_name']:"";
$member_lname     = isset($request['member_lname'])?$request['member_lname']:"";
$department_name  = isset($request['department_name'])?$request['department_name']:"";
$email            = isset($request['email'])?$request['email']:"";
$tel              = isset($request['tel'])?$request['tel']:"";
$objective        = isset($request['objective'])?$request['objective']:"";
$address          = isset($request['address'])?$request['address']:"";
$province_id      = isset($request['province_id'])?$request['province_id']:"";
$district_id      = isset($request['district_id'])?$request['district_id']:"";
$subdistrict_id   = isset($request['subdistrict_id'])?$request['subdistrict_id']:"";
$zipcode          = isset($request['zipcode'])?$request['zipcode']:"";

$user_login       = isset($request['user_login'])?$request['user_login']:"";
$password         = isset($request['user_password'])?$request['user_password']:"";
$pw               = isset($request['pw'])?$request['pw']:"";
$user_name        = $member_name." ".$member_lname;
// $user_img   = isset($_POST['$request'])?$request['action']:"";

$role_list        = @implode(",",isset($request['role_list'])?$request['role_list']:"");
$is_active        = isset($_POST['is_active'])?$_POST['is_active']:"";
$approve          = isset($_POST['approve'])?$_POST['approve']:"";

$user_password    = @md5($password);

$user_img   = "";
$document   = "";
$updateImg  = "";
$updateDoc  = "";

$user_id_update = isset($_SESSION['member'][0]['user_id'])?$_SESSION['member'][0]['user_id']:"";

if($user_id_update == "")
{
    exit("<script>window.location='../../../pages/login/'</script>");
}

$pathImg     = "../../../image/user/";
$pathUpload = "../../../upload/doc/";

$txt = "";
if($typeDevice == "WEB"){
    $regTxt = "REG WEB";
    $txt    = " ผ่านช่องทาง Website";
}else if($typeDevice == "MOBILE"){
    $regTxt = "REG MOBILE";
    $txt    = " ผ่านช่องทาง Mobile App";
}

if(isset($_FILES["user_img"]) && $typeDevice == "WEB")
{
  //$user_img = resizeImageToBase64($_FILES["user_img"],'256','256','100',$user_id_update,$path);
  $user_img = resizeImageToUpload($_FILES["user_img"],'256','256',$pathImg,$user_login);

  if($action == "EDIT" && $user_img != "")
  {
    $updateImg = "user_img = '$user_img',";
  }
}

if(isset($_FILES["document"]) && $typeDevice == "WEB")
{
  $document = uploadFile($_FILES["document"],$pathUpload,$user_login);

  if($action == "EDIT" && $document != "")
  {
    $updateDoc = "document = '$document',";
  }
}

if($action == 'ADD'){

  $sqlid  = "SELECT T_USER_SEQ.NEXTVAL AS next_id FROM DUAL";

  $query      = DbQuery($sqlid,null);
  $json       = json_decode($query, true);
  $user_id    = $json['data'][0]['next_id'];

  $sqlUser = "INSERT INTO t_user
             (user_id,user_login,user_name,user_password,is_active,
               role_list,user_img,user_id_update,note1,note2,pw,type_user)
             VALUES
             ($user_id,'$user_login','$user_name','$user_password','Y',
             '$role_list','$user_img','1','','','$password','MEMBER')";

  $query      = DbQuery($sqlUser,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];

  if(intval($errorInfo[0]) == 0)
  {
    $sqlMember  = "INSERT INTO pfit_t_member
                   (user_id,member_name,member_lname,department_name,email,tel,
                    objective,address,province_id,district_id,subdistrict_id,zipcode,document)
                   VALUES
                   ('$user_id','$member_name','$member_lname','$department_name','$email','$tel',
                    '$objective','$address','$province_id','$district_id','$subdistrict_id','$zipcode','$document')";
  }
  else
  {
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else if($action == 'EDIT'){
    $sqlUser = "UPDATE t_user SET
                  user_name       = '$user_name',
                  is_active       = '$is_active',
                  role_list       = '$role_list',
                  $updateImg
                  user_id_update  = '$user_id_update'
                  WHERE user_id   = $user_id";
    //echo $sqlUser;
    DbQuery($sqlUser,null);

    $sqlMember  = "UPDATE pfit_t_member SET
                   member_name = '$member_name',
                   member_lname = '$member_lname',
                   department_name = '$department_name',
                   email = '$email',
                   tel = '$tel',
                   objective = '$objective',
                   $updateDoc
                   address = '$address',
                   province_id = '$province_id',
                   district_id = '$district_id',
                   subdistrict_id = '$subdistrict_id',
                   zipcode = '$zipcode'
                   WHERE user_id   = $user_id";


   if($approve == 'Y' && $is_active == 'Y')
   {
     if(filter_var($email, FILTER_VALIDATE_EMAIL)){

       $sqlTemp = "SELECT * FROM  t_template where is_active = 'Y' ORDER BY TEMP_ID";

       //echo $sql;
       $queryTemp       = DbQuery($sqlTemp,null);
       $jsonTemp        = json_decode($queryTemp, true);
       $rowTemp         = $jsonTemp['data'];
       $dataCountTemp   = $jsonTemp['dataCount'];
       //print_r($json);
       $userManual = "";
       for($i=0 ; $i < $dataCountTemp ; $i++)
       {
         $temp_name = $rowTemp[$i]['temp_name'];
         $temp_file = $rowTemp[$i]['temp_file'];

         $userManual .= "https://sportsci.dpe.go.th/upload/doc/$temp_file"." $temp_name <br />";
       }

       require '../../../PHPMailer/PHPMailerAutoload.php';

       $content =  "อนุมัติผู้ใช้งาน ระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย<br>".
                   "ชื่อ - สกุล : ".$member_name." ".$member_lname."<br />".
                   "ชื่อหน่วยงาน/สังกัด : ".$department_name."<br />".
                   "Username : $user_login <br />".
                   "Password : $pw <br />".
                   "https://sportsci.dpe.go.th/pages/index/"." เว็บไซตหลัก <br />".$userManual.
                   "https://www.youtube.com/playlist?list=PLfUBM7H4pI6fltobXvN9MshrKymc2ln44"." วีดีโอขั้นตอนการทดสอบสมรรถภาพทางกายของเด็ก เยาวชนและประชาชนไทย <br />".
                   "https://drive.google.com/drive/folders/16VKDtTg8iaV7SiH5PsFeyYWupulhJGyG?usp=sharing"." ใบบันทึกผลการทดสอบ <br />".
                   "http://sportscience.dpe.go.th/"." เว็ปไซด์สำนักวิทยาศาสตร์การกีฬา";

       $arr['email']    = $email;
       $arr['title']    = 'อนุมัติผู้ใช้งานระบบ กรมพลศึกษา';
       $arr['content']  = $content;

       if(mailsend2($arr) == 200){
         $status = 200;
         $message = 'Appove Success. Please check your Email';

       }else{
         $status = 401;
         $message = 'mail fail';
       }
     }else{
       header('Content-Type: application/json');
       exit(json_encode(array('status' => 'danger','message' => 'Invalid email format')));
     }
   }

}else if($action == 'RESET'){

  $dateNow   =  queryDateTimeOracle(date('Y/m/d H:i:s'));
  $sqlMember = "UPDATE t_user SET
                user_login  = '$user_login',
                user_password  = '$user_password',
                pw  = '$password',
                update_date    =  $dateNow,
                user_id_update = '$user_id_update'
                WHERE user_id  = '$user_id'";


  if (filter_var($email, FILTER_VALIDATE_EMAIL))
  {
    require '../../../PHPMailer/PHPMailerAutoload.php';

    $content =  "Reset Password ระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย<br>".
                "ชื่อ - สกุล : ".$member_name." ".$member_lname."<br />".
                "ชื่อหน่วยงาน/สังกัด : ".$department_name."<br />".
                "Username : $user_login <br />".
                "Password : $password ";
    if($email != ""){
      $arr['email']    = $email;
      $arr['title']    = 'Reset Password ผู้ใช้งานระบบ กรมพลศึกษา';
      $arr['content']  = $content;

      if(mailsend2($arr) == 200){
        $status = 200;
        $message = 'Appove Success. Please check your Email';

      }else{
        $status = 401;
        $message = 'mail fail';
      }
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Invalid email format')));
  }

}else if($action == "DEL"){
  $sqlMember   = "UPDATE t_user SET is_active = 'D' WHERE user_id = '$user_id'";
}

//echo $sqlMember;
$query      = DbQuery($sqlMember,null);
$row        = json_decode($query, true);
//print_r($row);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){

  // $message   = "\r\n"."มีผู้ลงทะเบียน ".$txt."\r\n";
  // $message  .= "ชื่อผู้ลงทะเบียน : ".$user_name."\r\n";
  // $message  .= "หน่วยงาน : ".$department_name."\r\n";
  // $message  .= "เบอร์โทรติดต่อ : ".$tel."\r\n";
  // $message  .= "E-mail : ".$email."\r\n";
  // $message  .= "สถานะ : รอตรวจสอบข้อมูล";
  // $message  .= "Link : http://sportsci.dpe.go.th/pages/login/";
  //
  //
  // $token = GET_LINE_TOKEN("");
  //
  // LINE_NOTIFY_MESSAGE($message,"",$token);

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
