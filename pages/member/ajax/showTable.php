<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>No.</th>
      <th>User Login</th>
      <th>ชื่อ</th>
      <th>หน่วยงาน</th>
      <th style="width:100px">สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <th style="width:100px;">อนุมัติ/แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
       $sqls   = "SELECT u.*,pm.department_name
                 FROM t_user u
                 LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
                 where u.is_active not in ('D','C') and  u.type_user = 'MEMBER'
                 ORDER BY (
                 case u.is_active
                 when 'R' then 0
                 when 'Y' then 1
                 when 'C' then 2
                 end
            ),u.update_date desc,u.user_login,pm.department_name";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
          $is_active = "";
          $color = "";
          $fa = "";
          if($value['is_active'] == 'Y')
          {
            $is_active = "ใช้งาน";
            $fa = "fa-check-square-o";
            $text = "text-green";
          }
          else if($value['is_active'] == 'R')
          {
            $is_active = "รอตรวจสอบ";
            $color = "color:red;";

            $fa = "fa fa-check-square-o";
            $text = "text-yellow";
          }
          else
          {
            $is_active = "ไม่ใช้งาน";
            $fa = "fa fa-minus-square-o";
            $text = "text-red";

          }
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$value['user_login']?></td>
      <td align="left"><?=$value['user_name']?></td>
      <td align="left"><?=$value['department_name']?></td>
      <td style="<?=$color ?>"><?=$is_active ?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow "><i class="fa fa-key" onclick="resetPass('<?=$value['user_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['user_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['user_id']?>','<?=$value['user_login']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
