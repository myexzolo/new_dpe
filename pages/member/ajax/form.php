<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];
$arr_role_list = array();

$btn = 'บันทึก';

$user_id      = "";
$user_login   = "";
$user_name    = "";

$member_name      = "";
$member_lname     = "";
$department_name  = "";
$email            = "";
$tel              = "";
$objective        = "";
$document         = "";
$address          = "";
$province_id      = "";
$district_id      = "";
$subdistrict_id   = "";
$zipcode          = "";
$pw               = "";


$isActive = 'ใช้งาน';
$isInActive = 'ไม่ใช้งาน';


$colD = "col-md-5";

if($action == 'EDIT'){


  $sqls   = "SELECT *
            FROM t_user u
            LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
            where u.user_id = '$id'";

  //echo $sqls;

  $query      = DbQuery($sqls,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];



  $user_id      = $row[0]['user_id'];
  $user_login   = $row[0]['user_login'];
  $user_name    = $row[0]['user_name'];
  // $user_last    = $row[0]['user_last'];
  // $user_email   = $row[0]['user_email'];
  $role_list    = $row[0]['role_list'];
  $user_password= $row[0]['user_password'];
  $note1        = $row[0]['note1'];
  $note2        = $row[0]['note2'];
  $is_active    = $row[0]['is_active'];
  $pw           = $row[0]['pw'];
  $user_img     = isset($row[0]['user_img'])?"../../image/user/".$row[0]['user_img']:"";
  // $branch_code  = $row[0]['branch_code'];
  // $department_id = $row[0]['department_id'];

  $member_name      = isset($row[0]['member_name'])?$row[0]['member_name']:"";
  $member_lname     = isset($row[0]['member_lname'])?$row[0]['member_lname']:"";
  $department_name  = isset($row[0]['department_name'])?$row[0]['department_name']:"";
  $email            = isset($row[0]['email'])?$row[0]['email']:"";
  $tel              = isset($row[0]['tel'])?$row[0]['tel']:"";
  $objective        = isset($row[0]['objective'])?$row[0]['objective']:"";
  $document         = isset($row[0]['document'])?$row[0]['document']:"";
  $address          = isset($row[0]['address'])?$row[0]['address']:"";
  $province_id      = isset($row[0]['province_id'])?$row[0]['province_id']:"";
  $district_id      = isset($row[0]['district_id'])?$row[0]['district_id']:"";
  $subdistrict_id   = isset($row[0]['subdistrict_id'])?$row[0]['subdistrict_id']:"";
  $zipcode          = isset($row[0]['zipcode'])?$row[0]['zipcode']:"";


  if(!empty($role_list)){
      $arr_role_list = explode(",",$role_list);
  }

  $approve = "";
  if($is_active == "R"){
    $is_active == "Y";
    $isActive = 'อนุมัติ';
    $isInActive = 'ไม่อนุมัติ';
    $approve = "Y";
  }

  $linkDoc = "";
  if($document != ""){
    $colD = "col-md-4";
    $linkDoc = "../../upload/doc/".$document;
  }

}

$optionProvince     = getoptionProvince($province_id);
$optionDistrict     = getoptionDistrict($province_id,$district_id);
$optionSubDistrict  = getoptionSubDistrict($district_id,$subdistrict_id);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" id="typeDevice" name="typeDevice" value="WEB">
<input type="hidden" name="approve" value="<?= $approve ?>">
<input type="hidden" name="user_id" value="<?=$user_id?>">

<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?= $member_name ?>" name="member_name" type="text" class="form-control" placeholder="ชื่อ"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>นามสกุล</label>
        <input value="<?= $member_lname ?>" name="member_lname" type="text" class="form-control" placeholder="นามสกุล"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ชื่อหน่วยงาน/สังกัด</label>
        <input value="<?= $department_name ?>" name="department_name" type="text" class="form-control" placeholder="ชื่อหน่วยงาน"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Email</label>
        <input value="<?= $email ?>" name="email" type="email" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เบอร์โทรศัพท์</label>
        <input value="<?= $tel ?>" name="tel" type="tel" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="<?=$colD?>">
      <div class="form-group">
        <label>เอกสารของหน่วยงาน (JPG,PNG,PDF ไฟล์)</label>
        <input val name="document" type="file" class="form-control custom-file-input" accept="image/jpeg,image/png,application/pdf">
      </div>
    </div>
    <?php if($document != "") {?>
    <div class="col-md-1">
      <div class="form-group"style="margin-top: 38px;">
        <a href="<?=$linkDoc?>" target="_blank"><i class="fa fa-file-text-o" style="font-size:26px"></i></a>
      </div>
    </div>
  <?php }?>
    <div class="col-md-12">
      <div class="form-group">
        <label>วัตถุประสงค์ของการขอใช้งาน</label>
        <input value="<?=$objective?>" name="objective" type="text" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-7">
      <div class="form-group">
        <label>ที่อยู่หน่วยงาน</label>
        <input value="<?=$address?>" name="address" type="text" class="form-control" placeholder="เลขที่ หมู่ อาคาร"  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>จังหวัด</label>
        <select name="province_id" id="province" onchange="getProvince(this.value,'P')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?=$optionProvince;?>
        </select>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>อำเภอ</label>
        <select name="district_id" id="district" onchange="getDistrict(this.value,'D')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?=$optionDistrict;?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ตำบล</label>
        <select name="subdistrict_id" id="subdistrict"  onchange="getSubDistrict(this.value,'SD')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?= $optionSubDistrict; ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสไปรษณีย์</label>
        <input name="zipcode" id="zipcode" class="form-control" value="<?=$zipcode ?>">
      </div>
    </div>
    <?php
      if($action == 'ADD'){
    ?>

    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อผู้ใช้งาน</label>
        <input value="<?=@$user_login?>" onblur="checkUserCode()" id="user_login" name="user_login" type="text" class="form-control" placeholder="User Login" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Password</label>
        <input value="" name="user_password" id="pass1" type="password" autocomplete="new-password" data-smk-msg="&nbsp;" class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" class="form-control" data-smk-msg="&nbsp;" placeholder="Confirm Password" required>
      </div>
    </div>
    <?php
  }else if($approve == "Y")
  {
    ?>
        <input value="<?= $pw ?>" name="pw" type="hidden">
        <input value="<?= $user_login ?>" name="user_login" type="hidden">
    <?php
  }
    ?>
    <div class="col-md-6">
      <div class="form-group">
        <label>รูปโปรไฟล์</label>
        <input name="user_img" type="file" class="form-control custom-file-input" onchange="readURL(this,'showImg');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;">
          <img src="<?= $user_img ?>"  onerror="this.onerror='';this.src='../../image/user.png'" style="height: 60px;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>><?= $isActive ?></option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>><?= $isInActive ?></option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <label>สิทธิการใช้งาน</label>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <?php

          $flag   = in_array('0',$arr_role_list)?false:true;

            //print_r($arr_role_list);
            //echo $flag;
          $sqlp   = "SELECT * FROM t_role WHERE is_active ='Y' and  role_id not in (0,1) ORDER BY role_id ASC";

          $queryp     = DbQuery($sqlp,null);
          $json       = json_decode($queryp, true);
          $errorInfo  = $json['errorInfo'];
          $dataCount  = $json['dataCount'];
          $rowp       = $json['data'];

          foreach ($rowp as $key => $value) {
            $display = "";
            if($value['role_id'] == 0 and $flag){
              $display = "style='display:none;'";
            }
        ?>
        <div class="col-md-6" <?=$display?> >
          <div class="checkbox">
            <label>
              <input type="checkbox" name="role_list[]" value="<?=$value['role_id']?>"
              <?= @in_array($value['role_id'], @$arr_role_list)?"checked":"" ?> required data-smk-msg="&nbsp;">
              <?=$value['role_name']?>
            </label>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;"><?=$btn ?></button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
