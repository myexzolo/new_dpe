<?php
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

error_reporting(E_ALL);

require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("DPE")
							 ->setLastModifiedBy("DPE")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Project Detail file");

$sqls   = "SELECT u.*, pm.*,  p.province_name as province_name , d.name_th as district_name, sb.name_th as subdistrict_name
           FROM t_user u
           LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
           LEFT JOIN province p ON pm.province_id = p.province_id
           LEFT JOIN districts d ON pm.district_id = d.id
           LEFT JOIN subdistricts sb ON pm.subdistrict_id = sb.id
           where u.is_active not in ('D','C') and  u.type_user = 'MEMBER'
           ORDER BY (
           case u.is_active
           when 'R' then 0
           when 'Y' then 1
           when 'C' then 2
           end
      ),u.update_date,u.user_login,pm.department_name";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$row        = $json['data'];


$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");


$objPHPExcel->getActiveSheet()->setTitle("กิจกรรม");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ลำดับ');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ชื่อ');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'นามสกุล');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ชื่อหน่วยงาน/สังกัด');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Email');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'เบอร์โทรศัพท์');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'วัตถุประสงค์ของการขอใช้งาน');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'ที่อยู่หน่วยงาน');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'จังหวัด');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'อำเภอ');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'ตำบล');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'รหัสไปรษณีย์');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'สิทธิการใช้งาน');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'ชื่อผู้ใช้งาน');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'รหัสผ่าน');

$rw = 1;
for($i=0;$i <$dataCount ;$i++)
{
  $rw++;

  $user_id      = $row[$i]['user_id'];
  $user_login   = $row[$i]['user_login'];
  $user_name    = $row[$i]['user_name'];
  // $user_last    = $row[0]['user_last'];
  // $user_email   = $row[0]['user_email'];
  $role_list    = $row[$i]['role_list'];
  $user_password= $row[$i]['user_password'];
  $note1        = $row[$i]['note1'];
  $note2        = $row[$i]['note2'];
  $is_active    = $row[$i]['is_active'];
  $pw           = $row[$i]['pw'];
  $user_img     = isset($row[$i]['user_img'])?"../../image/user/".$row[$i]['user_img']:"";

  $role_list    = $row[$i]['role_list'];


  // $branch_code  = $row[0]['branch_code'];
  // $department_id = $row[0]['department_id'];

  $member_name      = isset($row[$i]['member_name'])?$row[$i]['member_name']:"";
  $member_lname     = isset($row[$i]['member_lname'])?$row[$i]['member_lname']:"";
  $department_name  = isset($row[$i]['department_name'])?$row[$i]['department_name']:"";
  $email            = isset($row[$i]['email'])?$row[$i]['email']:"";
  $tel              = isset($row[$i]['tel'])?$row[$i]['tel']:"";
  $objective        = isset($row[$i]['objective'])?$row[$i]['objective']:"";
  $document         = isset($row[$i]['document'])?$row[$i]['document']:"";
  $address          = isset($row[$i]['address'])?$row[$i]['address']:"";
  $province_id      = isset($row[$i]['province_id'])?$row[$i]['province_id']:"";
  $district_id      = isset($row[$i]['district_id'])?$row[$i]['district_id']:"";
  $subdistrict_id   = isset($row[$i]['subdistrict_id'])?$row[$i]['subdistrict_id']:"";
  $province_name    = isset($row[$i]['province_name'])?$row[$i]['province_name']:"";
  $district_name    = isset($row[$i]['district_name'])?$row[$i]['district_name']:"";
  $subdistrict_name = isset($row[$i]['subdistrict_name'])?$row[$i]['subdistrict_name']:"";

  $zipcode          = isset($row[$i]['zipcode'])?$row[$i]['zipcode']:"";

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$rw, $i+1);
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$rw, $member_name);
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$rw, $member_lname);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$rw, $department_name);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$rw, $email);
  $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$rw, $tel, PHPExcel_Cell_DataType::TYPE_STRING);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$rw, $objective);
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$rw, $address);
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$rw, $province_name);
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$rw, $district_name);
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$rw, $subdistrict_name);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$rw, $zipcode);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$rw, 'สิทธิการใช้งาน');
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$rw, $user_name);
  $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$rw, $pw, PHPExcel_Cell_DataType::TYPE_STRING);
}


foreach(range('A','O') as $columnID) {
    $objPHPExcel->getActiveSheet(1)->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="project.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
