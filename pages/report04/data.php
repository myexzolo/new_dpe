  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");

  // $projectCode = $_GET['project_code'];
  $projectCode = "63060269";
  $stageTxt = "";

  ?>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

      <link rel="stylesheet" href="css/pdf.css">
      <script src="../../dist/js/Chart.min.js"></script>
    </head>
    <body>
      <script>
        function chartJsShow(id,name,str,str_m,str_f){
          var res = str.split("|");
          var res_m = str_m.split("|");
          var res_f = str_f.split("|");
          let myChart = document.getElementById(id).getContext('2d');
          Chart.defaults.global.defaultFontFamily = 'chatthai';
          Chart.defaults.global.defaultFontSize = 12;
          Chart.defaults.global.defaultFontColor = '#777';

          let massPopChart = new Chart(myChart, {
            type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            defaultFontSize: '8',
            data:{
              labels: res ,
              datasets:[{
                label:'ชาย',
                data:res_m,
                fill: false,
                backgroundColor:'rgba(255, 99, 132, 0.6)',
                borderWidth:2,
                borderColor:'rgba(255, 99, 132, 0.6)',
                hoverBorderWidth:1,
                hoverBorderColor:'#000',
                lineTension: 0
              },
              {
                label:'หญิง',
                data:res_f,
                fill: false,
                backgroundColor:'rgba(54, 162, 235, 0.6)',
                borderWidth:2,
                borderColor:'rgba(54, 162, 235, 0.6)',
                hoverBorderWidth:1,
                hoverBorderColor:'#000',
                lineTension: 0
              }]
            },
            options:{
              responsive: true,
              title:{
                display:true,
                fontStyle:'bold',
                text:name,
                fontSize:16
              },
              legend:{
                display:true,
                position:'bottom',
                labels:{
                  fontColor:'#000'
                }
              },
              tooltips:{
                enabled:true
              },
              plugins: [{
                afterRender: function () {
                  // Do anything you want
                  renderIntoImage()
                },
              }],
              scales: {
                yAxes: [{
                    ticks: {
                        // max: 150,
                        min: 0,
                        stepSize: 10
                    }
                }]
              }
            }
          });

          // console.log(">>>>>>>>."+id);
          //
          const renderIntoImage = () => {
            console.log("renderIntoImage");
            // const canvas = document.getElementById(id)
            //
            // var idImg = id +"_m";
            // const imgWrap = document.getElementById(idImg)
            // var img = new Image();
            // img.src = canvas.toDataURL()
            // imgWrap.appendChild(img)
            // canvas.style.display = 'none'
          }

          setTimeout(function(){
            let canvas = document.getElementById(id);
            var idImg = id +"_m";
            let imgWrap = document.getElementById(idImg).src = canvas.toDataURL();
            canvas.style.display = 'none'
          }, 1000);

        }
      </script>
      <?php
        $break = false;
        $total_teat_c = array();
        $total_teat_p = array();

        $sql = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode'";
        $query = DbQuery($sql,null);
        $row  = json_decode($query, true);

        $num   = $row['dataCount'];
        if($num>0){

        foreach ($row['data'] as $value) {
          $project_codep = $value['project_code'];
          $project_name_title  = $value['project_name'];
          $sqlp = "SELECT DISTINCT(stage) FROM pfit_t_person WHERE project_code = '$project_codep' ORDER BY stage  ASC";
          $queryp = DbQuery($sqlp,null);
          $rowp  = json_decode($queryp, true);
          $stage = '';
          // print_r($rowp);
          $nump   = $rowp['dataCount'];

          if($nump>0){
          foreach ($rowp['data'] as $keyp => $valuep) {

            $stage = isset($valuep['stage'])?$valuep['stage']:"";
            $stage_id = isset($valuep['stage'])?$valuep['stage']:"";

            if($stage == ""){
              continue;
            }

            $sql_g = "SELECT DISTINCT(grade) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' ORDER BY grade  ASC";
            $query_g = DbQuery($sql_g,null);
            $row_g  = json_decode($query_g, true);
              //echo $stage_id;
              if($row_g['dataCount'] > 0){
              foreach ($row_g['data'] as $key_g => $value_g) {

                $grade = $value_g['grade'];

                $sql_r = "SELECT DISTINCT(room) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' ORDER BY room  ASC";
                $query_r = DbQuery($sql_r,null);
                $row_r  = json_decode($query_r, true);

                if($row_r['dataCount'] > 0){
                foreach ($row_r['data'] as $key_r => $value_r) {

                  $room   = $value_r['room'];
                  if($stage == 1){
                    $stageTxt = 'นักเรียนชั้นมัธยมศึกษาปีที่';
                  }elseif($valuep['stage'] == 2){
                    $stageTxt = 'นักเรียนชั้นประถมศึกษาปีที่';
                  }
                  $grade = $value_g['grade'];

                  $sqlpp = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                               WHERE ptpt.test_code = ptt.test_code
                               AND ptpt.project_code = '$project_codep' ORDER BY ptpt.test_seq ASC";
                  $querypp = DbQuery($sqlpp,null);
                  $rowpp  = json_decode($querypp, true);
                  $numpp   = $rowpp['dataCount'];

                  foreach ($rowpp['data'] as $keypp => $valuepp) {
                    $category_criteria_code = $valuepp['category_criteria_code'];
                    $test_code = $valuepp['test_code'];
                    $sqlppp = "SELECT * FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
                    $queryppp = DbQuery($sqlppp,null);
                    $rowppp  = json_decode($queryppp, true);
                    $numppp   = $rowppp['dataCount'];

                    foreach ($rowppp['data'] as $keyppp => $valueppp) {
                      $category_criteria_detail_code = $valueppp['category_criteria_detail_code'];

                      //echo $test_code.",".$category_criteria_detail_code."<br>";
                      if(!isset($total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                        $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                      }

                      if(!isset($total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                        $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                      }
                    }

                  }
                  if($break){
                    echo "<div class='break'></div>";
                  }else{
                    $break = true;
                  }
      ?>
      <div align="center" style="font-size:22pt">ผลการทดสอบสมรรถภาพทางกาย</div>
      <div align="center" style="font-size:20pt"><?=$project_name_title.' '?><?php echo $stageTxt.' '.$grade.'/'.$room; ?></div>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <thead>
      		<tr align="center">
      			<th rowspan="2" class="thStyle">เลขที่</th>
      			<th rowspan="2" class="thStyle">เลขประจำตัว</th>
      			<th rowspan="2" class="thStyle">เพศ</th>
      			<th rowspan="2" class="thStyle">ชื่อ</th>
      			<th rowspan="2" class="thStyle">นามสกุล</th>
      			<th class="thStyle">น้ำหนัก</th>
      			<th class="thStyle">ส่วนสูง</th>
            <?php
                foreach ($rowpp['data'] as $valuepp) {
            ?>
      			<th class="thStyle"><?php echo $valuepp['test_name']; ?></th>
            <?php } ?>

      			<th class="thStyle">คะแนน</th>
      		</tr>
      		<tr align="center">

      			<td class="thStyle">(กก.)</td>
      			<td class="thStyle">(ซม.)</td>
            <?php
                $total_max_values = 0;
                foreach ($rowpp['data'] as $valuepp) {
                  $category_criteria_code = $valuepp['category_criteria_code'];

                  $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
                  $query_value = DbQuery($sql_value,null);
                  $row_value  = json_decode($query_value, true);
                  $num_value  = $row_value['dataCount'];
                  $data_value = $row_value['data'];

                  $total_max_values += $data_value[0]['max_value'];
            ?>
      			<td class="thStyle">(<?php echo $valuepp['test_unit']; ?>)</td>
            <?php } ?>

      			<td class="thStyle">(เต็ม <?=$total_max_values; ?>)</td>
      		</tr>
        </thead>
        <tbody>
          <?php

          $sqlperson = "SELECT * FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' AND room = '$room' order by date_create";
          //echo $sqlperson;
          $queryperson = DbQuery($sqlperson,null);
          $rowperson  = json_decode($queryperson, true);
          $i = 1;
            foreach ($rowperson['data'] as $value) {
            $person_number = $value['person_number'];
            $person_name = $value['person_name'];
            $person_lname = $value['person_lname'];
            $wiegth = $value['wiegth'];
            $height = $value['height'];
            $person_gender = $value['person_gender']=='M'?'ชาย':'หญิง';
            $bmi = bmi($wiegth,$height)[0];
          ?>
          <tr>
      			<td class="content" style="text-align: center;"><?= $i; ?></td>
      			<td class="content"><?= $person_number; ?></td>
      			<td class="content"><?= $person_gender; ?></td>
      			<td class="content"><?= $person_name; ?></td>
      			<td class="content"><?= $person_lname; ?></td>

      			<td class="content" style="text-align: center;"><?php echo $wiegth; ?></td>
      			<td class="content" style="text-align: center;"><?php echo $height; ?></td>
            <?php
                $total_max_value = 0;
                $chkres = "F";
                $results = array();
                $resultTxt = "";
                $n = 0;
                foreach ($rowpp['data'] as $valuepp) {
                  $test_code = $valuepp['test_code'];

                  $sqlres = "SELECT * FROM pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd, pfit_t_person ptp
                             WHERE ptc.test_criteria_code = pts.test_criteria_code
                             AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
                             AND pts.project_code = '$project_codep' AND ptp.person_number = pts.person_number
                             AND pts.test_code = '$test_code' AND pts.person_number = '$person_number'";

                  //echo $sqlres."<br>";
                  $queryres = DbQuery($sqlres,null);
                  $rowres  = json_decode($queryres, true);

                  $numres   = $rowres['dataCount'];
                  $result = '-';
                  $resultCat = "-";
                  if($numres>0){
                    if(isset($rowres['data'][0]['result'])){
                      $total_max_value += $rowres['data'][0]['category_criteria_detail_value'];
                      //$result = $rowres['data'][0]['result'];
                      $result = $rowres['data'][0]['result_cal'];

                      $resultCat = $rowres['data'][0]['category_criteria_detail_name'];

                      $category_criteria_detail_code = $rowres['data'][0]['category_criteria_detail_code'];
                      if($rowres['data'][0]['gender'] == 'M'){
                        $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                      }else{
                        $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                      }

                    }else{
                      $chkres = "N";
                    }
                  }
                  $results[$n] = $resultCat;
                  $n++;
            ?>
      			<td class="content" style="text-align: center;"><?php echo $result; ?></td>
            <?php
            }
            if($chkres == "N"){
              $total_txt_res = "ข้อมูลไม่ครบ";
            }else{
              $total_txt_res = calTestFit($total_max_values,$total_max_value,60);
            }

            ?>

      			<td class="content" style="text-align: center;"><?=$total_max_value ?></td>
      		</tr>

          <tr align="center">
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
            <?php
              for($x = 0; $x < count($results); $x++) {

            ?>
      			<td class="content" style="text-align: center;"><?php echo $results[$x]; ?></td>
            <?php } ?>

      			<td class="content" style="text-align: center;"><?=$total_txt_res; ?></td>
      		</tr>

          <?php $i++; }  ?>
      	</tbody>
      </table>

    <?php }} ?>
      <div class="break"></div>


      <div align="center" style="font-size:22pt">แผนภูมิแสดงระดับสมรรถภาพทางกาย<?=$project_name_title ?></div>
      <div align="center" style="font-size:20pt"><?php echo $stageTxt.' '.$grade; ?></div>
      <?php
        $ids = 0;
      ?>
        <div class="row" align="center">

        <?php
        $m_score = array();
        // print_r($rowpp['data']);
        if(isset($rowpp) && $rowpp['dataCount'] > 0){
        foreach ($rowpp['data'] as $keypp => $valuepp) {
          $ids = $keypp;
          $test_name = $valuepp['test_name'];
          $test_code = $valuepp['test_code'];

          $category_criteria_code = $valuepp['category_criteria_code'];

          $sql_c = "SELECT DISTINCT(pttc.category_criteria_detail_code),ptccd.category_criteria_detail_name,ptccd.category_criteria_code FROM pfit_t_cat_criteria_detail ptccd , pfit_t_test_criteria pttc
                    WHERE ptccd.category_criteria_detail_code = pttc.category_criteria_detail_code
                    AND category_criteria_code = '$category_criteria_code'";
          $query_c = DbQuery($sql_c,null);
          $row_c  = json_decode($query_c, true);
          $category_criteria_detail_name = array();
          $m_score = array();
          $f_score = array();
          $score = array();
            foreach ($row_c['data'] as $key_c => $value_c) {
              $category_criteria_detail_code = isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
              if($category_criteria_detail_code!= ""){
                $m_score[] = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
                $f_score[] = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
                // print_r($f_score);
                $category_criteria_detail_name[] =  $value_c['category_criteria_detail_name'];
              }else{
                $m_score[] = "";
                $f_score[] = "";
                // print_r($f_score);
                $category_criteria_detail_name[] =  "";
              }

            }
            $str =  implode("|",$category_criteria_detail_name);
            $str_m =  implode("|",$m_score);
            $str_f =  implode("|",$f_score);

      ?>
        <?php
          $dd  =  $stage_id.'_'.$grade.'_'.$ids;
        ?>

          <div class="col-md-6">
            <div style="margin:0px;padding-top:40px">
              <canvas id="<?= $dd ?>"></canvas>
              <img id="<?= $dd ?>_m" style="width:100%" ></img>

              <script>
                chartJsShow('<?= $dd; ?>','<?= $test_name ?>','<?= $str; ?>','<?= $str_m; ?>','<?= $str_f; ?>');
              </script>
            </div>
          <br><br>
          <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
          		<tr align="center">
                <td class="thStyle"><?=$test_name?></td>
                <td class="thStyle">ชาย/จำนวน</td>
                <td class="thStyle">หญิง/จำนวน</td>
                <td class="thStyle">รวม/คน</td>
              </tr>
            <tbody>
              <?php
                foreach ($row_c['data'] as $key_c => $value_c) {
                  $category_criteria_detail_name =  isset($value_c['category_criteria_detail_name'])?$value_c['category_criteria_detail_name']:"";
                  $category_criteria_detail_code =  isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
                  $tm = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
                  $tf = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
                  $tt = (intval($tm) + intval($tf));
              ?>
              <tr align="center">
                <td class="content"><?=$category_criteria_detail_name?></td>
                <td class="content" style="text-align: center;"><?= $tm ?></td>
                <td class="content" style="text-align: center;"><?= $tf ?></td>
                <td class="content" style="text-align: center;"><?= $tt ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <br><br>
          <div class="col-md-6">
            <p class="text-center">&nbsp;</p>
          </div>
          <div class="col-md-6">
            <p class="text-center">&nbsp;</p>
          </div>
         </div>
       <!-- </div> -->
     <?php }}
      ?>
    </div>
    <div class="break"></div>
      <?php
              }
            }
          }
        }
        }
      }
    ?>
    <script src="../../dist/js/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>
