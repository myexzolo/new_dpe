  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  // $projectCode = '63070561';

  $projectCode = $_POST['project_code'];
  $stageTxt = "";


  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();

  ?>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

      <link rel="stylesheet" href="css/pdf.css">
      <script src="../../dist/js/Chart.min.js"></script>
    </head>
    <body>
    <?php
      $sql = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode'";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);

      $project_name_title = @$json['data'][0]['project_name'];

      $sqlTest = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                   WHERE ptpt.test_code = ptt.test_code
                   AND ptpt.project_code = '$projectCode' ORDER BY ptpt.test_seq ASC";
      $queryTest   = DbQuery($sqlTest,null);
      $jsonTest    = json_decode($queryTest, true);
      $numTest     = $jsonTest['dataCount'];
      $rowTest     = $jsonTest['data'];

      $arrDetail = array();
      $arrResultPerson = array();
      for($t =0;$t < $numTest; $t++)
      {
          $test_code     = $rowTest[$t]['test_code'];

          $sqltc  = "SELECT t.category_criteria_detail_code,td.category_criteria_detail_name
                     FROM pfit_t_test_criteria t, pfit_t_cat_criteria_detail td
                     where t.test_code = '$test_code' and t.category_criteria_detail_code = td.category_criteria_detail_code
                     group by t.category_criteria_detail_code, td.category_criteria_detail_name,  td.category_criteria_detail_value
                     order by td.category_criteria_detail_value";
          $querytc = DbQuery($sqltc,null);
          $jsonTc  = json_decode($querytc, true);

          //echo $sqltc;

          $dataCountTc  = $jsonTc['dataCount'];
          $rowTc        = $jsonTc['data'];

          for($tc =0;$tc < $dataCountTc ; $tc++)
          {
            $arrDetail[$test_code][$tc]['category_d_code'] = $rowTc[$tc]['category_criteria_detail_code'];
            $arrDetail[$test_code][$tc]['category_d_name'] = $rowTc[$tc]['category_criteria_detail_name'];
          }
      }

      $sqlp = "SELECT * From pfit_t_person where  project_code = '$projectCode' order by person_number ";

     $queryp = DbQuery($sqlp,null);
     $jsonp  = json_decode($queryp, true);
     $rowp        = $jsonp['data'];
     $dataCountp  = $jsonp['dataCount'];


     $sqlres = "SELECT *
                 FROM pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd
                 WHERE ptc.test_criteria_code = pts.test_criteria_code
                 AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
                 AND pts.project_code = '$projectCode'
                 order by person_number";

     //echo $sqlres."<br>";
     $queryres = DbQuery($sqlres,null);
     $jsonRes  = json_decode($queryres, true);

     $dataCountRes  = $jsonRes['dataCount'];
     $rowRes        = $jsonRes['data'];
     $resultPerson = array();
     for($r=0; $r < $dataCountRes ; $r++)
     {
       $category_d_value = $rowRes[$r]['category_criteria_detail_value'];
       $result_cal     = $rowRes[$r]['result_cal'];
       $result         = $rowRes[$r]['result'];
       $category_d_name  = $rowRes[$r]['category_criteria_detail_name'];
       $category_d_code  = $rowRes[$r]['category_criteria_detail_code'];
       $gender         = $rowRes[$r]['gender'];
       $test_code      = $rowRes[$r]['test_code'];
       $person_number  = $rowRes[$r]['person_number'];

       $resultPerson[$person_number][$test_code]['category_d_value'] = $category_d_value;
       $resultPerson[$person_number][$test_code]['result_cal'] = $result_cal;
       $resultPerson[$person_number][$test_code]['result'] = $result;
       $resultPerson[$person_number][$test_code]['category_d_name'] = $category_d_name;
       $resultPerson[$person_number][$test_code]['category_d_code'] = $category_d_code;
       $resultPerson[$person_number][$test_code]['gender'] = $gender;

     }

  ?>
  <div align="center" style="font-size:22pt">ผลการทดสอบสมรรถภาพทางกาย</div>
  <div align="center" style="font-size:20pt"><?=$project_name_title.' '?></div>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
    <thead>
      <tr align="center">
        <th rowspan="2" class="thStyle">เลขที่</th>
        <th rowspan="2" class="thStyle">เลขประจำตัว</th>
        <th rowspan="2" class="thStyle">เพศ</th>
        <th rowspan="2" class="thStyle">ชื่อ</th>
        <th rowspan="2" class="thStyle">นามสกุล</th>
        <th class="thStyle">น้ำหนัก</th>
        <th class="thStyle">ส่วนสูง</th>
        <?php
        if($numTest > 0 )
        {
          foreach ($rowTest as $valueT)
          {
        ?>
        <th class="thStyle"><?= $valueT['test_name']; ?></th>
        <?php
          }
        }
        ?>
        <th class="thStyle">คะแนน</th>
      </tr>
      <tr align="center">
        <td class="thStyle">(กก.)</td>
        <td class="thStyle">(ซม.)</td>
        <?php
        $total_max_values = 0;
        if($numTest > 0 )
        {
          foreach ($rowTest as $valueT) {
              $category_criteria_code = $valueT['category_criteria_code'];

              $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value
              FROM pfit_t_cat_criteria_detail
              WHERE category_criteria_code = '$category_criteria_code'";
              $query_value = DbQuery($sql_value,null);
              $row_value  = json_decode($query_value, true);
              $num_value  = $row_value['dataCount'];
              $data_value = $row_value['data'];

              $total_max_values += $data_value[0]['max_value'];
        ?>
        <td class="thStyle">(<?= $valueT['test_unit']; ?>)</td>
        <?php
          }
        }
        ?>
        <td class="thStyle">(เต็ม <?=$total_max_values; ?>)</td>
      </tr>
    </thead>
    <tbody>
      <?php
      for($p=0;$p < $dataCountp; $p++)
      {
          $person_number = $rowp[$p]['person_number'];
          $person_name = $rowp[$p]['person_name'];
          $person_lname = $rowp[$p]['person_lname'];
          $wiegth = $rowp[$p]['wiegth'];
          $height = $rowp[$p]['height'];
          $person_gender = $rowp[$p]['person_gender']=='M'?'ชาย':'หญิง';
          $bmi = bmi($wiegth,$height)[0];

          $total_txt_res = "";
      ?>
      <tr>
        <td class="content" style="text-align: center;"><?= ($p + 1); ?></td>
        <td class="content"><?= $person_number; ?></td>
        <td class="content"><?= $person_gender; ?></td>
        <td class="content"><?= $person_name; ?></td>
        <td class="content"><?= $person_lname; ?></td>

        <td class="content" style="text-align: center;"><?= $wiegth; ?></td>
        <td class="content" style="text-align: center;"><?= $height; ?></td>
      <?php
      if($numTest > 0 )
        {
        $chkres = "Y";
        $total_max_value = 0;
        foreach ($rowTest as $valueT) {
          $test_code      = $valueT['test_code'];

          $category_value = @$resultPerson[$person_number][$test_code]['category_d_value'];
          $result_cal     = @$resultPerson[$person_number][$test_code]['result_cal'];
          $result         = @$resultPerson[$person_number][$test_code]['result'];
          $category_name  = @$resultPerson[$person_number][$test_code]['category_d_name'];
          $category_code  = @$resultPerson[$person_number][$test_code]['category_d_code'];
          $gender         = @$resultPerson[$person_number][$test_code]['gender'];


          if($gender == "M")
          {
            if(!isset($arrResultPerson['M'][$test_code][$category_code])){
              $arrResultPerson['M'][$test_code][$category_code] = 0;
            }
            $arrResultPerson['M'][$test_code][$category_code]++;
          }else if ($gender == "F")
          {
            if(!isset($arrResultPerson['F'][$test_code][$category_code])){
              $arrResultPerson['F'][$test_code][$category_code] = 0;
            }
            $arrResultPerson['F'][$test_code][$category_code]++;
          }

          if($category_value != ""){
            $total_max_value += $category_value;
          }

          if($result_cal == "")
          {
            $result_cal = "-";
            if($chkres == "Y")
            {
              $chkres = "N";
            }
          }
      ?>
      <td class="content" style="text-align: center;"><?= $result_cal; ?></td>
      <?php
        }
        if($chkres == "N"){
          $total_txt_res = "ข้อมูลไม่ครบ";
        }else{
          $total_txt_res = calTestFit($total_max_values,$total_max_value,60);
        }
      }
      ?>
      <td class="content" style="text-align: center;"><?=$total_max_value ?></td>
      </tr>
      <tr align="center">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <?php
        if($numTest > 0 )
        {
          foreach ($rowTest as $valueT) {
            $test_code      = $valueT['test_code'];
            $category_name  = @$resultPerson[$person_number][$test_code]['category_d_name'];
        ?>
        <td class="content" style="text-align: center;"><?= $category_name; ?></td>
      <?php
        }
      }
      ?>
        <td class="content" style="text-align: center;"><?=$total_txt_res; ?></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
  <div class="break"></div>
  <div align="center" style="font-size:22pt">แผนภูมิแสดงระดับสมรรถภาพทางกาย<br><?=$project_name_title ?></div>
  <div class="row" align="center">
  <?php
  if($numTest > 0 )
  {
    foreach ($rowTest as $keyT => $valueT)
    {
        $test_code     = $valueT['test_code'];
        $test_name     = $valueT['test_name'];

        $dd  =  $projectCode.'_'.$keyT;
        //echo $dd;
  ?>
    <div class="col-md-6">
      <div style="margin:0px;padding-top:40px">
        <img style="width:100%" src="<?= "img/".$dd."_m.png" ?>">
      </div>
      <br><br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
          <tr align="center">
            <td class="thStyle"><?=$test_name?></td>
            <td class="thStyle">ชาย/จำนวน</td>
            <td class="thStyle">หญิง/จำนวน</td>
            <td class="thStyle">รวม/คน</td>
          </tr>
        <tbody>
        <?php
        $ttm = 0;
        $ttf = 0;
        $ttt = 0;

        for($a = 0; $a < count($arrDetail[$test_code]) ; $a ++)
        {
              $category_d_name  = $arrDetail[$test_code][$a]['category_d_name'];
              $category_d_code  = $arrDetail[$test_code][$a]['category_d_code'];
              $tm = isset($arrResultPerson['M'][$test_code][$category_d_code])?$arrResultPerson['M'][$test_code][$category_d_code]:"0";
              $tf = isset($arrResultPerson['F'][$test_code][$category_d_code])?$arrResultPerson['F'][$test_code][$category_d_code]:"0";
              $tt = ($tm + $tf);

              $ttm += $tm;
              $ttf += $tf;
              $ttt += $tt;
        ?>
        <tr>
          <td class="content"><?=$category_d_name?></td>
          <td class="content" style="text-align: center;"><?= $tm ?></td>
          <td class="content" style="text-align: center;"><?= $tf ?></td>
          <td class="content" style="text-align: center;"><?= $tt ?></td>
        </tr>
        <?php
        }
        ?>
        <tr>
          <td class="thStyle" style="text-align: center;">รวม</td>
          <td class="thStyle" style="text-align: center;"><?= $ttm ?></td>
          <td class="thStyle" style="text-align: center;"><?= $ttf ?></td>
          <td class="thStyle" style="text-align: center;"><?= $ttt ?></td>
        </tr>
        </tbody>
      </table>
    </div>
    <?php
      }
    }
    ?>
   </div>
  <?php
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('รายงานผลการทดสอบโรงเรียน');
    $mpdf->SetJS('print();');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('L','','','','',5,5,10,10,10,10);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
