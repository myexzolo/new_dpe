<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  $typeDate   = $_POST['typeDate'];
  $user_login_s   = @$_POST['user_login_s'];
  $role_id        = isset($_POST['role_id'])?$_POST['role_id']:"";

  $dateStart = queryDate($dateStart);
  $dateEnd   = queryDate($dateEnd);

  $user_login = $_SESSION['member'][0]['user_login'];
  $adminRole  = $_SESSION['member'][0]['adminRole'];

  $con = " and u.user_login = 'emtry'";

  if($user_login != ""  && $adminRole == ""){
      $con = " and u.user_login = '".$user_login."'";
  }
  else if ($adminRole != "")
  {
    $con = "";
  }

  $conr = "";
  //echo count($role_id);
  if($role_id != "" && count($role_id) > 0)
  {
    $role = implode(",",$role_id);
    for($x=0; $x < count($role_id); $x++)
    {
        $roleId = $role_id[$x];
        if($roleId != "")
        {
          if($conr == "")
          {
            $conr .= " and (','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }else{
            $conr .= " or ','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }
        }

    }
    if($conr != ""){
      $conr .= ")";
      $con  .= " and user_login in (select u.user_login FROM t_user u where u.is_active = 'Y' $conr)";
    }
  }


  if($user_login_s != ""){
      $con .= " and user_login = '".$user_login_s."'";
  }

  $typeDateStr = "";
  $typeDateQuery = "start_date";
  if($typeDate == "1"){
    $typeDateStr = "ตามวันที่ทดสอบ";
  }else if($typeDate == "2")
  {
    $typeDateQuery = "date_create";
    $typeDateStr = "ตามวันที่นำเข้าข้อมูล";
  }


  $sql = "SELECT project_code, project_name, TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date
  FROM pfit_t_project
  where status <> 'C' and project_type in (4) and $typeDateQuery between $dateStart and  $dateEnd $con ORDER BY start_date";

  //echo $sql;

  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];
?>
<div class="row">
<?php
  for ($i=0; $i < $num ; $i++) {
    $date = date('Y-m',strtotime($data[$i]['end_date']));
    $project_code = $data[$i]['project_code'];
    $pram = "?project_code=$project_code";
    $url = 'genChart.php'.$pram;
?>
  <div class="col-md-4 col-sm-6 col-lg-3">
    <div class="info-box btnList" style="padding:10px;" onclick="postURL_blank('<?=$url; ?>')" >
        <span style="font-size:22px;min-height:65px;" class="info-box-number"><?= $data[$i]['project_name'] ?></span>
        <span style="font-size:18px">
          วันที่ทดสอบ : <?= DateThai($data[$i]['start_date']) ?> - <?= DateThai($data[$i]['end_date']) ?>
        </span>
    </div>
  </div>
<?php
}
?>
</div>
