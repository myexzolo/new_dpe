<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$project_code = $_POST['project_code'];

$sql_test = "SELECT t.*
            FROM pfit_t_test t,pfit_t_project_test pt
            where t.test_code = pt.test_code and pt.project_code = '$project_code' order by pt.test_seq";
$query_test = DbQuery($sql_test,null);
$jsonTest   = json_decode($query_test, true);
$rowTest    = $jsonTest['data'];
$numTest    = $jsonTest['dataCount'];

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
        <th style="width:50px;vertical-align:middle">พิมพ์</th>
      <?php
      }
      ?>
      <th style="vertical-align:middle">เลขประจำตัว</th>
      <th style="vertical-align:middle">ชื่อ</th>
      <th style="vertical-align:middle">นามสกุล</th>
      <th style="vertical-align:middle">วันเดือนปีเกิด</th>
      <th style="vertical-align:middle">เพศ</th>
      <th style="vertical-align:middle">น้ำหนัก</th>
      <th style="vertical-align:middle">ส่วนสูง</th>
      <?php
        for ($x = 0; $x < $numTest; $x++)
        {
          $test_code = $rowTest[$x]["test_code"];
          $test_name = $rowTest[$x]["test_name"];
          $test_unit = $rowTest[$x]["test_unit"];
        ?>
          <th><?= $test_name ?><br>(<?=$test_unit?>)</th>
        <?php
        }
        ?>

    </tr>
  </thead>
  <tbody>
    <?php
       $sql  = "SELECT project_code,person_number,person_gender,person_name,person_lname,status_test,
                ".getQueryDate('date_of_birth').",wiegth,height,person_type
                FROM pfit_t_person WHERE project_code = '$project_code' ORDER BY date_create desc";
    //echo $sql;

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0){
    foreach ($rows as $key => $value)
    {
        $gender = $value['person_gender']=="M"?"ชาย":"หญิง";
        $person_type = $value['person_type'];
        $status_test = $value['status_test'];
        $project_code   = $value['project_code'];
        $person_number  = $value['person_number'];


        $sql_re = "SELECT result,test_code FROM pfit_t_result where project_code = '$project_code' and person_number = '$person_number'";
        $query_re = DbQuery($sql_re,null);
        $jsonRe   = json_decode($query_re, true);
        $nums    = $jsonRe['dataCount'];

        $resultArr = array();
        if($nums > 0){
          $rowRe    = $jsonRe['data'];
          for ($j = 0; $j < $nums; $j++)
          {
            $testCode  = $rowRe[$j]["test_code"];
            $Result    = str_replace("|", ", ", $rowRe[$j]['result']);
            $resultArr[$testCode] = $Result;
          }
        }

        $color    = "";
        $disable  = "disabled";
        $oncick   = "";

        if($status_test == 0 || $status_test == ""){
          $color = 'red';
        }else if($status_test == 1){
          $color = '#FFD100';
        }else if($status_test == 2){
          $color = 'green';
          $url    = "../result/index.php?project_code=$project_code&person_number=$person_number";
          $oncick = "postURL_blank('$url')";
          $class= "";
        }
    ?>
    <tr class="text-center">
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
      <td>
        <a class="btn_point <?=$disable ?>"><i class="fa fa-print" onclick="<?=$oncick ?>"></i></a>
      </td>
      <?php
      }
      ?>
      <td><?=$value['person_number'];?></td>
      <td><?=$value['person_name'];?></td>
      <td><?=$value['person_lname'];?></td>
      <td ><?= DateThai($value['date_of_birth']); ?></td>
      <td><?=$gender ;?></td>
      <td><?=$value['wiegth'];?></td>
      <td><?=$value['height'];?></td>
      <?php
        for ($x = 0; $x < $numTest; $x++)
        {
          $test_code = $rowTest[$x]["test_code"];
          $test_name = $rowTest[$x]["test_name"];
          $test_unit = $rowTest[$x]["test_unit"];
        ?>
          <td align="right"><?=isset($resultArr[$test_code])?$resultArr[$test_code]:"" ?></td>
        <?php
        }
        ?>

    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
