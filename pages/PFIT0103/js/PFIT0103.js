
$(function () {
    showTable();
})

function showTable(){
  var project_code = $('#project_code').val();
  $.post( "ajax/showTable.php",{project_code:project_code})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(projectCode,personNumber){
  $.post("ajax/form.php",{projectCode:projectCode,personNumber:personNumber})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function checkMaxmin(id,min,max){
  var ids = $('#'+id).val();
  var re  = $('#re_'+id).val();
  if(re == ''){
    if(parseInt(min) != 0 && parseInt(max) != 0){
      if(parseInt(ids) < parseInt(min) || parseInt(ids) > parseInt(max) && parseInt(min) != 'null' ){
          $('#re_'+id).val(1);
          $.smkConfirm({
            text:'ดำเนินการต่อ ค่าอยู่ระหว่าง '+min+' ถึง '+max,
            accept:'ใช่',
            cancel:'ยกเลิก'
          },function(res){
            if (!res) {
              $('#'+id).val("");
              $('#re_'+id).val('');
            }
          });
      }
    }
  }
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

function disableResult(id,obj,classN){
  $('#'+id).val("");
  $('#'+id).smkClear();

  if(classN == 1){
    if(obj.checked){
       $('.'+id).val('');
       $('.'+id).prop('required', false);
       $('.'+id).prop('readonly', true);
    }else{
       $('.'+id).prop('required', true);
       $('.'+id).prop('readonly', false);
    }
  }else{
    if(obj.checked){
       $('#'+id).val('');
       $('#'+id).prop('required', false);
       $('#'+id).prop('readonly', true);

    }else{
       $('#'+id).prop('required', true);
       $('#'+id).prop('readonly', false);
    }
  }
}

function print(project_code,person_number){
  var pram = "?person_number="+person_number+"&project_code="+project_code;
  var url  = 'print/index.php'+pram;
  postURL_blank(url);
}

function dobFomat(d){
  return  d.substring(6, 8) +'/'+ d.substring(4, 6) +'/'+d.substring(4, 0);
}
