<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
include('../../../inc/function/authen.php');


$action       = isset($_POST['action'])?$_POST['action']:"";
$id_media     = isset($_POST['id_media'])?$_POST['id_media']:"";
$title        = isset($_POST['title'])?$_POST['title']:"";
$is_active    = isset($_POST['is_active'])?$_POST['is_active']:"";
$type_media   = isset($_POST['type_media'])?$_POST['type_media']:"";

$img   = "";
$path   = "";
$updateImg  = "";
$updateDoc  = "";

$user_id_update = $_SESSION['member'][0]['user_id'];

$dateNow  =  queryDateTimeOracle(date('Y/m/d H:i:s'));

// --ADD EDIT DELETE Module-- //

$pathImg    = "../../../image/media/";
$pathUpload = "../../../upload/ebook/";


if(empty($id_media) && $action == 'ADD'){

  $sqlid  = "SELECT T_MEDIA_SEQ.NEXTVAL AS next_id FROM DUAL";

  $query      = DbQuery($sqlid,null);
  $json       = json_decode($query, true);
  $id_media   = $json['data'][0]['next_id'];



  if(isset($_FILES["img"])){
    //$user_img = resizeImageToBase64($_FILES["user_img"],'256','256','100',$user_id_update,$path);
    $img = resizeImageToUpload($_FILES["img"],'256','',$pathImg,$id_media);
  }

  if(isset($_FILES["path"]))
  {
    $path = uploadFile($_FILES["path"],$pathUpload,$id_media);
  }

  $sql = "INSERT INTO pfit_t_media
         (id_media,title,img,path,is_active,type_media,update_date)
         VALUES($id_media,'$title','$img','$path','$is_active','$type_media',$dateNow)";
}
else if($action == 'EDIT')
{
    if(isset($_FILES["img"])){
      $img = resizeImageToUpload($_FILES["img"],'256','',$pathImg,$id_media);
      if($img != "")
      {
        $updateImg = "img = '$img',";
      }
    }

    if(isset($_FILES["path"]))
    {
      $path = uploadFile($_FILES["path"],$pathUpload,$id_media);
      if($path != "")
      {
        $updateDoc = "path = '$path',";
      }
    }

    $sql = "UPDATE pfit_t_media SET
            title           = '$title',
            is_active       = '$is_active',
            type_media      = '$type_media',
            $updateImg
            $updateDoc
            update_date     = $dateNow
            WHERE id_media  = $id_media";
}
else if($action == "DEL")
{
  $sql   = "UPDATE pfit_t_media SET is_active = 'D' WHERE id_media = '$id_media'";
}
//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
//print_r($row);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
