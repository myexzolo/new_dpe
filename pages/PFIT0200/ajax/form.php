<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action     = $_POST['value'];
$code       = isset($_POST['code'])?$_POST['code']:"";
$statusType = isset($_POST['status'])?$_POST['status']:"";

$category_criteria_name = "";
$category_criteria_code = "";
$detail = "";
$status = "";

$disabled = "";
$display = "";
$visibility = "";

if(!empty($code)){
  $sql = "SELECT * FROM pfit_t_category_criteria where category_criteria_code  = '$code'";

  $query = DbQuery($sql,null);
  $row = json_decode($query, true);

  $num  = $row['dataCount'];
  $data = $row['data'];

  if($num > 0){
    $category_criteria_name = $data[0]['category_criteria_name'];
    $category_criteria_code = $data[0]['category_criteria_code'];
    $detail = $data[0]['detail'];
    $status = $data[0]['status'];

  }
  if($action == 'VIEW'){
    $disabled = 'disabled';
    $display = 'display:none;';
    $visibility = 'visibility-non';
  }
}else{
  $sql = "SELECT category_criteria_code FROM pfit_t_category_criteria order by category_criteria_code desc";
  $query = DbQuery($sql,null);
  $row = json_decode($query, true);

  $num  = $row['dataCount'];
  $data = $row['data'];

  if($num > 0){
    $category_criteria_code = $data[0]['category_criteria_code'];
  }

  if(!empty($category_criteria_code)){
    $lastNum = substr($category_criteria_code,3);
    $lastNum = $lastNum + 1;
    $category_criteria_code = "CAT".sprintf("%02d", $lastNum);
  }else{
    $category_criteria_code = "CAT".sprintf("%02d", 1);
  }

}
?>
<div class="modal-body">
<div class="row">
  <div class="col-md-8">
    <div class="form-group">
      <label>รายการแปลผลการทดสอบ</label>
      <input type="hidden" name="action" value="<?=$action?>">
      <input type="text" data-smk-msg="&nbsp;" class="form-control" value="<?= $category_criteria_name ?>" name="category_criteria_name" placeholder="รายการแปลผลการทดสอบ" <?=$disabled; ?> required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>รหัส</label>
      <input type="text" value="<?= $category_criteria_code ?>" name="category_criteria_code" class="form-control" placeholder="รหัส" <?=$disabled; ?> readonly required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>รายละเอียด</label>
      <textarea class="form-control" name="detail" rows="3" placeholder="รายละเอียด" <?=$disabled; ?>><?= $detail ?></textarea>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="pull-right <?=$visibility ?>">
      <button type="button" class="btn btn-primary add_field_button <?= $visibility ?>" onclick="add()">เพิ่ม</button>
    </div>
    <h3><b>แสดงรายการเกณฑ์การวัด</b></h3>
  </div>
    <div>
      <div class="col-md-1">
        <div class="form-group">
          <label>ลบ</label>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>รหัส</label>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>รายการผลการทดสอบ</label>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>คะแนนการทดสอบ</label>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>สถานะ</label>
        </div>
      </div>
  </div>
  <div id="input_fields_wrap">
    <?php
       $sql = "SELECT * FROM pfit_t_cat_criteria_detail where category_criteria_code = '$category_criteria_code' order by category_criteria_detail_code";

       //echo $sql;
       $query = DbQuery($sql,null);
       $row = json_decode($query, true);

       $num  = $row['dataCount'];
       $data = $row['data'];

       //print_r($data);

       //$category_criteria_detail_id = "";
       $category_criteria_detail_code = "";
       $category_criteria_detail_name = "";
       $category_criteria_detail_value = "";
       $statusCD = "";

       $checkData  = isset($data[0]['category_criteria_detail_code'])?"Y":"";
       if($num > 0 && $checkData == "Y"){
         for ($i=0; $i < $num ; $i++) {
           //$category_criteria_detail_id     = $data[$i]['category_criteria_detail_id'];
           $category_criteria_detail_code   = $data[$i]['category_criteria_detail_code'];
           $category_criteria_detail_name   = $data[$i]['category_criteria_detail_name'];
           $category_criteria_detail_value  = $data[$i]['category_criteria_detail_value'];
           $statusCD                        = $data[$i]['status'];
      ?>
        <div id="row<?= $i ?>" class="rowData">
          <div class="col-md-1">
            <div class="form-group" style="line-height:40px;<?=$display?>">
              <a class="btn_point text-red" <?=$disabled; ?> ><i class="fa fa-trash-o" onclick="remove('row<?= $i ?>')"></i></a>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <input type="text" name="category_criteria_detail_code[]" value="<?= $category_criteria_detail_code ?>" class="form-control" placeholder="รหัส" <?=$disabled; ?> readonly required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="text" name="category_criteria_detail_name[]" data-smk-msg="&nbsp;" value="<?= $category_criteria_detail_name ?>" class="form-control" placeholder="รายการผลการทดสอบ" <?=$disabled; ?> required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <input type="number" name="category_criteria_detail_value[]" data-smk-msg="&nbsp;" value="<?= $category_criteria_detail_value ?>" class="form-control" placeholder="คะแนนการทดสอบ" <?=$disabled; ?> required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <select name="statusCD[]" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
                <option value="Y" <?= ($statusCD == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
                <option value="N" <?= ($statusCD == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
              </select>
            </div>
          </div>
        </div>
      <?php
          }
       }else{
         $category_criteria_detail_code = $category_criteria_code.'1';
       ?>
         <div id="row0" class="rowData">
           <div class="col-md-1">
             <div class="form-group <?=$visibility ?>" style="line-height:40px;<?=$display?>">
               <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="remove('row0')"></i></a>
             </div>
           </div>
           <div class="col-md-2">
             <div class="form-group">
               <input type="text" name="category_criteria_detail_code[]" autocomplete="off" value="<?= $category_criteria_detail_code ?>" class="form-control" placeholder="รหัส" <?=$disabled; ?> readonly required>
             </div>
           </div>
           <div class="col-md-3">
             <div class="form-group">
               <input type="text" name="category_criteria_detail_name[]" data-smk-msg="&nbsp;" autocomplete="off" value="<?= $category_criteria_detail_name ?>" class="form-control" placeholder="รายการผลการทดสอบ" <?=$disabled; ?> required>
             </div>
           </div>
           <div class="col-md-3">
             <div class="form-group">
               <input type="number" name="category_criteria_detail_value[]" data-smk-msg="&nbsp;" autocomplete="off" value="<?= $category_criteria_detail_value ?>" class="form-control" placeholder="คะแนนการทดสอบ" <?=$disabled; ?> required>
             </div>
           </div>
           <div class="col-md-3">
             <div class="form-group">
               <select name="statusCD[]" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
                 <option value="Y" <?= ($statusCD == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
                 <option value="N" <?= ($statusCD == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
               </select>
             </div>
           </div>
         </div>
      <?php }  ?>
  </div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?= $display?>">บันทึก</button>
</div>
