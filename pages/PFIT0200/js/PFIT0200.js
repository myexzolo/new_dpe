
$(function () {
    showTable();
})


function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",code=""){
  $.post("ajax/form.php",{value:value,code:code})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function add(){
  var wrapper         = $("#input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  var rowData         = $('.rowData').length;
  var max_fields      = 5; //maximum input boxes allowed
  rowData++;
  var arrNameCategory  = $("input[name='category_criteria_detail_code[]']");
  var num = arrNameCategory.length - 1 ;
  var value = arrNameCategory[num].value;
  console.log(value);
  var codeCategory        = value.substr(0, 5);
  var codeCriteriaDetail  = value.substr(5, 1);
  var code  = codeCategory+(parseInt(codeCriteriaDetail) + 1);
  console.log(code);
  if(rowData <= max_fields){ //max input box allowed
          var id = 'row_'+ code;
          $(wrapper).append('<div id="'+id+'" class="rowData">'+
          '<div class="col-md-1">'+
             '<div class="form-group" style="line-height:40px;">'+
               '<a class="btn_point text-red"><i class="fa fa-trash-o" onclick="remove(\''+id+'\')"></i></a>'+
             '</div>'+
           '</div>'+
          '<div class="col-md-2">'+
             '<div class="form-group">'+
               '<input type="text"  name="category_criteria_detail_code[]" autocomplete="off" value="'+code+'" class="form-control" placeholder="รหัส" readonly required>'+
             '</div>'+
           '</div>'+
           '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<input type="text" name="category_criteria_detail_name[]" data-smk-msg="&nbsp;" autocomplete="off" class="form-control" placeholder="รายการผลการทดสอบ" required>'+
            '</div>'+
          '</div>'+
          '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<input type="number" name="category_criteria_detail_value[]" data-smk-msg="&nbsp;" autocomplete="off" class="form-control" placeholder="คะแนนการทดสอบ" required>'+
            '</div>'+
          '</div>'+
          '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<select name="statusCD[]"class="form-control">'+
                '<option value="Y" selected>ใช้งาน</option>'+
                '<option value="N">ไม่ใช้งาน</option>'+
              '</select>'+
            '</div>'+
          '</div>'+
          '</div>'); //add input box
      }else{
        //alert('max_fields 5');
      }
}
function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}


function delModule(code,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล แปลผลการทดสอบ : '+ name +' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/delete.php",{code:code})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
