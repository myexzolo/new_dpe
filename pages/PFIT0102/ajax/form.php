<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$personNumber = $_POST['personNumber'];
$projectCode  = $_POST['projectCode'];
$statusTest   = $_POST['statusTest'];


$project_code   = "";
$person_number  = "";
$person_gender  = "";
$person_name    = "";
$person_lname   = "";
$date_of_birth  = "";
$wiegth         = "";
$heght          = "";
$person_type    = "";
$status         = "";
$test_code      = "";

$disabled = '';
$visibility = '';
$readonly = '';

$sql = "SELECT p.project_code,p.person_number,p.person_gender,p.person_name,p.person_lname,
         TO_CHAR(p.date_of_birth,'YYYY-MM-DD') as date_of_birth,p.wiegth,p.height,
         p.person_type,p.education,p.stage,p.grade,TO_CHAR(pj.end_date,'YYYY-MM-DD') as end_date
        FROM pfit_t_person p,pfit_t_project pj
        WHERE p.project_code = '$projectCode' and p.project_code = pj.project_code
        and p.person_number = '$personNumber'";

$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$num   = $json['dataCount'];
$row   = $json['data'];

if($num > 0){
  $person_number  = $row[0]['person_number'];
  $person_gender  = $row[0]['person_gender'];
  $person_name    = $row[0]['person_name'];
  $person_lname   = $row[0]['person_lname'];
  $date_of_birth  = $row[0]['date_of_birth'];
  $wiegth         = $row[0]['wiegth'];
  $height         = $row[0]['height'];
  $person_type    = $row[0]['person_type'];
  $end_date       = $row[0]['end_date'];
}

$age = yearBirth2($date_of_birth,$end_date);
//echo $date_of_birth.":".$end_date.":".$age;
?>
<div class="modal-body">
  <input type="hidden" name="yearBirth" value="<?= $age  ?>">
  <input type="hidden" name="person_gender" value="<?= $person_gender; ?>">
  <input type="hidden" name="projectCode" value="<?= $projectCode; ?>">
  <input type="hidden" name="person_number" value="<?= $person_number; ?>">
  <input type="hidden" name="wiegth" value="<?= $wiegth; ?>">
  <input type="hidden" name="height" value="<?= $height; ?>">
  <input type="hidden" name="person_type" value="<?= $person_type; ?>">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group2">
        <label>เลขประจำตัว : </label><span> <?= $person_number ?></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group2">
        <label>ชื่อ : </label><span> <?= $person_name ?></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group2">
        <label>สกุล : </label><span> <?= $person_lname ?></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group2">
        <label>วันเดือนปี เกิด : </label><span> <?= DateThai($date_of_birth); ?></span>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group2">
        <label>เพศ : </label><span> <?= $person_gender == 'M'? 'ชาย':'หญิง'?><span/>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group2">
        <label>น้ำหนัก : </label><span> <?= $wiegth ?><span/>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group2">
        <label>ส่วนสูง : </label><span> <?= $height ?><span/>
      </div>
    </div>
    <div class="col-md-12">
      <table class="table table-bordered table-striped" id="tableDisplay">
        <thead>
          <tr class="text-center">
            <th width="5%"></th>
            <th width="35%">ชื่อฐานการทดสอบ</th>
            <th width="15%">หน่วย</th>
            <th width="30%">คะแนน</th>
            <th width="15%">ไม่สามารถทดสอบ</th>
          </tr>
        </thead>
        <tbody>
<?php
      $sql = "SELECT t.*, r.result
               FROM pfit_t_project_test pt
               INNER JOIN pfit_t_test t ON t.test_code = pt.test_code and pt.project_code = '$projectCode' and $age >= test_age_min and $age <= test_age_max
               LEFT JOIN pfit_t_result r ON t.test_code = r.test_code and  r.project_code = '$projectCode' and person_number = '$person_number'
               ORDER BY pt.test_seq,t.test_seq_def";
      //echo $sql;
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);
      $row   = $json['data'];

      $i = 0;
      $v = 0;
      $tdList = "";

      foreach ($row as $key => $value)
      {
        $i++;
        $id  = $value['test_code'];
        $min = "";
        $max = "";
        $valuemin = 'null';
        $valuemax = 'null';
        $checkeds = '';
        $readonlys = '';
        $requireds = '';
        $disableds = '';

        $result  = isset($value['result'])?$value['result']:"";

        if($result == "" && $statusTest == "Y"){
          $checkeds= 'checked';
          $readonlys = 'readonly';
          $requireds = '';
          $disableds = 'disabled';
        }

        if(isset($value['test_min']) && !empty($value['test_min'])){
          $min= "min='".$value['test_min']."'";
          $valuemin = $value['test_min'];
        }

        if(isset($value['test_max']) && !empty($value['test_max'])){
          $max= "max='".$value['test_max']."'";
          $valuemax = $value['test_max'];
        }

        $test_calculator = $value['test_calculator'];

        $class="";
        $testFix = "";
        if($id == 'VO2'){
          $testFix = 'Y';
          $place1 = "HR";
          $place2 = "Load";
        }else if($id == 'BMI'){
          $testFix = 'Y';
          $place1 = "น้ำหนัก";
          $place2 = "ส่วนสูง";
        }else if($id == 'ABP'){
          $testFix = 'Y';
          $place1 = "ค่าบน";
          $place2 = "ค่าล่าง";
        }
?>
      <tr class='text-center'>
        <td><?= $i ?>
            <input type="hidden" name="test_code[]" value=<?=$id ?>>
        </td>
        <td align='left'><?= $value['test_name'] ?></td>
        <td align='center'><?= $value['test_unit'] ?></td>
        <td>
<?php

        if($testFix == 'Y')
        {
          if($result != ""){
            $result = explode("|",$result);
          }else{
            $result = array("", "");
          }
          $class="1";
  ?>
          <div class='row'>
            <div class='col-md-6'>
              <div class='form-group'>
                <input placeholder="<?= $place1 ?>" class="form-control <?= $id ?>"
                 value="<?= @$result[0] ?>" <?=$readonlys ?> type="text"
                 name="resultc[<?=$key?>][]" id="<?= $id ?>" <?= $requireds ?>>
              </div>
            </div>
            <div class='col-md-6'>
              <div class='form-group'>
                <input placeholder="<?= $place2 ?>" class="form-control <?= $id ?>"
                value="<?= @$result[1] ?>" <?=$readonlys ?> type="text"
                name="resultc[<?=$key?>][]" id="<?= $id ?>" <?= $requireds ?>>
              </div>
            </div>
          </div>
   <?php
      }else{
    ?>
          <div class="form-group">
            <input class="form-control" value="<?= $result ?>" <?= $readonlys ?> type="text"
            onfocusout="checkMaxmin('<?=$id ?>','<?= $valuemin ?>','<?= $valuemax ?>')"
            name="result[<?= $key ?>]" id="<?=$id ?>" <?= $min." ".$max." ".$requireds ?>>
          </div>
  <?php
    }
   ?>
      </td>
      <td>
          <input type="hidden" id="re_<?= $id ?>" />
          <input type="checkbox" name="resultC[]" value="null"
             <?= $checkeds ?> onclick="disableResult('<?=$id ?>',this,'<?=$class?>')" >
          <input <?= $readonlys ?> type="hidden" name="result_id[]" value="">
        </td>
      </tr>
    <?php
      }
    ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
  <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
</div>
