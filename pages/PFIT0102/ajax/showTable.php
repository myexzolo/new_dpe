<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$project_code = $_POST['project_code'];

?>
<style>
.justify-content-center {
    -webkit-box-pack: center!important;
    -ms-flex-pack: center!important;
    justify-content: center!important;
}
</style>
<div class="container">
  <div class="row justify-content-center">
    <div style="padding:5px" class="col-md-3">
      <span class="fa fa-circle" style="color:red;font-size:20px;"></span>&nbsp;ยังไม่ได้บันทึกข้อมูลการทดสอบ
    </div>
    <div style="padding:5px" class="col-md-3">
      <span class="fa fa-circle" style="color:#FFD100;font-size:20px;"></span>&nbsp;บันทึกข้อมูลการทดสอบบางส่วน
    </div>
    <div style="padding:5px" class="col-md-3">
      <span class="fa fa-circle" style="color:green;font-size:20px;"></span>&nbsp;บันทึกข้อมูลการทดสอบครบ
    </div>
  </div>
</div>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px"></th>
      <th>เลขประจำตัว</th>
      <th>ชื่อ</th>
      <th>นามสกุล</th>
      <th>วันเดือนปีเกิด</th>
      <th>เพศ</th>
      <th>น้ำหนัก</th>
      <th>ส่วนสูง</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
        <th style="width:70px;">พิมพ์</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
       $sql  = "SELECT project_code,person_number,person_gender,person_name,person_lname,status_test,
                ".getQueryDate('date_of_birth').",wiegth,height,person_type
                FROM pfit_t_person WHERE project_code = '$project_code' ORDER BY date_create desc";
    //echo $sql;

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0){
    foreach ($rows as $key => $value)
    {
        $gender = $value['person_gender']=="M"?"ชาย":"หญิง";
        $person_type = $value['person_type'];
        $status_test = $value['status_test'];
        $project_code   = $value['project_code'];
        $person_number  = $value['person_number'];

        $color    = "";
        $disable  = "disabled";
        $oncick   = "";
        $statusTest  = "N";

        if($status_test == 0 || $status_test == ""){
          $color = 'red';
        }else if($status_test == 1){
          $color = '#FFD100';
          $statusTest  = "Y";
        }else if($status_test == 2){
          $color = 'green';
          $statusTest  = "Y";
          $url    = "../result/index.php?project_code=$project_code&person_number=$person_number";
          $oncick = "postURL_blank('$url')";
          $class= "";
        }
    ?>
    <tr class="text-center">
      <td>
        <a class="btn_point text-green">
          <i class="fa fa-list" onclick="showForm('<?= $value['project_code'] ?>','<?= $value['person_number'] ?>','<?= $statusTest ?>')"></i>
        </a>
      </td>
      <td><?=$value['person_number'];?></td>
      <td><?=$value['person_name'];?></td>
      <td><?=$value['person_lname'];?></td>
      <td ><?= DateThai($value['date_of_birth']); ?></td>
      <td><?=$gender ;?></td>
      <td><?=$value['wiegth'];?></td>
      <td><?=$value['height'];?></td>
      <td><span class="fa fa-circle" style="color:<?=$color ?>;font-size:20px;"></span></td>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
      <td>
        <a class="btn_point <?=$disable ?>"><i class="fa fa-print" onclick="<?=$oncick ?>"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
