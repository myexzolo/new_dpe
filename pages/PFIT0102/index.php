<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - จัดเก็บผลการทดสอบ</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/PFIT0102.css">
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>จัดเก็บผลการทดสอบ  <small>PFIT0102</small></h1>

          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">จัดเก็บผลการทดสอบ</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php");
            $project_code   = isset($_POST['project_code'])?$_POST['project_code']:"";
            $project_name   = isset($_POST['project_name'])?$_POST['project_name']:"";
            $project_type   = isset($_POST['project_type'])?$_POST['project_type']:"";

            $prameter = "?project_code=$project_code&project_name=$project_name&project_type=$project_type";

            if($project_code == ""){
              exit("<script>window.location='../home';</script>");
            }
          ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-md-6">
                      <h3 class="box-title">กิจกรรม : <?= $project_name?>
                      <input type="hidden" id="project_code" value="<?=$project_code ?>"></h3>
                    </div>
                </div>
              </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div id="showTable"></div>
                </div>
                <div class="box-footer with-border">
                  <div class="pull-right">
                    <button style="width: 100px;text-align: center;" class="btn btn-success btn-flat" onclick="gotoPage('../home')" style="width:80px;">ย้อนกลับ</button>
                    <button type="button" style="width: 150px;text-align: center;" class="btn btn-social bg-olive btn-flat" onclick="postURL('<?= "../PFIT0101/index.php".$prameter ?>')">
                      <i class="fa fa-user-plus" style="font-size:18px;"></i> ผู้ทดสอบ
                    </button>
                    <button type="button" style="width: 130px;text-align: center;" class="btn btn-social bg-light-blue btn-flat" onclick="postURL('<?= "../PFIT0103/index.php".$prameter ?>')">
                      <i class="fa fa-file-text-o" style="font-size:18px;"></i> รายงาน
                    </button>
                  </div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">ข้อมูลผลการทดสอบ</h4>
                    </div>
                    <form id="formAdd" novalidate>
                    <!-- <form data-smk-icon="glyphicon-remove-sign" method="post" action="ajax/AED.php"> -->
                    <div id="show-form"></div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="js/PFIT0102.js"></script>

  </body>
</html>
