  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");

  // $projectCode = $_GET['project_code'];
  $projectCode = "63060269";
  $stageTxt = "";

  ?>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

      <link rel="stylesheet" href="css/pdf.css">
      <script src="../../dist/js/Chart.min.js"></script>
    </head>
    <body>
      <script>
        function chartJsShow(id,name,str,str_m,str_f){
          var res = str.split("|");
          var res_m = str_m.split("|");
          var res_f = str_f.split("|");
          let myChart = document.getElementById(id).getContext('2d');
          Chart.defaults.global.defaultFontFamily = 'chatthai';
          Chart.defaults.global.defaultFontSize = 12;
          Chart.defaults.global.defaultFontColor = '#777';

          let massPopChart = new Chart(myChart, {
            type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            defaultFontSize: '8',
            data:{
              labels: res ,
              datasets:[{
                label:'ชาย',
                data:res_m,
                fill: false,
                backgroundColor:'rgba(255, 99, 132, 0.6)',
                borderWidth:2,
                borderColor:'rgba(255, 99, 132, 0.6)',
                hoverBorderWidth:1,
                hoverBorderColor:'#000',
                lineTension: 0
              },
              {
                label:'หญิง',
                data:res_f,
                fill: false,
                backgroundColor:'rgba(54, 162, 235, 0.6)',
                borderWidth:2,
                borderColor:'rgba(54, 162, 235, 0.6)',
                hoverBorderWidth:1,
                hoverBorderColor:'#000',
                lineTension: 0
              }]
            },
            options:{
              responsive: true,
              title:{
                display:true,
                fontStyle:'bold',
                text:name,
                fontSize:16
              },
              legend:{
                display:true,
                position:'bottom',
                labels:{
                  fontColor:'#000'
                }
              },
              tooltips:{
                enabled:true
              },
              plugins: [{
                afterRender: function () {
                  // Do anything you want
                  renderIntoImage()
                },
              }],
              scales: {
                yAxes: [{
                    ticks: {
                        // max: 150,
                        min: 0,
                        stepSize: 10
                    }
                }]
              }
            }
          });

          // console.log(">>>>>>>>."+id);
          //
          const renderIntoImage = () => {
            console.log("renderIntoImage");
            // const canvas = document.getElementById(id)
            //
            // var idImg = id +"_m";
            // const imgWrap = document.getElementById(idImg)
            // var img = new Image();
            // img.src = canvas.toDataURL()
            // imgWrap.appendChild(img)
            // canvas.style.display = 'none'
          }

          setTimeout(function(){
            let canvas = document.getElementById(id);
            var idImg = id +"_m";
            let imgWrap = document.getElementById(idImg).src = canvas.toDataURL();
            canvas.style.display = 'none'
          }, 1000);

        }
      </script>
      <?php
        $break = false;
        $total_teat_c = array();
        $total_teat_p = array();

        $sql = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode'";
        $query = DbQuery($sql,null);
        $row  = json_decode($query, true);

        $num   = $row['dataCount'];
        if($num>0){

        foreach ($row['data'] as $value) {
          $project_codep = $value['project_code'];
          $project_name_title  = $value['project_name'];
          $sqlp = "SELECT DISTINCT(stage) FROM pfit_t_person WHERE project_code = '$project_codep' ORDER BY stage  ASC";
          $queryp = DbQuery($sqlp,null);
          $rowp  = json_decode($queryp, true);
          $stage = '';
          // print_r($rowp);
          $nump   = $rowp['dataCount'];

          if($nump>0){
          foreach ($rowp['data'] as $keyp => $valuep) {

            $stage = isset($valuep['stage'])?$valuep['stage']:"";
            $stage_id = isset($valuep['stage'])?$valuep['stage']:"";

            if($stage == ""){
              continue;
            }

            $sql_g = "SELECT DISTINCT(grade) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' ORDER BY grade  ASC";
            $query_g = DbQuery($sql_g,null);
            $row_g  = json_decode($query_g, true);
              //echo $stage_id;
              if($row_g['dataCount'] > 0){
              foreach ($row_g['data'] as $key_g => $value_g) {

                $grade = $value_g['grade'];

                $sql_r = "SELECT DISTINCT(room) FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' ORDER BY room  ASC";
                $query_r = DbQuery($sql_r,null);
                $row_r  = json_decode($query_r, true);

                if($row_r['dataCount'] > 0){
                foreach ($row_r['data'] as $key_r => $value_r) {

                  $room   = $value_r['room'];
                  if($stage == 1){
                    $stageTxt = 'นักเรียนชั้นมัธยมศึกษาปีที่';
                  }elseif($valuep['stage'] == 2){
                    $stageTxt = 'นักเรียนชั้นประถมศึกษาปีที่';
                  }
                  $grade = $value_g['grade'];

                  $sqlpp = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                               WHERE ptpt.test_code = ptt.test_code
                               AND ptpt.project_code = '$project_codep' ORDER BY ptpt.test_seq ASC";
                  $querypp = DbQuery($sqlpp,null);
                  $rowpp  = json_decode($querypp, true);
                  $numpp   = $rowpp['dataCount'];

                  foreach ($rowpp['data'] as $keypp => $valuepp) {
                    $category_criteria_code = $valuepp['category_criteria_code'];
                    $test_code = $valuepp['test_code'];
                    $sqlppp = "SELECT * FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
                    $queryppp = DbQuery($sqlppp,null);
                    $rowppp  = json_decode($queryppp, true);
                    $numppp   = $rowppp['dataCount'];

                    foreach ($rowppp['data'] as $keyppp => $valueppp) {
                      $category_criteria_detail_code = $valueppp['category_criteria_detail_code'];

                      //echo $test_code.",".$category_criteria_detail_code."<br>";
                      if(!isset($total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                        $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                      }

                      if(!isset($total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade])){
                        $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade] = 0;
                      }
                    }

                  }
                  if($break){
                    echo "<div class='break'></div>";
                  }else{
                    $break = true;
                  }
      ?>
      <div align="center" style="font-size:22pt">ผลการทดสอบสมรรถภาพทางกาย</div>
      <div align="center" style="font-size:20pt"><?=$project_name_title.' '?><?php echo $stageTxt.' '.$grade.'/'.$room; ?></div>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <thead>
      		<tr align="center">
      			<th rowspan="2" class="thStyle">เลขที่</th>
      			<th rowspan="2" class="thStyle">เลขประจำตัว</th>
      			<th rowspan="2" class="thStyle">เพศ</th>
      			<th rowspan="2" class="thStyle">ชื่อ</th>
      			<th rowspan="2" class="thStyle">นามสกุล</th>
      			<th class="thStyle">น้ำหนัก</th>
      			<th class="thStyle">ส่วนสูง</th>
            <?php
                foreach ($rowpp['data'] as $valuepp) {
            ?>
      			<th class="thStyle"><?php echo $valuepp['test_name']; ?></th>
            <?php } ?>

      			<th class="thStyle">คะแนน</th>
      		</tr>
      		<tr align="center">

      			<td class="thStyle">(กก.)</td>
      			<td class="thStyle">(ซม.)</td>
            <?php
                $total_max_values = 0;
                foreach ($rowpp['data'] as $valuepp) {
                  $category_criteria_code = $valuepp['category_criteria_code'];

                  $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
                  $query_value = DbQuery($sql_value,null);
                  $row_value  = json_decode($query_value, true);
                  $num_value  = $row_value['dataCount'];
                  $data_value = $row_value['data'];

                  $total_max_values += $data_value[0]['max_value'];
            ?>
      			<td class="thStyle">(<?php echo $valuepp['test_unit']; ?>)</td>
            <?php } ?>

      			<td class="thStyle">(เต็ม <?=$total_max_values; ?>)</td>
      		</tr>
        </thead>
        <tbody>
          <?php

          $sqlperson = "SELECT * FROM pfit_t_person WHERE project_code = '$project_codep' AND stage = '$stage_id' AND grade = '$grade' AND room = '$room' order by date_create";
          //echo $sqlperson;
          $queryperson = DbQuery($sqlperson,null);
          $rowperson  = json_decode($queryperson, true);
          $i = 1;
            foreach ($rowperson['data'] as $value) {
            $person_number = $value['person_number'];
            $person_name = $value['person_name'];
            $person_lname = $value['person_lname'];
            $wiegth = $value['wiegth'];
            $height = $value['height'];
            $person_gender = $value['person_gender']=='M'?'ชาย':'หญิง';
            $bmi = bmi($wiegth,$height)[0];
          ?>
          <tr>
      			<td class="content" style="text-align: center;"><?= $i; ?></td>
      			<td class="content"><?= $person_number; ?></td>
      			<td class="content"><?= $person_gender; ?></td>
      			<td class="content"><?= $person_name; ?></td>
      			<td class="content"><?= $person_lname; ?></td>

      			<td class="content" style="text-align: center;"><?php echo $wiegth; ?></td>
      			<td class="content" style="text-align: center;"><?php echo $height; ?></td>
            <?php
                $total_max_value = 0;
                $chkres = "F";
                $results = array();
                $resultTxt = "";
                $n = 0;
                foreach ($rowpp['data'] as $valuepp) {
                  $test_code = $valuepp['test_code'];

                  $sqlres = "SELECT * FROM pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd, pfit_t_person ptp
                             WHERE ptc.test_criteria_code = pts.test_criteria_code
                             AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
                             AND pts.project_code = '$project_codep' AND ptp.person_number = pts.person_number
                             AND pts.test_code = '$test_code' AND pts.person_number = '$person_number'";

                  //echo $sqlres."<br>";
                  $queryres = DbQuery($sqlres,null);
                  $rowres  = json_decode($queryres, true);

                  $numres   = $rowres['dataCount'];
                  $result = '-';
                  $resultCat = "-";
                  if($numres>0){
                    if(isset($rowres['data'][0]['result'])){
                      $total_max_value += $rowres['data'][0]['category_criteria_detail_value'];
                      //$result = $rowres['data'][0]['result'];
                      $result = $rowres['data'][0]['result_cal'];

                      $resultCat = $rowres['data'][0]['category_criteria_detail_name'];

                      $category_criteria_detail_code = $rowres['data'][0]['category_criteria_detail_code'];
                      if($rowres['data'][0]['gender'] == 'M'){
                        $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                      }else{
                        $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade]++;
                      }

                    }else{
                      $chkres = "N";
                    }
                  }
                  $results[$n] = $resultCat;
                  $n++;
            ?>
      			<td class="content" style="text-align: center;"><?php echo $result; ?></td>
            <?php
            }
            if($chkres == "N"){
              $total_txt_res = "ข้อมูลไม่ครบ";
            }else{
              $total_txt_res = calTestFit($total_max_values,$total_max_value,60);
            }

            ?>

      			<td class="content" style="text-align: center;"><?=$total_max_value ?></td>
      		</tr>

          <tr align="center">
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
            <?php
              for($x = 0; $x < count($results); $x++) {

            ?>
      			<td class="content" style="text-align: center;"><?php echo $results[$x]; ?></td>
            <?php } ?>

      			<td class="content" style="text-align: center;"><?=$total_txt_res; ?></td>
      		</tr>

          <?php $i++; }  ?>
      	</tbody>
      </table>

    <?php }} ?>
      <div class="break"></div>


      <div align="center" style="font-size:22pt">แผนภูมิแสดงระดับสมรรถภาพทางกาย<?=$project_name_title ?></div>
      <div align="center" style="font-size:20pt"><?php echo $stageTxt.' '.$grade; ?></div>
      <?php
        $ids = 0;
      ?>
        <div class="row" align="center">

        <?php
        $m_score = array();
        // print_r($rowpp['data']);
        if(isset($rowpp) && $rowpp['dataCount'] > 0){
        foreach ($rowpp['data'] as $keypp => $valuepp) {
          $ids = $keypp;
          $test_name = $valuepp['test_name'];
          $test_code = $valuepp['test_code'];

          $category_criteria_code = $valuepp['category_criteria_code'];

          $sql_c = "SELECT DISTINCT(pttc.category_criteria_detail_code),ptccd.category_criteria_detail_name,ptccd.category_criteria_code
                    FROM pfit_t_cat_criteria_detail ptccd , pfit_t_test_criteria pttc
                    WHERE ptccd.category_criteria_detail_code = pttc.category_criteria_detail_code
                    AND category_criteria_code = '$category_criteria_code'";
          $query_c = DbQuery($sql_c,null);
          $row_c  = json_decode($query_c, true);
          $category_criteria_detail_name = array();
          $m_score = array();
          $f_score = array();
          $score = array();
            foreach ($row_c['data'] as $key_c => $value_c) {
              $category_criteria_detail_code = isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
              if($category_criteria_detail_code!= ""){
                $m_score[] = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
                $f_score[] = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
                // print_r($f_score);
                $category_criteria_detail_name[] =  $value_c['category_criteria_detail_name'];
              }else{
                $m_score[] = "";
                $f_score[] = "";
                // print_r($f_score);
                $category_criteria_detail_name[] =  "";
              }

            }
            $str =  implode("|",$category_criteria_detail_name);
            $str_m =  implode("|",$m_score);
            $str_f =  implode("|",$f_score);

      ?>
        <?php
          $dd  =  $stage_id.'_'.$grade.'_'.$ids;
        ?>

          <div class="col-md-6">
            <div style="margin:0px;padding-top:40px">
              <canvas id="<?= $dd ?>"></canvas>
              <img id="1_1_0_m" style="width:100%" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAo4AAAFHCAYAAAAvAc0BAAAgAElEQVR4Xu2de5wcVZn3n1PdPTOZkARICIiRRVxkAW+LF27rK6ACsrLIYkSuIZkQF9fd1fU1gcwk6VwmybCr+Lquq8lMEuKNq4ooK97ddVUQcXVBxPUKYblfkpC5dXed93M6XdlK0dXTlzpPnVPnx186qXrO83yf36n+9amq04LwHwiAAAiAAAiAAAiAAAg0QUA0cQwOAQEQAAEQAAEQAAEQAAGCcYQIQAAEQAAEQAAEQAAEmiIA49gUJhwEAiAAAiAAAiAAAiAA4wgNgAAIgAAIgAAIgAAINEUAxrEpTDgIBEAABEAABEAABEAAxhEaAAEQAAEQAAEQAAEQaIoAjGNTmHAQCIAACIAACIAACIAAjCM0AAIgYByBq6+++gIp5S1Syhuvvfbady9duvQGIprX29t7TrFY3GVcwkgIBEAABBwhAOPoSKNRJgjYRCAwjkT0r1LKmSp3mEabOohcQQAEskoAxjGrnUVd1hBYtmzZSiLqI6I7ieisfD5/yuDg4CPWFKAp0RqX1UT0zaGhobdqGiZTYfv7+19cLpd/QEQjQ0NDa2wtrlgszhwdHb2DiHaoFWdb60DeIJBFAjCOWewqarKCwDXXXHOi7/vKLM6qJXw/Ec2w2TiqmiqVys25XG7+hg0b7mq1EVEmUsr/UCZICPESm41QqxxaOV7dxhdCXKjOkVI+LIRQK7QfsY1X6IuCKmWnlFI9kvCDXC53XW2ekOd5Z7Wjq1Z44lgQAIHGBGAcoRAQSIFA7VbsSPBBGDJMO101jlHTmRUmuuRVb1UuZCJX2WQco8+wBiZSPeMK46hLQYgLAu0RgHFsjxvOAoGOCIRXiSKBHnLVOMYxUauOpj3fGJg2IcSptf6xG7U6K9ZhKU2ZT82oD6vV4UqlMhyqZV8cIcQ7N27ceGuc2EPPou47JHihSf0hsopYV9uh2+tHRMepZxx93z9LSnkcbmF3dAnCySDQNgEYx7bR4UQQAAEXCdQxjQrDTptuo4ZXvD3P21F7LjJq3Kb8ElPH7O/jEJhGZT5zudyPamNQq1+MQgZ5361qvGXv4sxDzaYQgHE0pRPIwxkCoVWYfR+yoZWb6oe17/vzQs8/7ls9ipiWfX8Pr9oEKz5xxyrQ9Vb3oqtL0ZzUCzsxpqmaR73j66xI7Wew6q02BXmEbldWVxzHx8ffqrboUfmHcw3HCP+9VYNXj2E9UdZqOl7VHFn1q7vKF2YwVd71VhHjzomuxIa2MGq4QhuMIYToU6uJS5cufWsul9sVfnZQxfJ9f36jVb1anL6hoaElUU4h9q8IDHWj2+jhXkXrqmcc8fKMM5dLFGogARhHA5uClLJLIPyhraoMPpyjpqtUKn3Y87yb1TFSyn3PQsaZwWAFplAoXFgulzd7nre6u7v7AfVmau0WZNXU1Ds/LmY9I6jGCfJSuQshfkFE6s3nVUKI+2vGrmp+K5XKSb7vfyC4zRwyZrOUmVD5jY2NqXir1e3HWq3HqRc9lFmSUh6vYisjEa6rdmxfsHJVzzgGNXmed11wqzXOYAZqqxlVtap1pzLtgbFqpMYwu3q3ddW/R2qMzVsIsUPdMi4UCmdHTXoQOzB0hULhg+G3p2u1bZZSvkQI8Yq4W8whw9Vwe6Ogz41uU0dfZgmvuEZ7rUxp+LnFqCGNq0uxr2cc1d/DK5qN8szu1QSVgUA6BGAc0+GOUR0lEFmBa3bFcd8KWxPGTxnN6qbZ9Y6NW5Wq9yJKPeMYMgTVW47KHNY26g7eft5CRLG3OKOrTsG4Usr7lMEcGxt7f80s3hiY0mAFKrziGL41PJUhDEstuooZ3ky8zurnlM8JhsxT7LFxPW8m76jZiq6iBr1Wx9X2uzwp/EUhOs1CvL8Wt5qo8iqVSjcqsx63LVTMau4+7dWrrRH7uLoaGcdmanH0MoOyQUArARhHrXgRHAT2J1DPjKkjpjBpRwQrSE0Yx4a3tSOrRGro6vH1VnXico0zYrVtc5oyjuEXKMLxAmOp/j1qHJXJq5dnnAGLGLa6t9PVyq5a4QzMqcolukobp+HI7f6Gzzi2knczRjfMSa0+lkqlrxHR39dWftULO3WNbLNGd6rb1EqHpVJpRszjC9Wxwzkqk9poxTGoOXpOk8ax+qUDvyiEqy0I8BCAceThjFFAYB+BkKGJXXFUH8iNbsFGV5Uiz4jFrjjG3ZZuxzhGn++L3qqut1pVzxgEYKLPAoZvVQfGoFkDFr2FrIxQ6Na/ek7yBc+SKnPe09PzjWaNY8TUzGr09nezeUenSdwqXdRgq7eMe3t7l0yVezPGsZnb1PWmc/RnIuNWJeNMrYrZinGMrn5j03xcZEGAhwCMIw9njAICVQJxexN2suIY/sANfyg3ulUdvZ3cjnEMmZB9zzSGn3Fs1TiGTEP15Y7Qbet9L3s0a8Ciplu91atuvwoh1HOjHw3XHx5X3Z6tHRe7aheVcjP7TTabd6vGUf0kozon7pnWuHhx5q3eberoi1SNDHLtWVuK3gZv1uTBOOJCCQLmE4BxNL9HyDBDBDq5Va0wBG8V15BUDVuhUNhdb6UpQeNYHS76wkX0Q76ZW9txK471cq232hY1YLUXZdTLOdX/AlOjbqNGtphZVTvkXUSkXrrZ9xxmOKaUcv5Ut3vryXGq7WGazTt6u3WqFcdwzep/T7XiGOpR7BvgU92mbjQd661WTvUCUTheK8Yx+nwsblVn6EKJUowmAONodHuQXNYItGscwxxqP8N3JRHdlMvlFtd7e1od36lxjHsDPMglZeMY/ExjYBirvzASvJUcmGn1W8fB39XvgddWHNV+hfu9wBOq5cNEVH3BJO4t6cCcBf9eW6X7mupF3M/hxW3WHWxwHX6bOtzrZoxjo+dfo/NnqhdKmr1NXe8xitrf3j80NPShYNy4rYji5nWbxjH2RZ+sXT9QDwiYQADG0YQuIAdnCNT7pY1I8fsMTeQt3+rzkOHNmmPeno5jGbvXojphqi1Pohtcx/3KS23wKd+qDr8c0+BZuMAU1rtVrYxjwz0v6+2xGL29Hr6dHqkp9i3pOi8YTblRdiSXhnm3YhzDt40b7dsZxAwd85LoRtzNvE1db3Uw9Ld6q7iqT1O+na5i1OEa1fJ+LyBhOx5nLpso1DACMI6GNQTpgAAIgIBOAtENwHWOpSs2NgDXRRZxQWBqAjCOUzPCESAAAiCQKQLhnxyMu71ucsFTPVNqcu7IDQRsJwDjaHsHkT8IgAAItEFArTzGPVvZRji2U2qbnR/X6OcQ2ZLBQCDgIAEYRwebjpJBAARAAARAAARAoB0CMI7tUMM5IAACIAACIAACIOAgARhHB5uOkkEABEAABEAABECgHQIwju1QwzkgAAIgAAIgAAIg4CABGEcHm46SQQAEQAAEQAAEQKAdAjCO7VDDOSAAAiAAAiAAAiDgIIGGxjH8aweNftjeQW4oGQRAAARAAARAAAScI9DQOC5dunRhoVD4evhnuZwjhIJBAARAAARAAARAAASqBGKNY+h3cmep38i18dcF0GMQAAEQAAEQAAEQAIHkCDQ0joVCYff4+PhbpZQjMI/JQUckEAABEAABEAABELCRQFMvx6jfNfV9fz5+4snGFiNnEAABEAABEAABEEiGQFPGUd22LpVKH+7t7V1SLBZ3JTM0ooAACIAACIAACIAACNhEoGnjWC6X3z80NNRPRHOaKPBVRPTzJo7DISDQDoHZRPR0OyfiHBBoggD01QQkHNIWAWirLWw4qUkCM4jowSaPbfuwpozjsmXLVgoh7t+4ceOtTY50ARE1e2yTIXEYCOwjcDgR/Q94gIAmAtCXJrAIS9AWRKCTAIu+Yo2jMotEtLpW4aqhoaE1LVQL49gCLBzaMgGWydFyVjghKwSgr6x00rw6oC3zepKljFj01dSKYxtUYRzbgIZTmibAMjmazgYHZo0A9JW1jppTD7RlTi+ymAmLvmAcsyid7NfEMjmyjxEVxhCAviANXQSgLV1kEVcRYNEXjCPEZiMBlslhIxjknAgB6CsRjAhShwC0BVnoJMCiLxhHnS1EbF0EWCaHruQR13gC0JfxLbI2QWjL2tZZkTiLvmAcrdACkowQYJkcoO4sAejL2dZrLxza0o7Y6QFY9AXj6LTGrC2eZXJYSweJd0oA+uqUIM6PIwBtQRs6CbDoC8ZRZwsRWxcBlsmhK3nENZ4A9GV8i6xNENqytnVWJM6iLxhHK7SAJHGrGhpgJMBy8WWsB0OZQwDaMqcXWcyERV8wjlmUTvZrYpkc2ceICmMIQF+Qhi4C0JYusoirCLDoC8YRYrORAMvksBEMck6EAPSVCEYEqUMA2oIsdBJg0ReMo84WIrYuAiyTQ1fyiGs8AejL+BZZmyC0ZW3rrEicRV8wjlZoAUlGCLBMDlB3lgD05WzrtRcObWlH7PQALPqCcXRaY9YWzzI5rKWDxDslAH11ShDnxxGAtqANnQRY9AXjqLOFiK2LAMvk0JU84hpPAPoyvkXWJghtWds6KxJn0ReMoxVaQJK4VQ0NMBJgufgy1oOhzCEAbZnTiyxmwqIvGMcsSif7NbFMjuxjRIUxBKAvSEMXAWhLF1nEVQRY9AXjCLHZSIBlctgIBjknQgD6SgQjgtQhAG1BFjoJsOgLxlFnCxFbFwGWyaErecQ1ngD0ZXyLrE0Q2rK2dVYkzqIvGEcrtIAkIwRYJgeoO0sA+nK29doLh7a0I3Z6ABZ9wTg6rTFri2eZHNbSQeKdEoC+OiWI8+MIQFvQhk4CLPqCcdTZQsTWRYBlcuhKHnGNJwB9Gd8iaxOEtqxtnRWJs+gLxtEKLSBJ3KqGBhgJsFx8GevBUOYQgLbM6UUWM2HRF4xjFqWT/ZpYJkf2MaLCGALQF6ShiwC0pYss4ioCLPqCcYTYbCTAMjlsBIOcEyEAfSWCEUHqEIC2IAudBFj0BeOos4WIrYsAy+TQlTziGk8A+jK+RdYmCG1Z2zorEmfRF4yjFVpAkhECLJMD1J0lAH0523rthUNb2hE7PQCLvmAcndaYtcWzTA5r6SDxTglAX50SxPlxBKAtaEMnARZ9wTjqbCFi6yLAMjl0JY+4xhOAvoxvkbUJQlvWts6KxFn0BeNohRaQJG5VQwOMBFguvoz1YChzCEBb5vQii5mw6AvGMYvSyX5NLJMj+xhRYQwB6AvS0EUA2tJFFnEVARZ9wThCbDYSYJkcNoJBzokQgL4SwYggdQhAW5CFTgIs+oJx1NlCxNZFgGVy6EoecY0nAH0Z3yJrE4S2rG2dFYmz6AvG0QotIMkIAZbJAerOEoC+nG299sKhLe2InR6ARV8wjk5rzNriWSaHtXSQeKcEoK9OCeL8OALQFrShkwCLvmAcdbYQsXURYJkcupJHXOMJQF/Gt8jaBKEta1tnReIs+oJxtEILSBK3qqEBRgIsF1/GejCUOQSgLXN6kcVMWPQF45hF6WS/JpbJkX2MqDCGAPQFaegiAG3pIou4igCLvmAcITYbCbBMDhvBIOdECEBfiWBEkDoEoC3IQicBFn3BOOpsIWLrIsAyOXQlj7jGE4C+jG+RtQlCW9a2zorEWfQF42iFFpBkhADL5AB1ZwlAX862Xnvh0JZ2xE4PwKIvGEenNWZt8SyTw1o6SLxTAtBXpwRxfhwBaAva0EmARV8wjjpbiNi6CLBMDl3JI67xBKAv41tkbYLQlrWtsyJxFn3BOFqhBSSJW9XQACMBlosvYz0YyhwC0JY5vchiJiz6gnHMonSyXxPL5Mg+RlQYQwD6gjR0EYC2dJFFXEWARV8wjhCbjQRYJoeNYJBzIgSgr0QwIkgdAtAWZKGTAIu+YBx1thCxdRFgmRy6kkdc4wlAX8a3yNoEoS1rW2dF4iz6gnG0QgtIMkKAZXKAurMEoC9nW6+9cGhLO2KnB2DRF4yj0xqztniWyWEtHSTeKQHoq1OCOD+OALQFbegkwKIvGEedLURsXQRYJoeu5BHXeALQl/EtsjZBaMva1lmROIu+YByt0AKSxK1qaICRAMvFl7EeDGUOAWjLnF5kMRMWfcE4ZlE62a+JZXJkHyMqjCEAfUEaughAW7rIIq4iwKIvGEeIzUYCLJPDRjDIOREC0FciGBGkDgFoC7LQSYBFXzCOOluI2LoIsEwOXckjrvEEoC/jW2RtgtCWta2zInEWfcE4WqEFJBkhwDI5QN1ZAtCXs63XXji0pR2x0wOw6AvG0WmNWVs8y+Swlg4S75QA9NUpQZwfRwDagjZ0EmDRF4yjzhYiti4CLJNDV/KIazwB6Mv4FlmbILRlbeusSJxFXzCOVmgBSeJWNTTASIDl4stYD4YyhwC0ZU4vspgJi75gHLMonezXxDI5so8RFcYQgL4gDV0EoC1dZBFXEWDRF4wjxGYjAZbJYSMY5JwIAegrEYwIUocAtAVZ6CTAoi8YR50tRGxdBFgmh67kEdd4AtCX8S2yNkFoy9rWWZE4i75gHK3QApKMEGCZHKDuLAHoy9nWay8c2tKO2OkBWPQF4+i0xqwtnmVyWEsHiXdKAPrqlCDOjyMAbUEbOgmw6AvGUWcLEVsXAZbJoSt5xDWeAPRlfIusTRDasrZ1ViTOoi8YRyu0gCRxqxoaYCTAcvFlrAdDmUMA2jKnF1nMhEVfMI5ZlE72a2KZHNnHiApjCEBfkIYuAtCWLrKIqwiw6AvGEWKzkQDL5LARDHJOhAD0lQhGBKlDANqCLHQSYNEXjKPOFiK2LgIsk0NX8ohrPAHoy/gWWZsgtGVt66xInEVfMI5WaAFJRgiwTA5Qd5YA9OVs67UXDm1pR+z0ACz6gnF0WmPWFs8yOaylg8Q7JQB9dUoQ58cRgLagDZ0EWPQF46izhYitiwDL5NCVPOIaTwD6Mr5F1iYIbVnbOisSZ9EXjKMVWkCSuFUNDTASYLn4MtaDocwhAG2Z04ssZsKiLxjHLEon+zWxTI7sY0SFMQSgL0hDFwFoSxdZxFUEWPQF4wix2UiAZXLYCAY5J0IA+koEI4LUIQBtQRY6CbDoC8ZRZwsRWxcBlsmhK3nENZ4A9GV8i6xNENqytnVWJM6iLxhHK7SAJCMEWCYHqDtLAPpytvXaC4e2tCN2egAWfcE4Oq0xa4tnmRzW0kHinRKAvjoliPPjCEBb0IZOAiz6gnHU2ULE1kWAZXLoSh5xjScAfRnfImsThLasbZ0VibPoC8bRCi0gSdyqhgYYCbBcfBnrwVDmEIC2zOlFFjNh0ReMYxalk/2aWCZH9jGiwhgC0BekoYsAtKWLLOIqAiz6gnGE2GwkwDI5bASDnBMhAH0lghFB6hCAtiALnQRY9AXjqLOFiK2LAMvk0JU84hpPAPoyvkXWJghtWds6KxJn0ReMoxVaQJIRAiyTA9SdJQB9Odt67YVDW9oROz0Ai75gHJ3WmLXFs0wOa+kg8U4JQF+dEsT5cQSgLWhDJwEWfcE46mwhYusiwDI5dCWPuMYTgL6Mb5G1CUJb1rbO3MTl33ysm57f9dqfPffYMa8+6NCHyc/tENuW/1JXxjCOusgirk4CuPjqpIvY0Bc0oIsAtKWLrKNx5YI1x1NOLCCiY8Yq5QOn5QujRPIZIvF1MTJwvQ4sMI46qCKmbgK4+Oom7HZ86Mvt/uusHtrSSdfB2HLhYD95/ttICjFWKU1Oyxcq1W15fLqPPPpHMTJwd9JYYByTJop4HARw8eWg7O4Y0Je7vdddObSlm7BD8eXC9YeQ8K8jQccT0T3PTYz3HNjVM0qCXkRSziIhhsXIwKeTRgLjmDRRxOMggIsvB2V3x4C+3O297sqhLd2EHYovF609hojWEImjSMiJ8Uol3+Plf0aCDiKS80iKm8SWgY8mjQTGMWmiiMdBABdfDsrujgF9udt73ZVDW7oJOxRfXr5+NhUqm0jSySTEc6VKJVfIefcSiZlE1EuShsWWgc8mjQTGMWmiiMdBABdfDsrujgF9udt73ZVDW7oJOxS/tuL4cRLieJJSjPvlR3pyhT0kaQ4J+XOScqPYsvJnSSOBcUyaKOJxEMDFl4Oyu2NAX+72Xnfl0JZuwo7ElwsHTyBPXkJSHk4k5hLR6Fhlctq0fNcoSfksebnbxfDyW3TggHHUQRUxdRPAxVc3YbfjQ19u919n9dCWTrqOxJaL17+RZNU0CvLoAfLKm2jSe9kPn9nxJyfPnvcQdXs7xCcHHtGFA8ZRF1nE1UkAF1+ddBEb+oIGdBGAtnSRdSSu7Ft7NklxfrVcIX4sRvqHQ6Wz6AvG0RGxZaxMlsmRMWYop3kC0FfzrHBkawSgrdZ44egQAblo3QVEdGb1Tx59WwwP3BgBxKIvGEfI0kYCLJPDRjDIOREC0FciGBGkDgFoC7JomYCcf1OOZv33JeTLU/eaRu82Mbz8jrT0BePYcgtxggEEcPE1oAkZTgH6ynBzUy4N2kq5AbYNL/+mOJPGui4h339NNXdBnxUjA/8WUweLvmAcbVMR8lUEWCYHUDtLAPpytvXaC4e2tCPOzgBy0arDSRQuJklHk0c+ydwmMXLNTxtUyKIvGMfsaMylSlgmh0tAUet+BKAvCEIXAWhLF9mMxZVXrD2a8nQJ+eJFJGgXVeQmsW3Ff09RJou+YBwzJjZHymGZHI6wRJkvJAB9QRW6CEBbushmKK68cvA15KvtdmgmCXqEKl2bxLaljzVRIou+YByb6AQOMY4Ay+QwrmokxEUA+uIi7d440JZ7PW+pYtm35lSinNqjMUdED1JPaZP4xOrnmwzCoi8Yxya7gcOMIsAyOYyqGMlwEoC+OGm7NRa05Va/W6pWLlp7JpFQW+4QkbxHbFmxuaUATM//wzi22BUcbgQBXHyNaENmk4C+Mtva1AuDtlJvgZkJyEXr1KbeZ+/1jPRdsXXg821kyqIvGMc2OoNTUifAMjlSrxIJpEUA+kqLfPbHhbay3+OWKpQkBS1er/ZofONe0yhvF1tXfKWlIP97MIu+YBzb7A5OS5UAy+RItUIMniYB6CtN+tkeG9rKdn9bqk6+d9UBNJG/hKQ4oXpizvuc2Lz8ey0F2f9gFn3BOHbQIZyaGgGWyZFadRg4bQLQV9odyO740FZ2e9tSZXLx4KHVN6eJjqnu0Uj+sBhe+ZOWgrzwYBZ9wTh22CWcngoBlsmRSmUY1AQC0JcJXchmDtBWNvvaUlVy0YajyPPV7el5RHI35XKbxOblv2opSP2DWfQF45hApxCCnQDL5GCvCgOaQgD6MqUT2csD2speT1uqSC4efBXJ6h6NB5InHyW/vElsWf0/LQWJP5hFXzCOCXULYVgJsEwO1oowmEkEoC+TupGtXKCtbPWzpWrkojUnk5dTK40F8sSvyM9vEluW7W4pSOODWfQF45hgxxCKjQDL5GCrBgOZRgD6Mq0j2ckH2spOL1uqRC5a+xYiMb96kpD30siKTUJtvJPsfyz6gnFMtmmIxkOAZXLwlIJRDCQAfRnYlIykBG1lpJGtlCEXrzuPfDpnr2kU3xMj/Z9r5fwWjmXRF4xjCx3BocYQYJkcxlSLRLgJQF/cxN0ZD9pyp9fVSmXf4MUk5Zv2mkb6ihgZuF0jAhZ9wThq7CBCayPAMjm0ZY/AphOAvkzvkL35QVv29q6lzOWSYi+Vc5cQidftPVHcILb0f6elIK0fzKIvGMfWG4Mz0ifAMjnSLxMZpEQA+koJvAPDQlsONFkuXH8I5dR2O3QsCSFJymGxZeAehtJZ9AXjyNBJDJE4AZbJkXjWCGgLAejLlk7Zlye0ZV/PWspY/tWGI6lc3aPxCPLEbiqLYbFt+S9bCtL+wSz6gnFsv0E4Mz0CLJMjvfIwcsoEoK+UG5Dh4aGtDDdXXrnmeKp4lxLRwSToMSrQJvHJgUcYS2bRF4wjY0cxVGIEWCZHYtkikG0EoC/bOmZPvtCWPb1qKVPZt+4NJMSl5MtuIvlryhc2iU1X72wpSOcHs+gLxrHzRiECPwGWycFfFkY0hAD0ZUgjMpgGtJXBpsqFa88gIS6slfZTOqK8SRSLfgqlsugLxjGFzmLIjgmwTI6Os0QAWwlAX7Z2zvy8oS3ze9RShrJv3bkk6e3Vkzz/38Xwys+0FCDZg1n0BeOYbNMQjYcAy+TgKQWjGEgA+jKwKRlJCdrKSCNVGbJvzbtJeqfvNY10hxgeuC3l8lj0BeOYcpcxfFsEWCZHW5nhpCwQgL6y0EUza4C2zOxLS1nJK4o91d+cJvGGvaZR3iiGV3y7pSB6DmbRF4yjnuYhql4CLJNDbwmIbjAB6Mvg5lieGrRleQPl5etnU1d1j8bjq6UIGhEjA3cbUhaLvmAcDek20miJAMvkaCkjHJwlAtBXlrppVi3Qlln9aCkbuWTjEVSpXEJSHkmCnichh8XwigdaCqL3YBZ9wTjqbSKi6yHAMjn0pI6oFhCAvixokqUpQluWNk4uXnsskdpuh+ZU92j0aVhsHXjYsHJY9AXjaFjXkU5TBFgmR1OZ4KAsEoC+sthVM2qCtszoQ0tZyEXrXlfdo1HKaSToN1QpbxLbis+1FITnYBZ9wTjyNBOjJEuAZXIkmzKiWUQA+rKoWZalCm1Z1jB55fo3UcW/uJq2oJ/Rb8qbxHeLZUPLYNEXjKOh3UdaDQmwTA70wFkC0JezrddeOLSlHXFyA8jF688h3z+vZhq/L0YGPp1cdC2RWPQF46ildwiqmQDL5NBcA8KbSwD6Mrc3tmcGbVnSQblozbuIvDfvNY3ev4qR5V+yIHUWfcE4WqAEpPgCAiyTA9ydJQB9Odt67YVDW9oRdzaAnL+qi2blLyFfnLQ3krxZbFnxzc6isp3Noi8YR7Z+YqAECbBMjgTzRSi7CEBfdvXLpmyhLYO7Ja/acBBNqu126JV7PaO3RWxdfpfBKUdTY9EXjKNFikCq+wiwTA7wdpYA9OVs67UXDm1pR9zeAHJxcR7Jgtqj8SgScg95ckRsXnl/e9k58vsAACAASURBVNFSO4tFXzCOqfUXA3dAgGVydJAfTrWbAPRld/9Mzh7aMrA7ctHaY/Zut0NzyZOPk1cYFpuufsjAVKdKiUVfMI5TtQH/biIBlslhYuHIiYUA9MWC2clBoC3D2i4XDp5AHqk9GqeTkL8lqmwWI8VnDEuz2XRY9AXj2Gw7cJxJBFgmh0kFIxdWAtAXK26nBoO2DGq3XLz+jeT7l+5NSf6c8nM3iU3vKRmUYqupsOgLxrHVtuB4EwiwTA4TCkUOqRCAvlLB7sSg0JYhbZZ9a88mKc6vpiPoB2Jk4HpDUuskDRZ9wTh20iKcmxYBlsmRVnEYN3UC0FfqLchsAtCWAa2Vi9ZdQERnVlPx5J1ieMUXDEgriRRY9AXjmESrEIObAMvk4C4K4xlDAPoyphWZSwTaSrGl8rRinv64cDH58tS9plHcIob7v5FiSkkPzaIvGMek24Z4HARYJgdHIRjDSALQl5FtyURS0FZKbZRLNs6iSlnt0fjqvSn428SWlT9MKR1dw7LoC8ZRV/sQVycBlsmhswDENpoA9GV0e6xODtpKoX1y0arDyeu6hHz5x0Q0SkKMiJH++1JIRfeQLPqCcdTdRsTXQYBlcuhIHDGtIAB9WdEmK5OEtpjbJq9YezTlqns0HkaCnqBKeURsK/6eOQ2u4Vj0BePI1U6MkyQBlsmRZMKIZRUB6MuqdlmVLLTF2C555eBrSNKl5MsZJMRvqSSGxfblTzOmwD0Ui75gHLnbivGSIMAyOZJIFDGsJAB9Wdk2K5KGtpjaJPvWn0okLyMpBQn6L+p9fLP4p3+aYBo+rWFY9AXjmFZ7MW4nBFgmRycJ4lyrCUBfVrfP6OShLYb2yEVrzyQSassdtUnjD8WW/m0Mw5owBIu+YBxNaDVyaJUAy+RoNSkcnxkC0FdmWmlcIdCW5pbIK9edTxU6uzbM18WWgVs1D2lSeBZ9wTia1HLk0iwBlsnRbDI4LnMEoK/MtdSYgqAtTa2Q6pb0levVm9NvrA6RE18Qm/vv1DScqWFZ9AXjaGr7kVcjAiyTAy1wlgD05WzrtRcObWlALN+76gAaL6jfnP7TvaaRrhebB36gYSjTQ7LoC8bRdBkgv3oEWCYH0DtLAPpytvXaC4e2EkYsr1h1GOWrezS+nDwaJRJbxXD/zxMexpZwLPqCcbRFDsgzTIBlcgC5swSgL2dbr71waCtBxHLB2pdRzruUSB5OQj5JgkbE8IrfJTiEbaFY9AXjaJsskK8iwDI5gNpZAtCXs63XXji0lRBiuXjwVSTVdjs0kzz6HVW8EbF1+ZMJhbc1DIu+YBxtlYfbebNMDrcRO1099OV0+7UWD20lgFcuWnMyiZzaozFX3aOxUh4W24rjCYS2PQSLvmAcbZeJm/mzTA430aJqrGhDAxoJ4NrVIVy5aO1biMT8ahgpfyS2rtjaYcgsnc6iLxjHLEnGnVpYJoc7OFFphAD0BUnoIgBtdUBWLl53Hvl0TjWEoG+KkYGbOwiXxVNZ9AXjmEXpZL8mlsmRfYyoMIYA9AVp6CIAbbVJVvYNXkxSvmmvaZRfFCMrvtZmqCyfxqIvGMcsSyi7tbFMjuziQ2VTEIC+IBFdBKCtFsnKJcVe8vOXkk+vrZ36abFl4PsthnHlcBZ9wTi6Iqds1ckyObKFDNW0QAD6agEWDm2JALTVAi65cP0hJPzLiOgYEmKMZGWr2LLyZy2EcO1QFn3BOLomq2zUyzI5soEKVbRBAPpqAxpOaYoAtNUUJiL5VxuOpLJ/GflyXnWPxjJtFdev+E2Tp7t6GIu+YBxdlZfddbNMDrsRIfsOCEBfHcDDqQ0JQFtNCET2Db6CqLpH44Ek5O+pUBkRnyw+0cSprh/Coi8YR9dlZmf9LJPDTjTIOgEC0FcCEBGiLgFoawphyIVrTqRcTq00FojkfZSvjIhNxVHoqSkCLPqCcWyqFzjIMAIsk8OwmpEOHwHoi4+1ayNBWw06LhevPYN8cWH1EE/+SAxjj8YWJwiLvmAcW+wKDjeCAMvkMKJSJJEGAegrDepujAltxfRZ9q07lyS9fe8/+98SW1be5IYkEq2SRV8wjon2DMGYCLBMDqZaMIx5BKAv83qSlYygrTqdlAvXXUSCTqutNN4mhlfckZWGM9fBoi8YR+auYrhECLBMjkQyRRAbCUBfNnbNjpyhrVCf5BXFHsoVLiUpX7/XNHqfEcPL/92OVhqZJYu+YByN7D2SmoIAy+RAF5wlAH0523rthUNbNcTy8vWzKV/do/FYIhon4W8TIyt/qr0D2R6ARV8wjtkWUVarY5kcWYWHuqYkAH1NiQgHtEkA2iIiuWTjEeRX1JvTR5BHT5HMbRUj1/y6TaY47X8JsOgLxhGSs5EAy+SwEQxyToQA9JUIRgSpQ8B5bcnFa48lX1xORAeTEL+nyuRWsW31Y1BLIgRY9AXjmEivEISZAMvkYK4Jw5lDAPoypxdZy8RpbclF615HnricfNlNHt1PE90j4tMf2pO1JqdYD4u+YBxT7DCGbpsAy+RoOzucaDsB6Mv2Dpqbv7PakovXnka+uKjaGiHuEiP9W8xtk7WZsegLxtFafTidOMvkcJqw28VDX273X2f1TmpL9g3+OUn5F3vBim+JLf3Yo1GPylj0BeOop3mIqpcAy+TQWwKiG0wA+jK4OZan5py25OJ1F5JPZ9RWGr8sRvq/ankPTU6fRV8wjiZLALnFEWCZHMDvLAHoy9nWay/cGW3J+au6aGaX2qPxxCpV6X1ObF3+Pe2E3R6ARV8wjm6LzNbqWSaHrXCQd8cEoK+OESJADAEntCWv2nAQldR2O3R8dY/GvLhebOq/F6rQToBFXzCO2vuIATQQYJkcGvJGSDsIQF929MnGLDOvLbl4aB7J8mUk5ZEk5NPk5baJzct/ZWOzLMyZRV8wjhYqAykTy+QAZ2cJQF/Otl574ZnWlly09pi92+3QHCL6A+XFVrGp/1HtVDFAQIBFXzCOEJyNBFgmh41gkHMiBKCvRDAiSB0CmdWWXDJ4AlXocpJyWnWPRr+wVWxZthsqYCXAoi8YR9aeYrCECLBMjoRyRRj7CEBf9vXMlowzqS3Zt+7/kKRL9jZB3i22rBixpSEZy5NFXzCOGVONI+WwTA5HWKLMFxKAvqAKXQQypy25aPBtRPIdVWAefVsMD9yoCx7iTkmARV8wjlP2AQcYSIBlchhYN1LiIQB98XB2cZRMaUv2rZtPkt5SbaQQ2KMxfUWz6AvGMf1GI4PWCbBMjtbTwhkZIQB9ZaSRBpaRCW3J04p5OqpwKZE8ee9Ko/y8GF7xXQN5u5YSi75gHF2TVTbqZZkc2UCFKtogAH21AQ2nNEXAem3JJRtnUUVtt0OvJE9MkC+3iy0D9zRVPQ7STYBFXzCOutuI+DoIsEwOHYkjphUEoC8r2mRlklZrSy5adTiJLrVH41EkxNMk/evFlhUPWtmJbCbNoi8Yx2yKJ+tVsUyOrENEfbEEoC+IQxcBa7Ulr1h7NOXE5SRpLnniIZqU28T2gUd0gULctgiw6AvGsa3e4KSUCbBMjpRrxPDpEYC+0mOf9ZGt1Ja8cvA15Ff3aJxOQv6CxitbxWeLu7LeLAvrY9EXjKOFykDK+OUYaEArAZaLr9YKENxUAtZpSy5a92dEdFkVqCfvpnmVraJY9E0F7HheLPqCcXRcZZaWzzI5LGWDtDsnAH11zhAR6hOwSlvyysGzqCL/slqK8L8jRlbegMYaTYBFXzCORmsAycUQYJkcoO8sAejL2dZrL9wabclF6y4gojOrRKS8XWxd8RXtdDBApwRY9AXj2GmbcH4aBFgmRxqFYUwjCEBfRrQhk0kYry1ZLHq0o3Ap+fLUagewR6NNQmTRF4yjTZJArgEBlskB3M4SgL6cbb32wo3Wllw0NINESe3R+GoSNEkVf7vYtvLH2qlggKQIsOgLxjGpdiEOJwGWycFZEMYyigD0ZVQ7MpWMsdqSV6w6jPJdl5Ev/5iIniFPbhfDKx7IFP3sF8OiLxjH7AspixWyTI4sgkNNTRGAvprChIPaIGCktuSStS+jSnWPxsOqezRS/noxvGxHG/XhlHQJsOgLxjHdJmP09giwTI72UsNZGSAAfWWgiYaWYJy25OLBVxHR5eTLGUT0APnlbWJb8TlD+SGtxgRY9AXjCBnaSIBlctgIBjknQgD6SgQjgtQhYJS25MJ1p5CnVhqlICF+TLuO3ipuflcFnbOWAIu+YByt1YfTibNMDqcJu1089OV2/3VWb4y25OLBt5Iv31ktVojviJF+7NGos/M8sVn0BePI00yMkiwBlsmRbMqIZhEB6MuiZlmWqhHakovWnU9EZ+81jfQVMTJwu2UckW59Aiz6gnGE/GwkwDI5bASDnBMhAH0lghFB6hBIXVty8aDao/GNe3MTN4gt/d9BpzJDgEVfMI6Z0YtThbBMDqeIotgwAegLetBFIDVtySXFXirnLyeiP63u0ejltovN12CPRl2dTicui75gHNNpLkbtjADL5OgsRZxtMQHoy+LmGZ56KtqSlxXnUndB7dH4chLyWfJz28XW5b8wnBXSa50Ai75gHFtvDM5InwDL5Ei/TGSQEgHoKyXwDgzLri25eO1LyfcuJ5KHk6CHKVfaLjatfsgB1i6WyKIvGEcXpWV/zSyTw35MqKBNAtBXm+Bw2pQEWLUl+wZfQSQXkKSZ5NEDVMhdL/7lmmenzBIH2EqARV8wjrbKw+28WSaH24idrh76crr9Wotn05ZcvO4kktU9GnNE8h7KP7xNbNpU0lodgqdNgEVfMI5ptxnjt0OAZXK0kxjOyQQB6CsTbTSyCBZtyb7BN5OU76oS8Oi7Ynjg80bSQFJJE2DRF4xj0m1DPA4CLJODoxCMYSQB6MvItmQiKe3akovXnUc+nVOlhT0aMyGaForQrq+9stLz3wVEdKue0IgKAsQyOcDZWQLQl7Ot1164Vm3JvsGLSco3VauQ8kaxdcW3tVeEAUwioFVfQaEwjia1HLk0S4BlcjSbDI7LHAHoK3MtNaYgLdqSVxR7KJ+/nHx6LXmiRFJuFyMDdxtTNRLhIqBFX9HkYRy52olxkiTAMjmSTBixrCIAfVnVLquSTVxb8q/Xz6YxfwERHUNCPEu+wB6NVkki0WQT11e97GAcE+0ZgjERYJkcTLVgGPMIQF/m9SQrGSWqLdm37o9IiMvJl/OIxA4S1ZXGP2QFFupomUCi+oobHcax5b7gBAMIsEwOA+pECukQgL7S4e7CqIlpSy5cfxx5vtqj8UAS4pdUEtvF9uVPuwARNcYSSExfjRjDOEKBNhJgmRw2gkHOiRCAvhLBiCB1CCSiLdm37g21lcZCdY/G3eXrxc2rJ0HceQKJ6GsqijCOUxHCv5tIgGVymFg4cmIhAH2xYHZykI61JRcNnk4k312lJ/zviZGVn3OSJIquR6BjfTWDFcaxGUo4xjQCLJPDtKKRDxsB6IsNtXMDdaQt2bfuXJL09r3U5FfFlhVfdo4gCm5EoCN9NYsWxrFZUjjOJAIsk8OkgpELKwHoixW3U4O1rS25cN1FJOi0Ki1P3iiGsUejU8pprti29dVc+L1HwTi2QgvHmkKAZXKYUizyYCcAfbEjd2bAlrUl56/qopld6jenX0+CyuT728XWlXc5QwyFtkKgZX21Ejw4FsaxHWo4J20CLJMj7SIxfmoEoK/U0Gd+4Ja0Ja/acBBNVNQejceSoOfI87eLzSvvzzwlFNgugZb01e4gMI7tksN5aRJgmRxpFoixUyUAfaWKP9ODN60tuXDdSyhX3aPxCPLEjtqvwWCPxkzLo+PimtZXJyPBOHZCD+emRYBlcqRVHMZNnQD0lXoLMptAU9qSS9b/CZWrvwZzMBE9SPnydrGp+FRmqaCwpAg0pa9OB4Nx7JQgzk+DAMvkSKMwjGkEAejLiDZkMokptSUXrXsdedWVxm7y6CdULm8X24rjmaSBopImMKW+khgQxjEJiojBTYBlcnAXhfGMIQB9GdOKzCXSUFty8drTyBcXVasW4ntipB97NGZOAloLYrl2wThq7SGCayLAMjk05Y6w5hOAvszvka0ZxmpL9g3+OUn5F9XCPLpDDA/cZmuRyDs1AizXLhjH1PqLgTsgwDI5OsgPp9pNAPqyu38mZ19XW3LxugvJpzNqK403iZH+b5lcBHIzlgDLtauhcbz66qsvkFLeQkQP5fP5UwYHBx9pEtcFRHRrk8fiMBBolQDL5Gg1KRyfGQLQV2ZaaVwh+2lLnlbM08sKao/GE0nICvm568XW5dij0bi2WZMQy7Ur1jj29/e/uFQq3VgoFC70fX+e7/t9Q0NDS5rBd8qrTv/bBZev/Nx7/u/peAusGWA4pmkCnyp+qve/Hnz4pFcec+KP3lM8d7TpE3EgCDRB4FP/+J059/7gK8d96gsf/rcmDschINASgUvOfu/rT37tm5943+AFf5BLNs4iv7yAfDqePLmTpLddjPTf11JAHAwCIQKHnveBV84750O/+cl7Dtf62RhrHJctW7ZS5TM0NLSmWCzOHBsbu8HzvNUbNmyI/Tb0xf5brzt0cs+Zk+N75kzrnrZ7V67r4ce7ez956Zp33IjugkAnBLau+upr5k7uuXp2afQEWfG7Rc6beKpr2k+f7eoduqx47r2dxMa5IPDZ1bfNnzu656qZlckjKpVyQeYLe57omXnnz3Lv+GCxKHwQAoFOCNy08gsfOnR8z0WF0sTsvJcrE9GeF008v2Pe6M6HSdAjVMltF9uu+X0nY+BcNwnMv+mm3O/2nD5f5Oj0yuiuOfnps3ZLX/7GI/+2uxYc+nMdVGKN49KlS5VRvHnjxo3VW87R/x9N5ksDt3z8qD3PvvPA0viBk1KKLuH5455Xfrqr99EHZ8656IqVb/+JjgIQ0w0CX1/2+W/OG991wozyRO+k5/ldvu/tzneP7uiZ8dMzhy5+sxsUUKUOAtevueNPj9n15A0HT+45vMf382VJlBe+eLbQs/O30w6+9fz173yvjnER0w0Cn1vxpfe+fM8z6kvvHPKl10U+9ZYncxWRG9+Ty3/tiNIzV2GPRje0oKPK121/6jKP5IWS6KWyXJIi3yUF0W6S/gPSp/4fL5z7WNLjJmIcr1//zdnHPPPIDw4fe+7Ix3tmPDoxMT5tWlf32MzyhNq8VP5+2oHfKHm565JOHvHcIFAR9JaXju786xmliRnPdvc+Oen7Iu95cvbE6CG7C13P/3barE/kSXzdDRqoMmkCeVn5uyNHnztLkBC78l3PVHw/N01Q7pCJPXMe7T7g8Yenz7oqX/GfTXpcxHODwKzyxMdePLrzuD35rtEJQWOHT45Nz1fKPRXP8/5z1uE/Xn78m+e7QQJVJk2gUpYHFHJirZB0giR5X2X8eT83bcbTRPRyIlkSRJvvvnzuzUmP25Jx3LNnz1f/+Z//+eFoEhedueiV7zv2xP65ldKsP+QKT0wTojC3PHFwl5Reb6WUe6gw7fnfSNqRdPKI5waBuULMOboycbCQUuzJ5dVtnup/0yvlvCRBv853P/24lHie1g05JF7lUYLmHVEaO2AsV6hMiv+9LX1QebKw28tX7s91PTIqpdZnhhIvCgGNIfBqqrzs0NJE15OFrhKRkCqx3bmCf3Blsvve3tm7lucO+IYxySIRqwjkeqYf0D33yFd4ha7p/sTYc1LK3fmZs+8iknNI0ouIvM/8eMGcTyRdVCLPOI6suf3Vr9r55G1zJsYOeXTaAX8YnRyf3tvVs6e7XDpwRqV0wK+mz/ny2zfMvzTp5BHPDQI3DHzx747b89TyvO8XnuvqeazsV/LqOaEDJ8cPKwmv/MCMOevfvfb8j7pBA1UmTeDLy2/+9DF7njpv1Cs8P5YvPBfo67Dx5//oma5pT/5n72HnL157zk+THhfx3CDw3Q995p6jxp55xSPdsx4uUUWqa5fwZdfh47vn/e6Ag35+2tClb3CDBKpMmsCJm3fM87u7/0EQHS2JflIZ3dWb6505KiQdKknOESS23b3gkOGkx23qrepKpXKS7/vzr7322nfXS6BI5J2x9NP3vGRs1zG7c117dknf7/HypVnliQNH84XRX8+Y86kL1pxffdkG/4FAqwS2Dtx29vFjz/zLi8Z3v+jJrt5nJgSVuiQVDpkcPfjxngMe+8X0w65asOacf201Lo4HAUXg5pW3Fl++++mreivl3p257ufKJKlXyu4ZlYkDHpo268Hv9M57fbF4+r6VblADgVYI3HHNjV88as+zp/tC+Lu9/GhOeJXeyuRMdQflt9MP/Nafb3i32r4O/4FAywROK8r86FFPf0RK+UYietSf2DOe65lelpL+iIiekCQ/cc+CuYl/Nia2j+ONK7/0gZc+/+zfzixPHJT3y93SE5Uxr2v0ia7pv/x+74vOwIW3ZU3ghBCBLy+/Zeu88Z1nHlCenJGTlXxF5MrP57t2PzJt5jfOHZx/BWCBQLsEisWi92djR39n7sTosdMqk70eSa8k8pO7Ct3P/m76gR+/cM35H243Ns4Dga3F2087Zs8zn5w1OT632y/1CBJyPFcYfy7f/eSvp89dvGD1274PSiDQLoHXX//UW0jIhUT0ElmanCXyXeNC0FMk6cd3Lzik2G7cRucl+ssxny7e/o6DJscvGN39zHEzDjhwx2gu/9+P5SrFv179rud1JI+YbhH43Iovvn9WefIUURqfJws9O3YVCj+8aM1f4qUrt2SgpdpPFe/pPaTy0OreSvno8sTYbNnV84edhd5bL11z7he1DIigThFQ24nNLo+9T47vOaqn0D0+nvP+57lC98cWFM/Tsl2KU3BRLJ38mWdeWalUTpnc+cTLug6c+6gk8Yfpv5395e8WhZY7JYkax1D/TiOi76KfIKCJAMvu+JpyR1jzCUBf5vfI1gyhLVs7Z0feLPqCcbRDDMhyfwIskwPQnSUAfTnbeu2FQ1vaETs9AIu+YByd1pi1xbNMDmvpIPFOCUBfnRLE+XEEoC1oQycBFn3BOOpsIWLrIsAyOXQlj7jGE4C+jG+RtQlCW9a2zorEWfTVknFUv1k9Ojq6sre3V/1+9a4GGPGMoxUaszZJlslhLR3HEl+2bNk/5PP5jw4ODj6SUOnQV0IgEeYFBKAtiEInARZ96TKOakPTu3XSQWynCcxVe1Q5TQDF7yOgwThCX9BXRwSuvvrq6t6MGzduvDUSCNrqiGx2T25hYa4RBBZ9tWQcs9syVNYpgf7+/heXSqUbC4XChQmu/HSaFs4HARAAARYCy5YtUz9ysZqIHiKijwghdtQxjiy5YBA7CKjPzXK5/AMiOkJKWd0vtok7uqkX15JxVN+ipJQjnuedtWHDhruazb523vH5fH4kBOk/ent7z1ExRkdHNxUKhQ/CcDRL1LzjbDeOy5Yt2+R5ntL2jpBGb1S/lnTNNdec6Pt+39DQ0BLzyLudUejCOzI0NLQmTRqBhtS1UWmmUqkMFwqFs9V1DRpKszP6x659xt0SHkkI8c56xrG2srTfZ15NH6umTZv27vHx8bdKKY9PW8/6qbk9Qk0HdwghTg1ISCmrvmiKRwETARe+JoUNrPrik8/nTymXy6vUZ2I9r6fdOKoJFfxcYXBLqVAo7B4dHb2DiHYEH8zhi2wiVBCElYDNxnHp0qU3eJ53s7rIBxpV8Eql0tdyudxiNXHUaoKU8ri4n91khY3B9hEwxTiGNRTKidQFOPhCDA1BuMosjI2NqevN6vAHstIPEc0LTIP6/0KIX8A8QjM6CIS/qCiTunTp0rfmcrldYU3WPtP3fQbu96VoqqRq36TUsvvf+75/XS6Xmx98o/Z9/04imhU41OiKYW3gD/f29i6JOuiaofxAMFHwjXyqTpjz7+FvSsG36rBx9H1/Xj1thG7lrFIXxCCO53nXKdMW/QYmpayu+KnKo9/ow9/Mwv8W/cYW+hCfVW+lPFgNj16g660KqPyFEPfj9lO6Wqyt5t3sed4HpJTVa1PQk9oH7oW1DKs6q11b7pRS3hdcb2paJPXv9c5R5zfSY5hAPQ3FfZGChtLVjq7RI9ennXF35er1v/ZZeLIQ4ji14hh8VoZXsHXljbjpEghfe8KfXcHfW/l8rfM5WVeH0c+2ep/nAZU4D9dwxTG43UJEfUT090Q0op7dULdfyuXy+4M3GeO+SYcvztH2RB1v3DexdNuK0esRCAstMHfhD8o4bYSfAVIrMcHKc2AcwzqaSg/BsepLSfhN/+g39cgSfNVIBDU1GqPeN7CoZqEOfgLBt+Da9Uhdlz6irktqZVhl4/v+SUNDQ/8v/G259vfql9zgQhxcmzzPu7PeOdFV5jitxP09zjhCQ/ya0T1itKfRxxQiH8L7PQde089Cz/N+5Pt+9VZ1YBzDd+t014D4/ASi/ij8+RcYx8BMlkqlGcE7BHGfr9EVwjj9RP+uNKjiBws+Qoi+8OJI+G5KQKmhcaw3QL1l9riLYb0Bg4HrvXXW6Hj+tmLEqQiE9dHsB2X4uLBx7Onp+YZ6fEE97xH9cA+vGgUf/urB8+iFNvjGFTwaEc6/3peYuJxD3wKrz3oEK+k2346fqpe2/Hu9a03cdSP698gjCepFhuqKY7j24Jip9NjIDKh/i9MKNGSL0prPs5PPyWXLlv2dMo21Lzf7GUd8yWi+BzYeGb0+1fsCEnzGhY1j+M5unWdjb6lnNsPntLKgp7jWO35K4yil7JNSzlQf6FLKRUKI+dHnM1o1jsG3LLUyUO+ijVuBdkyDJI2j6nn0AlznduIv1AtW6puXWqVU2gx/Qw8bx1wud12lUrk5eLSiFeMYfPCrl2TCt0HxoZ++LoNrTS2TtxHRKvXsafCMaqPrSSvGcSo9wjimrwVTMqjdpm75c7J2PTnz2muv3VrvMxTG0ZQO68mj9lzrDiGEuluyU33WhD/Twv1vxjiqlFhTyAAACDhJREFULMMLe40el6n3pVn9Le4RrejxU96qVo5XSvlBIcTXpJTFJIxj3LMbWHHUI1BdUZM2jtH+R41jYA5q414nhLgvzjiqZyPD57dqHINvWuFnGmEcdSmp+bhBD4joSiLaLIT4ui7j2EiPMI7N9yzrRwYf8K1+ToYe3dmHKPycG4xjtpUTfCapz5jgWe1OjOPExMSx4btwHRjH/X7kpeUVx9BFutpBtUdfuVze3OyKY70Bwxfj2rMgM6+99tpvTPVMW7YlZGd1nRjH2oqhemZ2VnCxVNsyhVeO6r3AoI5VOlSrjopadOuCuJxKpdKHo6tSjTQ31ZYZHNsl2KkKvVkHPauNorb4OqVeb9W/17vtrP6unstVb6zW+9Ydvj41YxzxjKPeftsQvdnPyUZfPOuZRDzjaEP3288xeEFYCPES9Zx27Xnr8JZMwfZOajWyz/f9D0T3SQ7rplnj2EhX9V70a/kZx+DFAinlD9UKTtxFMu6bUfiNHIU3eIYthHrfM2R4q7p9AaZxZr19y+q9XR/VRkhTD6vJIoTYErxg0+iDOhgvMJlqrzM1kRoZx2DVMNiUN/y8YsAsPFEiL9K84I00vBGbhtL2HzN4MSv8pWGqZxxzudyPantzVt+s933/rJom9ntZKmw21a3qZoyjOid6sY3oaL/nZKGh9DWUdAbNfk4G16N6OzPU+wzFW9VJd8qseMFnWvBMf1gDY2Nj71fXKPXZqPa4rl2/9tveS1XTjnGMLopEdiXZt5OJit/WW9VJYI5uu1MvJm4BJkEaMdol0Myeafj23y5dN86Dhtzoc6dVRrcgi4vX6AWGTnPA+SAQ9+Z/mEyjO3ItbQDeLu64vfJUvHq3BNsdB+eBQLsEGn27x2p4u1TdOg8acqvf7VY71Wdeo8/LdsfEeSAQJTDV51qj6xmLcUTLQAAEQAAEQAAEQAAE7CcA42h/D1EBCIAACIAACIAACLAQgHFkwYxBQAAEQAAEQAAEQMB+AjCO9vcQFYAACIAACIAACIAACwEYRxbMGAQEQAAEQAAEQAAE7CcA42h/D1EBCIAACIAACIAACLAQgHFkwYxBQAAEQAAEQAAEQMB+AjCO9vcQFYAACIAACIAACIAACwEYRxbMGAQEQAAEQAAEQAAE7CcA42h/D1EBCIAACIAACIAACLAQgHFkwYxBQAAEQAAEQAAEQMB+AjCO9vcQFYAACIAACIAACIAACwEYRxbMGAQEQAAEQAAEQAAE7CcA42h/D1EBCIAACIAACIAACLAQgHFkwYxBQAAEQAAEQAAEQMB+AjCO9vcQFYAACIAACIAACIAACwEYRxbMGAQEQAAEQAAEQAAE7CcA42h/D1EBCDRNQF684SDqqWxs+oS0DhTyWTGy4uq0hg+P+/rtT64kohebkEujHPJSrPnhgjmPJJDnJUT0mVqcS4noswnERAgQAIGMEIBxzEgjUQYINEOgahy7K9eRR29o5viUjpkgkncaZhzPFUTTU+Ix5bCS6N68FMsSMI6nEtH3QwP+jIguJKIHp0wCB4AACDhBAMbRiTajSBDYS2CfcRTyNUTiLuO4CNlNJF7dhnFcQUTfJqL/SLqm2orjuTXztCfp+J3GE0QnSKIHEzKOnaaD80EABDJOAMYx4w1GeSAQJpAB4zi7duv0LCL6FBF9gIiOIKIiEb2PiN5LRGtqNYdvswYracE5/7dZo2m5cTyGiG4koh/VWI0RkTLZF4RWEsNMFbo/CxnwqCEP4j1GROqW9tOYYSAAAm4RgHF0q9+o1nECGTCO5xHRU0R0HBEdVjNEJxHRJ4hoDhH9CRHdRkTKDH28ZijVbVZlgJShdNU4vrpmCO8louuISDELbkErNr+tGXLFbZCI+mumMM44BvESX+F1fIqifBAwngCMo/EtQoIgkByBDBjHYOVQrSYeVVs1VIDOIKK1EVJh0xM1QE3f2rZ8xTFAEtQbNY4P1Yzke0LGcjURjdRuzcdxappfcupFJBAAARMIwDia0AXkAAJMBDJgHANS6pZpHxGtIqITiOiy0K3YqFlSq2Lq+I8R0d9OYYhe0ImMGccjiej3NaMdPBOqbjkrEx4Y73qGW3FRfw9uT8M4Ms1ZDAMCphGAcTStI8gHBDQSyJBxVAZG/ae2ilGrkFMZR6w4Ej1Oe98MV7eqwyuQ4RXGwCAGpjJu1RbGUeM8RWgQMJkAjKPJ3UFuIJAwARjHfW9dN218MrTiqAx2dMUwuHWtblWr/SrVqmOcWQxWeYeIaFnodnbCKkU4EAABkwnAOJrcHeQGAgkTyIhxDN7sVS9oBP8FL72ot4aD/6IGKHjbOvj38NvDsaQzZByDF2Ciq4rBht9h4xhmFX3LOvySUZh3wmpFOBAAARMJwDia2BXkBAKaCGTEOGqiUz9sRoxjK8yU4Q5WJy8nojuwAXgr+HAsCGSbAIxjtvuL6kBgPwIwjq0LwkHjiJ8cbF0mOAMEnCEA4+hMq1EoCIR+OQY/Odi0HALj6MhPDjbNBQeCAAi4SQDG0c2+o2pHCVRXHHsqG40vX8hnDfut6hebziwvxZoEfqva9DKRHwiAQMoEYBxTbgCGBwEQAAEQAAEQAAFbCMA42tIp5AkCIAACIAACIAACKROAcUy5ARgeBEAABEAABEAABGwhAONoS6eQJwiAAAiAAAiAAAikTADGMeUGYHgQAAEQAAEQAAEQsIUAjKMtnUKeIAACIAACIAACIJAyARjHlBuA4UEABEAABEAABEDAFgIwjrZ0CnmCAAiAAAiAAAiAQMoEYBxTbgCGBwEQAAEQAAEQAAFbCPx/JWg1VWNAP4EAAAAASUVORK5CYII=">

              <script>
                chartJsShow('<?= $dd; ?>','<?= $test_name ?>','<?= $str; ?>','<?= $str_m; ?>','<?= $str_f; ?>');
              </script>
            </div>
          <br><br>
          <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
          		<tr align="center">
                <td class="thStyle"><?=$test_name?></td>
                <td class="thStyle">ชาย/จำนวน</td>
                <td class="thStyle">หญิง/จำนวน</td>
                <td class="thStyle">รวม/คน</td>
              </tr>
            <tbody>
              <?php
                foreach ($row_c['data'] as $key_c => $value_c) {
                  $category_criteria_detail_name =  isset($value_c['category_criteria_detail_name'])?$value_c['category_criteria_detail_name']:"";
                  $category_criteria_detail_code =  isset($value_c['category_criteria_detail_code'])?$value_c['category_criteria_detail_code']:"";
                  $tm = $total_teat_p['M'][$test_code][$category_criteria_detail_code][$stage][$grade];
                  $tf = $total_teat_p['F'][$test_code][$category_criteria_detail_code][$stage][$grade];
                  $tt = (intval($tm) + intval($tf));
              ?>
              <tr align="center">
                <td class="content"><?=$category_criteria_detail_name?></td>
                <td class="content" style="text-align: center;"><?= $tm ?></td>
                <td class="content" style="text-align: center;"><?= $tf ?></td>
                <td class="content" style="text-align: center;"><?= $tt ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <br><br>
          <div class="col-md-6">
            <p class="text-center">&nbsp;</p>
          </div>
          <div class="col-md-6">
            <p class="text-center">&nbsp;</p>
          </div>
         </div>
       <!-- </div> -->
     <?php }}
      ?>
    </div>
    <div class="break"></div>
      <?php
              }
            }
          }
        }
        }
      }
    ?>
    <script src="../../dist/js/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>
