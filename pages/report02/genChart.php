  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");

  $projectCode = $_POST['project_code'];
  // $projectCode = "63040474";
  //echo $projectCode;
  ?>
  <html lang="en" dir="ltr">
    <head>
      <link rel="stylesheet" href="css/pdf.css">
      <script src="../../dist/js/Chart.min.js"></script>
    </head>
    <body>
    <script>
      function chartJsShow(id,name,str,str_m,str_f){
        // console.log(str);
        // console.log(str_m);
        // console.log(str_f);
        var res = str.split("|");
        var res_m = str_m.split("|");
        var res_f = str_f.split("|");
        let myChart = document.getElementById(id).getContext('2d');
        Chart.defaults.global.defaultFontFamily = 'chatthai';
        Chart.defaults.global.defaultFontSize = 12;
        Chart.defaults.global.defaultFontColor = '#777';

        let massPopChart = new Chart(myChart, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          defaultFontSize: '8',
          data:{
            labels: res ,
            datasets:[{
              label:'ชาย',
              data:res_m,
              fill: false,
              backgroundColor:'rgba(255, 99, 132, 0.6)',
              borderWidth:2,
              borderColor:'rgba(255, 99, 132, 0.6)',
              hoverBorderWidth:1,
              hoverBorderColor:'#000',
              lineTension: 0
            },
            {
              label:'หญิง',
              data:res_f,
              fill: false,
              backgroundColor:'rgba(54, 162, 235, 0.6)',
              borderWidth:2,
              borderColor:'rgba(54, 162, 235, 0.6)',
              hoverBorderWidth:1,
              hoverBorderColor:'#000',
              lineTension: 0
            }]
          },
          options:{
            responsive: true,
            title:{
              display:true,
              fontStyle:'bold',
              text:name,
              fontSize:16
            },
            legend:{
              display:true,
              position:'bottom',
              labels:{
                fontColor:'#000'
              }
            },
            tooltips:{
              enabled:true
            },
            scales: {
              yAxes: [{
                  ticks: {
                      // max: 150,
                      min: 0,
                      stepSize: 1
                  }
              }]
            }
          }
        });

        setTimeout(function(){
          var idImg = id +"_m";
          let canvas = document.getElementById(id);
          let imgWrap = document.getElementById(idImg)
          imgWrap.src = canvas.toDataURL();
          // imgWrap.style.display = 'none';
          let base64Img = imgWrap.src;

          $.post( "saveChartImg.php",{base64Img:base64Img,name:idImg})
          .done(function( data ) {
            console.log(data);
          });
          canvas.style.display = "none";
        }, 1000);
      }
    </script>
    <?php
    $arrResultPerson = array();

    $sqlTest = "SELECT * FROM pfit_t_project_test ptpt , pfit_t_test ptt
                 WHERE ptpt.test_code = ptt.test_code
                 AND ptpt.project_code = '$projectCode' ORDER BY ptpt.test_seq ASC";

    $queryTest   = DbQuery($sqlTest,null);
    $jsonTest    = json_decode($queryTest, true);
    $numTest     = $jsonTest['dataCount'];
    $rowTest     = $jsonTest['data'];

    $arrDetail = array();
    for($t =0;$t < $numTest; $t++)
    {
        $test_code     = $rowTest[$t]['test_code'];

        $sqltc  = "SELECT t.category_criteria_detail_code,td.category_criteria_detail_name
                   FROM pfit_t_test_criteria t, pfit_t_cat_criteria_detail td
                   where t.test_code = '$test_code' and t.category_criteria_detail_code = td.category_criteria_detail_code
                   group by t.category_criteria_detail_code, td.category_criteria_detail_name,  td.category_criteria_detail_value
                   order by td.category_criteria_detail_value";
        $querytc = DbQuery($sqltc,null);
        $jsonTc  = json_decode($querytc, true);

        //echo $sqltc;

        $dataCountTc  = $jsonTc['dataCount'];
        $rowTc        = $jsonTc['data'];

        for($tc =0;$tc < $dataCountTc ; $tc++)
        {
          $arrDetail[$test_code][$tc]['category_d_code'] = $rowTc[$tc]['category_criteria_detail_code'];
          $arrDetail[$test_code][$tc]['category_d_name'] = $rowTc[$tc]['category_criteria_detail_name'];
        }
    }


    $sqlp = "SELECT *
             FROM pfit_t_person p ,pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd
             WHERE ptc.test_criteria_code = pts.test_criteria_code and  pts.person_number = p.person_number
             AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
             AND pts.project_code = '$projectCode'
             order by pts.person_number";

     $queryp = DbQuery($sqlp,null);
     $jsonp  = json_decode($queryp, true);
     $rowp        = $jsonp['data'];
     $dataCountp  = $jsonp['dataCount'];

     for($p =0;$p < $dataCountp ; $p++)
     {
       $stage           = $rowp[$p]['stage'];
       $grade           = $rowp[$p]['grade'];
       $person_gender   = $rowp[$p]['person_gender'];
       $test_code       = $rowp[$p]['test_code'];
       $category_d_code = $rowp[$p]['category_criteria_detail_code'];

       if($person_gender == "M")
       {
         if(!isset($arrResultPerson['M'][$stage][$grade][$test_code][$category_d_code])){
           $arrResultPerson['M'][$stage][$grade][$test_code][$category_d_code] = 0;
         }
         $arrResultPerson['M'][$stage][$grade][$test_code][$category_d_code]++;
       }
       else if($person_gender == "F")
       {
         if(!isset($arrResultPerson['F'][$stage][$grade][$test_code][$category_d_code]))
         {
           $arrResultPerson['F'][$stage][$grade][$test_code][$category_d_code] = 0;
         }
         $arrResultPerson['F'][$stage][$grade][$test_code][$category_d_code]++;
       }
     }


     $sql = "SELECT stage,grade From pfit_t_person
             where  project_code = '$projectCode'
             group by stage, grade order by stage desc, grade";

     $query = DbQuery($sql,null);
     $json  = json_decode($query, true);
     $rows        = $json['data'];
     $dataCount   = $json['dataCount'];

     for($x =0;$x < $dataCount ; $x++)
     {
       $stage = $rows[$x]['stage'];
       $grade = $rows[$x]['grade'];

       for($t =0;$t < $numTest;$t++)
       {
         $test_name  = $rowTest[$t]['test_name'];
         $test_code  = $rowTest[$t]['test_code'];

         $arrCategory_d_name = array();
         $m_score = array();
         $f_score = array();

         for($a = 0; $a < count($arrDetail[$test_code]) ; $a ++)
         {
               $arrCategory_d_name[$a]  = $arrDetail[$test_code][$a]['category_d_name'];
               $category_d_code         = $arrDetail[$test_code][$a]['category_d_code'];
               $m_score[$a] = isset($arrResultPerson['M'][$stage][$grade][$test_code][$category_d_code])?$arrResultPerson['M'][$stage][$grade][$test_code][$category_d_code]:"0";
               $f_score[$a] = isset($arrResultPerson['F'][$stage][$grade][$test_code][$category_d_code])?$arrResultPerson['F'][$stage][$grade][$test_code][$category_d_code]:"0";
          }
          $str =  implode("|",$arrCategory_d_name);
          $str_m =  implode("|",$m_score);
          $str_f =  implode("|",$f_score);

          $dd  =  $projectCode."_".$stage.'_'.$grade.'_'.$t;
        ?>
         <canvas id="<?= $dd ?>" style="display:none;"></canvas>
         <img id="<?= $dd."_m" ?>" style="width:100%;display:none;">

         <script>
           chartJsShow('<?= $dd; ?>','<?= $test_name ?>','<?= $str; ?>','<?= $str_m; ?>','<?= $str_f; ?>');
         </script>
      <?php
       }
     }
     ?>
    <script src="../../dist/js/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../dist/js/mainFunc.js"></script>
</body>
</html>
<script>
  var pram = "?date=&project_code=<?= $projectCode; ?>";
  var url = 'print.php'+ pram;
  setTimeout(function(){
    postURL(url);
  }, 2000);
</script>
