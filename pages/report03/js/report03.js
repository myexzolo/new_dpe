$(function () {
  $('.select2').select2();
  searchProject();
})


function searchProject(){
  //var date = $('#s_year').val()+"/"+$('#s_month').val() + "/01";
  var dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
  var dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");
  var typeDate  = $('#typeDate').val();
  var user_login_s  = $('#user_login_s').val();
  var role_id       = $('#role_id').val();

  $.post("ajax/showTable.php",{dateStart:dateStart,dateEnd:dateEnd,typeDate:typeDate,user_login_s:user_login_s,role_id:role_id})
    .done(function( data ) {
      $('#show-table').html(data);
    });
}
