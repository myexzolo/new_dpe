<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  $typeDate   = $_POST['typeDate'];

  $dateStartOld = $_POST['dateStart'];

  $user_login_s   = @$_POST['user_login_s'];
  $role_id        = isset($_POST['role_id'])?$_POST['role_id']:"";

  $cond= "";
  $con = "";


  $typeDateStr = "";
  $typeDateQuery = "end_date";
  if($typeDate == "1"){
    $typeDateStr = "ตามวันที่ทดสอบ";
  }else if($typeDate == "2")
  {
    $typeDateQuery = "date_create";
    $typeDateStr = "ตามวันที่นำเข้าข้อมูล";
  }


  // echo $dateStart  = '2018/10/01';
  $dateS = $dateStart;
  $dateE = $dateEnd;
  $month = monthThaiFull(date("n", strtotime($dateStart)));
  $year  = date("Y", strtotime($dateStart))+543;

  $user_login = "";

?>
  <div class="box box-primary">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:24px;">
          <b>รายงานสรุปผลการทดสอบสมรรถภาพแต่ละฐาน ตั้งแต่วันที่
            <?=convDatetoThaiFull($dateStart) ?> - <?= convDatetoThaiFull($dateEnd)."<br>".$typeDateStr?>
            <?=$cond ?>
          </b
        </p>
        <table class="table table-bordered table-striped">
          <thead>
        		<tr>
        			<th style="vertical-align:middle">ลำดับ</th>
              <th style="vertical-align:middle">รหัส</th>
              <th style="vertical-align:middle">วันที่ทดสอบ</th>
              <th style="vertical-align:middle">วันที่นำเข้าข้อมูล</th>
              <th style="vertical-align:middle">ชื่อกิจกรรม</th>
              <th style="vertical-align:middle">สถานที่</th>
              <th style="vertical-align:middle">ระยะเวลา</th>
        			<th style="vertical-align:middle">Excel</th>
        		</tr>
          </thead>
          <tbody class="text-center">
              <?php
                $end_date   = getQueryDate('end_date');
                $start_date = getQueryDate('start_date');
                $end_date2   = getQueryDateWere('end_date');

                $dateStart = queryDateOracle($dateStart);
                $dateEnd  = queryDateOracle($dateEnd);

                $con = "";

                $user_login = $_SESSION['member'][0]['user_login'];
                $adminRole  = $_SESSION['member'][0]['adminRole'];

                $con = " and user_login = 'emtry'";

                if($user_login != ""  && $adminRole == ""){
                    $con = " and user_login = '".$user_login."'";
                }
                else if ($adminRole != "")
                {
                  $con = "";
                }

                $conr = "";
                //echo count($role_id);
                if($role_id != "" && count($role_id) > 0)
                {
                  $role = implode(",",$role_id);
                  for($x=0; $x < count($role_id); $x++)
                  {
                      $roleId = $role_id[$x];
                      if($roleId != "")
                      {
                        if($conr == "")
                        {
                          $conr .= " and (','||u.role_list||',' like '%,'||'$roleId'||',%'";
                        }else{
                          $conr .= " or ','||u.role_list||',' like '%,'||'$roleId'||',%'";
                        }
                      }

                  }
                  if($conr != ""){
                    $conr .= ")";
                    $con  .= " and user_login in (select u.user_login FROM t_user u where u.is_active = 'Y' $conr)";
                  }
                }


                if($user_login_s != ""){
                    $con .= " and user_login = '".$user_login_s."'";
                }


                $sql = "SELECT $end_date,$start_date,project_name,location,project_code,date_create,project_type
                FROM pfit_t_project where $typeDateQuery between $dateStart and $dateEnd and status <> 'C'  $con
                ORDER BY $typeDateQuery ASC";

                $query = DbQuery($sql,null);
                $row  = json_decode($query, true);
                $num  = $row['dataCount'];
                $data = $row['data'];

                //echo $sql;

                if($num>0){
                  foreach ($data as $key => $value) {
                    $project_name = $value['project_name'];
                    $location     = $value['location'];
                    $start_date   = convDatetoThaiMonth($value['start_date']);
                    $end_date     = convDatetoThaiMonth($value['end_date']);
                    $date_create  = convDatetoThaiMonth(@$value['date_create']);
                    $project_code = $value['project_code'];
                    $project_type = $value['project_type'];
                    $end_date2    = $value['end_date'];

                    $urlProject = "excelexport.php?project_name=$project_name&project_code=$project_code&project_type=$project_type";
              ?>
              <tr>
                <td><?=$key+1; ?></td>
                <td class="text-left"><?=$project_code; ?></td>
                <td align="center"><?=$end_date; ?></td>
                <td align="center"><?=$date_create; ?></td>
                <td class="text-left"><?=$project_name; ?></td>
                <td class="text-left" ><?=$location; ?></td>
                <td ><?=$start_date." - ".$end_date; ?></td>
                <td><a onclick="postURL_blank('<?=$urlProject ?>')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
              </tr>
              <?php
                  }
                }else{
              ?>
               <tr>
           			<td colspan="13">ไม่พบข้อมูล</td>
               </tr>
             <?php } ?>
            </tbody>
          </table>
      </div>
    </div>
    </div>
  </div>
