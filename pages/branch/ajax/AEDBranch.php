<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
include('../../../inc/function/authen.php');


$action         = isset($_POST['action'])?$_POST['action']:"ADD";
$branch_id      = isset($_POST['branch_id'])?$_POST['branch_id']:"";
$branch_code    = strtoupper(isset($_POST['branch_code'])?$_POST['branch_code']:"");
$cname          = isset($_POST['cname'])?$_POST['cname']:"";
$branch_name    = isset($_POST['branch_name'])?$_POST['branch_name']:"";
$branch_address = isset($_POST['branch_address'])?$_POST['branch_address']:"";
$branch_tel     = isset($_POST['branch_tel'])?$_POST['branch_tel']:"";
$branch_fax     = isset($_POST['branch_fax'])?$_POST['branch_fax']:"";
$branch_tax     = isset($_POST['branch_tel'])?$_POST['branch_tax']:"";
$office_hours   = isset($_POST['office_hours'])?$_POST['office_hours']:"";
$branch_open    = isset($_POST['branch_open'])?$_POST['branch_open']:"";
$branch_close   = isset($_POST['branch_close'])?$_POST['branch_close']:"";
$office_hours2   = isset($_POST['office_hours2'])?$_POST['office_hours2']:"";
$branch_open2    = isset($_POST['branch_open2'])?$_POST['branch_open2']:"";
$branch_close2   = isset($_POST['branch_close2'])?$_POST['branch_close2']:"";
$is_active      = isset($_POST['is_active'])?$_POST['is_active']:"";
$lat            = isset($_POST['lat'])?$_POST['lat']:"";
$lng            = isset($_POST['lng'])?$_POST['lng']:"";


$user_id_update = $_SESSION['member'][0]['user_id'];

$branch_logo  = "";
$updateImg    = "";

$image_checkin  = "";
$updateImgCn    = "";

$path = "images/";
if(isset($_FILES["branch_logo"])){
  $branch_logo = resizeImageToBase64($_FILES["branch_logo"],'400','400','100',$user_id_update,$path);

  if(!empty($branch_logo)){
      $updateImg    =  "branch_logo = '$branch_logo',";
  }
}

if(isset($_FILES["image_checkin"])){
  $image_checkin = resizeImageToBase64($_FILES["image_checkin"],'128','128','100',$user_id_update,$path);

  if(!empty($image_checkin)){
      $updateImgCn    =  "image_checkin = '$image_checkin',";
  }
}


$sql = "";
// --ADD EDIT DELETE Module-- //
if(empty($branch_id) && $action == 'ADD'){
  $sql = "INSERT INTO t_branch
         (branch_code,cname,branch_name,branch_address,branch_tel,image_checkin,
          branch_fax,branch_tax,office_hours,branch_open,branch_close,office_hours2,branch_open2,branch_close2,is_active,user_id_update,branch_logo,lat,lng)
         VALUES('$branch_code','$cname','$branch_name','$branch_address','$branch_tel','$image_checkin',
         '$branch_fax','$branch_tax','$office_hours','$branch_open','$branch_close','$office_hours2','$branch_open2','$branch_close2','$is_active','$user_id_update','$branch_logo','$lat','$lng')";
}else if($action == 'EDIT'){
  $sql = "UPDATE t_branch SET
                branch_code = '$branch_code',
                cname = '$cname',
                branch_name = '$branch_name',
                branch_address = '$branch_address',
                branch_tel = '$branch_tel',
                branch_fax = '$branch_fax',
                branch_tax = '$branch_tax',
                office_hours = '$office_hours',
                branch_open = '$branch_open',
                branch_close = '$branch_close',
                office_hours2 = '$office_hours2',
                branch_open2 = '$branch_open2',
                branch_close2 = '$branch_close2',
                lat = '$lat',
                lng = '$lng',
                ".$updateImgCn."
                ".$updateImg."
                is_active = '$is_active',
                user_id_update = '$user_id_update'
            WHERE branch_id = '$branch_id'";
}else if($action == 'DEL'){
  $sql   = "DELETE FROM t_branch WHERE branch_id = '$branch_id'";
}
// --ADD EDIT Module-- //
//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if($errorInfo[0] == "00000"){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail : '.$errorInfo[2])));
}

?>
