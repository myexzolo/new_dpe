<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>Num</th>
      <th>Code</th>
      <th>Name</th>
      <th>ที่อยู่</th>
      <th>เบอร์โทร</th>
      <th>เวลาเปิด</th>
      <th>เวลาปิด</th>
      <th>สถานะ</th>
      <th style="width:50px;">Edit</th>
      <th style="width:50px;">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_branch ORDER BY branch_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?=$i + 1;?></td>
      <td><?=$rows[$i]['branch_code'];?></td>
      <td><?=$rows[$i]['branch_name'];?></td>
      <td><?=$rows[$i]['branch_address'];?></td>
      <td><?=$rows[$i]['branch_tel'];?></td>
      <td><?=$rows[$i]['branch_open'];?></td>
      <td><?=$rows[$i]['branch_close'];?></td>
      <td><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"INACTIVE";?></td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$rows[$i]['branch_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$rows[$i]['branch_id']?>','<?=$rows[$i]['branch_name']?>')"></i></a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
