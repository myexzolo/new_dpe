<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - Excel Template</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/template.css">

    <style>
      th {
        text-align: center;
        background-color: #ebebeb;
      }
    </style>
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>จัดการ Excel Template</h1>
          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">จัดการ Excel Template</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php"); ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="tableDisplay">
                    <thead>
                      <tr class="text-center">
                        <th style="width:40px;">No.</th>
                        <th>รายการ Excel template</th>
                        <th style="width:80px;">Excel</th>
                        <?php
                        if($_SESSION['ROLE_USER']['is_update'])
                        {
                        ?>
                        <th style="width:50px;">Edit</th>
                        <?php
                        }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="text-center">
                        <td>1</td>
                        <td align="left">นักเรียน (7 - 18 ปี)</td>
                        <td align="center">
                          <a class="btn_point" href="../../template/student_7_18_TP.xlsx" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </td>
                        <?php
                        if($_SESSION['ROLE_USER']['is_update'])
                        {
                        ?>
                        <td>
                          <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','1','นักเรียน (7 - 18 ปี)')"></i></a>
                        </td>
                        <?php
                        }
                        ?>
                      </tr>
                      <tr class="text-center">
                        <td>1</td>
                        <td align="left">ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)</td>
                        <td align="center">
                          <a class="btn_point" href="../../template/person_7_18_TP.xlsx" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </td>
                        <?php
                        if($_SESSION['ROLE_USER']['is_update'])
                        {
                        ?>
                        <td>
                          <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','2','ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)')"></i></a>
                        </td>
                        <?php
                        }
                        ?>
                      </tr>
                      <tr class="text-center">
                        <td>1</td>
                        <td align="left">ประชาชนทั่วไป ช่วงอายุ (19 - 59 ปี)</td>
                        <td align="center">
                          <a class="btn_point" href="../../template/person_19_59_TP.xlsx" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </td>
                        <?php
                        if($_SESSION['ROLE_USER']['is_update'])
                        {
                        ?>
                        <td>
                          <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','3','ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)')"></i></a>
                        </td>
                        <?php
                        }
                        ?>
                      </tr>
                      <tr class="text-center">
                        <td>1</td>
                        <td align="left">ประชาชนทั่วไป ช่วงอายุ (60 ปีขึ้นไป)</td>
                        <td align="center">
                          <a class="btn_point" href="../../template/person_60_TP.xlsx" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </td>
                        <?php
                        if($_SESSION['ROLE_USER']['is_update'])
                        {
                        ?>
                        <td>
                          <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','4')"></i></a>
                        </td>
                        <?php
                        }
                        ?>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">จัดการ Excel template</h4>
                    </div>
                    <form id="form" novalidate enctype="multipart/form-data">
                    <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                      <div id="show-form"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="js/template.js"></script>
    <script>
      $(function () {
        $('#tableDisplay').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        })
      })
    </script>
  </body>
</html>
