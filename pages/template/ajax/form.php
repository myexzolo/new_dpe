<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = $_POST['id'];
$name   = $_POST['name'];
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" id="action" name="id"  value="<?=$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-5">
      <div class="form-group">
        <label>Template <?=$name?> (Excel ไฟล์)</label>
        <input name="path" type="file" class="form-control custom-file-input" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
