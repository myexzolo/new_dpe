<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
include('../../../inc/function/authen.php');


$path   = "";
$pathUpload = "../../../template/";
$id    = isset($_POST['id'])?$_POST['id']:"";


if(isset($_FILES["path"]) && $id != "")
{
  $name = "";
  if($id == "1")
  {
    $name = "student_7_18_TP";
  }else if($id == "2")
  {
    $name = "person_7_18_TP";
  }else if($id == "3")
  {
    $name = "person_19_59_TP";
  }else if($id == "4")
  {
    $name = "person_60_TP";
  }
  $path = uploadFile($_FILES["path"],$pathUpload,$name);
}

if($path != ""){

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
