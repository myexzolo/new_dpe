<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
  </head>
  <body>
<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  require_once '../../../mpdf2/vendor/autoload.php';


  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();

  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
  //echo $PNG_TEMP_DIR;
  //html PNG location prefix
  $PNG_WEB_DIR = 'temp/';
  include "../../../qrcode/qrlib.php";

  if (!file_exists($PNG_TEMP_DIR)){
    mkdir($PNG_TEMP_DIR);
  }

  $project_name = $_POST['project_name'];
  $project_code = $_POST['project_code'];

  $CorrectionLevel = 'L';
  $matrixPointSize = 4;

  $sql = "SELECT p.project_name,ps.person_number,ps.project_code,ps.person_name,ps.person_lname ,
         ".getQueryDate('date_of_birth').",ps.wiegth,ps.height,ps.person_gender
          FROM pfit_t_project p
          INNER JOIN pfit_t_person ps ON p.project_code = ps.project_code
          WHERE ps.project_code = '$project_code'";

  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $j = 1;

  $sqlp = "SELECT * FROM pfit_t_project_test
          INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
          WHERE pfit_t_project_test.project_code = '$project_code'
          ORDER BY pfit_t_project_test.test_seq ASC";
  $queryp = DbQuery($sqlp,null);
  $rowp  = json_decode($queryp, true);

  if($json['dataCount'] > 0){
  foreach ($json['data'] as $value) {
  // for ($i=0; $i <2 ; $i++) {
  if(!isset($value['project_name'])){
    break;
  }
  $project_name = $value['project_name'];
  $person_number = $value['person_number'];
  $projectCode = $value['project_code'];
  $person_name = $value['person_name'];
  $person_lname = $value['person_lname'];
  $date_of_birth = $value['date_of_birth'];
  $wiegth = $value['wiegth'];
  $height = $value['height'];
  $person_gender = $value['person_gender']=='M'?"ชาย":"หญิง";

  $age  =  yearBirth($date_of_birth);


  $filename = $PNG_TEMP_DIR.$person_number.'.png';
  $textQr = "$person_number|$projectCode";
  QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);

  ?>

  <div align="center" style="font-size:18pt;width:100%"><b>แบบบันทึกผลการทดสอบ <?php echo $project_name; ?></b></div>
  <div class="half">
    <table border="0" cellspacing="0" style="width:100%" >
      <tr>
        <td style="width:56%;padding-right:15px;" valign="top">
          <table border="0" cellspacing="0" style="width:100%;font-size:13pt;" >
            <tr>
              <td style="width:100px;" rowspan="4">
                <img style="" src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" />
              </td>
              <td style="content">
                ชื่อ-สกุล: <?= $person_name; ?> <?php echo $person_lname; ?>
              </td>
              <td style="content">
                น้ำหนัก: <?=  $wiegth; ?>
              </td>
            </tr>
            <tr>
              <td style="content">
                เลขประจำตัว: <?= $person_number; ?>
              </td>
              <td style="content">
                ส่วนสูง: <?=  $height; ?>
              </td>
            </tr>
            <tr>
              <td style="content">
                วันเดือนปีเกิด: <?= DateThai($date_of_birth); ?>
              </td>
              <td style="content">
                เพศ: <?=  $person_gender; ?>
              </td>
            </tr>
            <tr>
              <td style="content">&nbsp;</td>
              <td style="content"></td>
            </tr>
          </table>
          <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
              <tr>
                <td class="text-center thStyle"><strong>ลำดับ</strong></td>
                <td class="text-center thStyle"><strong>รายการ</strong></td>
                <td class="text-center thStyle"><strong>ผลการวัด</strong></td>
              </tr>

              <?php

                $x= 0;
                foreach ($rowp['data'] as $key => $value)
                {
                  $test_age_min =  $value['test_age_min'];
                  $test_age_max =  $value['test_age_max'];
                  if($age < $test_age_min || $age > $test_age_max){
                    continue;
                  }
                  $x++;
                  if($x > 8){break;}
              ?>
              <tr>
                <td width="10%" class="text-center content"><?= $key+1; ?></td>
                <td width="73%" class="text-left content"><?= $value['test_name']." (".$value['test_unit'].")"; ?></td>
                <td width="17%" class="text-center content"></td>
              </tr>
              <?php }

                $q1 = "ข้อที่ 1 มีกิจกรรมทางกายและการออกกำลังกาย กี่วัน/สัปดาห์ (นอกจากชั่วโมงพลศึกษา)";
                $q2 = "ข้อที่ 2 รูปแบบกิจกรรมทางกาย (นอกจากชั่วโมงพลศึกษา)";
                $q3 = "ข้อที่ 3 ระยะเวลาในการทำกิจกรรมต่อวัน";

                $a[0] = "ไม่เคยเลย";
                $a[1] = "1-2 วันต่อสัปดาห์";
                $a[2] = "3-4 วันต่อสัปดาห์";
                $a[3] = "5-7 วันต่อสัปดาห์";

                $b[0] = "กีฬาชนิดต่างๆ เช่น ฟุตบอล, เทควันโด, ปั่นจักรยาน เป็นต้น";
                $b[1] = "กิจกรรมนันทนาการ เช่น เต้นรำ, เต้นแอโรบิก, ลีดเดอร์";
                $b[2] = "การละเล่นพื้นบ้าน เช่น หมากเก็บ, กระโดดยาง, ดี่จับ";
                $b[3] = "งานบ้าน เช่น ล้างจาน, ถูบ้าน, รดน้ำต้นไม้";

                $c[0] = "น้อยกว่า 15 นาที";
                $c[1] = "15-30 นาที";
                $c[2] = "31-60 นาที";
                $c[3] = "61 นาทีขึ้นไป";

              ?>
          </table>
        </td>
        <td valign="top">
          <div>
              <div style="font-size:13pt;">
                <b><?= $q1 ?></b>
                <div style="margin-bottom:8px;"></div>
                  <?php
                  for ($i=0; $i < count($a); $i++) { ?>
                  <div style="margin-bottom:5px;">
                      <input type="radio" id="points_1_<?= $i ?>" name="points_1">
                      <label for="points_1_<?= $i ?>" style="font-weight:400;"><?= $a[$i]; ?></label>
                  </div>
                  <?php } ?>
              </div>
              <br>
              <div style="font-size:13pt;">
                <b><?= $q2 ?></b>
                <div style="margin-bottom:8px;"></div>
                  <?php
                  for ($i=0; $i < count($b); $i++) { ?>
                  <div style="margin-bottom:5px;">
                      <input type="radio" id="points_2_<?= $i ?>" name="points_2">
                      <label for="points_2_<?= $i ?>" style="font-weight:400;"><?= $b[$i]; ?></label>
                  </div>
                  <?php } ?>
              </div>
              <br>
              <div style="font-size:13pt;">
                <b><?= $q3 ?></b>
                <div style="margin-bottom:8px;"></div>
                  <?php
                  for ($i=0; $i < count($c); $i++) { ?>
                  <div style="margin-bottom:5px;">
                      <input type="radio" id="points_3_<?= $i ?>" name="points_3">
                      <label for="points_3_<?= $i ?>" style="font-weight:400;"><?= $c[$i]; ?></label>
                  </div>
                  <?php } ?>
              </div>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <?php
    if($j%2 ==0){
      echo '<div class="break"></div>';
    }
    $j++
  ?>
<?php }} ?>
</body>
</html>

<?php
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/PFIT0101.css');
  $mpdf->SetTitle('รายชื่อผู้ทดสอบ');
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

  // $mpdf->Output('IVRENT201711000059.pdf', \Mpdf\Output\Destination::DOWNLOAD);
  $mpdf->Output($namepdf.'.pdf', \Mpdf\Output\Destination::INLINE);
?>
