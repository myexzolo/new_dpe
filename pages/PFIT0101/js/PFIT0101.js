
$(function () {
    showTable();
})

function showTable(){
  var code = $('#code').val();
  $.post( "ajax/showTable.php",{code:code})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",code=""){
  $.post("ajax/form.php",{value:value,code:code})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}



$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});


$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();
  var projectCode = $('#projectCode').val();
  var personType = $('#person_type').val();
  // $('#loading').removeClass("none");
  // $('#loading').addClass('loading');
  //waitingDialog.show();
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
      //console.log(data);
      $.smkAlert({text: data.message,type: data.status});
       $('#formDataUpload').smkClear();
        $('#projectCode').val(projectCode);
        $('#person_type').val(personType);
        showTableHistory(projectCode);
        // $('#loading').removeClass('loading');
        // $('#loading').addClass('none');
        showTable();
        //setTimeout(function () {waitingDialog.hide();}, 1000);
    }).fail(function (jqXHR, exception) {
        $('#formDataUpload').smkClear();
        $('#projectCode').val(projectCode);
        $.smkAlert({text: "ไม่สามารถนำเข้าได้",type: "danger"});
        console.log(jqXHR);
        console.log(exception);
        // $('#loading').removeClass('loading');
        // $('#loading').addClass('none');
        //setTimeout(function () {waitingDialog.hide();}, 1000);
    });
  }else{
    // $('#loading').removeClass('loading');
    // $('#loading').addClass('none');
    //setTimeout(function () {waitingDialog.hide();}, 1000);
  }
});

function exportExcel(project_type)
{
  $('#myModal3').modal({backdrop:'static'});

}

function loadExcel(url)
{
  postURL_blank(url);
  $('#myModal3').modal('toggle');
}

function openModal(projectCode,projectName,projectType){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,'ADD','',projectType);
}

function formShow(projectCode,status,code,projectType){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,status,code,projectType);
}

function formShows(projectCode,status,code,projectType){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.post("ajax/form.php",{code:code,status:status,projectCode:projectCode,projectType:projectType})
  .done(function( data ) {
      $("#formShow").html(data);
  });
}


function delModule(id,name){
  $.smkConfirm({
    text:'ต้องการลบผู้ทดสอบ '+name+' ใช่หรือไม่',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      var projectCode = $('#code').val();
      $.post("ajax/delete.php",{code:id,projectCode:projectCode})
        .done(function( data ) {
          console.log(data);
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
            showTable();
          }, 1000);

      });
    }
  });

}

function upload(projectCode,projectName,projectType){
  $('#myModal2').modal({backdrop:'static'});
  $.post("ajax/formUpload.php",{projectCode:projectCode,projectType:projectType,projectName:projectName})
  .done(function( data ) {
      $("#formShow2").html(data);
  });
}


function showTableHistory(project_code){
  $.post("ajax/showHisUpload.php",{type_file:'pfit_t_person',project_code:project_code})
  .done(function( data ) {
      $("#show-history").html(data);
  });
}


function print(project_code,person_number){
  var pram = "?person_number="+person_number+"&project_code="+project_code;
  var url  = 'print/index.php'+pram;
  postURL_blank(url);
}

function dobFomat(d){
  return  d.substring(6, 8) +'/'+ d.substring(4, 6) +'/'+d.substring(4, 0);
}


function readSmartCard(){
  $.get("http://localhost:20000/Readcard/")
    .done(function( res ) {
      // console.log(res);
      var status    = res.Status;
      var message   = res.Message;
      var data      = res.data;
      if(status == "Y")
      {
        if(data != ""){
          var dob = dobFomat(data.dateOfBirth);
          $('#person_number').val(data.personalId);
          $('#person_name').val(data.firstName);
          $('#person_lname').val(data.lastName);
          $('#date_of_birth').val(dob);
          if(data.genderCode == "1"){
            $("#gender_m").prop("checked", true);
          }else{
            $("#gender_f").prop("checked", true);
          }
          $('#person_number').smkValidate();
          $('#person_name').smkValidate();
          $('#person_lname').smkValidate();
          $('#date_of_birth').smkValidate();
          $('#gender_m').smkValidate();
          $('#gender_f').smkValidate();
        }
      }else{
        $.smkAlert({text: message, type:'warning'});
      }
  })
  .fail(function( jqXHR, textStatus, errorThrown ) {
        $.smkAlert({text: "ไม่พบโปรแกรมอ่านบัตรสมาร์ทการ์ด", type:'warning'});
  });
}
