<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code        = $_POST['code'];
$projectCode = $_POST['projectCode'];
$projectType = $_POST['projectType'];



$statusType = $_POST['status'];
$disabled = '';
$readonly = '';
$visibility = '';

$project_code   = "";
$person_number  = "";
$person_gender  = "";
$person_name    = "";
$person_lname   = "";
$date_of_birth  = date('Y/m/d');
$wiegth         = "";
$height         = "";
$person_type    = "";
$status         = "";
$education      = "";
$stage          = "";
$grade          = "";
$room           = "";
$points_1       = "";
$points_2       = "";
$points_3       = "";

//
if(!empty($code)){
  $type = "EDIT";

  $sql = "SELECT project_code,person_number,person_gender,person_name,person_lname,points_1,points_2,points_3,
           ".getQueryDate('date_of_birth').",wiegth,height,person_type,education,stage,grade,room
          FROM pfit_t_person where project_code = '$projectCode' and person_number = '$code'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  // echo $sql;


  if($num > 0){
    $person_number  = $row[0]['person_number'];
    $person_gender  = $row[0]['person_gender'];
    $person_name    = $row[0]['person_name'];
    $person_lname   = $row[0]['person_lname'];
    $date_of_birth  = $row[0]['date_of_birth'];
    $wiegth         = $row[0]['wiegth'];
    $height         = $row[0]['height'];
    $person_type    = $row[0]['person_type'];
    $education      = $row[0]['education'];
    $stage          = $row[0]['stage'];
    $grade          = $row[0]['grade'];
    $room           = $row[0]['room'];
    $points_1       = $row[0]['points_1'];
    $points_2       = $row[0]['points_2'];
    $points_3       = $row[0]['points_3'];

    //echo "points_1 :".$points_1.","."points_2 :".$points_2.","."points_3 :".$points_3;

  }

  $readonly = 'readonly';
  if($statusType == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

  }
}else{
  $type = "ADD";
}

if($person_type == "" && $projectType != "4"){
  $person_type = "1";
}else if($person_type == ""){
  $person_type = "2";
}
?>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-success" onclick="readSmartCard()" style="width:160px;">อ่านบัตรสมาร์ทการ์ด</button>
        </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>เลขประจำตัว/เบอร์โทรศัพท์</label>
        <input type="hidden" value="<?= $type ?>" name="type" required>
        <input type="hidden" value="<?= $person_type ?>"  name="person_type">
        <input type="hidden" value="<?= $projectCode ?>" name="project_code" required>

        <input type="text" <?=$readonly?> id="person_number" name="person_number" value="<?= $person_number ?>" class="form-control"   placeholder="เลขประจำตัว" <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ</label>
        <input type="text" name="person_name" id="person_name" value="<?= $person_name; ?>" class="form-control" placeholder="ชื่อ" <?=$disabled ?> required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สกุล</label>
        <input type="text" name="person_lname" id="person_lname" value="<?= $person_lname; ?>" class="form-control" placeholder="สกุล" <?=$disabled ?> required >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เพศ</label>
        <div class="form-control" style="border-style: hidden;">
          <input <?= $disabled?> type="radio" name="person_gender" id="gender_m" class="minimal" value="M" <?= $person_gender == 'M'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
          <input <?= $disabled?> type="radio" name="person_gender" id="gender_f" class="minimal" value="F" <?= $person_gender == 'F'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input class="form-control datepicker" value="<?= DateThai($date_of_birth) ?>" id="date_of_birth" name="date_of_birth" type="text" <?=$disabled ?> data-provide="datepicker" data-date-language="th-th" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>น้ำหนัก</label>
        <input type="text" name="wiegth" value="<?= $wiegth; ?>"  class="form-control" data-smk-type="decimal" placeholder="น้ำหนัก"  <?=$disabled ?> >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ส่วนสูง</label>
        <input type="text" name="height" value="<?= $height; ?>" class="form-control" data-smk-type="decimal" placeholder="ส่วนสูง"  <?=$disabled ?> >
      </div>
    </div>
    <div id="typeStudent" style="<?= $person_type == '1' ? '':'display:none;'?>">
      <div class="col-md-5">
        <div class="form-group">
          <label>ชื่อสถานศึกษา</label>
          <input type="text" name="education" value="<?= $education ?>" class="form-control" placeholder="ชื่อสถานศึกษา"  <?=$disabled ?> >
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>ระดับชั้น</label>
          <select name="stage" class="form-control select2" style="width: 100%;" <?=$disabled; ?>>
            <option value=""></option>
            <option value="2" <?= ($stage == '2' ? 'selected="selected"':'') ?>>ประถมศึกษา</option>
            <option value="1" <?= ($stage == '1' ? 'selected="selected"':'') ?>>มัธยมศึกษา</option>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>ชั้นปี</label>
          <input type="text" name="grade" value="<?=$grade ?>" class="form-control" placeholder=""  <?=$disabled ?> >
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>ห้องเรียน</label>
          <input type="text" name="room" value="<?=$room ?>" class="form-control" placeholder=""  <?=$disabled ?> >
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
  <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
</div>
<script>
$(function () {
  $('.datepicker').datepicker();
  $(".datepicker").change(function(){
    $(this).datepicker('hide');
  });
})
</script>
