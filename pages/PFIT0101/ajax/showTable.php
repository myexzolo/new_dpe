<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code = $_POST['code'];

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>เลขประจำตัว</th>
      <th>ชื่อ</th>
      <th>นามสกุล</th>
      <th>วันเดือนปีเกิด</th>
      <th>น้ำหนัก</th>
      <th>ส่วนสูง</th>
      <th>เพศ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
        <th style="width:70px;">พิมพ์</th>
      <?php
      }
      ?>
      <th style="width:70px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql  = "SELECT project_code,person_number,person_gender,person_name,person_lname,
             ".getDateOracle('date_of_birth').",wiegth,height,person_type
             FROM pfit_t_person WHERE project_code = '$code' ORDER BY date_create desc";

    //echo $sql;

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0){
    foreach ($rows as $key => $value)
    {
        $gender = $value['person_gender']=="M"?"ชาย":"หญิง";
        $person_type = $value['person_type'];
    ?>
    <tr class="text-center">
      <td><?=$value['person_number'];?></td>
      <td><?=$value['person_name'];?></td>
      <td><?=$value['person_lname'];?></td>
      <td ><?= DateThai($value['date_of_birth']); ?></td>
      <td><?=$value['wiegth'];?></td>
      <td><?=$value['height'];?></td>
      <td><?=$gender ;?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_print'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-print" onclick="print('<?=$code ?>','<?=$value['person_number']?>','<?=$person_type ?>')"></i></a>
      </td>
      <?php
      }
      ?>
      <td><a class="btn_point text-green"><i class="fa fa-search" onclick="formShow('<?=$code ?>','VIEW','<?=$value['person_number']?>','<?=$person_type ?>')"></i></a></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="formShow('<?=$code ?>','EDIT','<?=$value['person_number']?>','<?=$person_type ?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['person_number']?>','<?=$value['person_name']." ".$value['person_lname'] ?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
