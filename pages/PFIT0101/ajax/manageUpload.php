<?php
require_once '../../../Classes/PHPExcel.php';

include '../../../Classes/PHPExcel/IOFactory.php';
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

set_time_limit(1000);

$person_type    = $_POST['person_type'];
$remark         = $_POST['remark'];
$project_code   = $_POST['projectCode'];
$project_name   = $_POST['projectName'];
//$end_date       = isset($_POST['end_date'])?$_POST['end_date']:date("Y-m-d");

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

$datenow = queryDateTimeOracle(date("Y/m/d H:i:s"));

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;

$errPID = "";

// echo $person_type;

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try {
     $sql   = "SELECT TO_CHAR( end_date, 'YYYY-MM-DD') as end_date FROM pfit_t_project WHERE project_code = '$project_code'";
     $query = DbQuery($sql,null);
     $json  = json_decode($query, true);
     $row   = $json['data'];

     $end_date = $row[0]['end_date'];

     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
     $objReader->setReadDataOnly(true);
     $objPHPExcel = $objReader->load($inputFileName);

     $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
     $highestRow = $objWorksheet->getHighestRow();
     $highestColumn = $objWorksheet->getHighestColumn();

     $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
     $headingsArray = $headingsArray[1];

     $headerName = $headingsArray['A'];

     if(trim($headerName) == "ระบุชื่อสถานศึกษา"){
       $headerName = $project_name;
     }else if (trim($headerName) == "ระบุชื่อหน่วยงาน"){
       $headerName = $project_name;
     }

     if($person_type == '1')
     {
       $headingsArray = array("A" => "NO",
                              "B" => "person_number",
                              "C" => "person_gender",
                              "D" => "person_name",
                              "E" => "person_lname",
                              "F" => "date_of_birth",
                              "G" => "wiegth",
                              "H" => "height",
                              "I" => "stage",
                              "J" => "grade",
                              "K" => "room",
                              "L" => "TST16",
                              "M" => "TST14",
                              "N" => "TST02",
                              "O" => "TST17");
     }
     else if($person_type == '2_0')
     {
       $headingsArray = array("A" => "NO",
                              "B" => "person_number",
                              "C" => "person_gender",
                              "D" => "person_name",
                              "E" => "person_lname",
                              "F" => "date_of_birth",
                              "G" => "wiegth",
                              "H" => "height",
                              "I" => "TST16",
                              "J" => "TST14",
                              "K" => "TST02",
                              "L" => "TST17");
     }
     else if($person_type == '2_1')
     {
       $headingsArray = array("A" => "NO",
                              "B" => "person_gender",
                              "C" => "person_name",
                              "D" => "person_lname",
                              "E" => "date_of_birth",
                              "F" => "wiegth",
                              "G" => "height",
                              "H" => "ABP1",
                              "I" => "ABP2",
                              "J" => "TST04",
                              "K" => "TST01",
                              "L" => "TST02",
                              "M" => "TST19",
                              "N" => "TST17");
     }
     else if($person_type == '2_2')
     {
       $headingsArray = array("A" => "NO",
                              "B" => "person_gender",
                              "C" => "person_name",
                              "D" => "person_lname",
                              "E" => "date_of_birth",
                              "F" => "wiegth",
                              "G" => "height",
                              "H" => "ABP1",
                              "I" => "ABP2",
                              "J" => "TST04",
                              "K" => "TST20",
                              "L" => "TST15",
                              "M" => "TST21",
                              "N" => "TST22",
                              "O" => "TST23");
     }

     $r = -1;

     $namedDataArray = array();
     for ($row = 3; $row <= $highestRow; ++$row) {
         // echo  $highestColumn.$row."<br>";
         if($person_type == '2_1' || $person_type == '2_2')
         {
           $dataE = $objWorksheet->getCell('E'.$row)->getValue();
           //echo  $dataE."<br>";
           if(strlen($dataE) > 10){
              $objWorksheet->setCellValue('E'.$row, '-');
              $dataE = $objWorksheet->getCell('E'.$row)->getValue();
              //echo  "Edit >>>".$dataE."<br>";
           }
         }
         else if ($person_type == '1' || $person_type == '2_0')
         {
           $dataE = $objWorksheet->getCell('F'.$row)->getValue();
           //echo  $dataE."<br>";
           if(strlen($dataE) > 10){
              $objWorksheet->setCellValue('F'.$row, '-');
              $dataE = $objWorksheet->getCell('F'.$row)->getValue();
              //echo  "Edit >>>".$dataE."<br>";
           }
         }

         $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);

         if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
             ++$r;
             foreach($headingsArray as $columnKey => $columnHeading) {
                 $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
             }
         }
     }
     $i = 0;

       foreach ($namedDataArray as $result) {
          try
          {
            $i++;
            $total++;

            if($person_type == '1')
            {
                 $person_number  = trim(@$result["person_number"]);
                 $person_gender  = trim(@$result["person_gender"]);
                 $person_name    = @$result["person_name"];
                 $person_lname   = @$result["person_lname"];
                 $dob            = @$result["date_of_birth"];

                 if($dob == "-")
                 {

                   $ernum = ($i+2);
                   $errPID .= "errRow:$ernum Col:F$ernum, ";
                   $fail++;
                   continue;
                 }

                 $dateOfBirth    = formatDateThtoEn($dob);
                 //echo "date_of_birth >".$dob.",".$dateOfBirth."<br>";
                 $date_of_birth  = queryDate($dateOfBirth);
                 $wiegth         = trim(@$result["wiegth"]);
                 $height         = trim(@$result["height"]);
                 $stage          = trim(@$result["stage"]);
                 $grade          = trim(@$result["grade"]);
                 $room           = trim(@$result["room"]);
                 $TST16          = trim(@$result["TST16"]);
                 $TST14          = trim(@$result["TST14"]);
                 $TST02          = trim(@$result["TST02"]);
                 $TST17          = trim(@$result["TST17"]);

                 $ArrTest        = array("BMI"=>$wiegth.'|'.$height,"TST16"=>$TST16,"TST14"=>$TST14,"TST02"=>$TST02,"TST17"=>$TST17);
            }
            else if($person_type == '2_0')
            {
                $stage          = "";
                $grade          = "";
                $room           = "";

                $person_number  = trim(@$result["person_number"]);
                $person_gender  = trim(@$result["person_gender"]);
                $person_name    = @$result["person_name"];
                $person_lname   = @$result["person_lname"];
                $dob            = @$result["date_of_birth"];

                if($dob == "-")
                {
                  $ernum = ($i+2);
                  $errPID .= "errRow:$ernum Col:F$ernum, ";
                  $fail++;
                  continue;
                }

                // if(is_numeric($dob))
                // {
                //     $dob = PHPExcel_Style_NumberFormat::toFormattedString($dob,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                //     $dob = formatDateThV2($dob);
                // }
                //echo "date_of_birth >".$dob."<br>";

                $dateOfBirth    = formatDateThtoEn($dob);
                $date_of_birth  = queryDate($dateOfBirth);

                $wiegth         = trim(@$result["wiegth"]);
                $height         = trim(@$result["height"]);
                $TST16          = trim(@$result["TST16"]);
                $TST14          = trim(@$result["TST14"]);
                $TST02          = trim(@$result["TST02"]);
                $TST17          = trim(@$result["TST17"]);

                $ArrTest        = array("BMI"=>$wiegth.'|'.$height,"TST16"=>$TST16,"TST14"=>$TST14,"TST02"=>$TST02,"TST17"=>$TST17);
            }
            else if($person_type == '2_1')
            {
                $stage          = "";
                $grade          = "";
                $room           = "";

                // $person_number  = "";
                $NO             = trim(@$result["NO"]);
                $person_gender  = trim(@$result["person_gender"]);
                $person_name    = @$result["person_name"];
                $person_lname   = @$result["person_lname"];
                $dob            = @$result["date_of_birth"];
                if($dob == "-")
                {
                  $ernum = ($i+2);
                  $errPID .= "errRow:$ernum Col:E$ernum, ";
                  $fail++;
                  continue;
                }

                $person_number  = $project_code."_".$NO;

                $dateOfBirth    = formatDateThtoEn($dob);
                $date_of_birth  = queryDate($dateOfBirth);

                $wiegth         = trim(@$result["wiegth"]);
                $height         = trim(@$result["height"]);
                $ABP1           = trim(@$result["ABP1"]);
                $ABP2           = trim(@$result["ABP2"]);
                $TST04          = trim(@$result["TST04"]);
                $TST01          = trim(@$result["TST01"]);
                $TST02          = trim(@$result["TST02"]);
                $TST19          = trim(@$result["TST19"]);
                $TST17          = trim(@$result["TST17"]);

                $ArrTest        = array("BMI"=>$wiegth.'|'.$height,"ABP"=>$ABP1.'|'.$ABP2,"TST04"=>$TST04,"TST01"=>$TST01,"TST02"=>$TST02,"TST19"=>$TST19,"TST17"=>$TST17);
            }
            else if($person_type == '2_2')
            {
                $stage          = "";
                $grade          = "";
                $room           = "";


                $NO             = trim(@$result["NO"]);
                $person_gender  = trim(@$result["person_gender"]);
                $person_name    = @$result["person_name"];
                $person_lname   = @$result["person_lname"];
                $dob            = @$result["date_of_birth"];

                if($dob == "-")
                {
                  $ernum = ($i+2);
                  $errPID .= "errRow:$ernum Col:E$ernum, ";
                  $fail++;
                  continue;
                }

                $dateOfBirth    = formatDateThtoEn($dob);
                //echo "date_of_birth >".$dob.",".$dateOfBirth."<br>";
                $date_of_birth  = queryDate($dateOfBirth);

                // echo "date_of_birth >".$dob.",".$dateOfBirth.",".$date_of_birth."<br>";

                $person_number  = $project_code."_".$NO;


                $wiegth         = trim(@$result["wiegth"]);
                $height         = trim(@$result["height"]);
                $ABP1           = trim(@$result["ABP1"]);
                $ABP2           = trim(@$result["ABP2"]);
                $TST04          = trim(@$result["TST04"]);
                $TST20          = trim(@$result["TST20"]);
                $TST15          = trim(@$result["TST15"]);
                $TST21          = trim(@$result["TST21"]);
                $TST22          = trim(@$result["TST22"]);
                $TST23          = trim(@$result["TST23"]);

                $ArrTest        = array("BMI"=>$wiegth.'|'.$height,"ABP"=>$ABP1.'|'.$ABP2,"TST04"=>$TST04,"TST20"=>$TST20,"TST15"=>$TST15,"TST21"=>$TST21,"TST22"=>$TST22,"TST23"=>$TST23);
            }else
            {
              $errPID .= "errRow:$i, ";
              $fail++;
              continue;
            }

            if($person_number == ''){

              $sql = "SELECT * FROM pfit_t_person WHERE person_number LIKE '$project_code%' AND project_code = '$project_code' ORDER BY person_number DESC";
              $query = DbQuery($sql,null);
              $json  = json_decode($query, true);
              $num   = $json['dataCount'];
              $row   = $json['data'];

              $code           = $project_code;
              if($num > 0){
                $personNumber  = $row[0]['person_number'];
                $lastNum = substr($personNumber,strlen($code));
                $lastNum = $lastNum + 1;
                $person_number = $code.sprintf("%04d", $lastNum);
              }else{
                $person_number = $code.sprintf("%04d", 1);
              }
            }

            if($wiegth == ""){
              $wiegth = 0;
            }

            if($height == ""){
              $height = 0;
            }

            if($person_gender == "ชาย"){
              $person_gender = "M";
            }
            else if($person_gender == "ด.ช.")
            {
              $person_gender = "M";
            }
            else if($person_gender == "เด็กชาย")
            {
              $person_gender = "M";
            }
            else if($person_gender == "นาย")
            {
              $person_gender = "M";
            }
            else if($person_gender == "M")
            {
              $person_gender = "M";
            }
            else if($person_gender == "หญิง")
            {
              $person_gender = "F";
            }
            else if($person_gender == "ด.ญ.")
            {
              $person_gender = "F";
            }
            else if($person_gender == "เด็กหญิง")
            {
              $person_gender = "F";
            }
            else if($person_gender == "นาง")
            {
              $person_gender = "F";
            }
            else if($person_gender == "น.ส.")
            {
              $person_gender = "F";
            }
            else if($person_gender == "นางสาว")
            {
              $person_gender = "F";
            }
            else if($person_gender == "F")
            {
              $person_gender = "F";
            }
            else
            {
              $person_gender = "";
            }

            if($stage == "มัธยม"){
              $stage = '1';
            }else if($stage == "ประถม")
            {
              $stage = '2';
            }
            else
            {
              $stage = '0';
            }


            $personType = "";
            if($person_type == '1')
            {
              $personType = "1";
            }
            else if($person_type == '2_0')
            {
              $personType = "2";
            }
            else if($person_type == '2_1')
            {
              $personType = "2";
            }
            else if($person_type == '2_2')
            {
              $personType = "2";
            }

            $sql = "SELECT * FROM pfit_t_person WHERE person_number = '$person_number' and project_code = '$project_code'";
            $json = DbQuery($sql,null);
            //print_r (">>>>>".$json."\n");
            $obj = json_decode($json, true);
            $dataCount = $obj['dataCount'];
            $strSQL = "";
            if($dataCount > 0){
              $strSQL = "UPDATE pfit_t_person SET
                          person_gender = '$person_gender',
                          person_name   = '$person_name',
                          person_lname  = '$person_lname',
                          date_of_birth = $date_of_birth,
                          wiegth        = '$wiegth',
                          height        = '$height',
                          stage         = '$stage',
                          grade         = '$grade',
                          education     = '$headerName',
                          person_type   = '$personType',
                          room          = '$room'
                          WHERE project_code = '$project_code' and person_number = '$person_number'";
            }else{
              $strSQL = "INSERT INTO pfit_t_person
                        (project_code,person_number,person_gender,person_name,person_lname,date_of_birth,wiegth,height,person_type,stage,grade,education,status_test,room)
                        VALUES
                        ('$project_code','$person_number','$person_gender','$person_name','$person_lname',$date_of_birth,'$wiegth','$height','$personType','$stage','$grade','$headerName',0,'$room')";

              // echo $strSQL."<br>";
            }


            $js      = DbQuery($strSQL,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];

            $errorInfo  = $row['errorInfo'][0];
            //print_r($row['errorInfo']);
            if($errorInfo != "00000"){
              //echo "pfit_t_person".$strSQL."<br>";
              $errPID .= $person_number.", ";
              $fail++;
              continue;
            }

            //$date_2 = date("Y-m-d");
            //$yearBirth = dateDifference($dateOfBirth , $date_2 , '%y');
            $yearBirth = yearBirth2($dateOfBirth,$end_date);

            if($yearBirth == 0){
              //echo "pfit_t_person".$strSQL."<br>";
              $errPID .= $person_number.", ";
              $fail++;
              continue;
            }

            foreach($ArrTest as $value =>$result)
            {
              if($result != "" && $result != "|"){
                $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$value'";
                $query  = DbQuery($sql,null);
                $json   = json_decode($query, true);
                $row    = $json['data'];

                //echo $sql."<br>";
                $test_calculator = $row[0]['test_calculator'];

                $res = getResultCalculator($test_calculator,$result,$wiegth,$height,$yearBirth,$person_gender,$project_code,$person_number);

                $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$value'
                              AND age = '$yearBirth' AND gender = '$person_gender'
                              AND min <= '$res' ORDER BY min DESC";
                $querys     = DbQuery($sqls,null);
                $json  = json_decode($querys, true);
                $num   = $json['dataCount'];


                $test_criteria_code = '';
                if($num > 0)
                {
                  $row   = $json['data'];
                  $test_criteria_code = $row[0]['test_criteria_code'];
                }

                //$datenow = queryDate(date("Y/m/d"));
                $sqlr = "SELECT * FROM pfit_t_result WHERE  project_code = '$project_code'
                         and person_number = '$person_number' and test_code = '$value' ";
                $queryr = DbQuery($sqlr,null);
                $jsonr  = json_decode($queryr, true);
                $numr   = $jsonr['dataCount'];
                //echo $sqlr."  ".$numr."<br>";



                if($numr > 0){
                          //$result_id = $_POST['result_id'];

                  $sqlr = "UPDATE pfit_t_result SET
                          result = '$result',
                          test_code = '$value',
                          test_criteria_code = '$test_criteria_code',
                          date_create = $datenow,
                          result_cal = '$res'
                          WHERE  project_code = '$project_code' and person_number = '$person_number' and test_code = '$value' ";
                }else{
                  $sqlr = "INSERT INTO pfit_t_result
                          (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
                          VALUES('$project_code','$person_number','$result','$value','$test_criteria_code',$datenow,'$res')";
                }

                //echo $sqlr."<br>";

                $js      = DbQuery($sqlr,null);
                $row     = json_decode($js, true);
                $status  = $row['status'];

                $errorInfo  = $row['errorInfo'][0];

                if($errorInfo != "00000"){
                  //echo "pfit_t_result ".$value." ".$sqlr."<br>";
                  $errPID .= $person_number.", ";
                  $fail++;
                  //echo $sqlr."<br>";
                  continue;
                }
              }
            }

            $sql = "UPDATE pfit_t_person SET status_test = '2' WHERE person_number = '$person_number' and project_code = '$project_code' ";

            $js      = DbQuery($sql,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];

            $errorInfo  = $row['errorInfo'][0];
            if($errorInfo != "00000"){
              //echo "pfit_t_person update ".$sql."<br>";
              $errPID .= $person_number.", ";
              $fail++;
              continue;
            }
            $success++;
          }catch (Exception $ex) {
            $errPID .= $person_number.", ";
            //echo "pfit_t_person update ".$sql."<br>";
            $fail++;
            continue;
          }
        }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง '.$errPID, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }

  $str = "INSERT INTO pfit_t_history_upload
          (HISTORY_UPLOAD_ID,file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code)
          VALUES
          (T_HISTORY_UPLOAD.NEXTVAL,'$inputFileName','S','$total','$success','$fail','$errPID','pfit_t_person','$project_code')";

   DbQuery($str,null);

   $query    = DbQuery($sql,null);
   $json     = json_decode($query, true);
   $status   = $json['status'];

   // if($status == "success"){
   //   echo  "บันทึกสมบูรณ์ :".$total.", success :".$success." ,fail :".$fail;
   // }else{
   //  echo  "บันทึกไม่สมบูรณ์";
   // }

   if($status == "success"){

     $sql   = "UPDATE pfit_t_project SET DATE_CREATE = $datenow WHERE project_code = '$project_code'";
     //echo $sql;
     $query    = DbQuery($sql,null);

     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกสมบูรณ์'.$errPID, 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
   }else{
     //echo "fail";
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'บันทึกไม่สมบูรณ์ '.$errPID, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  //echo "ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง";
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง '.$errPID, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
}

?>
