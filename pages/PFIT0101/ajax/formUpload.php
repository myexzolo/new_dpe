<?php
$projectCode = $_POST['projectCode'];
$projectType = $_POST['projectType'];
$projectName = $_POST['projectName'];

if($projectType != "4"){
  $person_type = "1";
}else{
  $person_type = "2";
}

$disabled = '';
$readonly = '';
$visibility = '';
?>
<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required class="custom-file-input form-control">
                      <input type="hidden" name="projectCode" id="projectCode" value="<?= $projectCode ?>" required>
                      <input type="hidden" name="projectName" value="<?= $projectName ?>" >
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">ประเภทผู้ทดสอบ</label>
                    <div class="col-sm-8">
                      <select name="person_type" id="person_type" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required >
                        <option value="" selected></option>
                        <option value="1" <?= ($person_type == '1' ? 'selected="selected"':'') ?>>นักเรียน (7 - 18 ปี)</option>
                        <option value="2_0" <?= ($person_type == '2' ? 'selected="selected"':'') ?>>ประชาชนทั่วไป (7 - 18 ปี)</option>
                        <option value="2_1">ประชาชนทั่วไป (19 - 59 ปี)</option>
                        <option value="2_2">ประชาชนทั่วไป (60 ปีขึ้นไป)</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">เพิ่มเติม</label>
                    <div class="col-sm-8">
                      <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div id="show-history"><div>
    </div>
  </div>
</div>

<script>
$(function () {
  $('.select2').select2();
  showTableHistory('<?=$projectCode ?>');
})
</script>
