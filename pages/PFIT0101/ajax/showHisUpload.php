<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$project_code = $_POST['project_code'];
$type_file    = !empty($_GET['type_file'])?"{$_GET['type_file']}":"pfit_t_person";

?>

<div class="box-body">
  <table class="table table-bordered table-striped table-hover" id="tableUpload">
    <thead>
        <tr>
          <th>ลำดับ</th>
          <th>ชื่อไฟล์</th>
          <th>วันที่นำเข้า</th>
          <th>สถานะ</th>
          <th>ทั้งหมด</th>
          <th>สำเร็จ</th>
          <th>ล้มเหลว</th>
          <th>เลขที่ล้มเหลว</th>
        </tr>
    </thead>
    <tbody>
      <?php
        $date_upload =  getQueryDateOracle('date_upload','YYYY-MM-DD HH24:MI:SS');

        $sql  = "SELECT history_upload_id,file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code,$date_upload
        FROM pfit_t_history_upload WHERE project_code = '$project_code' and type_file = '$type_file' order by history_upload_id desc";


        $query     = DbQuery($sql,null);
        $json       = json_decode($query, true);
        $errorInfo  = $json['errorInfo'];
        $dataCount  = $json['dataCount'];
        $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $path_file = $value['path_file'];
          if($path_file != ""){
            $path_file = substr($path_file,0,strlen($path_file) - 2);
          }
        ?>
        <tr class="text-center">
          <td><?=$key+1;?></td>
          <td><?=$value['file_name'];?></td>
          <td><?=DateTimeThai($value['date_upload']);?></td>
          <td><?=$value['status_upload']=='F'?"ผิดพลาด":"สมบูรณ์";?></td>
          <td><?=$value['total_record'];?></td>
          <td><?=$value['success_record'];?></td>
          <td><?=$value['fail_record'];?></td>
          <td><div style="word-wrap: break-word;width: 150px;" ><?=$path_file ?></div></td>
        </tr>
    <?php }} ?>
    </tbody>
  </table>
</div>
<script>
  $(function () {
    $('#tableUpload').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
