<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
  </head>
  <body>
<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  require_once '../../../mpdf2/vendor/autoload.php';


  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();

  $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
  //echo $PNG_TEMP_DIR;
  //html PNG location prefix
  $PNG_WEB_DIR = 'temp/';
  include "../../../qrcode/qrlib.php";

  if (!file_exists($PNG_TEMP_DIR)){
    mkdir($PNG_TEMP_DIR);
  }

  $project_code   = $_POST['project_code'];
  $person_number  = $_POST['person_number'];

  $CorrectionLevel = 'L';
  $matrixPointSize = 4;

  $filename = $PNG_TEMP_DIR.$person_number.'.png';
  $textQr = "$person_number|$project_code";
  QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


  $sql = "SELECT p.project_name,ps.person_number,ps.project_code,ps.person_name,ps.person_lname ,
         ".getQueryDate('date_of_birth').",ps.wiegth,ps.height,ps.person_gender
          FROM pfit_t_person ps, pfit_t_project p
          WHERE ps.project_code = '$project_code' and p.project_code = ps.project_code and ps.person_number = '$person_number'";

  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $row   = $json['data'];


  $project_name   = $row[0]['project_name'];
  $person_number  = $row[0]['person_number'];
  $projectCode    = $row[0]['project_code'];
  $person_name    = $row[0]['person_name'];
  $person_lname   = $row[0]['person_lname'];
  $date_of_birth  = $row[0]['date_of_birth'];
  $wiegth         = $row[0]['wiegth'];
  $height         = $row[0]['height'];
  $person_gender  = $row[0]['person_gender']=='M'?"ชาย":"หญิง";

  $age            =  yearBirth($date_of_birth);

  $sqlp = "SELECT * FROM pfit_t_project_test pt
          INNER JOIN pfit_t_test t ON pt.test_code = t.test_code
          WHERE pt.project_code = '$project_code' and $age >= t.test_age_min and $age <= t.test_age_max
          ORDER BY pt.test_seq ASC";
  $queryp = DbQuery($sqlp,null);
  $jsonp  = json_decode($queryp, true);
  $rowp   = $jsonp['data'];
  ?>
  <div style="margin:20px;">
  <br>
  <div align="center" style="font-size:18pt;width:100%"><b>แบบบันทึกผลการทดสอบ <?php echo $project_name; ?></b></div>
  <br>
  <table style="font-size:14pt;width:100%">
  		<tr>
  			<td colspan="1" rowspan="3" style="position: relative;width:120px;">
          <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" />
  			<td colspan="4" rowspan="1" style="text-align:right">วันที่ทำการทดสอบ : ____/____/________</td>
  		</tr>
  		<tr valign="middle">
  			<td>เลขประจำตัว : <?=$person_number; ?></td>
  			<td colspan="2" rowspan="1" style="width:100px;">ชื่อ : <?= $person_name; ?></td>
  			<td style="width:200px;">สกุล : <?= $person_lname; ?></td>
  		</tr>
  		<tr>
  			<td>วันเดือนปี เกิด : <?= DateThai($date_of_birth) ?></td>
  			<td colspan="2">เพศ : <?= $person_gender ?></td>
  			<td>อายุ : <?= $age ?> ปี</td>
  		</tr>
  </table>
  <br>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
  <thead>
    <tr>
      <th class="text-center thStyle">ลำดับ</th>
      <th class="text-center thStyle">รายการ</th>
      <th class="text-center thStyle">หน่วย</th>
      <th class="text-center thStyle" >ผลการวัด</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $numRow = 0;
    foreach ($rowp as $key => $value) {
    if( $value['test_code'] == "BMI"){
      $numRow++;
    ?>
    <tr>
      <td width="10%" class="text-center content"><?= $numRow; ?></td>
      <td width="50%" class="text-left content"><?= "น้ำหนัก" ?></td>
      <td width="20%" class="text-center content">กิโลกรัม</h4></td>
      <td class="text-center content"><?= $wiegth; ?></td>
    </tr>
    <tr>
      <td class="text-center content"><?= $numRow+1; ?></td>
      <td class="text-left content"><?= "ส่วนสูง" ?></td>
      <td class="text-center content">เซนติเมตร</td>
      <td class="text-center content"><?= $height; ?></td>
    </tr>
    <?php
    $numRow++;
  }else{
    $numRow++;
  ?>
  <tr>
    <td width="10%" class="text-center content"><?= $numRow; ?></td>
    <td width="50%" class="text-left content"><?= $value['test_name']; ?></td>
    <td width="20%" class="text-center content"><?= $value['test_unit']; ?></td>
    <td class="text-center content"></td>
  </tr>
  <?php }} ?>
</tbody>
</table>
</div>
</body>
</html>

<?php
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/PFIT0101.css');
  $mpdf->SetTitle('รายชื่อผู้ทดสอบ');
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

  // $mpdf->Output('IVRENT201711000059.pdf', \Mpdf\Output\Destination::DOWNLOAD);
  $mpdf->Output($namepdf.'.pdf', \Mpdf\Output\Destination::INLINE);
?>
