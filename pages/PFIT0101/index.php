<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - จัดเก็บรายชื่อผู้ทดสอบ</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/PFIT0101.css">
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>จัดเก็บรายชื่อผู้ทดสอบ  <small>PFIT0101</small></h1>

          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">รายชื่อผู้ทดสอบ</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php");
            $project_code   = isset($_POST['project_code'])?$_POST['project_code']:"";
            $project_name   = isset($_POST['project_name'])?str_replace('"', "&quot;", $_POST['project_name']):"";
            $project_type   = isset($_POST['project_type'])?$_POST['project_type']:"";

            $prameter = "?project_code=$project_code&project_name=$project_name&project_type=$project_type";

            if($project_code == ""){
              exit("<script>window.location='../home';</script>");
            }
          ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-md-6">
                      <h3 class="box-title">กิจกรรม : <?= $project_name?><input type="hidden" id="code" value="<?=$project_code ?>"></h3>
                    </div>
                    <div class="box-tools col-md-6 btn-header">
                      <?php
                        $pram = "?project_name=$project_name&project_code=$project_code";
                        $url = 'qr/index.php'.$pram;
                      ?>
                      <?php if($_SESSION['ROLE_USER']['is_print'])
                      {
                      ?>
                        <button class="btn bg-navy" onclick="postURL_blank('<?=$url?>')"><i class="fa  fa-print"></i> พิมพ์ผู้ทดสอบทั้งหมด</button>
                      <?php
                      }
                      ?>
                      <button class="btn bg-navy web" onclick="exportExcel('<?=$project_type?>')" ><i class="fa fa-file-excel-o"></i> Excel Template</button>

                      <?php if($_SESSION['ROLE_USER']['is_insert'])
                      {
                      ?>
                        <button class="btn btn-primary" onclick="openModal('<?=$project_code?>','<?=$project_name?>','<?=$project_type?>')" style="width:80px;">เพิ่ม</button>
                      <?php
                      }
                      ?>
                      <?php if($_SESSION['ROLE_USER']['is_import'])
                      {
                      ?>
                        <button class="btn btn-success web" onclick="upload('<?=$project_code?>','<?=$project_name?>','<?=$project_type?>')" style="width:80px;">นำเข้า</button>
                      <?php
                      }
                      ?>
                    </div>
                </div>
              </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div id="showTable"></div>
                </div>
                <div class="box-footer with-border">
                  <div class="pull-right">
                    <button style="width: 100px;text-align: center;" class="btn btn-success btn-flat" onclick="gotoPage('../home')" style="width:80px;">ย้อนกลับ</button>
                    <button type="button" style="width: 150px;text-align: center;" class="btn btn-social bg-gray-active btn-flat" onclick="postURL('<?= "../PFIT0102/index.php".$prameter ?>')">
                      <i class="fa fa fa-floppy-o" style="font-size:18px;"></i> จัดเก็บผล
                    </button>
                    <button type="button" style="width: 130px;text-align: center;" class="btn btn-social bg-light-blue btn-flat" onclick="postURL('<?= "../PFIT0103/index.php".$prameter ?>')">
                      <i class="fa fa-file-text-o" style="font-size:18px;"></i> รายงาน
                    </button>
                  </div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">รายชื่อผู้ทดสอบ</h4>
                    </div>
                    <form id="formAdd" novalidate enctype="multipart/form-data">
                    <!-- <form novalidate enctype="multipart/form-data" method="post" action="ajax/AED.php"> -->
                      <div id="formShow"></div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel2">นำเข้าข้อมูลผู้ทดสอบ</h4>
                    </div>
                    <form id="formDataUpload" novalidate enctype="multipart/form-data" >
                    <!-- <form novalidate enctype="multipart/form-data" method="post" action="ajax/manageUpload.php"> -->
                      <div class="modal-body" id="formShow2">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="modal fade bs-example-modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Excel template</h4>
                    </div>
                      <div class="modal-body">
                        <table id="tableExcel" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                              <th data-align="center">ลำดับ</th>
                              <th data-align="center">รายการ Excel template</th>
                              <th data-align="center">DownLoad</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td style="text-align: center;">1</td>
                              <td>นักเรียน (7 - 18 ปี)</td>
                              <td style="text-align: center;">
                                <a onclick="loadExcel('../../template/person_template.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                          <tr>
                              <td align="center">2</td>
                              <td>ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)</td>
                              <td align="center">
                                  <a onclick="loadExcel('../../template/person_7_18.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                          <tr>
                              <td align="center">3</td>
                              <td>ประชาชนทั่วไป ช่วงอายุ (19 - 59 ปี)</td>
                              <td align="center">
                                  <a onclick="loadExcel('../../template/person_19_59.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                          <tr>
                              <td align="center">4</td>
                              <td>ประชาชนทั่วไป ช่วงอายุ (60 ปีขึ้นไป)</td>
                              <td align="center">
                                <a onclick="loadExcel('../../template/person_60.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                        </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="js/PFIT0101.js"></script>

  </body>
</html>
