<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
include('../../../inc/function/authen.php');


$path   = "";
$pathUpload = "../../../upload/doc/";
$id        = isset($_POST['id'])?$_POST['id']:"";
$temp_name = isset($_POST['temp_name'])?$_POST['temp_name']:"";
$is_active = isset($_POST['is_active'])?$_POST['is_active']:"";
$action    = isset($_POST['action'])?$_POST['action']:"";
$path      = "";

$dateNow   =  queryDateTimeOracle(date('Y/m/d'));

if($id == "")
{
  $sqlid  = "SELECT T_TEMP_SEQ.NEXTVAL AS next_id FROM DUAL";

  $query      = DbQuery($sqlid,null);
  $json       = json_decode($query, true);
  $id    = $json['data'][0]['next_id'];
}

if(isset($_FILES["path"]))
{
  $path = uploadFile($_FILES["path"],$pathUpload,'userManual_'.$id);
}


if($action == "ADD")
{
  $sql = "INSERT INTO t_template
         (temp_id,temp_name,is_active,date_create,date_update,temp_file)
         VALUES
         ($id,'$temp_name','$is_active',$dateNow,$dateNow,'$path')";
}
else if($action == "EDIT")
{
  $temp_file = "";
  if($path != ""){
    $temp_file = "temp_file = '$path',";
  }
  $sql = "UPDATE t_template SET
          temp_name       = '$temp_name',
          is_active       = '$is_active',
          $temp_file
          date_update     = $dateNow
          WHERE temp_id   = $id";
}

//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
//print_r($row);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
