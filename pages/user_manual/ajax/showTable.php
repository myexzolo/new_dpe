<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}
</style>
<table class="table table-bordered table-hover table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">No.</th>
      <th>รายการ</th>
      <th style="width:100px;">สถานะ</th>
      <th style="width:80px;">คู่มือ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">Edit</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql = "SELECT * FROM  t_template where is_active = 'Y' ORDER BY TEMP_ID";

    //echo $sql;
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $rows       = $json['data'];
    $dataCount  = $json['dataCount'];
    //print_r($json);
    for($i=0 ; $i < $dataCount ; $i++) {

    ?>
    <tr class="text-center">
      <td><?=$i+1 ?></td>
      <!-- <td align="left">คู่มือการใช้งานระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย</td> -->
      <td align="left"><?=$rows[$i]['temp_name'];?></td>
      <td><?=$rows[$i]['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <td align="center">
        <a class="btn_point" href="../../upload/doc/<?=$rows[$i]['temp_file'];?>" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$rows[$i]['temp_id'];?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
   'paging'      : true,
   'lengthMenu'  : [10,20,50,100],
   'lengthChange': true,
   'searching'   : true,
   'ordering'    : false,
   'info'        : true,
   'autoWidth'   : false
 })
})
</script>
