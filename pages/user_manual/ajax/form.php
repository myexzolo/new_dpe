<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = $_POST['id'];

$temp_name  = "";
$is_active  = "";

if($id != "")
{
  $sql = "SELECT * FROM t_template where temp_id  = '$id'";

  $query = DbQuery($sql,null);
  $row = json_decode($query, true);

  $num  = $row['dataCount'];
  $data = $row['data'];

  $temp_name  = $data[0]['temp_name'];;
  $is_active  = $data[0]['is_active'];;
}
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" id="id" name="id" value="<?=$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>รายการ</label>
        <input value="<?= $temp_name ?>" name="temp_name" type="text" class="form-control" placeholder="รายการ"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>คู่มือการใช้งาน (PDF ไฟล์)</label>
        <input name="path" type="file" class="form-control custom-file-input" accept="application/pdf" >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
