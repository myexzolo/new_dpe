<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:70px;">ลำดับ</th>
      <th style="width:100px;">รหัส</th>
      <th>ชื่อกิจกรรม</th>
      <th>สถานะ</th>
      <th style="width:70px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">แก้ไข</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql = "SELECT * FROM pfit_t_project where type_user = 'DPE'";

      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['project_code'];?></td>
      <td style="text-align:left;"><?=$value['project_name'];?></td>
      <td><?=$value['status']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <td><a class="btn_point text-green"><i class="fa fa-search" onclick="showForm('VIEW','<?=$value['project_code']?>')"></i></a></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['test_code']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
