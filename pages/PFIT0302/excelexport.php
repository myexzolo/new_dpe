<?php
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');

error_reporting(E_ALL);

require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("DPE")
							 ->setLastModifiedBy("DPE")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Project Detail file");

$dob 		= getDateOracle('dob');
$sqls   = "SELECT user_id,name,lname,id_card,$dob,gender,email,mobile,user_name
					 FROM pfit_t_user
					 ORDER BY user_id";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$row        = $json['data'];


$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");


$objPHPExcel->getActiveSheet()->setTitle("ผู้ใช้งานบุคคลทั่วไป");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ลำดับ');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ชื่อ');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'นามสกุล');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'เลขบัตรประชาชน');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'เพศ');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'วันเดือนปี เกิด');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Email');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'เบอร์โทรศัพท์');

$rw = 1;
for($i=0;$i <$dataCount ;$i++)
{
  $rw++;

  $name      		= isset($row[$i]['name'])?$row[$i]['name']:"";
  $lname     		= isset($row[$i]['lname'])?$row[$i]['lname']:"";
	$id_card      = isset($row[$i]['id_card'])?$row[$i]['id_card']:"";
  $dob  				= isset($row[$i]['dob'])?DateThai($row[$i]['dob']):"";
	$gender       = isset($row[$i]['gender'])?$row[$i]['gender']:"";
	$email       	= isset($row[$i]['email'])?$row[$i]['email']:"";
	$mobile       = isset($row[$i]['mobile'])?$row[$i]['mobile']:"";
	$user_name    = isset($row[$i]['user_name'])?$row[$i]['user_name']:"";
	$pasword      = isset($row[$i]['pasword'])?$row[$i]['pasword']:"";

	if($gender == "M")
	{
		$gender = "ชาย";
	}else if($gender == "F")
	{
		$gender = "หญิง";
	}

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$rw, $i+1);
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$rw, $name);
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$rw, $lname);
	$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rw, $id_card, PHPExcel_Cell_DataType::TYPE_STRING);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$rw, $gender);
  $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$rw, $dob, PHPExcel_Cell_DataType::TYPE_STRING);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$rw, $email);
  $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$rw, $mobile, PHPExcel_Cell_DataType::TYPE_STRING);
}


foreach(range('A','H') as $columnID) {
    $objPHPExcel->getActiveSheet(1)->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="user.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
