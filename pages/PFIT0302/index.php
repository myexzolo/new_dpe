<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>กรมพลศึกษา - จัดการผู้ใช้งาน (บุคคลทั่วไป)</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/PFIT03002.css">
    </head>
      <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>จัดการผู้ใช้งาน (บุคคลทั่วไป)</h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
              <li class="active">User</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">

                <div class="box box-primary">
                  <!-- /.box-header -->
                  <div class="box-header with-border">
                    <a class="btn btn-social btn-primary btnAdd pull-right" onclick="postURL_blank('excelexport.php')" style="margin-right:5px;text-align:center;width:200px;">
                      <i class="fa fa-file-excel-o"></i> รายชื่อผู้ใช้งาน Excel
                    </a>
                  </div>
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>
                <!-- /.box -->

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Management Users</h4>
                      </div>
                      <form id="formAddUsers" novalidate enctype="multipart/form-data">
                      <!-- <form novalidate enctype="multipart/form-data" action="ajax/AEDUsers.php" method="post"> -->
                        <div id="show-form"></div>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="modal fade" id="myModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalDetailLabel"></h4>
                      </div>
                        <div id="show-form-detail"></div>
                    </div>
                  </div>
                </div>
                <div class="modal fade" id="myModalReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Management Reset Password</h4>
                      </div>
                      <form id="formResetPassword" novalidate enctype="multipart/form-data">
                      <!-- <form novalidate enctype="multipart/form-data" action="ajax/AEDUsers.php" method="post"> -->
                        <div id="show-form-rw"></div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/PFIT03002.js"></script>
    </body>
  </html>
