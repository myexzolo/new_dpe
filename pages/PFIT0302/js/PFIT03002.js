showTable();

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",id=""){
  $.post("ajax/form.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function showFormDetail(id,name){
  $('#myModalDetailLabel').html(name);
  $.post("ajax/formDetail.php",{id:id})
    .done(function( data ) {
      $('#myModalDetail').modal({backdrop:'static'});
      $('#show-form-detail').html(data);
  });
}



function resetPass(id){
  $.post("ajax/formResetPass.php",{id:id})
    .done(function( data ) {
      $('#myModalReset').modal({backdrop:'static'});
      $('#show-form-rw').html(data);
  });
}



$('#formResetPassword').on('submit', function(event) {
    event.preventDefault();
    var action = $('#action').val();
    if ($('#formResetPassword').smkValidate()) {
        if( $.smkEqualPass('#pass1', '#pass2') ){
            $.ajax({
                url: 'ajax/resetPass.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              console.log(data);
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#fff',
                barColor: '#242d6d',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formResetPassword').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModalReset').modal('toggle');
              }, 1000);
            });
        }
    }
  });
