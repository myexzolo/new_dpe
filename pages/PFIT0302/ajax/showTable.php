<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>No.</th>
      <th>User Login</th>
      <th>ชื่อ</th>
      <th>วันเดือนปีเกิด</th>
      <th>เบอร์โทร</th>
      <th>Email</th>
      <th style="width:80px;">รายงาน</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <th style="width:80px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $dob = getDateOracle('dob');

       $sqls   = "SELECT user_id,name,lname,id_card,$dob,gender,email,mobile,user_name
                  FROM pfit_t_user
                  ORDER BY user_id";

      // echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0){
      foreach ($rows as $key => $value) {
          $fullname = $value['name']." ".$value['lname'];
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$value['user_name']?></td>
      <td align="left"><?=$fullname ?></td>
      <td align="left"><?=DateThai($value['dob'])?></td>
      <td align="left"><?=$value['mobile']?></td>
      <td align="left"><?=$value['email']?></td>
      <td><a class="btn_point text-green" onclick="showFormDetail('<?=$value['id_card']?>','<?=$fullname ?>')"><i class= "fa fa-clipboard"></a></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow "><i class="fa fa-key" onclick="resetPass('<?=$value['user_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['user_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['user_id']?>','<?=$fullname ?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
