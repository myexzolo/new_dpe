<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id = $_POST['id'];


$sql   = "SELECT * FROM pfit_t_user where user_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $name           = $row[0]['name'];
  $lname          = $row[0]['lname'];
  $user_name      = $row[0]['user_name'];
  $user_password  = $row[0]['password'];
  $email          = $row[0]['email'];

  $fname = $name." ".$lname;

?>
<input type="hidden" id="action" name="action" value="RESET">
<input type="hidden" name="user_id" value="<?=@$id?>">
<input type="hidden" name="name" value="<?=$name?>">
<input type="hidden" name="lname" value="<?=$lname?>">
<input type="hidden" name="email" value="<?=$email?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>User Login</label>
        <input value="<?=@$user_name?>" data-smk-msg="&nbsp;" name="user_name" type="text" class="form-control" placeholder="User Login" required readonly>
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?=@$fname?>" data-smk-msg="&nbsp;" type="text" class="form-control" placeholder="Name" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Reset Password</label>
        <input value="" data-smk-msg="&nbsp;" name="password" id="pass1" type="password" autocomplete="new-password"  class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" data-smk-msg="&nbsp;" id="pass2" type="password" autocomplete="new-password"  class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
