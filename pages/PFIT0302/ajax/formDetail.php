<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$id_card = $_POST['id'];
$projectCode = "M611101";

?>

<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-striped table-hover" id="tableDisplay2">
        <thead>
          <tr class="text-center">
            <th style="width:80px;">ลำดับ</th>
            <th>วันที่ทดสอบ</th>
            <th style="width:80px;">รายงาน</th>
          </tr>
        </thead>
        <tbody>
    <?php
    $date_create = getQueryDateOracle('date_create','YYYY-MM-DD HH24:MI:SS');

    $sql = "SELECT $date_create,time FROM pfit_t_result_history
            WHERE project_code = '$projectCode' and person_number = '$id_card'
            group by time, date_create
            order by date_create DESC";

    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $rows       = $json['data'];
    $count      = $json['dataCount'];

    //echo $sql;
    //if($request['member_id'] == 1){
      for ($i=0; $i < $count ; $i++) {
    ?>
    <tr>
      <td style="text-align:center"><?= $i+1 ?></td>
      <td style="text-align:center"><?= DateTimeThai($rows[$i]['date_create']); ?></td>
      <td style="text-align:center">
        <a class="btn_point text-green" target="_blank" href="<?= "../../result/".$id_card."_".$rows[$i]['time'].".pdf"; ?>">
          <i class= "fa fa-clipboard"></i>
        </a>
      </td>
    </tr>
    <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
</div>
<script>
$(function () {
  $('#tableDisplay2').DataTable({
    'paging'      : false,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : false,
    'info'        : false,
    'autoWidth'   : false
  })
})
</script>
