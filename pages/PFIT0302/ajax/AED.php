<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action           = isset($_POST['action'])?$_POST['action']:"";
$user_id          = isset($_POST['userId'])?$_POST['userId']:"";
$name             = isset($_POST['name'])?$_POST['name']:"";
$lname            = isset($_POST['lname'])?$_POST['lname']:"";
$id_card          = isset($_POST['idCard'])?$_POST['idCard']:"";
$dob              = isset($_POST['dateOfBirth'])?$_POST['dateOfBirth']:"";
$gender           = isset($_POST['sex'])?$_POST['sex']:"";
$email            = isset($_POST['email'])?$_POST['email']:"";
$mobile           = isset($_POST['mobile'])?$_POST['mobile']:"";
$user_name        = isset($_POST['username'])?$_POST['username']:"";
$password         = isset($_POST['password'])?$_POST['password']:"";

$type   = $action;

if($dob != "")
{
  $dob = queryDateOracle($dob);
}

$updateImg  = "";
$user_id_update = $_SESSION['member'][0]['user_login'];

$path = "../../../image/member/";
if(isset($_FILES["img"])){

  $user_img = resizeImageToUpload($_FILES["img"],'512','512',$path,$id_card);

  if($action == "EDIT" && $user_img != "")
  {
    $updateImg = "user_img = '$user_img',";
  }
}

if($action == "ADD")
{
  $sqlid  = "SELECT T_DPE_USER_SEQ.NEXTVAL AS next_id FROM DUAL";
  $query      = DbQuery($sqlid,null);
  $json       = json_decode($query, true);
  $user_id    = $json['data'][0]['next_id'];


  $sql = "INSERT INTO pfit_t_user
         (user_id,name,lname,id_card,dob,gender,email,
          mobile,user_name,password,img)
         VALUES
         ($user_id,'$name','$lname','$id_card',$dob,'$gender','$email',
          '$mobile','$user_name','$password','$user_img')";
}else if($type == "EDIT"){
  $sql = "UPDATE pfit_t_user SET
          name          = '$name',
          lname         = '$lname',
          dob           = '$dob',
          gender        = '$gender',
          $updateImg
          email         = '$email',
          mobile        = '$mobile'
          WHERE user_id   = $user_id";
}
//echo $sql;
$query  = DbQuery($sql,null);
$row    = json_decode($query, true);
$status = $row['status'];

if($status == "success")
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
