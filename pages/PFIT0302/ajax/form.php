<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];
$arr_role_list = array();

$btn = 'บันทึก';

$user_id      = "";

$name         = "";
$lname        = "";
$id_card      = "";
$bod          = "";
$gender       = "";
$email        = "";
$mobile       = "";
$user_name    = "";
$pasword      = "";
$img          = "";

$readonly = "";


$isActive = 'ใช้งาน';
$isInActive = 'ไม่ใช้งาน';


$colD = "col-md-5";

if($action == 'EDIT' ){

  $readonly = "readonly";
  $sqls  = "SELECT name,lname,id_card,TO_CHAR( dob, 'YYYY-MM-DD') as dob ,gender,
            email,mobile,user_name,password,img
            FROM pfit_t_user
            where user_id = '$id'";

  // echo $sqls;

  $query      = DbQuery($sqls,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];


  $name         = isset($row[0]['name'])?$row[0]['name']:"";
  $lname        = isset($row[0]['lname'])?$row[0]['lname']:"";
  $id_card      = isset($row[0]['id_card'])?$row[0]['id_card']:"";
  $bod          = isset($row[0]['dob'])?DateThai($row[0]['dob']):"";
  $gender       = isset($row[0]['gender'])?$row[0]['gender']:"";
  $email        = isset($row[0]['email'])?$row[0]['email']:"";
  $mobile       = isset($row[0]['mobile'])?$row[0]['mobile']:"";
  $user_name    = isset($row[0]['user_name'])?$row[0]['user_name']:"";
  $pasword      = isset($row[0]['pasword'])?$row[0]['pasword']:"";

  $user_img     = isset($row[0]['img'])?"../../image/member/".$row[0]['img']:"";

  //echo $bod;
}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" id="typeDevice" name="typeDevice" value="WEB">
<input type="hidden" name="user_id" value="<?=$user_id?>">

<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?= $name ?>" name="name" type="text" class="form-control" placeholder="ชื่อ"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>นามสกุล</label>
        <input value="<?= $lname ?>" name="lname" type="text" class="form-control" placeholder="นามสกุล"  data-smk-msg="&nbsp;" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขบัตรประชาชน</label>
        <input value="<?= $id_card ?>" <?= $readonly ?> name="id_card" type="text" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เพศ</label>
        <div class="form-control" style="border-style: hidden;">
          <input type="radio" name="gender" id="gender_m" class="minimal" value="M" <?= $gender == 'M'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
          <input type="radio" name="gender" id="gender_f" class="minimal" value="F" <?= $gender == 'F'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input class="form-control datepicker" value="<?= $bod ?>" id="bod" type="text" data-provide="datepicker" data-date-language="th-th" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Email</label>
        <input value="<?= $email ?>" name="email" type="email" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>เบอร์โทรศัพท์</label>
        <input value="<?= $mobile ?>" name="mobile" type="tel" class="form-control" placeholder=""  data-smk-msg="&nbsp;"  required>
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        <label>รูปโปรไฟล์</label>
        <input name="img" type="file" class="form-control custom-file-input" onchange="readURL(this,'showImg');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group" id="showImg" style="margin-bottom:0px;margin-top: 15px;">
          <img src="<?= $user_img ?>"  onerror="this.onerror='';this.src='../../image/user.png'" style="height: 60px;">
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;"><?=$btn ?></button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
