<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];

$title      = "";
$path       = "";
$is_active  = "";
$type_media = "V";
$img        = "";

if($action == 'EDIT'){

  $sql   = "SELECT * FROM pfit_t_media WHERE id_media = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;

  $id_media       = $row[0]['id_media'];
  $title          = $row[0]['title'];
  $path           = $row[0]['path'];
  $is_active      = $row[0]['is_active'];
  $type_media     = $row[0]['type_media'];
  $img            = isset($row[0]['img'])?"../../image/media/".$row[0]['img']:"";
}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="id_media" value="<?=$id?>">
<input type="hidden" name="type_media" value="<?=$type_media?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>Title</label>
        <input value="<?=$title?>" name="title" type="text" class="form-control" placeholder="" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>URL</label>
        <input value="<?=$path?>" name="path" type="title" class="form-control" placeholder="VQlrqvtO1Wo" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <p style="color:red;">ตัวอย่างการกรอก URL YouTube ให้นำค่าหลัง "v=" มากรอก เช่น Link : https://www.youtube.com/watch?v=<u>VQlrqvtO1Wo</u><p>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
