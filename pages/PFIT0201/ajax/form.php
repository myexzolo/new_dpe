<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action     = $_POST['value'];
$code       = isset($_POST['code'])?$_POST['code']:"";
$statusType = $action;
$type       = $action;
$disabled = '';
$visibility = '';

$test_code        = "";
$test_name        = "";
$test_opjective   = "";
$test_detail      = "";
$category_criteria_code = "";
$test_unit        = "";
$test_calculator  = "";
$status           = "";
$test_min         = "";
$test_max         = "";
$test_type        = "";
$test_display     = "";
$testDisplayArr   = "";
$link_youtube     = "";
$link_youtube2    = "";
$test_seq_def     = "";

$arr_age_range = array();

$optionCat= "<option></option>";

$sqlCat = "SELECT * FROM pfit_t_category_criteria where status = 'Y'";

$queryCat = DbQuery($sqlCat,null);
$jsonCat  = json_decode($queryCat, true);

$numCat   = $jsonCat['dataCount'];
$rowCat   = $jsonCat['data'];


if(!empty($code)){
  $sql = "SELECT * FROM pfit_t_test where test_code  = '$code'";

  $query = DbQuery($sql,null);
  $json = json_decode($query, true);

  $num  = $json['dataCount'];
  $row  = $json['data'];

  if($num > 0){
    $test_code        = $row[0]['test_code'];
    $test_name        = $row[0]['test_name'];
    $test_opjective   = $row[0]['test_opjective'];
    $test_detail      = $row[0]['test_detail'];
    $category_criteria_code = $row[0]['category_criteria_code'];
    $test_unit        = $row[0]['test_unit'];
    $test_calculator  = $row[0]['test_calculator'];
    $status           = $row[0]['status'];
    $test_min         = $row[0]['test_min'];
    $test_max         = $row[0]['test_max'];
    $test_type        = $row[0]['test_type'];
    $test_age_min     = $row[0]['test_age_min'];
    $test_age_max     = $row[0]['test_age_max'];
    $test_display     = $row[0]['test_display'];
    $age_range        = $row[0]['age_range'];
    $link_youtube     = $row[0]['link_youtube'];
    $link_youtube2    = $row[0]['link_youtube2'];
    $test_seq_def     = $row[0]['test_seq_def'];

    $testDisplayArr   = explode(",", $test_display);

    if(!empty($age_range)){
        $arr_age_range = explode(",",$age_range);
    }
    // foreach ($testDisplayArr as $item) {
    //     echo "<li>$item</li>";
    // }
  }

  if($numCat > 0){
    for ($i=0; $i < $numCat ; $i++) {
      $selected = "";
      $category_criteria_name = $rowCat[$i]['category_criteria_name'];
      $categoryCriteriaCode = $rowCat[$i]['category_criteria_code'];
      if($category_criteria_code == $categoryCriteriaCode){
        $selected = 'selected="selected"';
      }
      $optionCat .= "<option value='$categoryCriteriaCode' $selected >$category_criteria_name</option>";
    }
  }
  if($action == 'VIEW'){
    $disabled = 'disabled';
    $display = 'display:none;';
    $visibility = 'visibility-non';

    //echo $statusType;
  }
}else{
  $sql = "SELECT test_code FROM pfit_t_test where test_code like 'TST%' order by test_code desc";

  $query = DbQuery($sql,null);
  $json = json_decode($query, true);

  $num  = $json['dataCount'];
  $row  = $json['data'];

  $testDisplayArr = [];

  if($num > 0){
    $test_code = $row[0]['test_code'];
  }

  if(!empty($test_code)){
    $lastNum = substr($test_code,3);
    $lastNum = $lastNum + 1;
    $test_code = "TST".sprintf("%02d", $lastNum);
  }else{
    $test_code = "TST".sprintf("%02d", 1);
  }

  if($numCat > 0){
    for ($x=0; $x < $numCat; $x++) {
      $selected = "";
      $category_criteria_name = $rowCat[$x]['category_criteria_name'];
      $category_criteria_code = $rowCat[$x]['category_criteria_code'];
      $optionCat .= "<option value='$category_criteria_code' $selected >$category_criteria_name</option>";
    }
  }
}

?>
<div class="modal-body">
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัสฐาน</label>
      <input type="hidden" name="action" id="action"  value="<?=$action?>">
      <input type="text" name="test_code" value="<?= $test_code; ?>" class="form-control" placeholder="รหัสฐาน" <?=$disabled ?> readonly >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>ชื่อฐานการทดสอบ</label>
      <input type="hidden" value="<?= $type ?>" name="type" required>
      <input type="text" name="test_name" value="<?= $test_name ?>" class="form-control" data-smk-msg="&nbsp;" placeholder="ชื่อฐานการทดสอบ" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ช่วงอายุการทดสอบ</label>
      <select <?=$disabled; ?> data-smk-msg="&nbsp;" required class="form-control select2" name="age_range[]" id="age_range" multiple="multiple" style="width:100%;height:40px;">
        <option value="1" class="dis" <?=in_array(1, $arr_age_range)?"selected":""?>>7-18 ปี</option>
        <option value="2" class="dis" <?=in_array(2, $arr_age_range)?"selected":""?>>19-59 ปี</option>
        <option value="3" class="dis" <?=in_array(3, $arr_age_range)?"selected":""?>>60 ปีขึ้นไป</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>การแสดงฐาน</label>
      <select data-smk-msg="&nbsp;" <?=$disabled; ?>  class="form-control select2" name="test_display[]" id="test_display" multiple="multiple" style="width:100%;height:40px;" required>
        <option value="2" class="dis" <?=in_array(2, $testDisplayArr) || in_array(1, $testDisplayArr)?"selected":""?>>กรมพลศึกษา</option>
        <option value="3" class="dis" <?=in_array(3, $testDisplayArr) || in_array(1, $testDisplayArr)?"selected":""?>>จพล.</option>
        <option value="4" class="dis" <?=in_array(4, $testDisplayArr) || in_array(1, $testDisplayArr)?"selected":""?>>โรงเรียน</option>
        <option value="5" class="dis" <?=in_array(5, $testDisplayArr) || in_array(1, $testDisplayArr)?"selected":""?>>Mobile</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>องค์ประกอบที่ต้องการวัด</label>
      <select name="test_opjective" class="form-control" <?=$disabled ?> data-smk-msg="&nbsp;"  required>
        <option value=""></option>
        <option value="1" <?= ($test_opjective == '1' ? 'selected="selected"':'') ?>>
          ความแข็งแรงและความอดทนของกล้ามเนื้อแขน
        </option>
        <option value="2" <?= ($test_opjective == '2' ? 'selected="selected"':'') ?>>
          ความแข็งแรงและความอดทนของกล้ามเนื้อขา
        </option>
        <option value="3" <?= ($test_opjective == '3' ? 'selected="selected"':'') ?>>
          ความแข็งแรงและความอดทนของกล้ามเนื้อหน้าท้อง
        </option>
        <option value="4" <?= ($test_opjective == '4' ? 'selected="selected"':'') ?>>
          ความอ่อนตัวของกล้ามเนื้อต้นขาด้านหลัง/หลังล่าง
        </option>
        <option value="5" <?= ($test_opjective == '5' ? 'selected="selected"':'') ?>>
          ความอ่อนตัวของกล้ามเนื้อไหล่/แขน
        </option>
        <option value="6" <?= ($test_opjective == '6' ? 'selected="selected"':'') ?>>
          ความอ่อนตัวของกล้ามเนื้อลำตัว
        </option>
        <option value="7" <?= ($test_opjective == '7' ? 'selected="selected"':'') ?>>
          ความอดทนของระบบหัวใจและไหลเวียนเลือด
        </option>
        <option value="8" <?= ($test_opjective == '8' ? 'selected="selected"':'') ?>>
          ความแคล่วคล่องว่องไว
        </option>
        <option value="9" <?= ($test_opjective == '9' ? 'selected="selected"':'') ?>>
          ความเร็ว
        </option>
        <option value="10" <?= ($test_opjective == '10' ? 'selected="selected"':'') ?>>
          ปฏิกิริยาตอบสนอง
        </option>
        <option value="11" <?= ($test_opjective == '11' ? 'selected="selected"':'') ?>>
          การทรงตัว
        </option>
        <option value="12" <?= ($test_opjective == '12' ? 'selected="selected"':'') ?>>
          กำลัง/พลังกล้ามเนื้อขา
        </option>
        <option value="13" <?= ($test_opjective == '13' ? 'selected="selected"':'') ?>>
          องค์ประกอบของร่างกาย
        </option>
        <option value="14" <?= ($test_opjective == '14' ? 'selected="selected"':'') ?>>
          น้ำหนักของกระดูก Bone Mass (Kg)
        </option>
        <option value="15" <?= ($test_opjective == '15' ? 'selected="selected"':'') ?>>
          ระดับไขมันช่องท้อง Visceral Fat (Ratio)
        </option>
        <option value="16" <?= ($test_opjective == '16' ? 'selected="selected"':'') ?>>
          การเผาพลาญพลังงานของร่างกาย BMR (Kcal)
        </option>
        <option value="17" <?= ($test_opjective == '17' ? 'selected="selected"':'') ?>>
          เปรียบเทียบอายุการเผาพลาญ Matabolic Age (Year)
        </option>
        <option value="18" <?= ($test_opjective == '18' ? 'selected="selected"':'') ?>>
          ดัชนีความหนาแน่นในร่างกาย BMI (Kg/m<sup>2</sup>)
        </option>
        <option value="19" <?= ($test_opjective == '19' ? 'selected="selected"':'') ?>>
          การวัดสัดส่วนรอบเอว/รอบสะโพก WHR
        </option>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Link YouTube วิธีการทดสอบ</label>
      <input type="text" name="link_youtube" value="<?= $link_youtube ?>" class="form-control" data-smk-msg="&nbsp;" placeholder="" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Link YouTube การเสริมสร้าง</label>
      <input type="text" name="link_youtube2" value="<?= $link_youtube2 ?>" class="form-control" data-smk-msg="&nbsp;" placeholder="" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>รายละเอียด</label>
      <textarea class="form-control" name="test_detail" id="content_data" <?=$disabled ?>><?= $test_detail; ?></textarea>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>แปลผลการทดสอบ</label>
      <select name="category_criteria_code" class="form-control" <?=$disabled ?> data-smk-msg="&nbsp;" required>
        <?= $optionCat ?>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>หน่วยที่ใช้วัด</label>
      <input type="text" name="test_unit" value="<?= $test_unit; ?>" class="form-control" data-smk-msg="&nbsp;"  placeholder="หน่วยที่ใช้วัด" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ประเภทการทดสอบ</label>
      <select name="test_type" class="form-control" <?=$disabled ?> required>
        <option value="0" <?= ($test_type == '0' ? 'selected="selected"':'') ?>>การทดสอบสมรรถภาพทางกาย</option>
        <option value="1" <?= ($test_type == '1' ? 'selected="selected"':'') ?>>สมรรถภาพทางกายเชิงสุขภาพ</option>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ลำดับการแสดง</label>
      <input type="text" name="test_seq_def" value="<?= $test_seq_def; ?>" class="form-control" data-smk-msg="&nbsp;" placeholder="" <?=$disabled ?> required>
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      <label>สูตรการคำนวณ</label>
      <select data-smk-msg="&nbsp;"  name="test_calculator" class="form-control" <?=$disabled ?> required>
        <option></option>
        <option value="1" <?= ($test_calculator == '1' ? 'selected="selected"':'') ?>>ผลการทดสอบที่ได้</option>
        <option value="2" <?= ($test_calculator == '2' ? 'selected="selected"':'') ?>>ผลการทดสอบที่ได้/น้ำหนัก</option>
        <option value="3" <?= ($test_calculator == '3' ? 'selected="selected"':'') ?>>ผลการทดสอบที่ได้/ส่วนสูง</option>
        <option value="7" <?= ($test_calculator == '7' ? 'selected="selected"':'') ?>>ผลการทดสอบที่ได้/2</option>
        <option value="4" <?= ($test_calculator == '4' ? 'selected="selected"':'') ?>>BMI</option>
        <option value="5" <?= ($test_calculator == '5' ? 'selected="selected"':'') ?>>VO<sup>2</sup> Max (ปั่นจักรยาน)</option>
        <option value="6" <?= ($test_calculator == '6' ? 'selected="selected"':'') ?>>ความดันโลหิต</option>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ค่าต่ำสุดที่ยอมรับได้</label>
        <input type="text" name="test_min" value="<?= $test_min; ?>" class="form-control" placeholder="MIN" <?=$disabled ?> >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ค่าสูงสุดที่ยอมรับได้</label>
        <input type="text" name="test_max" value="<?= $test_max; ?>" class="form-control" placeholder="MAX" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?= $display?>">บันทึก</button>
</div>
<script>
    $(function () {
      $('.select2').select2();
      //
      // if($('#action').val() == "VIEW"){
      //   $(".select2").select2("disabled", true);
      // }
    })
  </script>
