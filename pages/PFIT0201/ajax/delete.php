<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$code = $_POST['code'];
$ParamData = array($code);


$sql    = "SELECT * FROM pfit_t_test_criteria where test_code = ?";
$query  = DbQuery($sql,$ParamData);
$row    = json_decode($query, true);
$num    = $row['dataCount'];

if($num == 0){
  $sql  = "DELETE FROM pfit_t_test WHERE test_code = ?";

  $query  = DbQuery($sql,$ParamData);
  $row    = json_decode($query, true);
  $status = $row['status'];

  if($status == "success")
  {
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail : ไม่สามารถลบได้ เนื่องจากมีรายการฐานการทดสอบมีการใช้งานในระบบ')));
}

?>
