<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>ลำดับ</th>
      <th>รหัสฐาน</th>
      <th>ชื่อฐานการทดสอบ</th>
      <th>องค์ประกอบที่ต้องการวัด</th>
      <th>ช่วงอายุ</th>
      <th>สถานะ</th>
      <th>QR</th>
      <th style="width:70px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql = "SELECT * FROM pfit_t_test order by test_opjective,test_code";

      $query     = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {

        $url = "qr/index.php?test_code=".$value['test_code'];
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['test_code'];?></td>
      <td style="text-align:left;"><?=$value['test_name'];?></td>
      <td style="text-align:left;"><?=getTestOpjective($value['test_opjective']);?></td>
      <td style="text-align:center;"><?=$value['test_age_min']." - ".$value['test_age_max'];?></td>
      <td><?=$value['status']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <td><a onclick="postURL_blank('<?=$url ?>')"><span class="fa fa-qrcode"></span></a></td>
      <td><a class="btn_point text-green"><i class="fa fa-search" onclick="showForm('VIEW','<?=$value['test_code']?>')"></i></a></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['test_code']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['test_code']?>','<?=$value['test_name']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
