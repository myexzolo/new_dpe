<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
  </head>
  <body>
<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  require_once '../../../mpdf2/vendor/autoload.php';

  $test_code = $_GET['test_code'];
  $mpdf = new \Mpdf\Mpdf();

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();

  $sql = "SELECT * FROM pfit_t_test WHERE test_code = '$test_code'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);

  // $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
  // //echo $PNG_TEMP_DIR;
  // //html PNG location prefix
  // $PNG_WEB_DIR = 'temp/';
  // include "../../../qrcode/qrlib.php";

  // if (!file_exists($PNG_TEMP_DIR)){
  //   mkdir($PNG_TEMP_DIR);
  // }

  $tetCode = $json['data'][0]['test_code'];
  $tetName = $json['data'][0]['test_name'];
  // $filename = $PNG_TEMP_DIR.$tetCode.'.png';
  // $CorrectionLevel = 'L';

  // $str = "http://sportsci.dpe.go.th/pdf_test.php?test_code=".$tetCode;
  // $textQr = "$str";
  // $matrixPointSize = 5;
  // QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


  ?>
  <table style="width:100%;">
    <tr>
      <td style="width:200px;" align="left"></td>
      <td align="center" style="font-size:16pt;"><p><b><?= $tetName ?></b></p></td>
      <td style="width:200px;"></td>
    </tr>
  </table>
  <div style="font-size:14pt;">
  <?php
  echo $test_detail = $json['data'][0]['test_detail'];
  if($json['data'][0]['link_youtube'] != "")
  {
  ?>
    <br>
    <table style="width:100%;">
      <tr>
        <td style="width:50px;" align="left"><img src='../../../image/youtube.png' style='height:40px;' ></td>
        <td ><p><b>: https://www.youtube.com/watch?v=<?= $json['data'][0]['link_youtube'] ?></b></p></td>
      </tr>
    </table>
  <?php
  }
  ?>
</div>
</body>
</html>

<?php
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/PFIT0201.css');
  $mpdf->SetTitle($doc_no);
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
  // $mpdf->Output('IVRENT201711000059.pdf', \Mpdf\Output\Destination::DOWNLOAD);
  $mpdf->Output($_reserve_code.'.pdf', \Mpdf\Output\Destination::INLINE);
?>
