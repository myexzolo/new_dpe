<?php
require_once '../../../Classes/PHPExcel.php';
include '../../../Classes/PHPExcel/IOFactory.php';
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');


$branchCode     = $_SESSION['branchCode'];
$name           = "temp_".$_FILES["filepath"]["name"].date("His");
$target_file    = "upload/".$name.".xlsx";
$inputFileName  = $target_file;
$file_name      = $_FILES["filepath"]["name"];

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;
$fail_list = "";

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try
   {

     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
     $objReader->setReadDataOnly(true);
     $objPHPExcel = $objReader->load($inputFileName);

     $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
     $highestRow = $objWorksheet->getHighestRow();
     $highestColumn = $objWorksheet->getHighestColumn();

     $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
     $headingsArray = $headingsArray[1];

     $headerName = $headingsArray['A'];

     $headingsArray = array("A" => "NO",
                            "B" => "user_name",
                            "C" => "user_last",
                            "D" => "user_login",
                            "E" => "user_password",
                            "F" => "department"
                          );



     $r = -1;

     $namedDataArray = array();
     for ($row = 3; $row <= $highestRow; ++$row) {
         $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
         if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
             ++$r;
             foreach($headingsArray as $columnKey => $columnHeading) {
                 $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
             }
         }
     }
     $i = 0;
       foreach ($namedDataArray as $result) {
          try
          {
            $i++;

            $NO             = trim($result["NO"]);
            $user_name      = trim($result["user_name"]);
            $user_last      = trim($result["user_last"]);
            $user_login     = $result["user_login"];
            $user_password  = @md5(isset($result["user_password"])?$result["user_password"]:"");
            $department     = $result["department"];

            $department     = explode(":", $department);
            $department_id  = $department[0];
            $total++;

            if($user_name == "" || $user_last == "" || $user_login == "" || $user_password == "")
            {
              $fail++;
              if($fail_list == ""){
                $fail_list = $NO;
              }else{
                $fail_list .= ", ".$NO;
              }
              continue;
            }

            $sql = "SELECT * FROM t_user WHERE user_login = '$user_login' and branch_code = '$branchCode'";
            $json = DbQuery($sql,null);

            $obj = json_decode($json, true);
            $dataCount = $obj['dataCount'];
            $strSQL = "";
            if($dataCount > 0){
              $strSQL = "UPDATE t_user SET
                          user_login     = '$user_login',
                          user_password  = '$user_password',
                          user_name      = '$user_name',
                          user_last      = '$user_last',
                          department_id  = '$department_id',
                          update_date    = NOW(),
                          is_active      = 'Y',
                          user_id_update = '1'
                          WHERE branch_code = '$branchCode' and user_login = '$user_login'";
            }else{
              $strSQL = "INSERT INTO t_user (
                        user_login,user_password,
                        user_name,user_last,
                        is_active,role_list,
                        update_date,branch_code,
                        user_id_update,department_id)
                        VALUES(
                       '$user_login','$user_password',
                       '$user_name','$user_last',
                       'Y','3',
                       NOW(),'$branchCode',
                       '1','$department_id')";
            }
            //echo $strSQL."<br>";
            $js      = DbQuery($strSQL,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];
            //print_r($row['errorInfo'])."<br>";
            $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
            if($errorInfo != "00000"){
              $fail++;
              if($fail_list == ""){
                $fail_list = $NO;
              }else{
                $fail_list .= ", ".$NO;
              }
              continue;
            }else{
              $success++;
            }
          }catch (Exception $ex) {
            $fail++;
          }
        }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }

   if($total == $success){
     $status_upload = "S";
   }else{
     $status_upload = "F";
   }

    $str = "INSERT INTO t_history_upload
           (file_name,status_upload,total_record,success_record,fail_record,fail_list, path_file,type_file,branch_code)
           VALUES
           ('$file_name','$status_upload','$total','$success','$fail','$fail_list','$target_file','user','$branchCode')";

   $query    = DbQuery($str,null);
   $json     = json_decode($query, true);
   $status   = $json['status'];

   if($status == "success"){
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกสมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'บันทึกไม่สมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
}


?>
