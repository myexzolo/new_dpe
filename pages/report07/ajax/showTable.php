<style>
  th {
    /* font-size: 16px; */
  }

  .sumTotal
  {
    text-decoration: underline;
  }
</style>
<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  $typeDate   = $_POST['typeDate'];

  // $dateStart  = '2021-08-01';
  // $dateEnd    = '2021-08-31';

  $typeDateStr = "";
  $typeDateQuery = "end_date";
  if($typeDate == "1"){
    $typeDateStr = "วันที่ทดสอบ";
  }else if($typeDate == "2")
  {
    $typeDateQuery = "date_create";
    $typeDateStr = "วันที่นำเข้าข้อมูล";
  }


  // echo $dateStart  = '2018/10/01';
  $dateS = $dateStart;
  $dateE = $dateEnd;
  $month = monthThaiFull(date("n", strtotime($dateStart)));
  $year  = date("Y", strtotime($dateStart))+543;

  $user_login = "";

  $pam = "?dateStart=$dateStart"."&dateEnd=".$dateEnd."&typeDate=".$typeDate;
  // $pam .= "&typeDate=$typeDate&gender=$gender";
  // $pam .= "&age_start=$age_start&age_end=$age_end";
  // $pam .= "&province=$province&district=$district&user_login=$userLogin&role=$role";

  $url = "excelexport.php".$pam;

?>
  <div class="box box-primary">
  <?php if($_SESSION['ROLE_USER']['is_export'])
  {
  ?>
    <div class="box-header with-border">
    <h3 class="box-title">
        <?php //print_r($_SESSION['ROLE_USER']); ?>
    </h3>
    <div class="box-tools pull-right" >
      <a class="btn btn-block btn-social btn-success btnAdd" onclick="postURL_blank('<?=$url?>')" style="text-align:center;width:200px;">
        <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
      </a>
    </div>
    </div>
  <?php
  }
  ?>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:24px;">
          <b>รายงานจำนวนข้อมูลผู้ทดสอบสมรรถภาพทางกาย ณ <?=$typeDateStr?> <br>ตั้งแต่วันที่
            <?=convDatetoThaiFull($dateStart) ?> - <?= convDatetoThaiFull($dateEnd)?>
          </b
        </p>
        <table class="table table-bordered table-striped" style="font-size: 16px;">
          <thead>
            <tr>
              <th style="vertical-align:middle" rowspan="3">อายุ<br>เต็ม</th>
        			<th style="vertical-align:middle" colspan="5">ผู้ดูแลระบบ/เจ้าหน้าที่ทั่วไป</th>
              <th style="vertical-align:middle" colspan="5">เจ้าหน้าที่พลศึกษา</th>
              <th style="vertical-align:middle" colspan="5">โรงเรียน</th>
              <th style="vertical-align:middle" colspan="5">หน่วยงานภายนอก</th>
              <th style="vertical-align:middle" colspan="3">ผู้ใช้ Application (ประชาชน)</th>
              <th style="vertical-align:middle" colspan="7">ข้อมูลรวม Total</th>
        		</tr>
            <tr>
              <th style="vertical-align:middle" colspan="2">ชาย (คน)</th>
              <th style="vertical-align:middle" colspan="2">หญิง (คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม (คน)</th>
              <th style="vertical-align:middle" colspan="2">ชาย (คน)</th>
              <th style="vertical-align:middle" colspan="2">หญิง (คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม (คน)</th>
              <th style="vertical-align:middle" colspan="2">ชาย (คน)</th>
              <th style="vertical-align:middle" colspan="2">หญิง (คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม (คน)</th>
              <th style="vertical-align:middle" colspan="2">ชาย (คน)</th>
              <th style="vertical-align:middle" colspan="2">หญิง (คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม (คน)</th>
              <th style="vertical-align:middle" rowspan="2">ชาย<br>(คน)</th>
              <th style="vertical-align:middle" rowspan="2">หญิง<br>(คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม<br>(คน)</th>
              <th style="vertical-align:middle" colspan="3">ชาย (คน)</th>
              <th style="vertical-align:middle" colspan="3">หญิง (คน)</th>
              <th style="vertical-align:middle" rowspan="2">รวม (คน)</th>
        		</tr>
        		<tr>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">app</th>
              <th style="vertical-align:middle">กทม.</th>
              <th style="vertical-align:middle">ภูมิภาค</th>
              <th style="vertical-align:middle">app</th>

        		</tr>
          </thead>
          <tbody class="text-center">
              <?php
                $sql = "SELECT  ps.person_number ,ps.person_gender,TO_CHAR(ps.date_of_birth,'YYYY-MM-DD') as date_of_birth , p.province_id,
                                p.end_date, p.user_login, u.role_list,floor(months_between(p.end_date, ps.date_of_birth) /12) as age
                        FROM pfit_t_person ps, pfit_t_project p , t_user u
                        WHERE ps.project_code = p.project_code and ps.status_test = 2 and  p.user_login =  u.user_login
                              and p.end_date between TO_DATE('$dateStart 00:00:00','YYYY-MM-DD HH24:MI:SS') and TO_DATE('$dateEnd 23:59:59','YYYY-MM-DD HH24:MI:SS')
                        ORDER BY age asc";

                //echo $sql;

                $query = DbQuery($sql,null);
                $row  = json_decode($query, true);
                $num  = $row['dataCount'];
                $data = $row['data'];

                // $num = 1;

                if($num>0){
                  foreach ($data as $key => $value)
                  {
                    $age            = $value['age'];
                    $person_gender  = $value['person_gender'];
                    $role_list      = $value['role_list'];
                    $province_id    = $value['province_id'];

                    if($age < 7)
                    {
                      $age = 6;
                    }else if($age > 89)
                    {
                      $age = 90;
                    }

                    $roleId   = "";
                    $role     = explode(",",$role_list);

                    if (in_array("0", $role))
                    {
                        $roleId = 1;
                    }
                    else if(in_array("1", $role))
                    {
                        $roleId = 1;
                    }
                    else if(in_array("2", $role))
                    {
                        $roleId = 1;
                    }
                    else if(in_array("15", $role))
                    {
                        $roleId = 15;
                    }
                    else if(in_array("16", $role))
                    {
                        $roleId = 15;
                    }
                    else if(in_array("14", $role))
                    {
                        $roleId = 14;
                    }
                    else if(in_array("17", $role))
                    {
                        $roleId = 17;
                    }


                    if($province_id != 1)
                    {
                      $province_id = 2;
                    }

                    if($roleId != "")
                    {

                       if(isset($arrRes[$age][$roleId][$person_gender][$province_id]))
                       {
                         $arrRes[$age][$roleId][$person_gender][$province_id]++;
                       }
                       else
                       {
                         $arrRes[$age][$roleId][$person_gender][$province_id] = 1;
                       }


                       if(isset($arrRes['All'][$roleId][$person_gender][$province_id]))
                       {
                         $arrRes['All'][$roleId][$person_gender][$province_id]++;
                       }
                       else
                       {
                         $arrRes['All'][$roleId][$person_gender][$province_id] = 1;
                       }

                       //echo ">>>>>>>>>>".$arrRes[$age][$roleId][$person_gender][$province_id];

                       if(isset($arrRes[$age][$roleId]['All']))
                       {
                         $arrRes[$age][$roleId]['All']++;
                       }
                       else
                       {
                         $arrRes[$age][$roleId]['All'] = 1;
                       }

                       if(isset($arrRes['All'][$roleId]['All']))
                       {
                         $arrRes['All'][$roleId]['All']++;
                       }
                       else
                       {
                         $arrRes['All'][$roleId]['All'] = 1;
                       }



                       if(isset($arrRes[$age]['All'][$person_gender][$province_id]))
                       {
                         $arrRes[$age]['All'][$person_gender][$province_id]++;
                       }
                       else
                       {
                         $arrRes[$age]['All'][$person_gender][$province_id] = 1;
                       }

                       if(isset($arrRes['All']['All'][$person_gender][$province_id]))
                       {
                         $arrRes['All']['All'][$person_gender][$province_id]++;
                       }
                       else
                       {
                         $arrRes['All']['All'][$person_gender][$province_id] = 1;
                       }



                       if(isset($arrRes['All']['All']['All']))
                       {
                         $arrRes['All']['All']['All']++;
                       }
                       else
                       {
                         $arrRes['All']['All']['All'] = 1;
                       }



                       if(isset($arrRes[$age]['All']['All']))
                       {
                         $arrRes[$age]['All']['All']++;
                       }
                       else
                       {
                         $arrRes[$age]['All']['All'] = 1;
                       }
                    }
                  }
                }

                  $sql2 = "SELECT p.project_name,p.location ,p.project_code, u.gender, u.id_card, h.time, EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob)   as age_now
                              FROM pfit_t_result_history h, pfit_t_project p, pfit_t_user u
                              where  h.project_code = p.project_code and h.person_number = u.id_card and p.type_user = 'DPE'
                              and h.date_create between TO_DATE('$dateStart 00:00:00','YYYY-MM-DD HH24:MI:SS') and TO_DATE('$dateEnd 23:59:59','YYYY-MM-DD HH24:MI:SS') and h.test_criteria_code is not null
                              group by  p.project_name,p.location ,p.project_code,u.gender, u.id_card,h.time,
                              EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob)
                              ORDER BY age_now,u.gender desc";

                  //echo $sql;

                  $query2 = DbQuery($sql2,null);
                  $row2  = json_decode($query2, true);
                  $num2  = $row2['dataCount'];
                  $data2 = $row2['data'];

                  if($num2>0){
                    foreach ($data2 as $key => $value)
                    {
                      $age     = $value['age_now'];
                      $gender  = $value['gender'];


                      if($age < 7)
                      {
                        $age = 6;
                      }else if($age > 89)
                      {
                        $age = 90;
                      }


                      if(isset($arrRes[$age]['App'][$gender]))
                      {
                        $arrRes[$age]['App'][$gender]++;
                      }
                      else
                      {
                        $arrRes[$age]['App'][$gender] = 1;
                      }

                      if(isset($arrRes['All']['App'][$gender]))
                      {
                        $arrRes['All']['App'][$gender]++;
                      }
                      else
                      {
                        $arrRes['All']['App'][$gender] = 1;
                      }

                      if(isset($arrRes[$age]['App']['All']))
                      {
                        $arrRes[$age]['App']['All']++;
                      }
                      else
                      {
                        $arrRes[$age]['App']['All'] = 1;
                      }


                      if(isset($arrRes[$age]['All'][$gender]['3']))
                      {
                        $arrRes[$age]['All'][$gender]['3']++;
                      }
                      else
                      {
                        $arrRes[$age]['All'][$gender]['3'] = 1;
                      }

                      if(isset($arrRes['All']['App']['All']))
                      {
                        $arrRes['All']['App']['All']++;
                      }
                      else
                      {
                        $arrRes['All']['App']['All'] = 1;
                      }


                      if(isset($arrRes[$age]['All']['All']))
                      {
                        $arrRes[$age]['All']['All']++;
                      }
                      else
                      {
                        $arrRes[$age]['All']['All'] = 1;
                      }

                      if(isset($arrRes['All']['All']['All']))
                      {
                        $arrRes['All']['All']['All']++;
                      }
                      else
                      {
                        $arrRes['All']['All']['All'] = 1;
                      }
                    }
                }


              if($num > 0 || $num2 > 0)
              {
                for($i=6; $i <= 90 ; $i++)
                {
                  $strAge = "";
                  if($i == 6)
                  {
                    $strAge = "น้อยกว่า 7";
                  }else if($i == 90)
                  {
                    $strAge = "90 ขึ้นไป";
                  }else{
                    $strAge = $i;
                  }
              ?>
              <tr>
                <td><?=$strAge; ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][1]['M'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][1]['M'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][1]['F'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][1]['F'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][1]['All']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][15]['M'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][15]['M'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][15]['F'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][15]['F'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][15]['All']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][14]['M'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][14]['M'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][14]['F'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][14]['F'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][14]['All']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][17]['M'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][17]['M'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][17]['F'][1]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][17]['F'][2]) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i][17]['All']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['App']['M']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['App']['F']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['App']['All']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['M']['1']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['M']['2']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['M']['3']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['F']['1']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['F']['2']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['F']['3']) ?></td>
                <td align="right"><?= numberShow(@$arrRes[$i]['All']['All']) ?></td>
              </tr>
              <?php
                  }
              ?>
              <tr>
                <td><b>รวม</b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][1]['M'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][1]['M'][2]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][1]['F'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][1]['F'][2]) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All'][1]['All']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][15]['M'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][15]['M'][2]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][15]['F'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][15]['F'][2]) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All'][15]['All']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][14]['M'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][14]['M'][2]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][14]['F'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][14]['F'][2]) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All'][14]['All']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][17]['M'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][17]['M'][2]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][17]['F'][1]) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All'][17]['F'][2]) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All'][17]['All']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['App']['M']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['App']['F']) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All']['App']['All']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['M']['1']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['M']['2']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['M']['3']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['F']['1']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['F']['2']) ?></b></td>
                <td align="right"><b><?= numberShow(@$arrRes['All']['All']['F']['3']) ?></b></td>
                <td align="right" class="sumTotal"><b><?= numberShow(@$arrRes['All']['All']['All']) ?></b></td>
              </tr>
              <?php
                }else{
              ?>
               <tr>
           			<td colspan="31">ไม่พบข้อมูล</td>
               </tr>
             <?php } ?>
            </tbody>
          </table>
      </div>
    </div>
    </div>
  </div>
