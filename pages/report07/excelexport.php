<?php
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

//date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("DPE")
							 ->setLastModifiedBy("DPE")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Report01 file");

//$dateStart  	= $_POST['dateStart'];
$dateStart  = $_POST['dateStart'];
$dateEnd    = $_POST['dateEnd'];
$typeDate   = $_POST['typeDate'];


$month = monthThaiFull(date("n", strtotime($dateStart)));
$year  = date("Y", strtotime($dateStart))+543;


$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");


$cond= "";
$con = "";


$typeDateStr = "";
$typeDateQuery = "end_date";
if($typeDate == "1"){
  $typeDateStr = " วันที่ทดสอบ";
}else if($typeDate == "2")
{
  $typeDateQuery = "date_create";
  $typeDateStr = " วันที่นำเข้าข้อมูล";
}



$tab	= 1;
$RowHeight = 20;

if($cond != "")
{
  $RowHeight = 40;
}

$txth =  convDatetoThaiFull($dateStart)." ถึง ".convDatetoThaiFull($dateEnd);
$name =  "รายงานจำนวนข้อมูลผู้ทดสอบสมรรถภาพทางกาย ณ ".$typeDateStr." ตั้งแต่วันที่ ".convDatetoThaiFull($dateStart)." - ".convDatetoThaiFull($dateEnd);
$title = $name;



$objPHPExcel->getActiveSheet()->setTitle("รายงานจำนวนข้อมูลผู้ทดสอบ");
$objPHPExcel->getActiveSheet()->mergeCells('A1:AE1')->setCellValue('A1',$title)->getStyle('A1:AE1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet($tab)->mergeCells('A2:A4')->setCellValue('A2','อายุเต็ม')->getStyle('A2:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('B2:F2')->setCellValue('B2','ผู้ดูแลระบบ/เจ้าหน้าที่ทั่วไป')->getStyle('B2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('G2:K2')->setCellValue('G2','เจ้าหน้าที่พลศึกษา')->getStyle('G2:K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('L2:P2')->setCellValue('L2','โรงเรียน')->getStyle('L2:P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('Q2:U2')->setCellValue('Q2','หน่วยงานภายนอก')->getStyle('Q2:U2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('V2:X2')->setCellValue('V2','ผู้ใช้ Application (ประชาชน)')->getStyle('V2:X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('Y2:AE2')->setCellValue('Y2','ข้อมูลรวม Total')->getStyle('Y2:AE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('B3:C3')->setCellValue('B3','ชาย (คน)')->getStyle('B3:C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('D3:E3')->setCellValue('D3','หญิง (คน)')->getStyle('D3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('F3:F4')->setCellValue('F3','รวม (คน)')->getStyle('F3:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(B4,'กทม.')->getStyle(B4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(C4,'ภูมิภาค')->getStyle(C4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(D4,'กทม.')->getStyle(D4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(E4,'ภูมิภาค')->getStyle(E4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('G3:H3')->setCellValue('G3','ชาย (คน)')->getStyle('G3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('I3:J3')->setCellValue('I3','หญิง (คน)')->getStyle('I3:J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('K3:K4')->setCellValue('K3','รวม (คน)')->getStyle('K3:K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(G4,'กทม.')->getStyle(G4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(H4,'ภูมิภาค')->getStyle(H4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(I4,'กทม.')->getStyle(I4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(J4,'ภูมิภาค')->getStyle(J4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('L3:M3')->setCellValue('L3','ชาย (คน)')->getStyle('L3:M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('N3:O3')->setCellValue('N3','หญิง (คน)')->getStyle('N3:O3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('P3:P4')->setCellValue('P3','รวม (คน)')->getStyle('P3:P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(L4,'กทม.')->getStyle(L4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(M4,'ภูมิภาค')->getStyle(M4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(N4,'กทม.')->getStyle(N4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(O4,'ภูมิภาค')->getStyle(O4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('Q3:R3')->setCellValue('Q3','ชาย (คน)')->getStyle('Q3:R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('S3:T3')->setCellValue('S3','หญิง (คน)')->getStyle('S3:T3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('U3:U4')->setCellValue('U3','รวม (คน)')->getStyle('U3:U4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(Q4,'กทม.')->getStyle(Q4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(R4,'ภูมิภาค')->getStyle(R4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(S4,'กทม.')->getStyle(S4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(T4,'ภูมิภาค')->getStyle(T4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$objPHPExcel->getActiveSheet()->mergeCells('V3:V4')->setCellValue('V3','ชาย (คน)')->getStyle('V3:V4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('W3:W4')->setCellValue('W3','หญิง (คน)')->getStyle('W3:W4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('X3:X4')->setCellValue('X3','รวม (คน)')->getStyle('X3:X4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$objPHPExcel->getActiveSheet()->mergeCells('Y3:AA3')->setCellValue('Y3','ชาย (คน)')->getStyle('Y3:AA3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('AB3:AD3')->setCellValue('AB3','หญิง (คน)')->getStyle('AB3:AD3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('AE3:AE4')->setCellValue('AE3','รวม (คน)')->getStyle('AE3:AE4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(Y4,'กทม.')->getStyle(Y4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(Z4,'ภูมิภาค')->getStyle(Z4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(AA4,'app')->getStyle(AA4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(AB4,'กทม.')->getStyle(AB4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(AC4,'ภูมิภาค')->getStyle(AC4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue(AD4,'app')->getStyle(AD4)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$sql = "SELECT  ps.person_number ,ps.person_gender,TO_CHAR(ps.date_of_birth,'YYYY-MM-DD') as date_of_birth , p.province_id,
								p.end_date, p.user_login, u.role_list,floor(months_between(p.end_date, ps.date_of_birth) /12) as age
				FROM pfit_t_person ps, pfit_t_project p , t_user u
				WHERE ps.project_code = p.project_code and ps.status_test = 2 and  p.user_login =  u.user_login
							and p.end_date between TO_DATE('$dateStart 00:00:00','YYYY-MM-DD HH24:MI:SS') and TO_DATE('$dateEnd 23:59:59','YYYY-MM-DD HH24:MI:SS')
				ORDER BY age asc";

//echo $sql;

$query = DbQuery($sql,null);
$row  = json_decode($query, true);
$num  = $row['dataCount'];
$data = $row['data'];


if($num>0){
	foreach ($data as $key => $value)
	{
		$age            = $value['age'];
		$person_gender  = $value['person_gender'];
		$role_list      = $value['role_list'];
		$province_id    = $value['province_id'];

		if($age < 7)
		{
			$age = 6;
		}else if($age > 89)
		{
			$age = 90;
		}

		$roleId   = "";
		$role     = explode(",",$role_list);

		if (in_array("0", $role))
		{
				$roleId = 1;
		}
		else if(in_array("1", $role))
		{
				$roleId = 1;
		}
		else if(in_array("2", $role))
		{
				$roleId = 1;
		}
		else if(in_array("15", $role))
		{
				$roleId = 15;
		}
		else if(in_array("16", $role))
		{
				$roleId = 15;
		}
		else if(in_array("14", $role))
		{
				$roleId = 14;
		}
		else if(in_array("17", $role))
		{
				$roleId = 17;
		}


		if($province_id != 1)
		{
			$province_id = 2;
		}

		if($roleId != "")
		{

			 if(isset($arrRes[$age][$roleId][$person_gender][$province_id]))
			 {
				 $arrRes[$age][$roleId][$person_gender][$province_id]++;
			 }
			 else
			 {
				 $arrRes[$age][$roleId][$person_gender][$province_id] = 1;
			 }


			 if(isset($arrRes['All'][$roleId][$person_gender][$province_id]))
			 {
				 $arrRes['All'][$roleId][$person_gender][$province_id]++;
			 }
			 else
			 {
				 $arrRes['All'][$roleId][$person_gender][$province_id] = 1;
			 }

			 //echo ">>>>>>>>>>".$arrRes[$age][$roleId][$person_gender][$province_id];

			 if(isset($arrRes[$age][$roleId]['All']))
			 {
				 $arrRes[$age][$roleId]['All']++;
			 }
			 else
			 {
				 $arrRes[$age][$roleId]['All'] = 1;
			 }

			 if(isset($arrRes['All'][$roleId]['All']))
			 {
				 $arrRes['All'][$roleId]['All']++;
			 }
			 else
			 {
				 $arrRes['All'][$roleId]['All'] = 1;
			 }



			 if(isset($arrRes[$age]['All'][$person_gender][$province_id]))
			 {
				 $arrRes[$age]['All'][$person_gender][$province_id]++;
			 }
			 else
			 {
				 $arrRes[$age]['All'][$person_gender][$province_id] = 1;
			 }

			 if(isset($arrRes['All']['All'][$person_gender][$province_id]))
			 {
				 $arrRes['All']['All'][$person_gender][$province_id]++;
			 }
			 else
			 {
				 $arrRes['All']['All'][$person_gender][$province_id] = 1;
			 }



			 if(isset($arrRes['All']['All']['All']))
			 {
				 $arrRes['All']['All']['All']++;
			 }
			 else
			 {
				 $arrRes['All']['All']['All'] = 1;
			 }



			 if(isset($arrRes[$age]['All']['All']))
			 {
				 $arrRes[$age]['All']['All']++;
			 }
			 else
			 {
				 $arrRes[$age]['All']['All'] = 1;
			 }
		}
	}
}


$sql2 = "SELECT p.project_name,p.location ,p.project_code, u.gender, u.id_card, h.time, EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob)   as age_now
						FROM pfit_t_result_history h, pfit_t_project p, pfit_t_user u
						where  h.project_code = p.project_code and h.person_number = u.id_card and p.type_user = 'DPE'
						and h.date_create between TO_DATE('$dateStart 00:00:00','YYYY-MM-DD HH24:MI:SS') and TO_DATE('$dateEnd 23:59:59','YYYY-MM-DD HH24:MI:SS') and h.test_criteria_code is not null
						group by  p.project_name,p.location ,p.project_code,u.gender, u.id_card,h.time,
						EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob)
						ORDER BY age_now,u.gender desc";

//echo $sql;

$query2 = DbQuery($sql2,null);
$row2  = json_decode($query2, true);
$num2  = $row2['dataCount'];
$data2 = $row2['data'];

if($num2>0){
	foreach ($data2 as $key => $value)
	{
		$age     = $value['age_now'];
		$gender  = $value['gender'];


		if($age < 7)
		{
			$age = 6;
		}else if($age > 89)
		{
			$age = 90;
		}


		if(isset($arrRes[$age]['App'][$gender]))
		{
			$arrRes[$age]['App'][$gender]++;
		}
		else
		{
			$arrRes[$age]['App'][$gender] = 1;
		}

		if(isset($arrRes['All']['App'][$gender]))
		{
			$arrRes['All']['App'][$gender]++;
		}
		else
		{
			$arrRes['All']['App'][$gender] = 1;
		}

		if(isset($arrRes[$age]['App']['All']))
		{
			$arrRes[$age]['App']['All']++;
		}
		else
		{
			$arrRes[$age]['App']['All'] = 1;
		}


		if(isset($arrRes[$age]['All'][$gender]['3']))
		{
			$arrRes[$age]['All'][$gender]['3']++;
		}
		else
		{
			$arrRes[$age]['All'][$gender]['3'] = 1;
		}

		if(isset($arrRes['All']['App']['All']))
		{
			$arrRes['All']['App']['All']++;
		}
		else
		{
			$arrRes['All']['App']['All'] = 1;
		}


		if(isset($arrRes[$age]['All']['All']))
		{
			$arrRes[$age]['All']['All']++;
		}
		else
		{
			$arrRes[$age]['All']['All'] = 1;
		}

		if(isset($arrRes['All']['All']['All']))
		{
			$arrRes['All']['All']['All']++;
		}
		else
		{
			$arrRes['All']['All']['All'] = 1;
		}
	}
}

$rowNum = 5;
if($num > 0 || $num2 > 0)
{
	for($i=6; $i <= 90 ; $i++)
	{
		$strAge = "";
		if($i == 6)
		{
			$strAge = "น้อยกว่า 7";
		}else if($i == 90)
		{
			$strAge = "90 ขึ้นไป";
		}else{
			$strAge = $i;
		}

		$objPHPExcel->getActiveSheet()->setCellValue(A.$rowNum,$strAge)->getStyle(A.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(B.$rowNum,numberShow(@$arrRes[$i][1]['M'][1]))->getStyle(B.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(C.$rowNum,numberShow(@$arrRes[$i][1]['M'][2]))->getStyle(C.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(D.$rowNum,numberShow(@$arrRes[$i][1]['F'][1]))->getStyle(D.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(E.$rowNum,numberShow(@$arrRes[$i][1]['F'][2]))->getStyle(E.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(F.$rowNum,numberShow(@$arrRes[$i][1]['All']))->getStyle(F.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue(G.$rowNum,numberShow(@$arrRes[$i][15]['M'][1]))->getStyle(G.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(H.$rowNum,numberShow(@$arrRes[$i][15]['M'][2]))->getStyle(H.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(I.$rowNum,numberShow(@$arrRes[$i][15]['F'][1]))->getStyle(I.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(J.$rowNum,numberShow(@$arrRes[$i][15]['F'][2]))->getStyle(J.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(K.$rowNum,numberShow(@$arrRes[$i][15]['All']))->getStyle(K.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue(L.$rowNum,numberShow(@$arrRes[$i][14]['M'][1]))->getStyle(L.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(M.$rowNum,numberShow(@$arrRes[$i][14]['M'][2]))->getStyle(M.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(N.$rowNum,numberShow(@$arrRes[$i][14]['F'][1]))->getStyle(N.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(O.$rowNum,numberShow(@$arrRes[$i][14]['F'][2]))->getStyle(O.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(P.$rowNum,numberShow(@$arrRes[$i][14]['All']))->getStyle(P.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue(Q.$rowNum,numberShow(@$arrRes[$i][17]['M'][1]))->getStyle(Q.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(R.$rowNum,numberShow(@$arrRes[$i][17]['M'][2]))->getStyle(R.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(S.$rowNum,numberShow(@$arrRes[$i][17]['F'][1]))->getStyle(S.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(T.$rowNum,numberShow(@$arrRes[$i][17]['F'][2]))->getStyle(T.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(U.$rowNum,numberShow(@$arrRes[$i][17]['All']))->getStyle(U.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue(V.$rowNum,numberShow(@$arrRes[$i]['App']['M']))->getStyle(V.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(W.$rowNum,numberShow(@$arrRes[$i]['App']['F']))->getStyle(W.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(X.$rowNum,numberShow(@$arrRes[$i]['App']['All']))->getStyle(X.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue(Y.$rowNum,numberShow(@$arrRes[$i]['All']['M']['1']))->getStyle(Y.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(Z.$rowNum,numberShow(@$arrRes[$i]['All']['M']['2']))->getStyle(Z.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(AA.$rowNum,numberShow(@$arrRes[$i]['All']['M']['3']))->getStyle(AA.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(AB.$rowNum,numberShow(@$arrRes[$i]['All']['F']['1']))->getStyle(AB.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(AC.$rowNum,numberShow(@$arrRes[$i]['All']['F']['2']))->getStyle(AC.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(AD.$rowNum,numberShow(@$arrRes[$i]['All']['F']['3']))->getStyle(AD.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue(AE.$rowNum,numberShow(@$arrRes[$i]['All']['All']))->getStyle(AE.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$rowNum++;
	}

	$objPHPExcel->getActiveSheet()->setCellValue(A.$rowNum, 'รวม')->getStyle(A.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(B.$rowNum,numberShow(@$arrRes['All'][1]['M'][1]))->getStyle(B.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(C.$rowNum,numberShow(@$arrRes['All'][1]['M'][2]))->getStyle(C.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(D.$rowNum,numberShow(@$arrRes['All'][1]['F'][1]))->getStyle(D.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(E.$rowNum,numberShow(@$arrRes['All'][1]['F'][2]))->getStyle(E.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(F.$rowNum,numberShow(@$arrRes['All'][1]['All']))->getStyle(F.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->setCellValue(G.$rowNum,numberShow(@$arrRes['All'][15]['M'][1]))->getStyle(G.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(H.$rowNum,numberShow(@$arrRes['All'][15]['M'][2]))->getStyle(H.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(I.$rowNum,numberShow(@$arrRes['All'][15]['F'][1]))->getStyle(I.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(J.$rowNum,numberShow(@$arrRes['All'][15]['F'][2]))->getStyle(J.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(K.$rowNum,numberShow(@$arrRes['All'][15]['All']))->getStyle(K.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->setCellValue(L.$rowNum,numberShow(@$arrRes['All'][14]['M'][1]))->getStyle(L.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(M.$rowNum,numberShow(@$arrRes['All'][14]['M'][2]))->getStyle(M.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(N.$rowNum,numberShow(@$arrRes['All'][14]['F'][1]))->getStyle(N.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(O.$rowNum,numberShow(@$arrRes['All'][14]['F'][2]))->getStyle(O.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(P.$rowNum,numberShow(@$arrRes['All'][14]['All']))->getStyle(P.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->setCellValue(Q.$rowNum,numberShow(@$arrRes['All'][17]['M'][1]))->getStyle(Q.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(R.$rowNum,numberShow(@$arrRes['All'][17]['M'][2]))->getStyle(R.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(S.$rowNum,numberShow(@$arrRes['All'][17]['F'][1]))->getStyle(S.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(T.$rowNum,numberShow(@$arrRes['All'][17]['F'][2]))->getStyle(T.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(U.$rowNum,numberShow(@$arrRes['All'][17]['All']))->getStyle(U.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->setCellValue(V.$rowNum,numberShow(@$arrRes['All']['App']['M']))->getStyle(V.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(W.$rowNum,numberShow(@$arrRes['All']['App']['F']))->getStyle(W.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(X.$rowNum,numberShow(@$arrRes['All']['App']['All']))->getStyle(X.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->setCellValue(Y.$rowNum,numberShow(@$arrRes['All']['All']['M']['1']))->getStyle(Y.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(Z.$rowNum,numberShow(@$arrRes['All']['All']['M']['2']))->getStyle(Z.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(AA.$rowNum,numberShow(@$arrRes['All']['All']['M']['3']))->getStyle(AA.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(AB.$rowNum,numberShow(@$arrRes['All']['All']['F']['1']))->getStyle(AB.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(AC.$rowNum,numberShow(@$arrRes['All']['All']['F']['2']))->getStyle(AC.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(AD.$rowNum,numberShow(@$arrRes['All']['All']['F']['3']))->getStyle(AD.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue(AE.$rowNum,numberShow(@$arrRes['All']['All']['All']))->getStyle(AE.$rowNum)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);



}else{
	$objPHPExcel->getActiveSheet()->mergeCells('A5:AE5')->setCellValue('A5','ไม่พบข้อมูล')->getStyle('A5:AE5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

}


foreach(range('A','AE') as $columnID) {
    $objPHPExcel->getActiveSheet($tab)->getColumnDimension($columnID)
        ->setAutoSize(true);
}


$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');


?>
