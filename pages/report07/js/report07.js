$(function () {
  $('.select2').select2();
  searchProject();
})


function searchProject(){
  //var date = $('#s_year').val()+"/"+$('#s_month').val() + "/01";
  var typeDate  = $('#typeDate').val();
  var dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
  var dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");

  $.post("ajax/showTable.php",{dateStart:dateStart,dateEnd:dateEnd,typeDate:typeDate})
    .done(function( data ) {
      $('#show-table').html(data);
    });
}
