<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
/* .btnList{
  cursor: pointer;
} */

.info-box-number {
    font-size: 24px;
}

.project-name {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

@media (max-width: 1400px) {
  .project-name {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
  }
}

@media (min-width:1400px)
{
  .col-lgg-4 {
    float: left
  }
  .col-lgg-4 {
    width: 33.33333333%
  }
}

.tablePage
{
  border: 1px solid #ddd;
}


.tdPageActive
{
  z-index: 3;
  color: #fff;
  width: 40px;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
  padding: 5px;
  border: 1px solid #ddd;
}

.tdPage
{
  padding: 5px;
  z-index: 3;
  width: 40px;
  cursor: default;
  border: 1px solid #ddd;
}

.tdPageD
{
  padding: 5px;
  z-index: 3;
  width: 40px;
  cursor: not-allowed;
  border: 1px solid #ddd;
}

.tdP
{
  padding: 5px;
  z-index: 3;
  cursor: default;
  border: 1px solid #ddd;
}

.tdPage:hover
{
  background-color: #ddd;
  border-color: #ddd;
}


.col-lgg-4 {
  position: relative;
  min-height: 1px;
  padding-right: 2px;
  padding-left: 2px
}

.tooltip-inner {
    max-width: 600px;
    padding: 3px 8px;
    color: #fff;
    text-align: center;
    background-color: #000;
    border-radius: 4px;
    font-size: 20px;
    font-family: "CSChatThai";
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = isset($_POST['dateStart'])?$_POST['dateStart']:"";
  $dateEnd    = isset($_POST['dateEnd'])?$_POST['dateEnd']:"";
  $userLogin  = isset($_POST['userLogin'])?$_POST['userLogin']:"";
  $role       = isset($_POST['role'])?$_POST['role']:"";
  $role_id    = isset($_POST['role_id'])?$_POST['role_id']:"";
  $role_15    = isset($_POST['role_15'])?$_POST['role_15']:"";
  $page       = isset($_POST['page'])?$_POST['page']:1;
  $project_name = isset($_POST['project_name'])?$_POST['project_name']:"";

  $_SESSION['SEAECH'] = array();

  if($page == ""){
    $page = 1;
  }


  // print_r($_POST);

  //Array ( [dateStart] => 2020/12/01 [dateEnd] => 2020/12/31 [userLogin] => [role] => admin [role_id] => Array ( [0] => 1 [1] => 2 ) )


  $adminRole  = $_SESSION['member'][0]['adminRole'];

  $con = " and u.user_login = 'emtry'";

  if($userLogin != ""){
      $con = " and u.user_login = '".$userLogin."'";
      $_SESSION['SEAECH']['MEMBER_SH'] = $userLogin;
  }else if( $role_15 != "")
  {
    $con = "";
  }

  else if ($adminRole != "")
  {
    $con = "";
  }
  $conr = "";
  //echo count($role_id);
  if($role_id != "" && count($role_id) > 0)
  {
    $_SESSION['SEAECH']['ROLE_ID_SH'] = $role_id;
    for($x=0; $x < count($role_id); $x++)
    {
        $roleId = $role_id[$x];
        if($roleId != "")
        {
          if($conr == "")
          {
            $conr .= " and (','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }else{
            $conr .= " or ','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }
        }

    }
    if($conr != ""){
      $conr .= ")";
    }
  }
  $con  .= $conr;


  if($role_15 != "")
  {
    $con  .= " and (','||u.role_list||',' like '%,'||'15'||',%')";
  }

  if($dateStart != "" && $dateEnd != ""){
    $dateStart = queryDateOracle($dateStart);
    $dateEnd   = queryDateOracle($dateEnd);

    $con  .= " and p.start_date between $dateStart and  $dateEnd ";

  }

  if($project_name != "")
  {
    $con  .= " and p.project_name LIKE '%$project_name%' ";
    $_SESSION['SEAECH']['PROJECT_NAME_SH'] = $project_name;
  }



  $sql = "SELECT p.project_code, p.project_name, TO_CHAR(p.start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR(p.end_date, 'YYYY-MM-DD') as end_date,
          p.user_login, m.department_name,u.type_user,p.project_type
          FROM pfit_t_project p, t_user u
          LEFT JOIN pfit_t_member m ON u.user_id = m.user_id
          where p.status not in ('C') and p.user_login = u.user_login and u.is_active = 'Y' $con
          ORDER BY p.start_date desc";

  //echo $sql;
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];
  $Num_Rows  =   $num;

  $Per_Page = 30;   // Per Page

  $Prev_Page = $page-1;
  $Next_Page = $page+1;


  $Page_Start = (($Per_Page*$page)-$Per_Page);
  $Page_End   = $Page_Start +  $Per_Page;
  if($Num_Rows<=$Per_Page)
  {
    $Num_Pages =1;
  }
  else if(($Num_Rows % $Per_Page)==0)
  {
    $Num_Pages =($Num_Rows/$Per_Page) ;
  }
  else
  {
    $Num_Pages =($Num_Rows/$Per_Page)+1;
    $Num_Pages = (int)$Num_Pages;
  }

  if($Page_End > $Num_Rows )
  {
    $Page_End = $Num_Rows;
  }

  if($Next_Page > $Num_Pages)
  {
    $Next_Page = $Num_Pages;
  }

  if($Prev_Page < 0)
  {
    $Prev_Page = 0;
  }

  //echo $Page_Start."<==>".$Page_End;

  for ($i=$Page_Start; $i < $Page_End ; $i++) {
    $project_code = $data[$i]['project_code'];
    $project_name = $data[$i]['project_name'];
    $department_name = $data[$i]['department_name'];
    $project_type = $data[$i]['project_type'];
    $type_user = $data[$i]['type_user'];

    $pram = "?project_code=".$project_code."&project_name=".urlencode($project_name)."&project_type=".$project_type;

    if($type_user == "ADMIN")
    {
      $department_name = "กรมพลศึกษา";
    }
?>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-gray btnList" style="padding:10px;">
        <div class="info-box-number project-name" style="height:auto;"><span class="tooltipProject" title="<?= $project_name ?>"><?= $project_name ?></span></div>
        <?php if($role == "admin"){
        ?>
        <div style="font-size:21px;margin-top:10px;height:50px;">
          หน่วยงาน : <?= $department_name; ?>
        </div>
        <?php } ?>

        <div style="font-size:18px;height:30px;margin-top:10px;">
          <?= DateThai($data[$i]['start_date']) ?> - <?= DateThai($data[$i]['end_date']) ?>
        </div>
        <div class="row" style="padding-right: 15px;padding-left: 15px;">
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-olive btn-flat btnList" onclick="postURL('<?= "../PFIT0101/index.php".$pram ?>')">
              <i class="fa fa-user-plus" style="font-size:18px;"></i> ผู้ทดสอบ
            </button>
          </div>
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-gray-active btn-flat" onclick="postURL('<?= "../PFIT0102/index.php".$pram ?>')">
              <i class="fa fa fa-floppy-o" style="font-size:18px;"></i> จัดเก็บผล</button>
          </div>
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-light-blue btn-flat" onclick="postURL('<?= "../PFIT0103/index.php".$pram ?>')">
              <i class="fa fa-file-text-o" style="font-size:18px;"></i> รายงาน</button>
          </div>
        </div>
    </div>
  </div>
<?php
}
  if($num == 0){
  ?>
    <div class="col-xs-12 col-md-12">
      <div style="padding:15px;height:50px;">
        <p align="center">ไม่พบกิจกรรม</p>
      </div>
    </div>
  <?php
  }else{
  ?>
    <div class="col-md-12 col-xs-12">
      <table class="tablePage">
        <tr>
          <td class="tdP">
            Total <?= $Num_Rows;?> Page :
          </td>
      <?php
      if($Prev_Page)
      {
        echo " <td class='tdP' align='center' width='90' onclick='searchProject($Prev_Page)'>";
      	echo " <a >Previous</a> ";
        echo " </td>";
      }

      for($i=1; $i<=$Num_Pages; $i++){

      	if($i != $page)
      	{
          if($Num_Pages > 10)
          {
            if($page >= 7)
            {
              if($i == 1)
              {
                echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
                echo "<a>$i</a>";
                echo " </td>";
              }else if($i == 2)
              {
                echo " <td class='tdPageD' align='center'>";
            		echo "<a>...</a>";
                echo " </td>";
              }else if($i >= ($page-2) && $i <= ($page+2))
              {
                echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
                echo "<a>$i</a>";
                echo " </td>";
              }else if($i == ($page+3) && $i != $Num_Pages)
              {
                echo " <td class='tdPageD' align='center'>";
                echo "<a>...</a>";
                echo " </td>";
              }
              else if($i == $Num_Pages)
              {
                echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
                echo "<a>$i</a>";
                echo " </td>";
              }
            }else
            {
              if($i < 8)
              {
                echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
                echo "<a>$i</a>";
                echo " </td>";
              }else if($i==$Num_Pages)
              {
                echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
                echo "<a>$i</a>";
                echo " </td>";
              }else if($i == 8){
                echo " <td class='tdPageD' align='center'>";
            		echo "<a>...</a>";
                echo " </td>";
              }
            }

          }
           else
          {
            echo " <td class='tdPage' align='center' onclick='searchProject($i)' >";
        		echo "<a>$i</a>";
            echo " </td>";
          }
      	}
      	else
      	{
          echo " <td class='tdPageActive'  align='center' >";
      		echo "<b> $i </b>";
          echo " </td>";
      	}
      }
      if($page!=$Num_Pages)
      {
        echo " <td align='center' class='tdP' width='90' onclick='searchProject($Next_Page)'>";
      	echo " <a>Next</a> ";
        echo " </td>";
      }
  ?>
        </tr>
      </table>
    </div>
  <?php
  }
  ?>
<script>
  $('.tooltipProject').tooltip();
</script>
