<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>กรมพลศึกษา - หน้าหลัก</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list  = $_SESSION['member'][0]['role_list'];
          $user_login = $_SESSION['member'][0]['user_login'];
          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $venDorID = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

          $RoleAdmin = false;
          $roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
          //print_r($_SESSION['ROLE_USER']);
          if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)|| in_array("PFIT", $roleCodeArr)){
            $RoleAdmin = true;
          }
          $role_15 = "";
          if(in_array("AJPR", $roleCodeArr))
          {
            $role_15 = '15';
            $user_login = "";
          }
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <div class="row">
              <div class="col-md-4">
                <h1>

                </h1>
              </div>
              <div class="col-md-8 ">
                <?php
                if ($RoleAdmin)
                {
                    if($_SESSION['ROLE_USER']['is_approve'])
                    {
                    ?>
                      <a class="btn btn-app btn-shortcut pull-right" style="font-size:20px;margin: 0 0 0px 10px;" href="../../pages/member">
                        <span class="badge bg-yellow" id="checkApprove">0</span>
                        <i class="fa fa-check" style="font-size:30px;"></i> รออนุมัติ
                      </a>
                    <?php
                    }
                 }
                 if($_SESSION['ROLE_USER']['is_insert'])
                 {
                 ?>
                     <a class="btn btn-app btn-shortcut pull-right" style="font-size:20px;margin: 0 0 0px 10px;" href="../../pages/PFIT0100">
                       <i class="fa fa-plus" style="font-size:30px;"></i>
                       สร้างกิจกรรม
                     </a>
                 <?php
                 }
                 $role_id_sh = "";
                 $member_sh = "";
                 $project_name_sh = "";

                 // print_r($_SESSION['SEAECH']);
                 if(isset($_SESSION['SEAECH']['ROLE_ID_SH']))
                 {
                   $role_id_sh = $_SESSION['SEAECH']['ROLE_ID_SH'];
                 }

                 if(isset($_SESSION['SEAECH']['MEMBER_SH']))
                 {
                   $member_sh = $_SESSION['SEAECH']['MEMBER_SH'];
                 }

                 if(isset($_SESSION['SEAECH']['PROJECT_NAME_SH']))
                 {
                   $project_name_sh = $_SESSION['SEAECH']['PROJECT_NAME_SH'];
                 }

                  // $optionMember       = getoptionMember("");
                  $optionMember       = getoptionMember2($member_sh);
                  $optionGroupMember  = getoptionGroupMember($role_id_sh);
                ?>
              </div>
            </div>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
            <section class="col-lg-12 connectedSortable ui-sortable">
          <!-- Custom tabs (Charts with tabs)-->
             <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
               <div class="box-header with-border" style="cursor: move;">
                 <i class="fa fa-file-text-o"></i>
                 <h3 class="box-title">รายชื่อกิจกรรม</h3>
               </div>

               <!-- /.box-header -->
               <div class="box-body">
                 <div style="margin-top:10px;">
                   <div class="col-md-3">
                     <!-- <div class="form-group">
                        <label>วันที่</label>
                        <input type="text" class="form-control" id="daterage"  value="<?= $date; ?>" style="width:100%;">
                      </div> -->
                      <div class="form-group">
                         <label>รายชื่อกิจกรรม</label>
                         <input type="text" class="form-control" id="project_name"  value="<?=$project_name_sh ?>" style="width:100%;">
                       </div>
                   </div>
                   <?php
                   if ($RoleAdmin)
                   {
                   ?>
                   <div class="col-md-6">
                     <div class="form-group">
                        <label>รายชื่อหน่วยงาน</label>
                        <input type="hidden" id="role"  value="admin">
                        <select id="user_login" class="form-control select2" style="width: 100%;" required>
                          <option value="">&nbsp;</option>
                          <?=$optionMember;?>
                        </select>
                      </div>
                   </div>
                   <div class="col-md-3">
                     <div class="form-group">
                       <label>กลุ่มการใช้งาน</label>
                       <select id="role_id" class="form-control select2" multiple="multiple" style="width: 100%;">
                         <option value="">&nbsp;</option>
                         <?=$optionGroupMember;?>
                       </select>
                     </div>
                   </div>
                   <?php
                 }
                 else if($role_15 != "")
                 {
                     $optionMember = getoptionMemberByGroup('', $role_15);
                ?>
                      <div class="col-md-6">
                        <div class="form-group">
                           <label>รายชื่อหน่วยงาน</label>
                           <input type="hidden" id="role"  value="">
                           <input type="hidden" id="user_login"  value="<?= $user_login; ?>">
                           <input type="hidden" id="role_15"  value="<?= $role_15?>">
                           <select id="user_login" class="form-control select2" style="width: 100%;" required>
                             <option value="">&nbsp;</option>
                             <?=$optionMember;?>
                           </select>
                         </div>
                      </div>
                <?php
                 }
                 else
                 {
                   ?>
                   <input type="hidden" id="user_login"  value="<?= $user_login; ?>">
                   <input type="hidden" id="role_15"  value="<?= $role_15?>">
                   <input type="hidden" id="role"  value="">
                   <?php
                  }
                  ?>
                  <div class="col-md-12" align="center">
                    <button class="btn btn-primary" onclick="searchProject('')" style="width:80px;">ค้นหา</button>
                  </div>
               </div>
               </div>
               <div class="box-footer">
                 <div id="show-form"></div>
               </div>
             </div>

            <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body"></div>
              <!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
              </div>
            </div>

            <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Quick Email</h3>
              </div>
              <div class="box-body">

              </div>
              <div class="box-footer clearfix">
                <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                  <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
        </section>
        <section class="col-lg-4 connectedSortable ui-sortable" style="display:none;">
          <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-user"></i>
              <h3 class="box-title"></h3>โปรไฟล์
            </div>
            <div class="box-body">
            </div>
          </div>
        </section>
          </div>
              <!-- /.row -->

        </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
    </body>
  </html>
