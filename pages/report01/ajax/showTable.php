<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}

</style>
<?php

  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  $typeDate   = $_POST['typeDate'];
  $gender     = $_POST['gender'];
  $age_start  = $_POST['age_start'];
  $age_end    = $_POST['age_end'];
  $province   = $_POST['province'];
  $district       = $_POST['district'];
  $user_login_s   = @$_POST['user_login_s'];
  $role_id        = isset($_POST['role_id'])?$_POST['role_id']:"";

  $cond = "";
  $con  = "";
  $role = "";

  $genderM  = "Y";
  $genderF  = "Y";

  if($gender == "M"){
    $genderM  = "Y";
    $genderF  = "";
    $cond    .= " เพศชาย";
  }else if($gender == "F"){
    $genderM  = "";
    $genderF  = "Y";
    $cond    .= " เพศหญิง";
  }

  $ageCon = "";
  $age_start = intval($age_start);
  $age_end   = intval($age_end);
  if($age_start > 0 && $age_end > 0 && $age_end >=  $age_start)
  {
    $cond    .= " ช่วงอายุตั้งแต่ ".$age_start."-".$age_end."  ปี";
    $ageCon = "Y";

  }

  if ($province != "")
  {
    $cond    .= " จังหวัด".getProvinceName($province);
    $con     .= " and p.province_id ='$province'";
  }

  if ($district != "")
  {
    $cond    .= " อำเภอ".getDistrictName($district);
    $con     .= " and p.districts_id ='$district'";
  }

  $conr = "";
  //echo count($role_id);
  if($role_id != "" && count($role_id) > 0)
  {
    $role = implode(",",$role_id);
    for($x=0; $x < count($role_id); $x++)
    {
        $roleId = $role_id[$x];
        if($roleId != "")
        {
          if($conr == "")
          {
            $conr .= " and (','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }else{
            $conr .= " or ','||u.role_list||',' like '%,'||'$roleId'||',%'";
          }
        }

    }
    if($conr != ""){
      $conr .= ")";
    }
  }

  $con  .= $conr;



  $typeDateStr = "";
  $typeDateQuery = "p.end_date";
  if($typeDate == "1"){
    $typeDateStr = "ตามวันที่ทดสอบ";
  }else if($typeDate == "2")
  {
    $typeDateQuery = "p.date_create";
    $typeDateStr = "ตามวันที่นำเข้าข้อมูล";
  }

  if(!isset( $_SESSION['member']))
  {
      exit("<script>window.location='$urlLoggin'</script>");
  }


  $user_login = $_SESSION['member'][0]['user_login'];
  $adminRole  = $_SESSION['member'][0]['adminRole'];





  //echo $adminRole.",".$user_login;

  $userLogin = "";

  if($user_login != "" && $adminRole == ""){
      $con .= " and p.user_login = '".$user_login."'";
      $userLogin = $user_login;
  }


  if($user_login_s != ""){
      $con .= " and p.user_login = '".$user_login_s."'";
      $userLogin = $user_login;
  }


  // echo $dateStart  = '2018/10/01';
  $dateS = $dateStart;
  $dateE = $dateEnd;
  $month = monthThaiFull(date("n", strtotime($dateStart)));
  $year  = date("Y", strtotime($dateStart))+543;

  $pam = "?dateStart=$dateStart"."&dateEnd=".$dateEnd;
  $pam .= "&typeDate=$typeDate&gender=$gender";
  $pam .= "&age_start=$age_start&age_end=$age_end";
  $pam .= "&province=$province&district=$district&user_login=$userLogin&role=$role";

  $url = "excelexport.php".$pam;

  $user_login = "";

?>
  <div class="box box-primary">
    <?php if($_SESSION['ROLE_USER']['is_export'])
    {
    ?>
      <div class="box-header with-border">
      <h3 class="box-title">
          <?php //print_r($_SESSION['ROLE_USER']); ?>
      </h3>
      <div class="box-tools pull-right" >
        <a class="btn btn-block btn-social btn-success btnAdd" onclick="postURL_blank('<?=$url?>')" style="text-align:center;width:200px;">
          <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
        </a>
      </div>
      </div>
    <?php
    }
    ?>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:24px;">
          <b>รายงานการทดสอบสมรรถภาพทางกาย ตั้งแต่วันที่
            <?=convDatetoThaiFull($dateStart) ?> - <?= convDatetoThaiFull($dateEnd)."<br>".$typeDateStr?>
            <?=$cond ?>
          </b
        </p>
        <table class="table table-bordered table-striped">
          <thead>
        		<tr>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">ลำดับ</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">สถานที่</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">วัน/เดือน/ปี<br>วันที่ทดสอบ</th>
              <th colspan="1" rowspan="2" style="vertical-align:middle">วัน/เดือน/ปี<br>วันที่นำเข้าข้อมูล</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">จำนวนรวม</th>
              <?php if($genderM == "Y" ){
              ?>
                <th colspan="4" rowspan="1">ชาย (คน)</th>
              <?php } ?>
              <?php if($genderF == "Y" ){
              ?>
                <th colspan="4" rowspan="1">หญิง (คน)</th>
              <?php } ?>
        			<th rowspan="2" style="vertical-align:middle">Excel</th>
        		</tr>
        		<tr>
              <?php if($genderM == "Y" ){
              ?>
                <th>ต่ำกว่า 14 ปี</th>
                <th>14-24 ปี</th>
                <th>25-59 ปี</th>
                <th>60 ปีขึ้นไป</th>
              <?php } ?>
              <?php if($genderF == "Y" ){
              ?>
                <th>ต่ำกว่า 14 ปี</th>
                <th>14-24 ปี</th>
                <th>25-59 ปี</th>
                <th>60 ปีขึ้นไป</th>
              <?php } ?>
        		</tr>
          </thead>
        <tbody class="text-center">
            <?php
              //$dateStart  = $_POST['dateStart'];
              //$dateStart    = date("Y-m", strtotime($dateStart));
              //$end_date   = getDateOracle('end_date');
              //$date_create  = getDateOracle('date_create');
              //$end_date2   = getQueryDateWere('end_date');

              //$dateStart = queryDateOracle($dateStart);
              //$dateEnd  = queryDateOracle($dateEnd);


              // $sql = "SELECT $end_date,p.project_name,p.location,p.project_code,$date_create
              // FROM pfit_t_project p
              // where $typeDateQuery between $dateStart and $dateEnd and p.status <> 'C' $con
              // ORDER BY $typeDateQuery ASC";


              $sql = " SELECT TO_CHAR(p.end_date,'YYYY-MM-DD') as end_date ,p.project_name,p.location,p.project_code,
                       TO_CHAR(p.date_create,'YYYY-MM-DD') as date_create , count (ps.project_code) as person_c
                       FROM t_user u, pfit_t_project p
                       left join pfit_t_person ps on p.project_code = ps.project_code
                       where p.date_create between TO_DATE('$dateStart','YYYY-MM-DD') and TO_DATE('$dateEnd','YYYY-MM-DD') and p.status <> 'C' $con
                       and p.user_login = u.user_login and u.is_active = 'Y'
                       group by  p.end_date ,p.project_name,p.location,p.project_code, p.date_create , ps.project_code
                       ORDER BY p.date_create ASC";

              //echo $sql;

              $query = DbQuery($sql,null);
              $row  = json_decode($query, true);
              $num  = $row['dataCount'];
              $data = $row['data'];

              $totalc = 0;

              $total_m_14 = 0;
              $total_m_25 = 0;
              $total_m_60 = 0;
              $total_m_61 = 0;

              $total_f_14 = 0;
              $total_f_25 = 0;
              $total_f_60 = 0;
              $total_f_61 = 0;


              if($num>0){
                foreach ($data as $key => $value) {
                  $project_name = @$value['project_name'];
                  $location     = @$value['location'];
                  $end_date     = convDatetoThaiMonth(@$value['end_date']);
                  $date_create  = convDatetoThaiMonth(@$value['date_create']);
                  $project_code = @$value['project_code'];
                  $end_date2    = @$value['end_date'];
                  $person_c     = @$value['person_c'];

                  $pam  =  "?dateStart=$dateS&dateEnd=$dateE&project_code=$project_code";
                  $pam .= "&typeDate=$typeDate&gender=$gender";
                  $pam .= "&age_start=$age_start&age_end=$age_end";
                  $pam .= "&province=$province&district=$district";

                  $urlProject = "excelexport.php".$pam;

                  // $sqlc = "SELECT COUNT(person_number) as person_c
                  // FROM pfit_t_person
                  // WHERE project_code = '$project_code'";
                  // //echo $sqlc;
                  // $queryc = DbQuery($sqlc,null);
                  // $rowc  = json_decode($queryc, true);
                  //
                  // $person_c  = $rowc['data'][0]['person_c']==0?"0":$rowc['data'][0]['person_c'];


                  //$sqlm = "SELECT person_gender,".getDateOracle('date_of_birth')." FROM pfit_t_person WHERE project_code = '$project_code'";

                  // $sqlm = "SELECT person_gender, CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$end_date2','YYYY-MM-DD')) / 12)) age_now, count (project_code) count_person
                  //          FROM pfit_t_person
                  //          WHERE project_code = '$project_code'
                  //          group by person_gender, CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$end_date2','YYYY-MM-DD')) / 12))
                  //          order by person_gender desc, age_now";


                  // $sqlm = "SELECT person_gender,
                  //          CASE
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 13 THEN  13
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 24 THEN  24
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 59 THEN  59
                  //                     ELSE 60
                  //          END  as age_now
                  //          , count(project_code)  count_person
                  //          FROM pfit_t_person
                  //          WHERE project_code = '$project_code'
                  //          group by person_gender,
                  //          CASE
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 13 THEN  13
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 24 THEN  24
                  //                     WHEN  CEIL(ABS (MONTHS_BETWEEN(date_of_birth, TO_DATE('$dateEnd','YYYY-MM-DD')) / 12)) <= 59 THEN  59
                  //                     ELSE 60
                  //            END
                  //          order by person_gender desc, age_now";


                  $sqlm = "SELECT person_gender,
                           CASE
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 13 THEN  13
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 24 THEN  24
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 59 THEN  59
                                      ELSE 60
                           END  as age_now
                           , count(project_code)  count_person
                           FROM pfit_t_person
                           WHERE project_code = '$project_code' and date_of_birth is not null
                           group by person_gender,
                           CASE
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 13 THEN  13
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 24 THEN  24
                                      WHEN  EXTRACT(YEAR FROM TO_DATE('$dateEnd','YYYY-MM-DD')) - EXTRACT(YEAR FROM date_of_birth) <= 59 THEN  59
                                      ELSE 60
                            END
                           order by person_gender desc, age_now";

                  //echo $sqlm;

                  $querym = DbQuery($sqlm,null);
                  $rowm  = json_decode($querym, true);
                  $numm  = $rowm['dataCount'];

                  $person['M'][14] = 0;
                  $person['M'][25] = 0;
                  $person['M'][60] = 0;
                  $person['M'][61] = 0;

                  $person['F'][14] = 0;
                  $person['F'][25] = 0;
                  $person['F'][60] = 0;
                  $person['F'][61] = 0;

                  if($numm > 0){
                  //echo ">>>".date("H:i:s");
                  foreach ($rowm['data'] as $keym => $valuem) {
                    $person_genderm = $valuem['person_gender'];
                    $sex = $person_genderm == 'M'?'M':'F';
                    $count_person = $valuem['count_person'];
                    $age      = $valuem['age_now'];


                    if($genderF == "" && $sex == "F"){
                      $person_c-= $count_person;
                      continue;
                    }
                    else if($genderM == "" && $sex == "M")
                    {
                      $person_c-= $count_person;
                      continue;
                    }
                    // $age = yearBirth($valuem['date_of_birth']);
                    // echo $end_date2;
                    // $age = yearBirth2($valuem['date_of_birth'],$end_date2);

                    //echo "|>>>>".$ageCon."-->".$age."-->".$age_start."-->".$age_end;

                    if($ageCon == "Y" && (($age < $age_start) || ($age > $age_end)))
                    {
                      $person_c-= $count_person;
                      continue;
                    }

                    //echo $age.",";
                    if($age<14){
                      $person[$sex][14] += $count_person;
                    }elseif($age<=25){
                      $person[$sex][25] += $count_person;
                    }elseif($age<=59){
                      //echo $age;
                      $person[$sex][60] += $count_person;
                    }else{
                      $person[$sex][61] += $count_person;
                    }
                  }

                  $totalc = $totalc+$person_c;

                  $total_m_14 += $person['M'][14];
                  $total_m_25 += $person['M'][25];
                  $total_m_60 += $person['M'][60];
                  $total_m_61 += $person['M'][61];

                  $total_f_14 += $person['F'][14];
                  $total_f_25 += $person['F'][25];
                  $total_f_60 += $person['F'][60];
                  $total_f_61 += $person['F'][61];

                }

            ?>
            <tr>
              <td><?=$key+1; ?></td>
              <td class="text-left"><?=$project_name; ?></td>
              <td align="right"><?=$end_date; ?></td>
              <td align="right"><?=$date_create; ?></td>
              <td align="right"><?=$person_c; ?></td>
              <?php if($genderM == "Y" ){
              ?>
                <td align="right"><?=number_format($person['M'][14]); ?></td>
                <td align="right"><?=number_format($person['M'][25]); ?></td>
                <td align="right"><?=number_format($person['M'][60]); ?></td>
                <td align="right"><?=number_format($person['M'][61]); ?></td>
              <?php } ?>
              <?php if($genderF == "Y" ){
              ?>
                <td align="right"><?=number_format($person['F'][14]); ?></td>
                <td align="right"><?=number_format($person['F'][25]); ?></td>
                <td align="right"><?=number_format($person['F'][60]); ?></td>
                <td align="right"><?=number_format($person['F'][61]); ?></td>
              <?php } ?>
              <td><a onclick="postURL_blank('<?=$urlProject ?>')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
            </tr>
            <?php } ?>
            <tfoot>
              <tr>
          			<td colspan='4' align='right'><strong>ผลรวม</strong></td>
          			<td align="right"><strong><?=number_format($totalc); ?></strong></td>
                <?php if($genderM == "Y" ){
                ?>
                  <td align="right"><strong><?=number_format($total_m_14); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_m_25); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_m_60); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_m_61); ?></strong></td>
                <?php } ?>
                <?php if($genderF == "Y" ){
                ?>
                  <td align="right"><strong><?=number_format($total_f_14); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_f_25); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_f_60); ?></strong></td>
                  <td align="right"><strong><?=number_format($total_f_61); ?></strong></td>
                <?php } ?>
          			<td>&nbsp;</td>
          		</tr>
            </tfoot>
            <?php }else{ ?>
              <tr>
          			<td colspan="14">ไม่พบข้อมูล</td>
              </tr>
            <?php } ?>
        	</tbody>
        </table>

      </div>
    </div>
    </div>
  </div>
