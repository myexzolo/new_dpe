$(function () {
  $('.select2').select2();
  searchProject();
})


function searchProject(){
//var date = $('#s_year').val()+"/"+$('#s_month').val() + "/01";
var dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
var dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");
var typeDate  = $('#typeDate').val();
var gender    = $('#gender').val();
var age_start = $('#age_start').val();
var age_end   = $('#age_end').val();
var province  = $('#province').val();
var district  = $('#district').val();
var user_login_s  = $('#user_login_s').val();
var role_id   = $('#role_id').val();

$.post("ajax/showtable.php",{dateStart:dateStart,dateEnd:dateEnd,typeDate:typeDate,gender:gender,age_start:age_start,age_end:age_end,province:province,district:district,user_login_s:user_login_s,role_id:role_id})
  .done(function( data ) {
    //console.log(data);
    $('#show-table').html(data);
});
}

function getProvince(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#district').html(data);
        getDistrict($('#district').val(),'D');
  });
}

function getDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#subdistrict').html(data);
        //getSubDistrict($('#subdistrict').val(),'SD');
  });
}
