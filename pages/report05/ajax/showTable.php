<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}

</style>
<?php

  $dateStart    = $_POST['dateStart'];
  $dateEnd      = $_POST['dateEnd'];
  $projectCode  = "";
  $con  = "";



  if($projectCode != "")
  {
    $con .= " and project_code = '$projectCode' ";
  }

  if(!isset( $_SESSION['member']))
  {
      exit("<script>window.location='$urlLoggin'</script>");
  }


  $user_login = $_SESSION['member'][0]['user_login'];
  $adminRole  = $_SESSION['member'][0]['adminRole'];


  // echo $dateStart  = '2018/10/01';
  $dateS = $dateStart;
  $dateE = $dateEnd;
  // $month = monthThaiFull(date("n", strtotime($dateStart)));
  // $year  = date("Y", strtotime($dateStart))+543;
  //
  // $pam = "?dateStart=$dateStart"."&dateEnd=".$dateEnd;
  // $pam .= "&typeDate=$typeDate&gender=$gender";
  // $pam .= "&age_start=$age_start&age_end=$age_end";
  // $pam .= "&province=$province&district=$district&user_login=$userLogin&role=$role";
  //
  // $url = "excelexport.php".$pam;
  //
  // $user_login = "";

?>
  <div class="box box-primary">
    <?php if($_SESSION['ROLE_USER']['is_export'])
    {
    ?>
      <div class="box-header with-border">
      <h3 class="box-title">
          <?php //print_r($_SESSION['ROLE_USER']); ?>
      </h3>
      <!-- <div class="box-tools pull-right" >
        <a class="btn btn-block btn-social btn-success btnAdd" onclick="postURL_blank('<?=$url?>')" style="text-align:center;width:200px;">
          <i class="fa fa-file-excel-o"></i> ออกรายงาน Excel
        </a>
      </div> -->
      </div>
    <?php
    }
    ?>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:24px;">
          <b>รายงานการทดสอบสมรรถภาพทางกาย ตั้งแต่วันที่
            <?=convDatetoThaiFull($dateStart) ?> - <?= convDatetoThaiFull($dateEnd)?>
          </b
        </p>
        <table class="table table-bordered table-striped">
          <thead>
        		<tr>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">ลำดับ</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">สถานที่</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">จำนวนรวม</th>
              <th colspan="4" rowspan="1">ชาย (คน)</th>
              <th colspan="4" rowspan="1">หญิง (คน)</th>
        			<th rowspan="2" style="vertical-align:middle">Excel</th>
        		</tr>
        		<tr>
                <th>ต่ำกว่า 14 ปี</th>
                <th>14-24 ปี</th>
                <th>25-59 ปี</th>
                <th>60 ปีขึ้นไป</th>
                <th>ต่ำกว่า 14 ปี</th>
                <th>14-24 ปี</th>
                <th>25-59 ปี</th>
                <th>60 ปีขึ้นไป</th>
        		</tr>
          </thead>
        <tbody class="text-center">
            <?php


              $sql = " SELECT project_name,project_code,location FROM pfit_t_project where type_user = 'DPE' ORDER BY date_create ASC";

              $query = DbQuery($sql,null);
              $row  = json_decode($query, true);
              $num  = $row['dataCount'];
              $data = $row['data'];

              $totalc = 0;

              $total_m_14 = 0;
              $total_m_25 = 0;
              $total_m_60 = 0;
              $total_m_61 = 0;

              $total_f_14 = 0;
              $total_f_25 = 0;
              $total_f_60 = 0;
              $total_f_61 = 0;


              if($num>0){
                foreach ($data as $key => $value) {
                  $project_name = @$value['project_name'];
                  $location     = @$value['location'];
                  $project_code = @$value['project_code'];
                  $end_date2    = @$value['end_date'];


                  $pam  =  "?dateStart=$dateS&dateEnd=$dateE&project_code=$project_code";
                  $urlProject = "excelexport.php".$pam;


                  $sqlm = "SELECT p.project_name,p.location ,p.project_code, u.gender, u.id_card, h.time,
                           CASE
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 13 THEN  13
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 24 THEN  24
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 59 THEN  59
                                      ELSE 60
                            END  as age_now
                            FROM pfit_t_result_history h, pfit_t_project p, pfit_t_user u
                            where  h.project_code = p.project_code and h.person_number = u.id_card and p.project_code = '$project_code'
                            and h.date_create between TO_DATE('$dateStart','YYYY-MM-DD') and TO_DATE('$dateEnd','YYYY-MM-DD') and h.test_criteria_code is not null
                            group by  p.project_name,p.location ,p.project_code,u.gender, u.id_card,h.time,
                            CASE
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 13 THEN  13
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 24 THEN  24
                                      WHEN  EXTRACT(YEAR FROM h.date_create) - EXTRACT(YEAR FROM u.dob) <= 59 THEN  59
                                      ELSE 60
                            END
                            ORDER BY u.gender desc";

                  //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$sqlm;

                  $querym = DbQuery($sqlm,null);
                  $rowm  = json_decode($querym, true);
                  $numm  = $rowm['dataCount'];

                  $person['M'][14] = 0;
                  $person['M'][25] = 0;
                  $person['M'][60] = 0;
                  $person['M'][61] = 0;

                  $person['F'][14] = 0;
                  $person['F'][25] = 0;
                  $person['F'][60] = 0;
                  $person['F'][61] = 0;
                  $person_c        = 0;

                  if($numm > 0){
                  //echo ">>>".date("H:i:s");
                  foreach ($rowm['data'] as $keym => $valuem) {
                    $person_genderm = $valuem['gender'];
                    $sex = $person_genderm == 'M'?'M':'F';
                    $age = $valuem['age_now'];

                    //echo $age.",";
                    if($age<14){
                      $person[$sex][14]++;
                    }elseif($age<=25){
                      $person[$sex][25]++;
                    }elseif($age<=59){
                      //echo $age;
                      $person[$sex][60]++;
                    }else{
                      $person[$sex][61]++;
                    }
                    $person_c++;
                    $totalc++;
                  }

                  $total_m_14 += $person['M'][14];
                  $total_m_25 += $person['M'][25];
                  $total_m_60 += $person['M'][60];
                  $total_m_61 += $person['M'][61];

                  $total_f_14 += $person['F'][14];
                  $total_f_25 += $person['F'][25];
                  $total_f_60 += $person['F'][60];
                  $total_f_61 += $person['F'][61];

                }

            ?>
            <tr>
              <td><?=$key+1; ?></td>
              <td class="text-left"><?=$project_name; ?></td>
              <td align="right"><?=$person_c; ?></td>
              <td align="right"><?=number_format($person['M'][14]); ?></td>
              <td align="right"><?=number_format($person['M'][25]); ?></td>
              <td align="right"><?=number_format($person['M'][60]); ?></td>
              <td align="right"><?=number_format($person['M'][61]); ?></td>
              <td align="right"><?=number_format($person['F'][14]); ?></td>
              <td align="right"><?=number_format($person['F'][25]); ?></td>
              <td align="right"><?=number_format($person['F'][60]); ?></td>
              <td align="right"><?=number_format($person['F'][61]); ?></td>
              <td><a onclick="postURL_blank('<?=$urlProject ?>')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
            </tr>
            <?php } ?>
            <tfoot>
              <tr>
          			<td colspan='2' align='right'><strong>ผลรวม</strong></td>
          			<td align="right"><strong><?=number_format($totalc); ?></strong></td>
                <td align="right"><strong><?=number_format($total_m_14); ?></strong></td>
                <td align="right"><strong><?=number_format($total_m_25); ?></strong></td>
                <td align="right"><strong><?=number_format($total_m_60); ?></strong></td>
                <td align="right"><strong><?=number_format($total_m_61); ?></strong></td>
                <td align="right"><strong><?=number_format($total_f_14); ?></strong></td>
                <td align="right"><strong><?=number_format($total_f_25); ?></strong></td>
                <td align="right"><strong><?=number_format($total_f_60); ?></strong></td>
                <td align="right"><strong><?=number_format($total_f_61); ?></strong></td>
          			<td>&nbsp;</td>
          		</tr>
            </tfoot>
            <?php }else{ ?>
              <tr>
          			<td colspan="11">ไม่พบข้อมูล</td>
              </tr>
            <?php } ?>
        	</tbody>
        </table>

      </div>
    </div>
    </div>
  </div>
