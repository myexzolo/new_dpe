<?php

include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = $_POST['value'];
$id           = isset($_POST['id'])?$_POST['id']:"";
$branch_code  = $_SESSION['branchCode'];

$img                = "";
$is_active          = "";
$seq                = "";

$readonly = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_slide WHERE  id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $is_active    = $rows[0]['is_active'];
  $seq          = $rows[0]['seq'];
  $img          = $rows[0]['img'];

  $img          = "../../image/slides/mobile/".$img;


  $readonly = "return false;";
}
else if($action == 'ADD')
{
 $btn = 'Save changes';
}

//echo $img;
?>
<input type="hidden" name="id" value="<?= $id ?>">
<input type="hidden" name="action" value="<?= $action ?>">
<div class="modal-body">
    <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input value="" name="file" type="file" onchange="readURL(this,'showImg');" class="form-control" placeholder=""  accept="image/*" >
        <input class="form-control" id="img" type="hidden" value="<?=$img?>">
        <input class="form-control" name="img" type="hidden" value="<?=$img?>">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?= $seq ?>" name="seq" type="text" class="form-control" placeholder="" onkeyup="checknumber(this)"  data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group" id="showImg">
          <img src="<?= $img ?>" onerror="this.onerror='';this.src='../../image/picture.png'" style="height: 100px;">
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
<?php } ?>
</div>
