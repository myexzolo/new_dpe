
$(function () {
    showTable();
})

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",code=""){
  $.post("ajax/form.php",{value:value,code:code})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


function getProvince(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#district').html(data);
        getDistrict($('#district').val(),'D');
  });
}

function getDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#subdistrict').html(data);
        //getSubDistrict($('#subdistrict').val(),'SD');
  });
}

function checkAll(obj) {
  if(obj.checked){
    $('.checkId').each(function(){
        $(this).prop('checked', true);
    });
  }else{
    $('.checkId').prop('checked', false);
  }
}

function chkList(){
  $( ".checkId" ).each(function( index ) {
      if(!this.checked){
        $('#checkApp').prop('checked', false);
        return false;
      }
  });
}

function testCodeList(){
  var code = $('#code').val();
  var age_range =  $('#age_range').val();
  var action =  $('#action').val();
  //console.log(age_range);
  $.post("ajax/testCodeList.php",{code:code,age_range:age_range,action:action})
    .done(function( data ) {
        $('#testCodeList').html(data);
        //getSubDistrict($('#subdistrict').val(),'SD');
  });
}


function delModule(code,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล กิจกรรม : '+ name +' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/delete.php",{code:code})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
