<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$role_list  = $_SESSION['member'][0]['role_list'];
$user_login = $_SESSION['member'][0]['user_login'];
$roleArr   = explode(",",$role_list);

// echo $user_login;



$RoleAdmin = false;
$roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
// print_r($roleCodeArr);

if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)|| in_array("PFIT", $roleCodeArr)){
  $user_login = "";
  $RoleAdmin = true;
}

$con = "";
if(in_array("AJPR", $roleCodeArr))
{
  $RoleAdmin = true;
  $user_login = "";

}

?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>ลำดับ</th>
      <th>รหัส</th>
      <th>ชื่อกิจกรรม</th>
      <th>ระยะเวลา</th>
      <th>วันที่นำเข้าข้อมูล</th>
      <?php
      if($RoleAdmin)
      {
      ?>
        <th>หน่วยงาน</th>
      <?php
      }
      ?>
      <th>สถานะ</th>
      <th style="width:70px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    // $con = "";
    if($user_login != ""){
      $con .= " and p.user_login = '$user_login'";
    }

    $start_date = getDateOracle('start_date');
    $end_date   = getDateOracle('end_date');
    $date_create = getDateOracle('date_create');

    $sql = "SELECT p.project_code,p.project_name,$start_date,$end_date,p.location,p.project_detail,
            p.status,p.user_login,p.project_type,$date_create, m.department_name,u.type_user
    FROM pfit_t_project p, t_user u
    LEFT JOIN pfit_t_member m ON u.user_id = m.user_id
    WHERE p.status <> 'C' and u.is_active = 'Y'  and p.user_login = u.user_login $con and p.project_code <> 'M611101' order by project_code desc";

    // echo $sql;

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0)
    {

    foreach ($rows as $key => $value)
    {
      $department_name = $value['department_name'];
      $type_user = $value['type_user'];
      if($type_user == "ADMIN")
      {
        $department_name = "กรมพลศึกษา";
      }

      $project_name   = isset($value['project_name'])?str_replace('"', "&quot;", $value['project_name']):"";
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['project_code'];?></td>
      <td style="text-align:left;"><?=$value['project_name'];?></td>
      <td ><?= DateThai($value['start_date'])." - ".DateThai($value['end_date']) ;?></td>
      <td ><?= DateThai($value['date_create']);?></td>
      <?php
      if($RoleAdmin)
      {
      ?>
        <td><?= $department_name." (".$value['user_login'].")" ?></th>
      <?php
      }
      ?>
      <td><?=$value['status']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <td><a class="btn_point text-green"><i class="fa fa-search" onclick="showForm('VIEW','<?=$value['project_code']?>')"></i></a></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['project_code']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['project_code']?>','<?=$project_name?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php }
    } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
