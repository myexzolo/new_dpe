<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$project_code     = $_POST['project_code'];
// $project_name     = $_POST['project_name'];
$project_name     = isset($_POST['project_name'])?str_replace('"', "&quot;", $_POST['project_name']):"";
$start_date       = queryDateOracle(dateThToEn($_POST['start_date'],"dd/mm/yyyy","/"));
$end_date         = queryDateOracle(dateThToEn($_POST['end_date'],"dd/mm/yyyy","/"));
$location         = $_POST['location'];
$project_detail   = $_POST['project_detail'];
$status           = $_POST['status'];
$project_type     = $_POST['project_type'];
$province_id      = $_POST['province_id'];
$districts_id     = $_POST['districts_id'];
$age_range        = implode(",",@$_POST['age_range']);

$user_login       = $_SESSION['member'][0]['user_login'];

$test_code = "";


$action   = $_POST['action'];

if($action == "EDIT"){

  $sql  = "DELETE FROM pfit_t_project_test WHERE project_code = '$project_code'";
  DbQuery($sql,null);

  $sql = "UPDATE pfit_t_project SET
          project_name    = '$project_name',
          start_date      = $start_date,
          end_date        = $end_date,
          location        = '$location',
          project_detail  = '$project_detail',
          status          = '$status',
          age_range       = '$age_range',
          province_id     = '$province_id',
          districts_id     = '$districts_id',
          project_type    = '$project_type'
          WHERE project_code   = '$project_code'";
}else{
  $dateNow   =  queryDateTimeOracle(date('Y/m/d'));


  $sql = "SELECT project_code FROM pfit_t_project where project_code NOT LIKE 'M%' order by project_code desc";

  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  if($num > 0){
    $project_code = $row[0]['project_code'];
  }
  $date = date("Y");
  $dates = date("m");

  if($date < 2500){
    $date += 543;
  }

  if(!empty($project_code)){
    $project_code = explode("_", $project_code);
    $lastNum = substr($project_code[0],4);
    $lastNum = $lastNum + 1;

    $project_code = substr($date, 2, 4).$dates.sprintf("%04d", $lastNum);
  }else{
    $project_code = substr($date, 2, 4).$dates.sprintf("%04d", 1);
  }

  $sql = "INSERT INTO pfit_t_project
         (project_code,project_name,start_date,end_date,
          location,project_detail,status,user_login,
          age_range,province_id,districts_id,project_type,date_create)
         VALUES
         ('$project_code','$project_name',$start_date,$end_date,
          '$location','$project_detail','$status','$user_login',
          '$age_range','$province_id','$districts_id','$project_type',$dateNow)";
}

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'][0];


if(!empty($_POST['test_code']))
{
  foreach( $_POST['test_code'] as $v ) {
    $test_seq = $_POST['test_seq'][$v];
    $sqlt = "INSERT INTO pfit_t_project_test(project_code,test_code,test_seq) VALUES('$project_code','$v','$test_seq')";
    //echo ">>".$sql;
    DbQuery($sqlt,null);
  }
}

//echo $sql;


if(intval($errorInfo) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
