<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action     = $_POST['value'];
$code       = isset($_POST['code'])?$_POST['code']:"";
$statusType = isset($_POST['status'])?$_POST['status']:"";

$project_code     = "";
$project_name     = "";
$start_date       = date('Y/m/d');
$end_date         = date('Y/m/d');
$location         = "";
$project_detail   = "";
$status           = "";
$test_code        = "";
$project_type     = "4";
$province_id      = "";
$districts_id     = "";
$arr_age_range    = array(1,2,3);

$category_criteria_code = "";

$option = '';
$type = "";

$disabled = "";
$display = "";
$display2 = "";
$visibility = "";

$role_list   = isset($_SESSION['member'][0]['role_list'])?$_SESSION['member'][0]['role_list']:'0';
$arrRole     = explode(",",$role_list);

$str1 = "";
if(count($arrRole) > 0)
{
  foreach ($arrRole as $value)
  {
    // echo $value."<br>";
    // if($value == "0" || $value == "1" || $value == "1"){
    //   $project_type = "";
    //   $display      = "";
    //   break;
    // }
    // else if($value == "14")
    // {
    //   $display = "display:none;";
    //   $project_type   = "1";
    //   break;
    // }else{
    //   $display = "display:none;";
    // }
    if($value == "14")
    {
      $display = "display:none;";
      $project_type   = "1";
      break;
    }
  }
}
if(!empty($code)){

  $sql = "SELECT project_code, project_name, location, project_detail,status,project_type,
          TO_CHAR( start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date,province_id,districts_id,age_range
          FROM pfit_t_project
          where project_code  = '$code'";
  //echo $sql;
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);

  $num  = $json['dataCount'];
  $row  = $json['data'];

  if($num > 0){
    $project_code     = $row[0]['project_code'];
    $project_name     = $row[0]['project_name'];
    $start_date       = $row[0]['start_date'];
    $end_date         = $row[0]['end_date'];
    $location         = $row[0]['location'];
    $project_detail   = $row[0]['project_detail'];
    $status           = $row[0]['status'];
    $project_type     = $row[0]['project_type'];
    $province_id      = $row[0]['province_id'];
    $districts_id     = $row[0]['districts_id'];
    $age_range        = isset($row[0]['age_range'])?$row[0]['age_range']:"";

    if($age_range != ""){
      $arr_age_range    = explode(",", $age_range);
    }
  }

  if($action == 'VIEW'){
    $disabled = 'disabled';
    $display2 = 'display:none;';
    $visibility = 'visibility-non';
  }
}

$optionProvince     = getoptionProvince($province_id);
$optionDistrict     = getoptionDistrict($province_id,$districts_id);
?>
<style>
th {
  text-align: center;
  background-color: #ebebeb;
}
</style>
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อกิจกรรม</label>
        <input type="hidden" value="<?= $action ?>" name="action" id="action" required >
        <input type="hidden" value="<?= $code ?>" id="code" >
        <input type="text" name="project_name" value="<?= $project_name ?>" class="form-control" placeholder="ชื่อกิจกรรม" <?=$disabled ?> required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่เริ่ม</label>
        <!-- <input type="date" name="start_date" value="<?= $start_date; ?>" class="form-control" placeholder="วันที่เริ่ม"  <?=$disabled ?> required> -->
        <input class="form-control datepicker" value="<?= DateThai($start_date) ?>" name="start_date" type="text" placeholder="วันที่เริ่ม" data-provide="datepicker" data-date-language="th-th" <?=$disabled ?> required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่สิ้นสุด</label>
        <!-- <input type="date" name="end_date" value="<?= $end_date; ?>" class="form-control" placeholder="วันที่สิ้นสุด"  <?=$disabled ?> required> -->
        <input class="form-control datepicker" value="<?= DateThai($end_date) ?>" name="end_date" type="text" placeholder="วันที่สิ้นสุด" data-provide="datepicker" data-date-language="th-th" <?=$disabled ?> required>
      </div>
    </div>
    <div class="col-md-6" style="display:none">
      <div class="form-group">
        <label>รหัส</label>
        <input type="text" name="project_code" value="<?= $project_code; ?>"  class="form-control" placeholder="รหัสกิจกรรม" <?=$disabled ?> readonly >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สถานที่</label>
        <input type="text" name="location" value="<?= $location; ?>" class="form-control" placeholder="ชื่อโรงเรียน สวนสาธารณะ"  <?=$disabled ?> required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>จังหวัด</label>
        <select name="province_id" id="province" onchange="getProvince(this.value,'P')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?=$optionProvince;?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>อำเภอ</label>
        <select name="districts_id" id="district" onchange="getDistrict(this.value,'D')" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?=$optionDistrict;?>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <input type="text" class="form-control" value="<?= $project_detail; ?>" name="project_detail" placeholder="รายละเอียด" <?=$disabled ?>>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ช่วงอายุการทดสอบ</label>
        <select <?=$disabled; ?> data-smk-msg="&nbsp;" required class="form-control select2" name="age_range[]" onchange="testCodeList()" id="age_range" multiple="multiple" style="width:100%;height:40px;">
          <option value="1" class="dis" <?=in_array(1, $arr_age_range)?"selected":""?>>7-18 ปี</option>
          <option value="2" class="dis" <?=in_array(2, $arr_age_range)?"selected":""?>>19-59 ปี</option>
          <option value="3" class="dis" <?=in_array(3, $arr_age_range)?"selected":""?>>60 ปีขึ้นไป</option>
        </select>
      </div>
    </div>
    <div class="col-md-4" style="<?=$display; ?>">
      <div class="form-group">
        <label>ประเภทองค์กร</label>
        <select name="project_type" class="form-control" <?=$disabled ?> required>
          <option value="" >&nbsp;</option>
          <option value="1" <?= ($project_type == '1' ? 'selected="selected"':'') ?>>โรงเรียน</option>
          <option value="4" <?= ($project_type == '4' ? 'selected="selected"':'') ?>>ประชาชนทั่วไป</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div id="testCodeList"></div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?= $display2?>">บันทึก</button>
</div>
<script>
$(function () {
  $('.datepicker').datepicker();
  $('.select2').select2();
  testCodeList();
})
</script>
