<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required class="custom-file-input form-control">
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div id="show-history"><div>
    </div>
  </div>
</div>

<script>
  showTableHistory();
</script>
