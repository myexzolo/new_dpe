<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$code = $_POST['code'];
$ParamData = array($code);

$sql  = "UPDATE PFIT_T_PROJECT SET STATUS = 'C' WHERE project_code = ?";

$query      = DbQuery($sql,$ParamData);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'][0];

if(intval($errorInfo) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
