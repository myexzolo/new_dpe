<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code          = $_POST['code'];
$age_range     = isset($_POST['age_range'])?$_POST['age_range']:array();
$action        = $_POST['action'];


$disabled = "";

if($action == 'VIEW'){
  $disabled = 'disabled';
}
?>
<table class="table table-bordered table-hover table-striped">
  <thead>
    <tr class="text-center">
      <th><input type="checkbox" id="checkApp" onclick="checkAll(this)" checked></th>
      <th>รายการทดสอบ</th>
      <th>องค์ประกอบที่ต้องการวัด</th>
      <th>ลำดับรายการทดสอบ</th>
    </tr>
  </thead>
  <tbody>
<?php
$testCodeArr = array();
$testSeq     = array();

$sqlPT = "SELECT * FROM pfit_t_project_test where project_code = '$code'";
$queryPT  = DbQuery($sqlPT,null);
$jsonPT   = json_decode($queryPT, true);

$numPT    = $jsonPT['dataCount'];
$rowPT    = $jsonPT['data'];

if($numPT > 0){
  foreach ($rowPT as $key => $value) {
    if(isset($value['test_code'])){
      $test_code  = $value['test_code'];
      $test_seq   = $value['test_seq'];
      array_push($testCodeArr,$test_code);
      $testSeq[$test_code] = $test_seq;
    }
  }
}

$role_list   = isset($_SESSION['member'][0]['role_list'])?$_SESSION['member'][0]['role_list']:'0';

$arrRole = explode(",",$role_list);

$str1 = "";
$str2 = "";
$str3 = "";
if(count($arrRole) > 0)
{
  foreach ($arrRole as $value)
  {
    //role 0 = ผู้ดูแลระบบสูงสุด  1 = ผู้ดูแลระบบ  2 = เจ้าหน้าที่กรม  14 = โรงเรียน  15 = จพล.
    //test_display 1 = all 2 = กรมพลศึกษา  3 = จพล.  4 = โรงเรียน  5 = Mobile
    if($value == "0" || $value == "1"){
      $str1 = "";
      $project_type     = "";
      break;
    }else if($value == "2")
    {
      $value = "2";
    }else if($value == "14")
    {
      $value = "4";
      $project_type     = "4";
      $arr_age_range    = array(1);
    }else if($value == "15")
    {
      $value = "3";
    }else if($value == "17")
    {
      $value = "6";
    }

    if($str1 == ""){
     $str1 = " (test_display = '$value' or  test_display like '%,$value' or  test_display like '$value,%' or test_display like '%,$value,%')";
    }else{
     $str1 .= " or (test_display = '$value' or  test_display like '%,$value' or  test_display like '$value,%'  or test_display like '%,$value,%') ";
    }
  }
}

foreach ($age_range as $value)
{
 if($str2 == ""){
   $str2 = " (age_range = '$value' or  age_range like '%,$value' or age_range like '$value,%' or age_range like '%,$value,%')";
 }
   $str2 .= " or (age_range = '$value' or  age_range like '%,$value' or age_range like '$value,%' or age_range like '%,$value,%') ";
}

$str3 = "";

if($str1 !=""){
  $str3 = " where (".$str1.")";
}

if($str3 !=""){
  $str3 .= " and (".$str2.")";
}else{
  $str3 = " where (".$str2.")";
}

$sql = "select * from pfit_t_test $str3 and status = 'Y' order by test_code";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$count      = $json['dataCount'];

$data = array();
// print_r($testCodeArr);
for ($i=0; $i < $count ; $i++)
{
  $test_code = $rows[$i]['test_code'];
  $test_name = $rows[$i]['test_name'];
  $test_opjective = $rows[$i]['test_opjective'];

  $check = '';
  $test_seq = "";
  // echo ">>>".$test_code."<br>";
  if (in_array($test_code,$testCodeArr))
  {
       $check     = 'checked';
       $test_seq  = $testSeq[$test_code];
  }
?>
  <tr class='text-center'>
    <td><input class="checkId" type='checkbox' id='<?=$test_code?>' onchange="chkList();" name='test_code[]' value="<?=$test_code?>" <?= $check." ".$disabled?> ></td>
    <td align='left'><label for='<?=$test_code?>' style='font-weight:200;margin-bottom: 0px;'><?=$test_name?></label></td>
    <td align='left'><?= getTestOpjective($test_opjective)?></td>
    <td align='center'>
      <input <?=$disabled ?> style="width:50px;text-align: right;height: 30px;" class="form-control"  OnKeyPress="return chkNumber(this)"  name="test_seq[<?=$test_code?>]" value="<?=$test_seq?>">
    </td>
  </tr>
<?php
}
?>
</tbody>
</table>
<script>
  chkList();
</script>
