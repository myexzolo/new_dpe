<?php

$status = 200;
$message = 'success';
$token  = isset($request['token'])?$request['token']:"";
$pid    = isset($request['pid'])?$request['pid']:"";


if(empty($token)){
  header('Content-Type: application/json');
    exit(response_data(401,'user_id is not macth',$data));
}else{

  $sql        = "SELECT * FROM t_user WHERE token = '$token' AND is_active = 'Y'";
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];

  if($dataCount > 0){
    $sqlPerson  = "SELECT person_number,person_name,person_lname,TO_CHAR(date_of_birth,'YYYY-MM-DD') as date_of_birth, person_gender,project_code
                   FROM pfit_t_person where person_number = '$pid'";
    $queryPerson  = DbQuery($sqlPerson,null);
    $jsonPerson   = json_decode($queryPerson, true);
    $rowPerson    = $jsonPerson['data'];
    $dataCountPerson  = $jsonPerson['dataCount'];

    if($dataCountPerson == 0)
    {
      $sqlUser      = "SELECT id_card,name,lname,TO_CHAR(dob,'YYYY-MM-DD') as dob,gender  FROM pfit_t_user where id_card = '$pid'";
      $queryUser    = DbQuery($sqlUser,null);
      $jsonUser     = json_decode($queryUser, true);
      $rowUser      = $jsonUser['data'];
      $dataCountUser  = $jsonUser['dataCount'];

      if($dataCountUser > 0)
      {
        $data[0]['person_number'] = $rowUser[0]['id_card'];
        $data[0]['person_name']   = $rowUser[0]['name'];
        $data[0]['person_lname']  = $rowUser[0]['lname'];
        $data[0]['person_dob']    = $rowUser[0]['dob'];
        $data[0]['person_gender'] = $rowUser[0]['gender'];
        $data[0]['resultInfo']    = array();

        $sqlResult    = "SELECT r.result , TO_CHAR(r.date_create,'YYYY-MM-DD HH24:MI:SS') as date_test , r.result_cal , t.test_name, tc.test_suggest, cd.category_criteria_detail_name
                        FROM pfit_t_result r, pfit_t_test_criteria tc, pfit_t_test t, pfit_t_cat_criteria_detail cd
                        where r.test_code = t.test_code  and r.test_criteria_code = tc.test_criteria_code
                        and tc.category_criteria_detail_code = cd.category_criteria_detail_code  and r.person_number = '$pid'
                        order by r.date_create , t.test_seq_def";

        $queryResult    = DbQuery($sqlResult,null);
        $jsonResult     = json_decode($queryResult, true);
        $rowResult      = $jsonResult['data'];
        $dataCountResult  = $jsonResult['dataCount'];

        if($dataCountResult > 0){
          for($i=0; $i < $dataCountResult ;$i++)
          {
            $data[0]['resultInfo'][$i]['testname'] = $rowResult[$i]['test_name'];
            $data[0]['resultInfo'][$i]['result'] = $rowResult[$i]['result'];
            $data[0]['resultInfo'][$i]['criteria_detail'] = $rowResult[$i]['category_criteria_detail_name'];
            $data[0]['resultInfo'][$i]['test_suggest'] = $rowResult[$i]['test_suggest'];
            $data[0]['resultInfo'][$i]['date_test'] = $rowResult[$i]['date_test'];
          }
        }
      }
    }else{
      for($x=0; $x < $dataCountPerson ;$x++)
      {
        $data[$x]['person_number'] = $rowPerson[$x]['person_number'];
        $data[$x]['person_name']   = $rowPerson[$x]['person_name'];
        $data[$x]['person_lname']  = $rowPerson[$x]['person_lname'];
        $data[$x]['person_dob']    = $rowPerson[$x]['date_of_birth'];
        $data[$x]['person_gender'] = $rowPerson[$x]['person_gender'];
        $projectCode               = $rowPerson[$x]['project_code'];

        $sqlResult    = "SELECT r.result , TO_CHAR(r.date_create,'YYYY-MM-DD HH24:MI:SS') as date_test , r.result_cal , t.test_name, tc.test_suggest, cd.category_criteria_detail_name
                        FROM pfit_t_result r, pfit_t_test_criteria tc, pfit_t_test t, pfit_t_cat_criteria_detail cd
                        where r.test_code = t.test_code  and r.test_criteria_code = tc.test_criteria_code and r.project_code = '$projectCode'
                        and tc.category_criteria_detail_code = cd.category_criteria_detail_code  and r.person_number = '$pid'
                        order by r.date_create , t.test_seq_def";

        $queryResult    = DbQuery($sqlResult,null);
        $jsonResult     = json_decode($queryResult, true);
        $rowResult      = $jsonResult['data'];
        $dataCountResult  = $jsonResult['dataCount'];

        if($dataCountResult > 0){
          for($i=0; $i < $dataCountResult ;$i++)
          {
            $data[$x]['resultInfo'][$i]['testname'] = $rowResult[$i]['test_name'];
            $data[$x]['resultInfo'][$i]['result'] = $rowResult[$i]['result'];
            $data[$x]['resultInfo'][$i]['criteria_detail'] = $rowResult[$i]['category_criteria_detail_name'];
            $data[$x]['resultInfo'][$i]['test_suggest'] = $rowResult[$i]['test_suggest'];
            $data[$x]['resultInfo'][$i]['date_test'] = $rowResult[$i]['date_test'];
          }
        }
      }
    }

  }else{
    header('Content-Type: application/json');
    exit(response_data(401,'token is not macth',$data));
  }
}




?>
