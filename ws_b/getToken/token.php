<?php

require '../lib/authen.php';

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

$token = require_auth();

if($token != ""){
  header('Content-Type: application/json');
  exit(json_encode(array( 'status' => 200 , 'message' => 'success' , 'token' => $token )));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array( 'status' => 201 , 'message' => 'Token is fail' , 'token' => '' )));
}

?>
