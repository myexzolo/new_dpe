<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$hostPath = getUrlHost();
$status = 200;
$message = 'success';
$projectCode = "M611101";

$data = array();


$user_id        = isset($request['result']['user_id'])?$request['result']['user_id']:"";
$person_number  = isset($request['result']['id_card'])?$request['result']['id_card']:"";
$dob            = isset($request['result']['dob'])?$request['result']['dob']:"";
$person_gender  = isset($request['result']['gender'])?$request['result']['gender']:"";
$dataResult     = $request['result']['data'];
$yearBirth      = yearBirth($dob);


$test_str = '';
$testSeq = 0;
$time = 0;

if($user_id != ""){
  $dateNow = queryDateTimeOracle(date("Y/m/d H:i:s"));

  $sql = "SELECT MAX(DISTINCT time) as time FROM pfit_t_result_history
          WHERE project_code = '$projectCode' and person_number = '$person_number'";
  $query = DbQuery($sql,null);
  $json  = json_decode($query, true);
  $num   = $json['dataCount'];
  $row   = $json['data'];

  if($num > 0){
    $time  = $row[0]['time'];
    $time = $time + 1;
  }else{
    $time = 1;
  }
  $url = '';
  foreach ($dataResult as $key => $value) {
    $testSeq++;
    $result    = $value['score'];
    $test_code = $value['test_code'];
    $wiegth = 0;
    $height = 0;
    $res    = '';
    $test_criteria_code = '';

    if($result != '-')
    {
      $sql    = "SELECT * FROM pfit_t_test WHERE test_code = '$test_code'";
      $query  = DbQuery($sql,null);
      $json   = json_decode($query, true);
      $row    = $json['data'];

      $test_calculator = $row[0]['test_calculator'];

      $res = getResultCalculatorForService($test_calculator,$result,$wiegth,$height,$yearBirth,$person_gender,$projectCode,$person_number);
      //$url .= $test_calculator.",".$result.",".$wiegth.",".$height.",".$yearBirth.",".$person_gender.",".$projectCode.",".$person_number.",".$res.'<<>>';
      $sqls       = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' AND age = '$yearBirth' AND gender = '$person_gender' AND min <= '$res' ORDER BY min DESC";
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $num        = $json['dataCount'];
      $row        = $json['data'];

      if($num > 0){
        $test_criteria_code = $row[0]['test_criteria_code'];
      }
    }
    $sqlr = "INSERT INTO pfit_t_result
            (project_code,person_number,result,test_code,test_criteria_code,date_create,result_cal)
            VALUES('$projectCode','$person_number','$result','$test_code','$test_criteria_code',$dateNow,'$res')";

    DbQuery($sqlr,null);

    $sqlrr = "INSERT INTO pfit_t_result_history
              (project_code,person_number,result,test_code,test_criteria_code,date_create,resultHis_cal,time,test_seq)
              VALUES('$projectCode','$person_number','$result','$test_code','$test_criteria_code',$dateNow,'$res','$time','$testSeq')";
    DbQuery($sqlrr,null);
  }

  require 'pdf.php';

  //$url = 'https://kosummarket.onesittichok.co.th/pages/sale_silp/pdf.php';
}else{
  $status = 401;
  $message = 'ไม่พบ User ID';
}
?>
